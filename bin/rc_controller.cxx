/*
 * rc_controller.cxx
 *
 *  Created on: Dec 12, 2012
 *      Author: avolio
 */

#include "RunControl/Controller/Controller.h"
#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/Exceptions.h"

#include <ipc/core.h>
#include <ipc/exceptions.h>
#include <ers/Issue.h>
#include <ers/LocalContext.h>
#include <ers/ers.h>

#include <stdlib.h>
#include <string>
#include <list>
#include <utility>
#include <iostream>

int main(int argc, char** argv) {
    try {
        // Parse the command line options (this parses only the "Run Control" parameters
        // See the daq::rc::CmdLineParser documentation for details
        // Note: the "help" function is enabled
        const daq::rc::CmdLineParser cmdParser(argc, argv, true);

        // IMPORTANT: the following IPC settings HAVE TO be applied for every controller
        // (also a custom one) in order to meet the performance requirements
        std::list<std::pair<std::string, std::string>> opt = IPCCore::extractOptions(argc, argv);
        opt.push_front(std::make_pair(std::string("threadPerConnectionPolicy"), std::string("0")));
        opt.push_front(std::make_pair(std::string("maxServerThreadPoolSize")  , std::string("20")));
        opt.push_front(std::make_pair(std::string("threadPoolWatchConnection"), std::string("0")));

        try {
            // The IPC initialization is mandatory!
            IPCCore::init(opt);
        }
        catch(daq::ipc::AlreadyInitialized& ex) {
            ers::info(ex);
        }

        // Create the controller (in case of customization the constructor accepts instances of daq::rc::UserRoutines
        daq::rc::Controller ctrl(cmdParser);

        // Initialize the controller
        ctrl.init();

        // Run the controller: the PMG sync is done and the ProcessManagement system will report the process as up
        // This blocks
        ctrl.run();
    }
    catch(daq::rc::CmdLineHelp& ex) {
        // Just show the help message
        std::cout << ex.message() << std::endl;
    }
    catch(ers::Issue& ex) {
        // Any exception trying to create and/or initialize the controller is a fatal error
        ers::fatal(daq::rc::ControllerCannotStart(ERS_HERE, ex.message(), ex));
        return EXIT_FAILURE;
    }
    catch(std::exception& ex) {
        // Any exception trying to create and/or initialize the controller is a fatal error
        ers::fatal(daq::rc::ControllerCannotStart(ERS_HERE, ex.what(), ex));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
