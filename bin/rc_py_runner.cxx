/*
 * RCPyControllableBind.cc

 *  Created on: August, 2013
 *      Author: Igor Pelevanyuk gavelock@gmail.com
 */
#include "RunControl/Python/RCPyToolkit.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"
#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/Exceptions.h"

#include <boost/program_options.hpp>

#include <ers/ers.h>
#include <ers/Issue.h>
#include <ers/LocalContext.h>

#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>

#include <list>
#include <vector>
#include <memory>

using namespace std;
namespace po = boost::program_options;

// General split string function. Use the next one
std::vector<std::string> &split(const std::string &s,
                                char delim, 
                                std::vector<std::string> &elems) 
{
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

//Simplified split string function
std::vector<std::string> split(const std::string &s, char delim) 
{
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

int main (int argc, char **argv)
{
    po::options_description desc("This program manages one ore several controllable Python classes");

    try {
        // Command line parsing
        vector<std::string> classes;

        desc.add_options()("classes,M", po::value<std::vector<std::string> >(&classes)->multitoken(), "Classes and modules as class@module");
        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
        po::notify(vm);

        daq::rc::CmdLineParser cmdParser(argc, argv, true);

        if (!vm.count("classes")) {
            throw daq::rc::CmdLineError(ERS_HERE, "Missing at least one class@module!");
        }

        //The core of program: adding controllable Python classes to list
        std::list<std::shared_ptr<daq::rc::Controllable>> appList;
        try {
            for (unsigned int i = 0; i < classes.size(); i++) {
                std::string modulName = split(classes[i],'@')[1];
                std::string className = split(classes[i],'@')[0];
                appList.push_back(std::shared_ptr<daq::rc::Controllable>(daq::rc::PyToolkit::Instance().createWrap(modulName, className)));

                ERS_LOG("Class " + className + " from module " + modulName + " added successfully");
            }
        }
        catch (std::exception& ex) {
            throw daq::rc::PythonModuleLoadException(ERS_HERE, ex.what(), ex);
        }

        {
            daq::rc::ItemCtrl myItem(cmdParser,
                                     appList);

            myItem.init();
            myItem.run();
        }
    }
    catch(daq::rc::CmdLineHelp& ex) {
        std::cout << desc << std::endl;
        std::cout << ex.message() << std::endl;
    }
    catch(ers::Issue& ex) {
        ers::fatal(ex);
        return EXIT_FAILURE;
    }
    catch(boost::program_options::error& ex) {
        ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
