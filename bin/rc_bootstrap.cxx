/*
 * rc_bootstrap.cxx
 *
 *  Created on: May 3, 2013
 *      Author: avolio
 */

#include "RunControl/FSM/FSMStates.h"
#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/Controller/ApplicationList.h"
#include "RunControl/Controller/ApplicationController.h"
#include "RunControl/Controller/ApplicationSelfInfo.h"
#include "RunControl/Controller/InformationPublisher.h"
#include "RunControl/Controller/DVSController.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/Exceptions.h"

#include <ers/ers.h>
#include <ipc/core.h>
#include <ipc/exceptions.h>
#include <ResourceManager/RM_Client.h>
#include <ResourceManager/exceptions.h>
#include <rc/rc.hh>
#include <dal/BaseApplication.h>
#include <dal/Segment.h>

#include <boost/program_options.hpp>
#include <boost/bind/bind.hpp>
#include <boost/bind/placeholders.hpp>
#include <boost/function.hpp>

#include <string>
#include <list>
#include <vector>
#include <utility>
#include <cstdlib>
#include <memory>
#include <exception>
#include <condition_variable>
#include <mutex>
#include <functional>

#include <stdlib.h>


namespace po = boost::program_options;
using namespace daq::rc;

namespace {
    void issueCatcher(const std::weak_ptr<daq::rc::ApplicationController>& appCtrl_weak, const ers::Issue& issue) {
        try {
            // An application could not be started
            const ApplicationStartingFailure& appIssue = dynamic_cast<const ApplicationStartingFailure&>(issue);
            ers::fatal(appIssue);

            const auto& appCtrl = appCtrl_weak.lock();
            if(appCtrl.get() != nullptr) {
                appCtrl->interrupt(true);
            }
        }
        catch(std::bad_cast&) {
            if(issue.severity() == ers::Warning) {
                ers::warning(issue);
            } else if(issue.severity() == ers::Error) {
                ers::error(issue);
            } else if(issue.severity() == ers::Fatal) {
                ers::fatal(issue);

                const auto& appCtrl = appCtrl_weak.lock();
                if(appCtrl.get() != nullptr) {
                    appCtrl->interrupt(true);
                }
            }
        }
    }
}

int main(int argc, char** argv) {
    std::string partitionName = ((std::getenv("TDAQ_PARTITION") == nullptr) ? "" : std::getenv("TDAQ_PARTITION"));
    std::string dbName = ((std::getenv("TDAQ_DB") == nullptr) ? "" : std::getenv("TDAQ_DB"));
    std::string applicationName = ((std::getenv("TDAQ_APPLICATION_NAME") == nullptr) ? "" : std::getenv("TDAQ_APPLICATION_NAME"));
    std::string segmentName;

    // Command line
    try {
        po::options_description desc("Binary to start the basic infrastructure.");

        desc.add_options()("partition,p", po::value<std::string>(&partitionName)->default_value(partitionName)    , "Name of the partition (TDAQ_PARTITION)")
                          ("database,d",  po::value<std::string>(&dbName)->default_value(dbName)                  , "Name of the database (TDAQ_DB)")
                          ("segment,s",   po::value<std::string>(&segmentName)                                    , "Name of the segment")
                          ("name,n",      po::value<std::string>(&applicationName)->default_value(applicationName), "Name of the controller")
                          ("help,h"                                                                               , "Print help message");

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if(vm.count("help")) {
            std::cout << desc << std::endl;
            return EXIT_SUCCESS;
        }

        if(!vm.count("segment")) {
            throw daq::rc::CmdLineError(ERS_HERE, "The segment name has to be defined as command line parameters!");
        }

        if(dbName.empty() == true) {
            throw daq::rc::CmdLineError(ERS_HERE, "The database name is not defined!");
        } else {
            ::setenv("TDAQ_DB", dbName.c_str(), 1);
        }

        if(partitionName.empty() == true) {
            throw daq::rc::CmdLineError(ERS_HERE, "The partition name is not defined!");
        } else {
            ::setenv("TDAQ_PARTITION", partitionName.c_str(), 1);
        }

        if(applicationName.empty() == true) {
            throw daq::rc::CmdLineError(ERS_HERE, "The application name is not defined!");
        } else {
            ::setenv("TDAQ_APPLICATION_NAME", applicationName.c_str(), 1);
        }
    }
    catch(daq::rc::CmdLineError& ex) {
        ers::fatal(ex);
        return EXIT_FAILURE;
    }
    catch(boost::program_options::error& ex) {
        ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
        return EXIT_FAILURE;
    }

    // IPC initialization
    std::list<std::pair<std::string, std::string>> opt = IPCCore::extractOptions(argc, argv);
    opt.push_front(std::make_pair(std::string("threadPerConnectionPolicy"), std::string("0")));
    opt.push_front(std::make_pair(std::string("maxServerThreadPoolSize")  , std::string("3")));
    opt.push_front(std::make_pair(std::string("threadPoolWatchConnection"), std::string("0")));

    try {
        IPCCore::init(opt);
    }
    catch(daq::ipc::AlreadyInitialized& ex) {
        ers::info(ex);
    }
    catch(ers::Issue& ex) {
        ers::fatal(ex);
        return EXIT_FAILURE;
    }

    // The real and serious things...

    // Declare the issue handler here, so that it is destroyed at the very end
    std::unique_ptr<ers::IssueCatcherHandler> catcherHandler;
    try {
        OnlineServices::initialize(partitionName, segmentName);

        OnlineServices& onlSvc = OnlineServices::instance();

        bool running = false;
        try {
            running = onlSvc.getIPCPartition().isObjectValid< ::rc::commander>(onlSvc.applicationName());
        }
        catch(daq::ipc::InvalidPartition& ex) {
            // This is normal if we are just starting
            ers::warning(ex);
        }

        if(running == false) {
            try {
                daq::rmgr::RM_Client rmClient;
                rmClient.rmConfigurationUpdate(partitionName);
            }
            catch(daq::rmgr::Exception& ex) {
                ers::error(ex);
            }

            std::shared_ptr<ApplicationList> appList(new ApplicationList());
            appList->load(onlSvc.getConfiguration(),
                          onlSvc.getPartition(),
                          onlSvc.segmentName());

            std::shared_ptr<InformationPublisher> infoPub(new InformationPublisher([]() { return std::make_pair(false, ApplicationSelfInfo()); }));

            std::shared_ptr<DVSController> dvsCtrl(new DVSController(appList, infoPub));
            dvsCtrl->load(onlSvc.getConfiguration(),
                          partitionName,
                          segmentName);

            std::shared_ptr<ApplicationController> appCtrl = ApplicationController::create(appList,
                                                                                           dvsCtrl,
                                                                                           infoPub);

            std::vector<std::string> appsToStart;

            {
                // Here we get the applications the RootController depends upon
                const daq::core::Segment* segConf = onlSvc.getPartition().get_segment(segmentName);
                if(segConf->is_disabled() == false) {
                    const daq::core::BaseApplication* ctrl = segConf->get_controller();

                    const auto& allSegApps = segConf->get_all_applications();
                    const auto& depApps = ctrl->get_initialization_depends_from(allSegApps);

                    for(const auto app : depApps) {
                        appsToStart.push_back(app->UID());
                    }
                } else {
                    throw daq::rc::ConfigurationIssue(ERS_HERE, "it looks like the Online Segment is disabled!");
                }
            }

            StartStopAppCmd cmd(StartStopAppCmd::is_start_app_cmd, appsToStart);
            cmd.currentState(FSM_STATE::ABSENT);
            cmd.destinationState(FSM_STATE::NONE);
            cmd.transitionCommand(FSM_COMMAND::INIT_FSM);

            // The issue catcher is needed in order to interrupt the ApplicationController in case of errors
            // It is important that the ApplicationController is destroyed before the issue handler. If not
            // then bad things happen during the removal of the issue handler (possible race condition).
            catcherHandler.reset(ers::set_issue_catcher(boost::bind(&issueCatcher,
                                                                    std::weak_ptr<ApplicationController>(appCtrl),
                                                                    boost::placeholders::_1)));

            // This blocks: it may be interrupted by the issue catcher if something goes bad
            appCtrl->startApps(cmd);
        } else {
            ERS_LOG(onlSvc.applicationName() + " is already running");
        }
    }
    catch(daq::rc::InterruptedException& ex) {
        ers::warning(ex);

        return EXIT_FAILURE;
    }
    catch(ers::Issue& ex) {
        ers::fatal(ex);

        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}


