import ers											
from rcpy import Controllable
from ispy import *
import pm.project
"""This is example of controllable python class definition.
1. After start it will increment special variable in IS DQM.Counters.
2. Also, for the sake of example, code for getting information 
from OKS was added"""

IS_ENTITY_NAME = 'DQM.Counters'
PARTITION_NAME = 'igor'
#OKS_SOURCE = 'rdbconfig:RDB@part_l2ef'
OKS_SOURCE  = 'rdbconfig:RDB@' + PARTITION_NAME


class publisher(Controllable):
	def __init__(self):
		self.partition = IPCPartition(PARTITION_NAME)
		self.dict = ISInfoDictionary(self.partition)
		self.isPrepared = False
		super(publisher, self).__init__()
	#---------------------------------------------------------------------------
	def configure(self, opts):
		ers.info('configure')
		if self.dict.contains(IS_ENTITY_NAME):
			self.dict.remove(IS_ENTITY_NAME)
		dqm = IS.Result(self.partition, IS_ENTITY_NAME)
		dqm['tags'].resize(2)
		dqm['tags'][0].name = 'ProbeCounter'
		dqm['tags'][0].value = 0
		dqm['tags'][1].name = 'PublishFullStatisticsCounter'
		dqm['tags'][1].value = 0
		dqm.checkin()
		#print opts.toString()
		db = pm.project.Project(OKS_SOURCE)
		c = db.getObject('RunControlApplication')
		#for k in c:
			#ers.info(str(k))
	#---------------------------------------------------------------------------
	def prepareForRun(self, opts):
		ers.info('prepareForRun')
		self.isPrepared = True
		#This variable was created to avoid incrementing IS variable before start
	#---------------------------------------------------------------------------
	def stopROIB(self, opts):
		ers.info('stopROIB')
		self.isPrepared = False
	#---------------------------------------------------------------------------
	def publishFullStats(self):
		if (self.isPrepared and False):
			ers.info('publishFullStatistics')
			dqm = IS.Result(self.partition, IS_ENTITY_NAME)
			dqm.checkout()
			dqm['tags'][1].value += 1
			dqm.checkin()
	#---------------------------------------------------------------------------
	def publish(self):
		if (self.isPrepared):
			ers.info('probe')
			dqm = IS.Result(self.partition, IS_ENTITY_NAME)
			dqm.checkout()
			dqm['tags'][0].value += 1
			dqm.checkin()
