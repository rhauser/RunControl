from __future__ import print_function

from rcpy import Controllable
import ers
from ipc import IPCPartition, getPartitions
from ispy import *
"""This is example of controllable python class definition.
It assumes to work together with "publisher.py" example
[1] It creates new IS entity IS_REACTION
[2] After start it will subscribe for events of IS_SUBSCRIPTION
[3] On each event <handler> will be called 
[4] Unsubscribe after <stopROIB>
"""

IS_SUBSCRIPTION = 'DQM.Counters'
IS_REACTION = 'DQM.Subscriber'
PARTITION_NAME = 'igor'


partition = IPCPartition(PARTITION_NAME)
receiver = ISInfoReceiver(partition)

#[3]
def handler(cd):
	ers.info('Callback')
	infoSubscriber = IS.Result(partition, IS_REACTION)
	infoSubscriber.checkout()
	infoSubscriber['tags'][0].value += 1
	infoSubscriber.checkin()

class subscriber(Controllable):
	def __init__(self):
		super(subscriber, self).__init__()
		print("hello from Subscriber class constuctor")
	#---------------------------------------------------------------------------
	def initialize(self, opts):
		ers.info('initialize')
	#---------------------------------------------------------------------------
	def configure(self, opts):
		ers.info('configure')
		# [1]
		dictionary = ISInfoDictionary(partition)
		if dictionary.contains(IS_REACTION):
			dictionary.remove(IS_REACTION)

		dqm = IS.Result(partition, IS_REACTION)
		dqm['tags'].resize(1)
		dqm['tags'][0].name = 'CounterCallbacks'
		dqm['tags'][0].value = 0

		dqm.checkin()
		ers.info("after handler run")
	#---------------------------------------------------------------------------
	def prepareForRun(self, opts):
		# [2]
		ers.info('prepareForRun')
		receiver.subscribe_event(IS_SUBSCRIPTION, handler)
	#---------------------------------------------------------------------------
	def stopROIB(self, opts):
		# [4]
		ers.info('stopROIB')
		#receiver.stop()
		receiver.unsubscribe(IS_SUBSCRIPTION, True)
#-------------------------------------------------------------------------------
