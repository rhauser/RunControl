/*
 * rc_sender.cxx
 *
 *  Created on: Dec 12, 2012
 *      Author: avolio
 */

#include "RunControl/Common/Algorithms.h"
#include "RunControl/Common/CommandSender.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/RunControlBasicCommand.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/FSM/FSMCommands.h"

#include <rc/rc.hh>

#include <ipc/core.h>
#include <ipc/partition.h>
#include <ers/ers.h>
#include <ers/Issue.h>
#include <ers/LocalContext.h>
#include <ipc/exceptions.h>

#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <deque>
#include <cstdlib>
#include <memory>
#include <cstdint>
#include <regex>
#include <future>
#include <algorithm>
#include <atomic>
#include <sstream>
#include <system_error>
#include <utility>

namespace po = boost::program_options;
using namespace daq::rc;

const unsigned int TIMEOUT_ELAPSED = 123;

int main(int argc, char** argv) {
    std::string partitionName = ((std::getenv("TDAQ_PARTITION") == nullptr) ? "" : std::getenv("TDAQ_PARTITION"));
    std::string applicationName;
    std::string commandName;
    std::vector<std::string> args;
    unsigned long timeout = 0;
    bool timeoutDefined = false;

    // Command line parsing
    try {
        const std::string msg = std::string("\nValid commands are:\n") +
                                "- Transition commands:\n" +
                                "  INITIALIZE, CONFIGURE, CONFIG, CONNECT, START, STOP, STOPROIB, STOPDC, STOPHLT,\n" +
                                "  STOPRECORDING, STOPGATHERING, STOPARCHIVING, DISCONNECT, UNCONFIG, UNCONFIGURE,\n" +
                                "  SHUTDOWN, RELOAD_DB, NEXT_TRANSITION\n" +
                                "  (sub-transitions may be skipped adding the argument \"NO_SUBTR\")\n" +
                                "- Transition commands with arguments:\n" +
                                "  USERBROADCAST <command name> <command arguments>,\n" +
                                "  RESYNCH <ecr counts> <extended L1 ID - optional> <modules>,\n" +
                                "  SUB_TRANSITION <sub-transition name> <main transition name> (mainly for debugging, use with care!),\n" +
                                "- Standard commands:\n" +
                                "  USER <command name> <command options>, ENABLE/DISABLE <components>,\n" +
                                "  STARTAPP/STOPAPP/RESTARTAPP <application list>, PUBLISH/PUBLISHSTATS,\n" +
                                "  TESTAPP <application list> <test scope [any]> <test level [0]>,\n" +
                                "  CHANGE_PROBE_INTERVAL/CHANGE_FULLSTATS_INTERVAL <new interval>,\n" +
                                "  DYN_RESTART <segment list>,\n" +
                                "  IGNORE_ERROR, EXIT\n\n" +
                                "The STARTAPP/STOPAPP/RESTARTAP/TESTAPP/ENABLE/DISABLE commands support regular expressions but, in order to be treated as such, " +
                                "lookup strings must end with the \"" + RunControlCommands::REGEX_TOKEN + "\" sequence of characters.\n\n" +
                                "The same command can be sent to multiple applications using a regular expression for the application name (the regular expression " +
                                "must end with the \"" + RunControlCommands::REGEX_TOKEN + "\" sequence of characters).\n\n"
                                "Some commands may be accepted or rejected by the receiving application being it a controller or a leaf application\n\n" +
                                "If the timeout option is enabled, then this program will exit with code \"" + std::to_string(TIMEOUT_ELAPSED) + "\" when the timeout elapses\n";

        po::options_description desc(std::string("Utility application to send commands to run control applications\n") + msg);
        desc.add_options()
                ("help,h", "Produce an help message")
                ("timeout,t", po::value<unsigned long>(&timeout), "Time in milliseconds to wait for the command to be executed (0 waits for ever)")
                ("partition,p", po::value<std::string>(&partitionName)->default_value(partitionName), "The partition name")
                ("name,n", po::value<std::string>(&applicationName), "The name of the run control application")
                ("command,c", po::value<std::string>(&commandName), "The name of the command to send")
                ("arguments", po::value<std::vector<std::string>>(&args), "The command options");

        po::positional_options_description p;
        p.add("arguments", -1);

        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
        po::notify(vm);

        if(vm.count("help")) {
            std::cout << desc;
            return EXIT_SUCCESS;
        }

        if((!vm.count("name")) || (!vm.count("command"))) {
            throw daq::rc::CmdLineError(ERS_HERE, "The run control application and the command names have to be defined as command line parameters!");
        }

        if(vm.count("timeout")) {
            timeoutDefined = true;
        }
    }
    catch(daq::rc::CmdLineError& ex) {
        ers::fatal(ex);
        return EXIT_FAILURE;
    }
    catch(boost::program_options::error& ex) {
        ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
        return EXIT_FAILURE;
    }

    // Here the serious things...
    try {
        try {
            IPCCore::init(argc, argv);
        }
        catch(daq::ipc::AlreadyInitialized& ex) {
            ers::info(ex);
        }

        // Check
        bool isTransitionCmd = true;
        try {
            // This will throw if the command is not a transition one
            FSMCommands::stringToCommand(commandName);
        }
        catch(daq::rc::BadCommand&) {
            isTransitionCmd = false;
        }

        std::unique_ptr<RunControlBasicCommand> cmd;
        if(isTransitionCmd == true) {
            std::deque<std::string> cmdArgs;
            cmdArgs.push_back(commandName);
            cmdArgs.insert(cmdArgs.end(), args.begin(), args.end());

            cmd = Algorithms::buildCommand(RC_COMMAND::MAKE_TRANSITION, cmdArgs);
        } else {
            const RC_COMMAND rcCmd = RunControlCommands::stringToCommand(commandName);
            std::deque<std::string> cmdArgs(args.begin(), args.end());

            cmd = Algorithms::buildCommand(rcCmd, cmdArgs);
        }

        CommandSender snd(partitionName, "command-line-sender");
        std::set<std::string> allApps;

        if(boost::algorithm::ends_with(applicationName, RunControlCommands::REGEX_TOKEN) == true) {
            IPCPartition p(partitionName);

            std::map<std::string, ::rc::commander_var> appMap;
            p.getObjects<::rc::commander>(appMap);

            const std::string& regEx = applicationName.substr(0, applicationName.size() - RunControlCommands::REGEX_TOKEN.size());

            for(const auto& it : appMap) {
                if(std::regex_match(it.first, std::regex(regEx)) == true) {
                    allApps.insert(it.first);
                }
            }
        } else {
            allApps.insert(applicationName);
        }

        std::atomic<unsigned int> timeouts(0);
        std::atomic<unsigned int> failures(0);
        std::vector<std::future<void>> tasks;

        for(const std::string& app : allApps) {
            auto&& f = std::async(allApps.size() == 1 ? std::launch::deferred : std::launch::async,
                                  [&snd, app, &cmd, timeoutDefined, timeout, &timeouts, &failures]() {
                                      try {
                                          if(timeoutDefined == false) {
                                              snd.sendCommand(app, *(cmd->clone()));
                                          } else {
                                              bool timeoutElapsed = !snd.sendCommand(app, *(cmd->clone()), timeout);
                                              if(timeoutElapsed == true) {
                                                  ERS_LOG("Timeout elapsed sending command to application \"" + app + "\"");
                                                  ++timeouts;
                                              }
                                          }
                                      }
                                      catch(ers::Issue& ex) {
                                          ers::error(ex);
                                          ++failures;
                                      }
                                  });
            tasks.push_back(std::move(f));
        }

        for(auto&& t : tasks) {
            t.get();
        }

        if(failures > 0) {
            return EXIT_FAILURE;
        }

        if(timeouts > 0) {
            return TIMEOUT_ELAPSED;
        }
    }
    catch(ers::Issue& ex) {
        ers::fatal(ex);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}



