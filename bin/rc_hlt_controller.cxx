/*
 * rc_hlt_controller.cxx
 *
 *  Created on: Oct 18, 2018
 *      Author: avolio
 */

#include "RunControl/Controller/Controller.h"
#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/CommandSender.h"
#include "RunControl/Common/UserRoutines.h"
#include "RunControl/Common/RunControlCommands.h"

#include <ipc/core.h>
#include <ipc/exceptions.h>
#include <config/SubscriptionCriteria.h>
#include <config/Change.h>
#include <config/Configuration.h>
#include <config/DalObject.h>
#include <config/Errors.h>
#include <config/map.h>
#include <ers/Issue.h>
#include <ers/LocalContext.h>
#include <ers/ers.h>
#include <dal/BaseApplication.h>
#include <dal/Segment.h>
#include <dal/TemplateSegment.h>
#include <dal/Rack.h>
#include <dal/ComputerBase.h>
#include <dal/Computer.h>
#include <dal/ComputerSet.h>
#include <dal/Partition.h>

#include <string>
#include <list>
#include <vector>
#include <set>
#include <utility>
#include <iostream>
#include <functional>
#include <algorithm>
#include <cstdlib>
#include <memory>

// This is a custom controller allowing to enable/disable rack's segments at runtime
// It should be configured to control a segment having a template segment as child

// The dynamic configuration is retrieved from a dedicated RDB server that should be
// configured as part of the infrastructure of the controlled segment and that should
// ideally contain only the needed information (i.e., only Rack description). The name
// of that RDB server should be defined in the TDAQ_HLT_DB_NAME environment variable.

// Taking the structure of the ATLAS partition as example, this controller should control
// the HLT segment and the RDB server should be part of the HLT segment's infrastructure.

// See https://its.cern.ch/jira/browse/ADTCC-191.

namespace {
const std::string TDAQ_HLT_DB_NAME = (std::getenv("TDAQ_HLT_DB_NAME") != nullptr) ? std::string(std::getenv("TDAQ_HLT_DB_NAME")) : "UNKNOWN_RDB";
const std::string DB_TECHNOLOGY = "rdbconfig";

class HLTRacksHandler: public daq::rc::UserRoutines {
    public:
        HLTRacksHandler(const std::string& appName,
                        const std::string& segName,
                        const std::string& partitionName,
                        const std::string& connectionString) :
                UserRoutines(), m_app_name(appName), m_segment_name(segName), m_partition_name(partitionName), m_specific_conf(connectionString), m_conf_callback(0)
        {
            // This can throw
            // Let the exception go: this controller cannot do its job if the subscription fails
            subscribe();
        }

        ~HLTRacksHandler() {
            unsubscribe();
        }

        // This is executed just before the FSM is initiated and the FSM initial state is reached
        void initAction(const daq::rc::TransitionCmd& trCmd) override {
            if(trCmd.isRestart() == false) {
                try {
                    // Note the empty list of racks: at the beginning all of them are checked
                    setRackStatus(true, {});
                }
                catch(ers::Issue& ex) {
                    ers::error(daq::rc::Exception(ERS_HERE, "Failed to properly enable/disable Rack segments", ex));
                }
            }
        }

    protected:
        // This can throw daq::Config::Generic
        void subscribe() {
            ::ConfigurationSubscriptionCriteria subCrt;
            subCrt.add("Rack");
            m_conf_callback = m_specific_conf.subscribe(subCrt, &HLTRacksHandler::configCallback, this);
        }

        void unsubscribe() {
            try {
                m_specific_conf.unsubscribe(m_conf_callback);
            }
            catch(daq::config::Generic& ex) {
                ers::warning(ex);
            }
        }

        static void configCallback(const std::vector<::ConfigurationChange*>& changes, void* _this) {
            std::set<std::string> racks;

            // Interested only of Racks whose configuration has been modified
            for(::ConfigurationChange* cfc : changes) {
                for(const std::string& obj : cfc->get_modified_objs()) {
                    racks.insert(obj);
                }
            }

            HLTRacksHandler* myself = (HLTRacksHandler*) _this;
            try {
                // Here the list of racks is passed
                myself->setRackStatus(false, racks);
            }
            catch(ers::Issue& ex) {
                ers::error(daq::rc::Exception(ERS_HERE, "Failed to properly enable/disable Rack segments", ex));
            }
        }

        /**
         * Here segments and applications are properly started/stopped taking into account the Rack's state
         *
         * @param isStart true if this method is invoked when the application is started (i.e., from the user routine)
         * @param modRacks List of racks whose state is changed (empty if all racks should be checked)
         */
        void setRackStatus(bool isStart, const std::set<std::string>& modRacks) {
            std::function<void(std::set<std::string>& collection, const daq::core::ComputerBase* cb)> add_computer =
                    [&add_computer] (std::set<std::string>& collection, const daq::core::ComputerBase* cb) {
                        const daq::core::ComputerSet* cs = cb->cast<daq::core::ComputerSet>();
                        if(cs != nullptr) {
                            for(const daq::core::ComputerBase* c : cs->get_Contains()) {
                                add_computer(collection, c);
                            }
                        } else {
                            const daq::core::Computer* c = cb->cast<daq::core::Computer>();
                            if((c != nullptr) && (c->get_State() == true)) {
                                collection.insert(c->UID());
                            }
                        }
                    };

            // Get the state of the racks (and included computers) from the configuration that may be modified dynamically
            std::vector<const daq::core::Rack*> racks;
            m_specific_conf.get(racks);

            // Eventually take into account only racks that changed their state
            if(isStart == false) {
                const auto newEnd =
                        std::remove_if(racks.begin(),
                                       racks.end(),
                                       [&modRacks] (const daq::core::Rack* r) {return modRacks.find(r->UID()) == modRacks.end();});
                racks.erase(newEnd, racks.end());
            }

            std::set<std::string> enabledHosts;
            std::set<std::string> disabledHosts;

            for(const daq::core::Rack* r : racks) {
                auto collection = (r->get_State() == true) ? std::ref(enabledHosts) : std::ref(disabledHosts);
                for(const daq::core::ComputerBase* c : r->get_Nodes()) {
                    add_computer(collection.get(), c);
                }
            }

            // Get applications to be enabled/disabled
            std::vector<std::string> appsToEnable;
            std::vector<std::string> appsToDisable;

            auto classifyApp =
                    [&enabledHosts, &disabledHosts, &appsToEnable, &appsToDisable] (const daq::core::BaseApplication* app) {
                        const std::string& host = app->get_host()->UID();
                        if(enabledHosts.find(host) != enabledHosts.end()) {
                            appsToEnable.push_back(app->UID());
                        } else if(disabledHosts.find(host) != disabledHosts.end()) {
                            appsToDisable.push_back(app->UID());
                        }
                    };

            const daq::core::Segment* seg = daq::rc::OnlineServices::instance().getPartition().get_segment(m_segment_name);

            for(const daq::core::Segment* s : seg->get_nested_segments()) {
                if(s->is_disabled() == false) {
                    const daq::core::TemplateSegment* ts = m_specific_conf.cast<daq::core::TemplateSegment>(s);
                    if(ts != nullptr) {
                        classifyApp(s->get_controller());

                        for(const daq::core::BaseApplication* infrApp : s->get_infrastructure()) {
                            classifyApp(infrApp);
                        }
                    }
                }
            }

            // Now enable/disable the apps
            daq::rc::CommandSender cmdSender(m_partition_name, m_app_name);

            if(appsToEnable.empty() == false) {
                if(isStart == false) {
                    daq::rc::StartStopAppCmd cmd(daq::rc::StartStopAppCmd::is_start_app_cmd, appsToEnable);
                    cmdSender.sendCommand(m_app_name, cmd, 0);
                }
                cmdSender.changeStatus(m_app_name, true, appsToEnable);
            }

            if(appsToDisable.empty() == false) {
                cmdSender.changeStatus(m_app_name, false, appsToDisable);
                if(isStart == false) {
                    cmdSender.stopApplications(m_app_name, appsToDisable);
                }
            }
        }

    private:
        const std::string m_app_name;
        const std::string m_segment_name;
        const std::string m_partition_name;
        ::Configuration m_specific_conf;
        ::Configuration::CallbackId m_conf_callback;
};
}

int main(int argc, char** argv) {
    try {
        // Parse the command line options (this parses only the "Run Control" parameters
        // See the daq::rc::CmdLineParser documentation for details
        // Note: the "help" function is enabled
        const daq::rc::CmdLineParser cmdParser(argc, argv, true);

        // IMPORTANT: the following IPC settings HAVE TO be applied for every controller
        // (also a custom one) in order to meet the performance requirements
        std::list<std::pair<std::string, std::string>> opt = IPCCore::extractOptions(argc, argv);
        opt.push_front(std::make_pair(std::string("threadPerConnectionPolicy"), std::string("0")));
        opt.push_front(std::make_pair(std::string("maxServerThreadPoolSize"), std::string("20")));
        opt.push_front(std::make_pair(std::string("threadPoolWatchConnection"), std::string("0")));

        try {
            // The IPC initialization is mandatory!
            IPCCore::init(opt);
        }
        catch(daq::ipc::AlreadyInitialized& ex) {
            ers::log(ex);
        }

        // Create the controller (in case of customization the constructor accepts instances of daq::rc::UserRoutines
        daq::rc::Controller ctrl(cmdParser,
                                 std::make_shared<HLTRacksHandler>(cmdParser.applicationName(),
                                                                   cmdParser.segmentName(),
                                                                   cmdParser.partitionName(),
                                                                   DB_TECHNOLOGY + ":"
                                                                           + TDAQ_HLT_DB_NAME
                                                                           + "@" + cmdParser.partitionName()));

        // Initialize the controller
        ctrl.init();

        // Run the controller: the PMG sync is done and the ProcessManagement system will report the process as up
        // This blocks
        ctrl.run();
    }
    catch(daq::rc::CmdLineHelp& ex) {
        // Just show the help message
        std::cout << ex.message() << std::endl;
    }
    catch(ers::Issue& ex) {
        // Any exception trying to create and/or initialize the controller is a fatal error
        ers::fatal(daq::rc::ControllerCannotStart(ERS_HERE, ex.message(), ex));
        return EXIT_FAILURE;
    }
    catch(std::exception& ex) {
        // Any exception trying to create and/or initialize the controller is a fatal error
        ers::fatal(daq::rc::ControllerCannotStart(ERS_HERE, ex.what(), ex));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
