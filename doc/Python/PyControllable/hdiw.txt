How does it work
================

To let RCPy to run Python classes the same way as C++ classes we use remarkable
technic.

First of all we created special binding for Controllable.h (C++).
After compiling we have a kind of shared object which we can import in Python.
If class inherits controllable interface from this shared object than it could 
be loaded back to C++ as normal C++ object.

For this we use combination of Boost technics and low level Python.h technics.

General architecture of such implementation:

.. image:: ./images/arch.png