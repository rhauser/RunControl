# RunControl

[This](https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltRunControl) is the link to the main RunControl twiki.

## tdaq-09-03-00

**Internal changes**:   

-   Fixed controller dead-lock at exit ([ATDAQCCRC-47](https://its.cern.ch/jira/browse/ATDAQCCRC-47));
-   The `RootController` synchronizes with `CHIP` when a FSM state transition is completed;
-   Implemented the `daq tokens` mechanism;
-   Java: added unit tests for the client library;
-   Java: SWIG is now executed with the `-DSWIGWORDSIZE64` flag;
-   Java: handling new exceptions from the `config` layer:
-   IDL: `setError` now can throw exceptions.


**Public changes in APIs**:

-   Java: exposing the CORBA `ping` command in `CommandSender`.
 

## tdaq-09-02-01

**Internal changes**:   

-   Python library: fixed initialization of the python sub-system;    
-   When `OKS GIT` is used, the RootController sets the currently used configuration version ([ADTCC-227](https://its.cern.ch/jira/browse/ADTCC-227)).

## tdaq-09-00-00

**Enhancements**:  

-   The Java library has been extended with the possibility to create Java Run Control applications ([ATDAQCCRC-40](https://its.cern.ch/jira/browse/ATDAQCCRC-40)). It is now possible to have Run Control applications in the three main languages used in TDAQ: C++, Java and Python.

    An example [application](https://gitlab.cern.ch/atlas-tdaq-software/RunControl/blob/master/jsrc/daq/rc/example/RCExampleApplication.java) and the related [script](https://gitlab.cern.ch/atlas-tdaq-software/RunControl/blob/master/bin/jrc_example_application) to start it are provided.  
    
    Additional details can be found [here](https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltRunControl#Java_Library).

**Changes in utility and/or tools**:

-   *rc_sender* (see the online help for details about the syntax to be used):

    The `STARTAPP/STOPAPP/RESTARTAP/TESTAPP/ENABLE/DISABLE` commands support regular expressions;  
    
    The same command can be sent to multiple applications using a regular expression for the application name.

**Public changes in APIs**:

-   Regular expressions enabled for the `STARTAPP/STOPAPP/RESTARTAP/TESTAPP/ENABLE/DISABLE` commands:

    In the concerned command objects, application or component names can be replaced by regular expressions. In order to be treated as regular expressions, the names must end with the  `//r`  characters.
  
-   The `Resynch` command has been extended to include the extended `L1ID` (and not only the `ECR` counter);
-   Added `initAction` to user routines ([ATDAQCCRC-191](https://its.cern.ch/jira/browse/ATDAQCCRC-191)).


**Internal changes**:

-   Adding the `SWROD` to the computation of the detector mask ([ATDAQCCRC-46](https://its.cern.ch/jira/browse/ATDAQCCRC-43));
-   Adapting to changes in the `PMG` library (smart pointers for `daq::pmg::Process` instances);
-   After a reload, tests for apps out of membership and not up are reset;
-   Adapting to changes in `DAL` (new implementation of template applications and segments);
-   Adapting to changes in `DVS/TestManager` (new implementations);
-   `Boost` shared mutex replaced with `std` version.
