/*
 * test_callback.cxx
 *
 *  Created on: May 12, 2014
 *      Author: avolio
 */

#include "RunControl/Common/CommandSender.h"
#include "RunControl/Common/CommandNotifier.h"
#include "RunControl/Common/RunControlBasicCommand.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/FSM/FSMCommands.h"

#include <ers/Issue.h>

#include <boost/program_options.hpp>

#include <ipc/core.h>
#include <ers/ers.h>

#include <memory>
#include <cstdlib>
#include <mutex>
#include <condition_variable>
#include <iostream>
#include <string>


namespace po = boost::program_options;

bool received = false;
std::condition_variable cv;
std::mutex cvMutex;

class MyNotifier : public daq::rc::CommandNotifier {
    protected:
        void commandExecuted(const daq::rc::RunControlBasicCommand& cmd) noexcept override {
            try {
                ERS_LOG("Command executed: " << cmd.serialize());
            }
            catch(daq::rc::BadCommand& ex) {
                ers::error(ex);
            }

            {
                std::lock_guard<std::mutex> lk(cvMutex);
                received = true;
            }

            cv.notify_one();
        }
};

int main(int argc, char** argv) {
    try {
        // Parse the command line
        std::string partitionName = (std::getenv("TDAQ_PARTITION") == nullptr) ? "" : std::getenv("TDAQ_PARTITION");
        std::string applicationName;
        std::string transitionName;

        po::options_description desc("Utility to test call-back reception sending transition commands to a Run Control application");
        desc.add_options()
                        ("help,h", "Produce an help message")
                        ("partition,p", po::value<std::string>(&partitionName)->default_value(partitionName), "The partition name")
                        ("name,n", po::value<std::string>(&applicationName), "The name of the run control application")
                        ("transition,t", po::value<std::string>(&transitionName), "The name of the command to send");

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if(vm.count("help")) {
            std::cout << desc << std::endl;
            return EXIT_SUCCESS;
        }

        if(partitionName.empty() == true) {
            std::cerr << "The name of the partition is not defined" << std::endl;
            std::cout << desc << std::endl;
            return EXIT_FAILURE;
        }

        if(applicationName.empty() == true) {
            std::cerr << "The name of the application is not defined" << std::endl;
            std::cout << desc << std::endl;
            return EXIT_FAILURE;
        }

        if(transitionName.empty() == true) {
            std::cerr << "The name of the transition is not defined" << std::endl;
            std::cout << desc << std::endl;
            return EXIT_FAILURE;
        }

        // Initialize IPC
        IPCCore::init(argc, argv);

        // Create the notifier and then send the command
        MyNotifier mn;

        daq::rc::TransitionCmd cmd(daq::rc::FSMCommands::stringToCommand(transitionName));

        daq::rc::CommandSender cmdSender(partitionName, "cmdln-test-application");
        cmdSender.sendCommand(applicationName, cmd, mn);

        // Wait for the command to be executed
        std::unique_lock<std::mutex> lk(cvMutex);
        while(received == false) {
            cv.wait(lk);
        }
    }
    catch(ers::Issue& ex) {
        ers::fatal(ex);

        return EXIT_FAILURE;
    }
    catch(std::exception& ex) {
        std::cerr << "Got exception: " << ex.what() << std::endl;

        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
