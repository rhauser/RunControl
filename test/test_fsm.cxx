/*
 * test_fsm.cxx
 *
 *  Created on: Apr 4, 2013
 *      Author: avolio
 */
#include "RunControl/FSM/FSM.h"
#include "RunControl/FSM/FSMDefs.h"
#include "RunControl/FSM/FSMStates.h"
#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/UserRoutines.h"
#include "RunControl/Common/RunControlTransitionActions.h"
#include "RunControl/Common/Exceptions.h"

#include <cppunit/TestFixture.h>
#include <cppunit/TestAssert.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestCaller.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>

#include <memory>
#include <tuple>
#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <iterator>
#include <set>
#include <stdexcept>
#include <utility>

#include <stdlib.h>

using namespace daq::rc;

class EmptyActions : public RunControlTransitionActions {
    public:
        EmptyActions() : RunControlTransitionActions(), m_no_transitions(0) {
        }

        void interrupt(bool) override {}

        void reset() {
            m_no_transitions = 0;
        }

        void noTransition(const TransitionCmd&) {
            ++m_no_transitions;
        }

        unsigned int noTransitions() const {
            return m_no_transitions;
        }

    private:
        unsigned int m_no_transitions;
};

class SimpleActions : public RunControlTransitionActions {
    public:
        SimpleActions(FSM_STATE initial) : RunControlTransitionActions(),
            m_current_state(FSM_STATE::ABSENT), m_destination_state(FSM_STATE::ABSENT),
            m_tr_cmd(FSM_COMMAND::INIT_FSM), m_initial_state(initial), m_busy(false), m_reloads(0)
        {
        }

        void enterFSM(const TransitionCmd& tr) override {
            m_current_state = FSM_STATE::ABSENT;
            m_destination_state = m_initial_state;
            m_tr_cmd = FSM_COMMAND::INIT_FSM;

            check(tr);
        }

        void initialize(const TransitionCmd& tr) override {
            m_current_state = m_destination_state;
            m_destination_state = FSM_STATE::INITIAL;
            m_tr_cmd = FSM_COMMAND::INITIALIZE;

            check(tr);
        }

        void shutdown(const TransitionCmd& tr) override {
            m_current_state = m_destination_state;
            m_destination_state = m_initial_state;
            m_tr_cmd = FSM_COMMAND::SHUTDOWN;

            check(tr);
        }

        void configure(const TransitionCmd& tr) override {
            m_current_state = m_destination_state;
            m_destination_state = FSM_STATE::CONFIGURED;
            m_tr_cmd = FSM_COMMAND::CONFIGURE;

            check(tr);
        }

        void connect(const TransitionCmd& tr) override {
            m_current_state = m_destination_state;
            m_destination_state = FSM_STATE::CONNECTED;
            m_tr_cmd = FSM_COMMAND::CONNECT;

            check(tr);
        }

        void prepareForRun(const TransitionCmd& tr) override {
            m_current_state = m_destination_state;
            m_destination_state = FSM_STATE::RUNNING;
            m_tr_cmd = FSM_COMMAND::START;

            check(tr);
       }

        void stopROIB(const TransitionCmd& tr) override {
            m_current_state = m_destination_state;
            m_destination_state = FSM_STATE::ROIBSTOPPED;
            m_tr_cmd = FSM_COMMAND::STOPROIB;

            check(tr);
      }

        void stopDC(const TransitionCmd& tr) override {
            m_current_state = m_destination_state;
            m_destination_state = FSM_STATE::DCSTOPPED;
            m_tr_cmd = FSM_COMMAND::STOPDC;

            check(tr);
       }

        void stopHLT(const TransitionCmd& tr) override {
            m_current_state = m_destination_state;
            m_destination_state = FSM_STATE::HLTSTOPPED;
            m_tr_cmd = FSM_COMMAND::STOPHLT;

            check(tr);
        }

        void stopRecording(const TransitionCmd& tr) override {
            m_current_state = m_destination_state;
            m_destination_state = FSM_STATE::SFOSTOPPED;
            m_tr_cmd = FSM_COMMAND::STOPRECORDING;

            check(tr);
        }

        void stopGathering(const TransitionCmd& tr) override {
            m_current_state = m_destination_state;
            m_destination_state = FSM_STATE::GTHSTOPPED;
            m_tr_cmd = FSM_COMMAND::STOPGATHERING;

            check(tr);
       }

        void stopArchiving(const TransitionCmd& tr) override {
            m_current_state = m_destination_state;
            m_destination_state = FSM_STATE::CONNECTED;
            m_tr_cmd = FSM_COMMAND::STOPARCHIVING;

            check(tr);
        }

        void disconnect(const TransitionCmd& tr) override {
            m_current_state = m_destination_state;
            m_destination_state = FSM_STATE::CONFIGURED;
            m_tr_cmd = FSM_COMMAND::DISCONNECT;

            check(tr);
        }

        void unconfigure(const TransitionCmd& tr) override {
            m_current_state = m_destination_state;
            m_destination_state = FSM_STATE::INITIAL;
            m_tr_cmd = FSM_COMMAND::UNCONFIGURE;

            check(tr);
        }

        void interrupt(bool) override {}

        void userBroadcast(const UserBroadcastCmd& tr) override {
            m_tr_cmd = FSM_COMMAND::USERBROADCAST;

            check(tr);
        }

        void reloadDB(const TransitionCmd& tr) override {
            m_tr_cmd = FSM_COMMAND::RELOAD_DB;

            check(tr);

            ++m_reloads;
        }

        void resynch(const ResynchCmd& tr) override {
            m_tr_cmd = FSM_COMMAND::RESYNCH;

            check(tr);
        }

        unsigned int reloads() const {
            return m_reloads;
        }

        FSM_STATE currentState() const {
            return m_current_state;
        }

        void transitioning(const TransitionCmd&) {
            CPPUNIT_ASSERT(m_busy == false);
            m_busy = true;
        }

        void transitionDone(const TransitionCmd&) {
            m_current_state = m_destination_state;

            CPPUNIT_ASSERT(m_busy == true);
            m_busy = false;
        }

        void reset() {
            m_current_state = FSM_STATE::ABSENT;
            m_destination_state = m_initial_state;
            m_tr_cmd = FSM_COMMAND::INIT_FSM;
            m_busy = false;
            m_reloads = 0;
        }

    protected:
        void check(const TransitionCmd& tr) const {
            CPPUNIT_ASSERT(m_current_state == FSMStates::stringToState(tr.fsmSourceState()));
            CPPUNIT_ASSERT(m_destination_state == FSMStates::stringToState(tr.fsmDestinationState()));
            CPPUNIT_ASSERT(m_tr_cmd == FSMCommands::stringToCommand(tr.fsmCommand()));
        }

    private:
        FSM_STATE m_current_state;
        FSM_STATE m_destination_state;
        FSM_COMMAND m_tr_cmd;
        FSM_STATE m_initial_state;
        bool m_busy;
        unsigned int m_reloads;
};

class ThrowingActions : public RunControlTransitionActions {
    public:
        ThrowingActions() : RunControlTransitionActions(), m_thrown_ex(0), m_received_ex(0) {
        }

        void interrupt(bool) override {
        }

        void configure(const TransitionCmd&) override {
            if(m_thrown_ex == 0) {
                ++m_thrown_ex;
                throw std::runtime_error("First attempt to configure always fails!");
            }
        }

        void prepareForRun(const TransitionCmd&) override {
            if(m_thrown_ex == 1) {
                ++m_thrown_ex;
                throw std::runtime_error("First attempt to run always fails too!");
            }
        }

        void fsmException(const TransitionCmd& cmd, std::exception&) {
            // Check that the context is correct
            if(m_thrown_ex == 2) {
                CPPUNIT_ASSERT(cmd.fsmCommand() == FSMCommands::START_CMD);
                CPPUNIT_ASSERT(cmd.fsmSourceState() == FSMStates::CONNECTED_STATE);
                CPPUNIT_ASSERT(cmd.fsmDestinationState() == FSMStates::RUNNING_STATE);
            } else {
                CPPUNIT_ASSERT(cmd.fsmCommand() == FSMCommands::CONFIGURE_CMD);
                CPPUNIT_ASSERT(cmd.fsmSourceState() == FSMStates::INITIAL_STATE);
                CPPUNIT_ASSERT(cmd.fsmDestinationState() == FSMStates::CONFIGURED_STATE);
            }

            ++m_received_ex;
            CPPUNIT_ASSERT(m_received_ex < 3);
            CPPUNIT_ASSERT(m_received_ex == m_thrown_ex);
        }

        bool gotException() const {
            return (m_received_ex != 0);
        }

        void reset() {
            m_thrown_ex = 0;
            m_received_ex = 0;
        }

    private:
        unsigned int m_thrown_ex;
        unsigned int m_received_ex;
};

template<FSM_STATE InitialState>
class FSMTest : public CppUnit::TestFixture {
    CPPUNIT_TEST_SUITE(FSMTest<InitialState>);
    CPPUNIT_TEST(testStateList);
    CPPUNIT_TEST(testStateValidity);
    CPPUNIT_TEST(testCommandList);
    CPPUNIT_TEST(testCommandValidity);
    CPPUNIT_TEST(testCommandAccepted);
    CPPUNIT_TEST(testFSMRoundTrip);
    CPPUNIT_TEST(testFSMRoundTripEx);
    CPPUNIT_TEST(testTransitionActions);
    CPPUNIT_TEST(testDeferred);
    CPPUNIT_TEST(testChaining);
    CPPUNIT_TEST(testGraph);
    CPPUNIT_TEST_SUITE_END();

    public:
        FSMTest() : CppUnit::TestFixture(), m_empty_actions(new EmptyActions()), m_simple_actions(new SimpleActions(InitialState)), m_throwing_actions(new ThrowingActions()),
                    m_fsm_empty(0), m_fsm_simple(0), m_fsm_throwing(0)
        {
        }

        constexpr unsigned int maxNumOfTransitions() {
            return (InitialState == FSM_STATE::NONE ? 13 : 11);
        }

        void setUp() {
            m_empty_actions->reset();
            m_fsm_empty = new FSM<InitialState>(m_empty_actions, std::shared_ptr<UserRoutines>(new UserRoutines()));
            m_fsm_empty->registerOnNoTransitionCallback(std::bind(&EmptyActions::noTransition, m_empty_actions, std::placeholders::_1));
            m_fsm_empty->start(false);

            m_simple_actions->reset();
            m_fsm_simple = new FSM<InitialState>(m_simple_actions, std::shared_ptr<UserRoutines>(new UserRoutines()));
            m_fsm_simple->registerOnTransitionDoneCallback(std::bind(&SimpleActions::transitionDone, m_simple_actions, std::placeholders::_1));
            m_fsm_simple->registerOnTransitioningCallback(std::bind(&SimpleActions::transitioning, m_simple_actions, std::placeholders::_1));
            m_fsm_simple->start(false);

            m_throwing_actions->reset();
            m_fsm_throwing = new FSM<InitialState>(m_throwing_actions, std::shared_ptr<UserRoutines>(new UserRoutines()));
            m_fsm_throwing->registerOnTransitionExceptionCallback(std::bind(&ThrowingActions::fsmException, m_throwing_actions, std::placeholders::_1, std::placeholders::_2));
            m_fsm_throwing->start(false);

            m_invalid_states.insert(FSM_STATE::ABSENT);
            if(InitialState != FSM_STATE::NONE) { // Not the best way to use templates...
                m_invalid_states.insert(FSM_STATE::NONE);
            }

            m_invalid_commands.insert(FSM_COMMAND::INIT_FSM);
            m_invalid_commands.insert(FSM_COMMAND::STOP_FSM);
            if(InitialState != FSM_STATE::NONE) { // Not the best way to use templates...
                // These are the commands accepted only if the initial state is NONE (i.e., the RootController's FSM)
                m_invalid_commands.insert(FSM_COMMAND::INITIALIZE);
                m_invalid_commands.insert(FSM_COMMAND::SHUTDOWN);
                m_invalid_commands.insert(FSM_COMMAND::RELOAD_DB);
            }
        }

        void tearDown() {
            delete m_fsm_empty;
            delete m_fsm_simple;
            delete m_fsm_throwing;

            m_invalid_states.clear();
            m_invalid_commands.clear();
        }

        void testStateList() {
            // This test checks that the all the states used by the FSM are correctly listed in "FSMStates"
            auto statesFromFSM = m_fsm_empty->states();
            for(const auto& s : m_invalid_states) {
                // Add states that do not come directly from the transition table
                statesFromFSM.insert(s);
            }

            std::set<FSM_STATE> statesFromStruct;
            const auto& its = FSMStates::states();
            for(auto it = its.first; it != its.second; ++it) {
                statesFromStruct.insert(it->first);
            }

            std::vector<FSM_STATE> diff;
            std::set_symmetric_difference(statesFromFSM.begin(), statesFromFSM.end(),
                                          statesFromStruct.begin(), statesFromStruct.end(),
                                          std::back_inserter(diff));

            std::string msg = "The following states are not defined in the FSM and/or in the data structure: ";
            for(const auto& s : diff) {
                msg += std::to_string(static_cast<unsigned int>(s)) + " ";
            }

            CPPUNIT_ASSERT_MESSAGE(msg, statesFromFSM == statesFromStruct);
        }

        void testStateValidity() {
            // Check that the FSM does not declare invalid a state that *is* valid
            std::set<FSM_STATE> invalid;

            const auto& its = FSMStates::states();
            for(auto it = its.first; it != its.second; ++it) {
                if(m_fsm_empty->isStateValid(it->first) == false) {
                    invalid.insert(it->first);
                }
            }

            std::vector<FSM_STATE> diff;
            std::set_symmetric_difference(m_invalid_states.begin(), m_invalid_states.end(),
                                          invalid.begin(), invalid.end(),
                                          std::back_inserter(diff));

            std::string msg = "The following states are declared valid (invalid) by the FSM while they should be invalid (valid): ";
            for(const auto& s : diff) {
                msg += std::to_string(static_cast<unsigned int>(s)) + " ";
            }


            CPPUNIT_ASSERT_MESSAGE(msg, m_invalid_states == invalid);
        }

        void testCommandList() {
            // This test checks that the all the commands used by the FSM are correctly listed in "FSMCommands"
            auto commandsFromFSM = m_fsm_empty->commands();
            for(const auto& c : m_invalid_commands) {
                // Add commands that do not come directly from the transition table
                commandsFromFSM.insert(c);
            }

            std::set<FSM_COMMAND> commandsFromStruct;
            const auto& its = FSMCommands::commands();
            for(auto it = its.first; it != its.second; ++it) {
                commandsFromStruct.insert(it->first);
            }

            std::vector<FSM_COMMAND> diff;
            std::set_symmetric_difference(commandsFromFSM.begin(), commandsFromFSM.end(),
                                          commandsFromStruct.begin(), commandsFromStruct.end(),
                                          std::back_inserter(diff));

            std::string msg = "The following transition commands are not defined in the FSM and/or in the data structure: ";
            for(const auto& c : diff) {
                msg += std::to_string(static_cast<unsigned int>(c)) + " ";
            }

            CPPUNIT_ASSERT_MESSAGE(msg, commandsFromFSM == commandsFromStruct);
        }

        void testCommandValidity() {
            // Check that the FSM does not declare invalid a command that *is* valid
            std::set<FSM_COMMAND> invalid;

            FSMCommands::command_iterator it;
            const auto& its = FSMCommands::commands();
            for(it = its.first; it != its.second; ++it) {
                if(m_fsm_empty->isCommandValid(it->first) == false) {
                    invalid.insert(it->first);
                }
            }

            std::vector<FSM_COMMAND> diff;
            std::set_symmetric_difference(m_invalid_commands.begin(), m_invalid_commands.end(),
                                          invalid.begin(), invalid.end(),
                                          std::back_inserter(diff));

            std::string msg = "The following commands are declared valid (invalid) by the FSM while they should be invalid (valid): ";
            for(const auto& s : diff) {
                msg += std::to_string(static_cast<unsigned int>(s)) + " ";
            }


            CPPUNIT_ASSERT_MESSAGE(msg, m_invalid_commands == invalid);
        }

        void testCommandAccepted() {
            // Test that a command declared as valid is actually accepted by the FSM
            FSMCommands::command_iterator it;
            const auto& its = FSMCommands::commands();
            for(it = its.first; it != its.second; ++it) {
                if(m_fsm_empty->isCommandValid(it->first) == true) {
                    const std::string msg = std::string("The command ") + it->second + " has not been accepted by the FSM";
                    CPPUNIT_ASSERT_NO_THROW_MESSAGE(msg, m_fsm_empty->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(it->first))));
                }
            }
        }

        void testFSMRoundTrip() {
            // Make two full round-trip of the FSM using NEXT_COMMAND
            // maxtTr is the number of commands to be sent in order to return to
            // the FSM's initial state
            const unsigned int maxTr = maxNumOfTransitions();
            for(unsigned int j = 0; j < 2; ++j) {
                for(unsigned int i = 0; i < maxTr; ++i) {
                    m_fsm_empty->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::NEXT_TRANSITION)));
                    if(std::get<0>(m_fsm_empty->status()) == FSM_STATE::RUNNING) {
                        // This should trigger a 'noTransition' call-back
                        m_fsm_empty->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::START)));
                    }
                }

                const auto& fsmStatus = m_fsm_empty->status();

                const std::string msg = std::string("The reached FSM state is ") + FSMStates::stateToString(std::get<0>(fsmStatus)) +
                                        " while it should be " + FSMStates::stateToString(InitialState);
                CPPUNIT_ASSERT_MESSAGE(msg, std::get<0>(fsmStatus) == InitialState);
            }

            // Now send some transition commands that should trigger a 'noTransition' call-back
            m_fsm_empty->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::UNCONFIGURE)));
            m_fsm_empty->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::START)));

            // Expected: the previous 2 + the 2 in the loop
            CPPUNIT_ASSERT(m_empty_actions->noTransitions() == 4);
        }

        void testFSMRoundTripEx() {
            // Like "testFSMRoundTrip" but using the FSM with RunControlActions throwing exceptions
            // Since the exception is thrown at the first attempt to perform the CONFIGURE and START transitions
            // the number of transitions to perform the first round-trip is increased by two
            const int maxTr = 2 * maxNumOfTransitions() + 2;
            for(int i = 0; i < maxTr; ++i) {
                m_fsm_throwing->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::NEXT_TRANSITION)));
             }

            const auto& fsmStatus = m_fsm_throwing->status();

            const std::string msg = std::string("The reached FSM state is ") + FSMStates::stateToString(std::get<0>(fsmStatus)) +
                                    " while it should be " + FSMStates::stateToString(InitialState);
            CPPUNIT_ASSERT_MESSAGE(msg, std::get<0>(fsmStatus) == InitialState);

            CPPUNIT_ASSERT(m_throwing_actions->gotException() == true);
        }

        void testTransitionActions() {
            // Make a round-trip and check that the transition actions are correctly called
            std::list<FSM_COMMAND> inStateTrans {FSM_COMMAND::USERBROADCAST, FSM_COMMAND::RELOAD_DB, FSM_COMMAND::RESYNCH};

            const int maxTr = maxNumOfTransitions() + 2;
            for(int i = 0; i < maxTr; ++i) {
                m_fsm_simple->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::NEXT_TRANSITION)));

                const FSM_STATE fsmState = std::get<0>(m_fsm_simple->status());
                const FSM_STATE ctrlState = m_simple_actions->currentState();
                const std::string msg = std::string("FSM says the current state is ") + FSMStates::stateToString(fsmState) +
                                        " while the Controllable says " + FSMStates::stateToString(ctrlState);
                CPPUNIT_ASSERT_MESSAGE(msg, fsmState == ctrlState);

                // Now sending a command for an in-state transition should not change the current state
                for(const auto& s : inStateTrans) {
                    m_fsm_simple->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(s)));

                    const FSM_STATE fsmState_new = std::get<0>(m_fsm_simple->status());
                    const FSM_STATE ctrlState_new = m_simple_actions->currentState();

                    {
                        const std::string msg_new = std::string("FSM says the current state is ") + std::to_string(static_cast<unsigned int>(fsmState_new)) +
                                                    " while the Controllable says " + std::to_string(static_cast<unsigned int>(ctrlState_new));
                        CPPUNIT_ASSERT_MESSAGE(msg_new, fsmState_new == ctrlState_new);
                    }

                    {
                        const std::string msg_new = std::string("An in-state transition has modified the current state! The state should be") +
                                                    std::to_string(static_cast<unsigned int>(ctrlState)) + " while it is " + std::to_string(static_cast<unsigned int>(ctrlState));
                        CPPUNIT_ASSERT(ctrlState_new == ctrlState);
                    }
                }
            }
        }

        void testDeferred() {
            // When the RELOAD_DB command is received in a state different that NONE, it is deferred
            // The deferring mechanism is configured in such a way that only one command can be in the deferred event queue

            // 1. Move away from initial state (NONE or INITIAL)
            m_fsm_simple->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::NEXT_TRANSITION)));

            // 2. Send RELOAD_DB twice: only one event should go into the deferred queue
            m_fsm_simple->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::RELOAD_DB)));
            m_fsm_simple->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::RELOAD_DB)));

            // 3. Go back to initial state (this will work only if the initial state is NONE - SHUTDOWN is not defined when the initial state is INITIAL)
            m_fsm_simple->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::SHUTDOWN)));

            // Results:
            // Initial state is NONE: INITIAL -> RELOAD -> RELOAD -> NONE (the reload should be executed only once)
            // Initial state is INITIAL: CONFIGURED (the REALOAD and SHUTDOWN commands are not accepted)
            unsigned int numOfReloads = m_simple_actions->reloads();
            CPPUNIT_ASSERT(((numOfReloads == 1) && (InitialState == FSM_STATE::NONE)) || ((numOfReloads == 0) && (InitialState == FSM_STATE::INITIAL)));

            // 4. Two additional RELOAD_DB
            m_fsm_simple->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::RELOAD_DB)));
            m_fsm_simple->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::RELOAD_DB)));

            numOfReloads = m_simple_actions->reloads();
            CPPUNIT_ASSERT(((numOfReloads == 3) && (InitialState == FSM_STATE::NONE)) || ((numOfReloads == 0) && (InitialState == FSM_STATE::INITIAL)));
        }

        void testChaining() {
            // Here we test the CONFIG and STOP commands that generates multiple events
            // in the FSM so that several transitions are performed
            unsigned int pass = 0;

            while(true) {
                if(std::get<0>(m_fsm_simple->status()) == FSM_STATE::INITIAL) {
                    ++pass;
                    m_fsm_simple->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::CONFIG)));
                    CPPUNIT_ASSERT(std::get<0>(m_fsm_simple->status()) == FSM_STATE::CONNECTED);
                }

                if(std::get<0>(m_fsm_simple->status()) == FSM_STATE::RUNNING) {
                    ++pass;
                    m_fsm_simple->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::STOP)));
                    CPPUNIT_ASSERT(std::get<0>(m_fsm_simple->status()) == FSM_STATE::CONNECTED);

                    m_fsm_simple->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::UNCONFIG)));
                    CPPUNIT_ASSERT(std::get<0>(m_fsm_simple->status()) == FSM_STATE::INITIAL);
                    ++pass;

                    break;
                }

                m_fsm_simple->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::NEXT_TRANSITION)));
            }

            CPPUNIT_ASSERT(pass == 3);
        }

        void testGraph() {
            {
                const auto& path = m_fsm_empty->transitions(InitialState, FSM_STATE::RUNNING);
                CPPUNIT_ASSERT(InitialState == FSM_STATE::NONE ? (path.size() == 4) : (path.size() == 3));
            }

            {
                const auto& path = m_fsm_empty->transitions(FSM_STATE::RUNNING, InitialState);
                CPPUNIT_ASSERT(InitialState == FSM_STATE::NONE ? (path.size() == 9) : (path.size() == 8));
            }
        }

    private:
        std::shared_ptr<EmptyActions> m_empty_actions;
        std::shared_ptr<SimpleActions> m_simple_actions;
        std::shared_ptr<ThrowingActions> m_throwing_actions;
        FSM<InitialState>* m_fsm_empty;
        FSM<InitialState>* m_fsm_simple;
        FSM<InitialState>* m_fsm_throwing;
        std::set<FSM_STATE> m_invalid_states;
        std::set<FSM_COMMAND> m_invalid_commands;
};

int main(int, char**) {
    CppUnit::TextUi::TestRunner runner;

    runner.addTest(FSMTest<FSM_STATE::NONE>::suite());
    runner.addTest(FSMTest<FSM_STATE::INITIAL>::suite());

    if(runner.run() == true) {
        return EXIT_SUCCESS;
    }

    return EXIT_FAILURE;
}
