%module(directors="1") rc_JItemCtrl

%{
#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/FSM/FSMStates.h"
#include "RunControl/Common/RunControlBasicCommand.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/Controllable.h"
#include "RunControl/ItemCtrl/ControllableDispatcher.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"
#include "RunControl/ItemCtrl/ItemCtrlImpl.h"
%}

%include <typemaps.i>
%include <cpointer.i>
%include <carrays.i>
%include <stl.i>
%include <std_common.i>
%include <std_list.i>
%include <std_map.i>
%include <std_vector.i>
%include <std_string.i>
%include <various.i>
%include <std_shared_ptr.i>
%include <stdint.i>
%include <swiginterface.i>
%include <enums.swg>

// Enable director in order to extend Controllable in java
%feature("director") daq::rc::Controllable;
%feature("director") daq::rc::TransitionActions;
%javaconst(1);

 // Shared pointer definition
%shared_ptr(daq::rc::Controllable);
%shared_ptr(daq::rc::TransitionActions);
%shared_ptr(daq::rc::ControllableDispatcher);
%shared_ptr(daq::rc::DefaultDispatcher);
%shared_ptr(daq::rc::InverseDispatcher);
%shared_ptr(daq::rc::ParallelDispatcher);

// Mapping for char**
%apply char **STRING_ARRAY { char **argv };

// Ignore move constuctors and move assignemnt operators
%ignore daq::rc::RunControlBasicCommand::RunControlBasicCommand(RunControlBasicCommand&&);
%ignore daq::rc::ItemCtrl::ItemCtrl(ItemCtrl&&);
%ignore daq::rc::ItemCtrl::operator=(ItemCtrl&&);

// Template specifications
%template(StringVector) std::vector<std::string>;
%template(FSMCommandToStringVector) std::map<daq::rc::FSM_COMMAND, std::vector<std::string>, std::less<daq::rc::FSM_COMMAND>>;
%template(ControllableList) std::list<std::shared_ptr<daq::rc::Controllable>>;

// Ignored because using unique_ptr
%ignore daq::rc::RunControlBasicCommand::clone() const;
%ignore daq::rc::TransitionCmd::create(const std::string&);
%ignore daq::rc::UserBroadcastCmd::userCommand() const;
%ignore daq::rc::ApplicationStatusCmd::currentTransitionCommand() const;
%ignore daq::rc::ApplicationStatusCmd::lastTransitionCommand() const;
%ignore daq::rc::ApplicationStatusCmd::lastCommand() const;
%ignore daq::rc::RunControlCommands::factory(const std::string&);

// Ignored because using std::set
%ignore daq::rc::StartStopAppCmd::applications;
%ignore daq::rc::RunControlBasicCommand::notConcurrentWith;

// Ignored because using std::function
%ignore daq::rc::ControllableDispatcher::dispatch;

// Ignored because of boost::chrono
%ignore daq::rc::ApplicationStatusCmd::ApplicationStatusCmd;
%ignore daq::rc::ApplicationStatusCmd::lastTransitionDuration;
%ignore daq::rc::ApplicationStatusCmd::infoTimeTag;

// Ignore methods using boost bimaps (not needed with Java enumerations)
%ignore daq::rc::FSMCommands::commands();
%ignore daq::rc::FSMStates::states();

// Exception handling
%javaexception("daq.rc.RCException.ItemCtrlException") daq::rc::ItemCtrl::ItemCtrl {
  try {
    $action
  } catch(std::exception& ex) {
    jclass clazz = jenv->FindClass("daq/rc/RCException$ItemCtrlException");
    jenv->ThrowNew(clazz, ex.what());
    return $null;
  }
}

%javaexception("daq.rc.RCException.ItemCtrlException") daq::rc::ItemCtrl::init {
  try {
    $action
  } catch(std::exception& ex) {
    jclass clazz = jenv->FindClass("daq/rc/RCException$ItemCtrlException");
    jenv->ThrowNew(clazz, ex.what());
    return $null;
  }
}

%javaexception("daq.rc.RCException.ItemCtrlException") daq::rc::ItemCtrl::run {
  try {
    $action
  } catch(std::exception& ex) {
    jclass clazz = jenv->FindClass("daq/rc/RCException$ItemCtrlException");
    jenv->ThrowNew(clazz, ex.what());
    return $null;
  }
}

%include "../RunControl/FSM/FSMCommands.h"
%include "../RunControl/FSM/FSMStates.h"
%include "../RunControl/Common/CmdLineParser.h"
%include "../RunControl/Common/RunControlBasicCommand.h"
%include "../RunControl/Common/RunControlCommands.h"
%include "../RunControl/Common/Controllable.h"
%include "../RunControl/ItemCtrl/ControllableDispatcher.h"
%include "../RunControl/ItemCtrl/ItemCtrl.h"
