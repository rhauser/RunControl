package daq.rc;

import java.lang.ref.WeakReference;

import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAPackage.ObjectNotActive;
import org.omg.PortableServer.POAPackage.WrongAdapter;
import org.omg.PortableServer.POAPackage.WrongPolicy;

import daq.rc.RCException.BadCommandException;


/**
 * Class to be used to receive notifications by Run Control applications when a sent command has completed its execution.
 * <p>
 * Derived class have to implement the {@link #commandExecuted(Command)} method, that is called when the command execution is done.
 * <p>
 * When this object is not used anymore, the {@link #close()} method should be executed.
 * 
 * {@link CommandSender#executeCommand(String, Command, CommandNotifier)} and
 * {@link CommandSender#executeCommand(String, daq.rc.Command.TransitionCmd, CommandNotifier)}
 */
public abstract class CommandNotifier implements AutoCloseable {
    private final CommandNotifierAdapter notifierAdapter;

    /**
     * Adapter class implementing the CORBA interface
     */
    static private class CommandNotifierAdapter extends rc.senderPOA {
        private final WeakReference<CommandNotifier> cmdNotifier;
        private volatile boolean activated = false;

        /**
         * Constructor
         * 
         * @param notifier Weak reference to the {@link CommandNotifier}
         */
        CommandNotifierAdapter(final WeakReference<CommandNotifier> notifier) {
            this.cmdNotifier = notifier;
        }

        /**
         * Remotely called when the command's execution is done
         * 
         * @param cmdString String representation of the executed command
         */
        @Override
        public void commandExecuted(final String cmdString) {
            @SuppressWarnings("resource")
            final CommandNotifier c = this.cmdNotifier.get();
            if(c != null) {
                try {
                    final daq.rc.Command cmd = daq.rc.Command.buildCommand(cmdString);
                    c.commandExecuted(cmd);
                }
                catch(final BadCommandException ex) {
                    ex.printStackTrace();
                }
            }
        }

        /**
         * It returns the CORBA reference of this object
         * <p>
         * It activates the object if not done yet
         * 
         * @return The CORBA reference of this object
         */
        String reference() {
            if(this.activated == false) {
                synchronized(this) {
                    if(this.activated == false) {
                        this._this(ipc.Core.getORB());
                        this.activated = true;
                    }
                }
            }

            return ipc.Core.objectToString(this._this(), ipc.Core.CORBALOC);
        }

        /**
         * It deactivates this object (needed to release the CORBA resources)
         */
        void deactivate() {
            try {
                final POA p = ipc.Core.getRootPOA();
                p.deactivate_object(p.reference_to_id(this._this()));
            }
            catch(final ObjectNotActive | WrongPolicy | WrongAdapter ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Constructor.
     */
    public CommandNotifier() {
        this.notifierAdapter = new CommandNotifierAdapter(new WeakReference<CommandNotifier>(this));
    }

    /**
     * It releases the resources used by this object.
     * <p>
     * After a call to this method, the object will not receive notifications any more.
     */
    @Override
    public void close() {
        this.notifierAdapter.deactivate();
    }

    /**
     * Called when a command is completed
     * 
     * @param cmd The executed command
     */
    protected abstract void commandExecuted(final daq.rc.Command cmd);

    /**
     * It returns the CORBA reference identifying this object
     * 
     * @return The CORBA reference identifying this object
     */
    String reference() {
        return this.notifierAdapter.reference();
    }
}
