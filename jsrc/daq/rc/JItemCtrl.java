package daq.rc;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import daq.rc.RCException.ItemCtrlException;
import daq.rc.RCException.OnlineServicesFailure;
import daq.rc.internal.Controllable;
import daq.rc.internal.ControllableDispatcher;
import daq.rc.internal.ControllableList;
import daq.rc.internal.DefaultDispatcher;
import daq.rc.internal.InverseDispatcher;
import daq.rc.internal.ItemCtrl;
import daq.rc.internal.ParallelDispatcher;


/**
 * Entry class for the creation of a run control application.
 * <p>
 * Users need to properly instantiate an instance of this class in order to integrate the application in the run control framework.
 * <p>
 * Actions executed during state transitions and upon the reception of any external command, are defined using the {@link JControllable}
 * interface. Multiple {@link JControllable} interfaces can be passed to the {@link JItemCtrl} in order to better classify different tasks
 * the application may execute (see {@link ControllableDispatcherType}).
 * <p>
 * For a simple example of how the {@link JItemCtrl} should be used, see {@link daq.rc.example.RCExampleApplication}
 * <p>
 * Note: this class uses the <em>PMG sync</em> mechanism in order to notify the Process Management system when the application is ready.
 * Only at that moment the physical process will be reported as up and running. The <em>PMG sync</em> is done once {@link #run()} is called.
 * <p>
 * Note: because of the usage of the <em>PMG sync</em> mechanism, any Run Control application has to be described in the OKS database with
 * an <em>InitTimeout</em> value grater than zero. The timeout can be tuned taking into account the amount of work to be done before
 * {@link #run()} is called.
 * <p>
 * Note: the {@link OnlineServices} class is initialized by the {@link JItemCtrl} constructor(s).
 * 
 * @see <a href="https://gitlab.cern.ch/atlas-tdaq-software/RunControl/blob/master/jsrc/daq/rc/example/RCExampleApplication.java">Example run control
 *      application</a> and the related <a href="https://gitlab.cern.ch/atlas-tdaq-software/RunControl/blob/master/bin/jrc_example_application">script</a>
 *      to run it.
 */
public class JItemCtrl {
    // Load the JNI library
    static {
        try {
            System.loadLibrary("rc_JItemCtrl");
        }
        catch(final UnsatisfiedLinkError ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    private final ItemCtrl itemCtrl;

    // Why to keep the two following references?
    // They are passed to the C++ layer and the SWIG proxy classes will just take their address
    // So we need to be sure that they are not garbage collected too early
    // Keeping here their references gives them the same lifetime as the JItemCtrl
    @SuppressWarnings("unused")
    private final List<? super Controllable> cList;
    @SuppressWarnings("unused")
    private final ControllableDispatcher cDispatcher;

    /**
     * Constructor.
     * 
     * @param name The application's name
     * @param parent The application's parent name
     * @param segment The application's segment name
     * @param partition The partition name
     * @param ctrl A {@link JControllable} instance encapsulating application's actions
     * @param isInteractive <em>true</em> if the application has to be executed in interactive way
     * @throws ItemCtrlException Some error occurred
     * @throws IllegalArgumentException <em>ctrl</em> is null, or any of <em>name</em>, <em>parent</em>, <em>segment</em>,
     *             <em>partition</em> is null or empty
     */
    @SuppressWarnings("cast")
    public JItemCtrl(final String name,
                     final String parent,
                     final String segment,
                     final String partition,
                     final JControllable ctrl,
                     final boolean isInteractive)
        throws ItemCtrlException
    {
        this(name,
             parent,
             segment,
             partition,
             (List<JControllable>) Stream.of(ctrl).collect(Collectors.toCollection(ArrayList::new)),
             ControllableDispatcherType.DEFAULT,
             isInteractive);
    }

    /**
     * Constructor.
     * 
     * @param name The application's name
     * @param parent The application's parent name
     * @param segment The application's segment name
     * @param partition The partition name
     * @param ctrl A list of {@link JControllable} instances encapsulating application's actions
     * @param dType This specifies the way the {@link JControllable} instances should be executed
     * @param isInteractive <em>true</em> if the application has to be executed in interactive way
     * @throws ItemCtrlException Some error occurred
     * @throws IllegalArgumentException <em>ctrl</em>, or any of <em>name</em>, <em>parent</em>, <em>segment</em>, <em>partition</em> is
     *             null or empty
     */
    public JItemCtrl(final String name,
                     final String parent,
                     final String segment,
                     final String partition,
                     final List<? extends JControllable> ctrl,
                     final ControllableDispatcherType dType,
                     final boolean isInteractive)
        throws ItemCtrlException
    {
        if((ctrl == null) || (ctrl.isEmpty() == true)) {
            throw new IllegalArgumentException("The list of JControllable objects cannot be null or empty");
        }

        for(final JControllable jc : ctrl) {
            if(jc == null) {
                throw new IllegalArgumentException("None of the JControllable instance can be null");
            }
        }

        if((name == null) || (parent == null) || (segment == null) || (partition == null)) {
            throw new IllegalArgumentException("None of the application, parent, segment and partition names can be null");
        }

        if(name.isEmpty() || parent.isEmpty() || segment.isEmpty() || partition.isEmpty()) {
            throw new IllegalArgumentException("None of the application, parent, segment and partition names can be empty");
        }

        final List<ControllableProxy> cps =
                                          ctrl.stream().map((c) -> new ControllableProxy(c)).collect(Collectors.toCollection(ArrayList::new));
        final ControllableList cl = new ControllableList(cps);

        final ControllableDispatcher cd;
        switch(dType) {
            case DEFAULT:
                cd = new DefaultDispatcher();
                break;
            case INVERSE:
                cd = new InverseDispatcher();
                break;
            case PARALLEL:
                cd = new ParallelDispatcher();
                break;
            default:
                cd = new DefaultDispatcher();
                break;
        }

        this.cList = cl;
        this.cDispatcher = cd;
        this.itemCtrl = new ItemCtrl(name, parent, segment, partition, cl, isInteractive, cd);

        try {
            OnlineServices.initialize(partition, segment);
        }
        catch(final OnlineServicesFailure ex) {
            throw new ItemCtrlException("Failed to initialize some online service: " + ex, ex);
        }
        
        if(isInteractive == true) {
            System.out.println("***************************************");
            System.out.println("***** Running in interactive mode *****");
            System.out.println("***************************************");
            System.out.println("Application's name: " + name);
            System.out.println("Application's parent name: " + parent);
            System.out.println("Application's segment name: " + segment);
            System.out.println("Application's partition name: " + partition);
        }
    }

    /**
     * It initializes the Run Control framework (<em>i.e.</em>, the IPC system is initialized and the IPC publication is done).
     * <p>
     * Note: the process will not be up for the Process Management system until {@link #run()} is called.
     * <p>
     * To be called before {@link #run()}.
     * 
     * @throws ItemCtrlException The initialization failed (this shall be considered a <em>FATAL</em> error)
     */
    public void init() throws ItemCtrlException {
        try {
            this.itemCtrl.init();
        }
        catch(final ItemCtrlException ex) {
            // The ItemCtrl is coming from C++, calling "delete" explicitly allows to execute the destructor
            this.itemCtrl.delete();
            throw ex;
        }
    }

    /**
     * This method should be called when this Run Control application is ready to start its operations (<em>i.e.</em>, it is ready to
     * receive and execute external commands).
     * <p>
     * At this stage the Process Management system will report this process as up and running.
     * <p>
     * To be called only after {@link #init()}.
     * <p>
     * Note: this method blocks and will return only when this application receives a SIGINT or SIGTERM signal.
     * 
     * @throws ItemCtrlException Some error occurred
     */
    public void run() throws ItemCtrlException {
        try {
            // This blocks
            this.itemCtrl.run();
        }
        finally {
            // The ItemCtrl is coming from C++, calling "delete" explicitly allows to execute the destructor
            this.itemCtrl.delete();
        }
    }
}
