package daq.rc;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import TM.Test.Scope;
import daq.rc.RCException.BadCommandException;


/**
 * Abstract class to be used to describe commands to be sent to Run Control applications
 * <p>
 * Derived classes can use the {@link #addParameter(String[], String)}, {@link #addParameters(String[], Map)},
 * {@link #addParameters(String[], String[])} and {@link #addCommand(String[], Command)} methods to add parameters to the commands they
 * represent.<br>
 * <p>
 * In order to properly de-serialize commands (starting from their XML-string encoded format), derived classed shall use the
 * {@link #getValue(String)}, {@link #getValues(String, String)} and {@link #getNestedCommand(String)} methods.
 * <p>
 * If the command does not support the asynchronous notification of the command sender, then the corresponding property should be modified
 * (in the derived class) using the {@link #replaceParameter(String, String)} method.
 * <p>
 * An instance of a command object should be used only once and not re-used to send the same command multiple times.
 */
public abstract class Command {
    private final Document xmlDoc;
    private final RCCommands command;
    private final String uuid;
    private volatile boolean alreadySent = false;

    private final static String TOP_HEADER = "_rc_command_";
    private final static String CMD_NAME_TAG = "_rc_command_name_";
    private final static String CMD_ID_TAG = "_rc_command_id_";
    private final static String VALUE_TAG = "_parameter_value_";
    private final static String REMOTE_REFERENCE_TAG = "_remote_reference_";
    private final static String NOTIFY_SUPPORTED_TAG = "_notify_supported_";

    /**
     * Enumeration for all the available FSM commands (i.e., FSM transitions)
     * 
     * @see CommandSender#makeTransition(String, Command.FSMCommands)
     * @see CommandSender#executeCommand(String, Command.TransitionCmd)
     * @see CommandSender#userBroadcast(String, String, String[])
     * @see CommandSender#resynch(String, int, String[])
     * @see CommandSender#subTransition(String, String, Command.FSMCommands)
     */
    public static enum FSMCommands {
        INITIALIZE, CONFIGURE, CONFIG, CONNECT, START, STOP, STOPROIB, STOPDC, STOPHLT, STOPRECORDING, STOPGATHERING, STOPARCHIVING,
        DISCONNECT, UNCONFIG, UNCONFIGURE,
        /**
         * Only available for the Root Controller
         */
        SHUTDOWN,
        /**
         * @see CommandSender#userBroadcast(String, String, String[])
         */
        USERBROADCAST,
        /**
         * Only available for the Root Controller
         * <p>
         * The database reload is allowed only in the {@link ApplicationState#NONE} state
         */
        RELOAD_DB,
        /**
         * This is allowed only in the {@link ApplicationState#RUNNING} state
         * 
         * @see CommandSender#resynch(String, int, String[])
         */
        RESYNCH, NEXT_TRANSITION,
        /**
         * @see CommandSender#subTransition(String, String, Command.FSMCommands)
         */
        SUB_TRANSITION,
        /**
         * Used internally in order to initialize the FSM
         */
        INIT_FSM,
        /**
         * Used internally in order to properly terminate the FSM
         */
        STOP_FSM;
    }

    /**
     * Enumeration for all the commands that can be sent to a Run Control application
     * <p>
     * Some commands may be executed or not depending on the recipient being a leaf application or a segment's controller.
     */
    public static enum RCCommands {
        /**
         * @see CommandSender#makeTransition(String, Command.FSMCommands)
         * @see CommandSender#executeCommand(String, Command.TransitionCmd)
         */
        MAKE_TRANSITION,
        /**
         * @see CommandSender#changeMembership(String, boolean, String[])
         * @see CommandSender#changeStatus(String, boolean, String[])
         */
        ENABLE,
        /**
         * @see CommandSender#changeMembership(String, boolean, String[])
         * @see CommandSender#changeStatus(String, boolean, String[])
         */
        DISABLE,
        /**
         * Available only for segment's controllers
         * 
         * @see CommandSender#ignoreError(String)
         */
        IGNORE_ERROR,
        /**
         * Available only for segment's controllers
         * 
         * @see CommandSender#stopApplications(String, String[])
         */
        STOPAPP,
        /**
         * Available only for segment's controllers
         * 
         * @see CommandSender#startApplications(String, String[])
         */
        STARTAPP,
        /**
         * Available only for segment's controllers
         * 
         * @see CommandSender#restartApplications(String, String[])
         */
        RESTARTAPP,
        /**
         * @see CommandSender#publish(String)
         */
        PUBLISH,
        /**
         * @see CommandSender#publishFullStats(String)
         */
        PUBLISHSTATS,
        /**
         * Available only for segment's controllers
         * 
         * @see CommandSender#exit(String)
         */
        EXIT,
        /**
         * Available only for leaf applications
         * 
         * @see CommandSender#userCommand(String, String, String[])
         */
        USER,
        /**
         * Available only for segment's controllers
         * 
         * @see CommandSender#dynamicRestart(String, String[])
         */
        DYN_RESTART,
        /**
         * @see CommandSender#changePublishInterval(String, int)
         */
        CHANGE_PROBE_INTERVAL,
        /**
         * @see CommandSender#changeFullStatsInterval(String, int)
         */
        CHANGE_FULLSTATS_INTERVAL,
        /**
         * Reserved for child applications updating the parent's controller
         */
        UPDATE_CHILD_STATUS,
        /**
         * @see CommandSender#testApp(String, String[])
         */
        TESTAPP;
    }

    /**
     * Class encapsulating an FSM transition command
     * 
     * @see RCCommands#MAKE_TRANSITION
     */
    public static class TransitionCmd extends Command {
        private final static String FSM_HEADER_TAG = "_fsm_";
        private final static String FSM_COMMAND_TAG = "event";
        private final static String FSM_SUBTR_DONE_TAG = "done_subtransitions";
        private final static String FSM_IS_RESTART_TAG = "is_restart";
        private final static String FSM_SKIP_SUBTR_TAG = "skip_subtransitions";

        private final FSMCommands command;

        /**
         * Constructor
         * <p>
         * Use {@link UserBroadCastCmd}, {@link SubTransitionCmd} or {@link ResynchCmd} if <em>fsmCommand</em> is any of
         * {@link FSMCommands#USERBROADCAST}, {@link FSMCommands#SUB_TRANSITION}, {@link FSMCommands#RESYNCH}
         * <p>
         * Derived classes shall use the {@link TransitionCmd#TransitionCmd(FSMCommands, boolean)} constructor.
         * 
         * @param fsmCommand The FSM command (i.e., the transition)
         * @throws BadCommandException Some error occurred building the command object
         * @throws IllegalArgumentException <em>fsmCommand</em> is any of {@link FSMCommands#USERBROADCAST},
         *             {@link FSMCommands#SUB_TRANSITION}, {@link FSMCommands#RESYNCH}
         */
        public TransitionCmd(final FSMCommands fsmCommand) throws BadCommandException, IllegalArgumentException {
            this(fsmCommand, true);
        }

        /**
         * Constructor
         * <p>
         * To be used by derived classes with <em>check</em> set to <em>false</em>
         * 
         * @param fsmCommand The FSM command (i.e., the transition)
         * @param check If <em>true</em> a check is done on the <em>fsmCommand</em> in order to verify that it is not one of the transition
         *            commands having a proper implementation class
         * @throws BadCommandException Some error occurred building the command object]
         * @throws IllegalArgumentException <em>fsmCommand</em> is any of {@link FSMCommands#USERBROADCAST},
         *             {@link FSMCommands#SUB_TRANSITION}, {@link FSMCommands#RESYNCH}         
         */
        protected TransitionCmd(final FSMCommands fsmCommand, final boolean check) throws BadCommandException, IllegalArgumentException {
            super(RCCommands.MAKE_TRANSITION);

            if(fsmCommand == null) {
                throw new IllegalArgumentException("The FSM command cannot be null");
            }

            // Update this when a new command is created for a specific transition
            // This is needed because those commands need additional mandatory parameters
            if((check == true)
               && ((fsmCommand == FSMCommands.USERBROADCAST) || (fsmCommand == FSMCommands.SUB_TRANSITION) || (fsmCommand == FSMCommands.RESYNCH)))
            {
                throw new IllegalArgumentException("The FSM command cannot be \"" + fsmCommand + "\"");
            }

            this.command = fsmCommand;

            final Map<String, String> map = new TreeMap<String, String>();
            map.put(TransitionCmd.FSM_COMMAND_TAG, fsmCommand.toString());
            map.put(TransitionCmd.FSM_SUBTR_DONE_TAG, Boolean.toString(false));
            map.put(TransitionCmd.FSM_IS_RESTART_TAG, Boolean.toString(false));
            map.put(TransitionCmd.FSM_SKIP_SUBTR_TAG, Boolean.toString(false));

            super.addParameters(new String[] {TransitionCmd.FSM_HEADER_TAG}, map);
        }

        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @throws BadCommandException The command description could not be decoded
         */
        private TransitionCmd(final String cmdString) throws BadCommandException {
            super(RCCommands.MAKE_TRANSITION, cmdString);

            try {
                final String command_ = super.getValue("/" + Command.TOP_HEADER + "/" + TransitionCmd.FSM_HEADER_TAG + "/"
                                                       + TransitionCmd.FSM_COMMAND_TAG);
                try {
                    this.command = FSMCommands.valueOf(command_);
                }
                catch(final IllegalArgumentException | NullPointerException ex) {
                    throw new BadCommandException(RCCommands.MAKE_TRANSITION.toString(),
                                                  "Found invalid FSM command: \"" + command_ + "\"",
                                                  ex);
                }
            }
            catch(final XPathExpressionException ex) {
                throw new BadCommandException(RCCommands.MAKE_TRANSITION.toString(), "The command could not be decoded", ex);
            }
        }

        /**
         * It returns the FSM command (i.e., transition)
         * 
         * @return The FSM command (i.e., transition)
         */
        public FSMCommands getFSMCommand() {
            return this.command;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = (prime * result) + ((this.command == null) ? 0 : this.command.hashCode());
            return result;
        }

        /**
         * Two {@link TransitionCmd} are equals if they represent the same {@link FSMCommands} command.
         */
        @Override
        public boolean equals(final Object obj) {
            if(this == obj) {
                return true;
            }
            if(!super.equals(obj)) {
                return false;
            }
            if(!(obj instanceof TransitionCmd)) {
                return false;
            }
            final TransitionCmd other = (TransitionCmd) obj;
            if(this.command != other.command) {
                return false;
            }
            return true;
        }
    }

    /**
     * Class encapsulating a command to ask a segment's controller to broadcast an user defined command
     * 
     * @see FSMCommands#USERBROADCAST
     */
    public static final class UserBroadCastCmd extends TransitionCmd {
        private final static String USR_BRDCST_USR_CMD_TAG = "_user_cmd_";

        private final UserCmd userCmd;

        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @throws BadCommandException The command description could not be decoded
         */
        private UserBroadCastCmd(final String cmdString) throws BadCommandException {
            super(cmdString);

            try {
                final Command cmd = super.getNestedCommand("/" + Command.TOP_HEADER + "/" + UserBroadCastCmd.USR_BRDCST_USR_CMD_TAG);
                final UserCmd usrCmd = UserCmd.class.cast(cmd);
                if(usrCmd != null) {
                    this.userCmd = usrCmd;
                } else {
                    throw new BadCommandException(FSMCommands.USERBROADCAST.toString(), "Failed to build the included user defined command");
                }
            }
            catch(final XPathExpressionException | TransformerException ex) {
                throw new BadCommandException(FSMCommands.USERBROADCAST.toString(), "The command could not be decoded", ex);
            }
        }

        /**
         * Constructor
         * 
         * @param cmd The user-defined command
         * @throws BadCommandException Some error occurred building the command object
         * @throws IllegalArgumentException If <em>cmd</em> is null
         */
        public UserBroadCastCmd(final UserCmd cmd) throws BadCommandException, IllegalArgumentException {
            super(FSMCommands.USERBROADCAST, false);

            if(cmd == null) {
                throw new IllegalArgumentException("The user-defined command cannot be null");
            }

            final UserCmd uc = new UserCmd(cmd.getCommandName(), cmd.getParameters());
            try {
                uc.isBroadcast(true);
            }
            catch(final XPathExpressionException ex) {
                throw new BadCommandException(FSMCommands.USERBROADCAST.toString(), "Error accessing the underlining user command", ex);
            }
            
            super.addCommand(new String[] {UserBroadCastCmd.USR_BRDCST_USR_CMD_TAG}, uc);

            this.userCmd = uc;
        }

        /**
         * It returns the user-defined command
         * 
         * @return The user-defined command
         */
        public UserCmd getUserCommand() {
            return this.userCmd;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = (prime * result) + ((this.userCmd == null) ? 0 : this.userCmd.hashCode());
            return result;
        }

        /**
         * Two {@link UserBroadCastCmd} commands are equal if they refer to the same {@link UserCmd}
         */
        @Override
        public boolean equals(final Object obj) {
            if(this == obj) {
                return true;
            }
            if(!super.equals(obj)) {
                return false;
            }
            if(!(obj instanceof UserBroadCastCmd)) {
                return false;
            }
            final UserBroadCastCmd other = (UserBroadCastCmd) obj;
            if(this.userCmd == null) {
                if(other.userCmd != null) {
                    return false;
                }
            } else if(!this.userCmd.equals(other.userCmd)) {
                return false;
            }
            return true;
        }
    }

    /**
     * Class encapsulating a command to ask a Run Control application to perform a sub-transition
     * 
     * @see FSMCommands#SUB_TRANSITION
     */
    public static final class SubTransitionCmd extends TransitionCmd {
        private final static String SUB_TRANSITION_NAME_TAG = "sub_transition_name";
        private final static String SUB_TRANSITION_MAIN_TR_TAG = "main_transition";

        private final String subTrName;
        private final FSMCommands fsmCmd;

        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @throws BadCommandException The command description could not be decoded
         */
        private SubTransitionCmd(final String cmdString) throws BadCommandException {
            super(cmdString);

            try {
                this.subTrName = super.getValue("/" + Command.TOP_HEADER + "/" + SubTransitionCmd.SUB_TRANSITION_NAME_TAG);
                if(this.subTrName.isEmpty() == true) {
                    throw new BadCommandException(FSMCommands.SUB_TRANSITION.toString(), "The name of the sub-transition is not defined");
                }

                final String mainTrCmd = super.getValue("/" + Command.TOP_HEADER + "/" + SubTransitionCmd.SUB_TRANSITION_MAIN_TR_TAG);
                try {
                    this.fsmCmd = FSMCommands.valueOf(mainTrCmd);
                }
                catch(final IllegalArgumentException | NullPointerException ex) {
                    throw new BadCommandException(FSMCommands.SUB_TRANSITION.toString(), "The main transition is invalid: \"" + mainTrCmd
                                                                                         + "\"", ex);
                }
            }
            catch(final XPathExpressionException ex) {
                throw new BadCommandException(FSMCommands.SUB_TRANSITION.toString(), "The command could not be decoded", ex);
            }
        }

        /**
         * Constructor
         * 
         * @param subTransitionName The name of the sub-transition (as defined in the configuration database)
         * @param mainTrCmd The name of the main transition the sub-transition refers to
         * @throws BadCommandException Some error occurred building the command object
         * @throws IllegalArgumentException <em>subTransitionName</em> is null or empty, or <em>mainTrCmd</em> is null
         */
        public SubTransitionCmd(final String subTransitionName, final FSMCommands mainTrCmd) throws BadCommandException, IllegalArgumentException {
            super(FSMCommands.SUB_TRANSITION, false);

            if((subTransitionName == null) || (subTransitionName.isEmpty() == true) || (mainTrCmd == null)) {
                throw new IllegalArgumentException("The name of the sub-transition and the main transition command must be valid and not null");
            }

            super.addParameter(new String[] {SubTransitionCmd.SUB_TRANSITION_NAME_TAG}, subTransitionName);
            super.addParameter(new String[] {SubTransitionCmd.SUB_TRANSITION_MAIN_TR_TAG}, mainTrCmd.toString());

            this.subTrName = subTransitionName;
            this.fsmCmd = mainTrCmd;
        }

        /**
         * It returns the name of the sub-transition
         * 
         * @return The name of the sub-transition
         */
        public String getSubTransitionName() {
            return this.subTrName;
        }

        /**
         * It returns the name of the main transition
         * 
         * @return The name of the main transition
         */
        public FSMCommands getMainTransitionCommand() {
            return this.fsmCmd;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = (prime * result) + ((this.fsmCmd == null) ? 0 : this.fsmCmd.hashCode());
            result = (prime * result) + ((this.subTrName == null) ? 0 : this.subTrName.hashCode());
            return result;
        }

        /**
         * Two {@link SubTransitionCmd} are equals if they represent the same sub-transition of the same sub-transition.
         */
        @Override
        public boolean equals(final Object obj) {
            if(this == obj) {
                return true;
            }
            if(!super.equals(obj)) {
                return false;
            }
            if(!(obj instanceof SubTransitionCmd)) {
                return false;
            }
            final SubTransitionCmd other = (SubTransitionCmd) obj;
            if(this.fsmCmd != other.fsmCmd) {
                return false;
            }
            if(this.subTrName == null) {
                if(other.subTrName != null) {
                    return false;
                }
            } else if(!this.subTrName.equals(other.subTrName)) {
                return false;
            }
            return true;
        }
    }

    /**
     * Class encapsulating a re-synchronization command
     * <p>
     * Note: for the <em>extendedL1ID</em>, only the first 32 least significative bits are meaningful.
     *       Indeed the extended L1 ID is a 32 bit word: 8 bits ECR + 24 bits EC.
     * 
     * @see FSMCommands#RESYNCH
     */
    public static final class ResynchCmd extends TransitionCmd {
        private final static String RESYNCH_ECR_TAG = "_resynch_ecr_";
        private final static String RESYNCH_EL1ID_TAG = "_resynch_el1id_";
        private final static String RESYNCH_MODULES_TAG = "_resync_modules_";

        private final long ecrCounts;
        private final long extendedL1ID;
        private final String[] modules;

        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @throws BadCommandException The command description could not be decoded
         */
        private ResynchCmd(final String cmdString) throws BadCommandException {
            super(cmdString);

            try {
                final String ecr_ = super.getValue("/" + Command.TOP_HEADER + "/" + ResynchCmd.RESYNCH_ECR_TAG);
                try {
                    this.ecrCounts = Long.parseLong(ecr_);
                }
                catch(final NumberFormatException ex) {
                    throw new BadCommandException(FSMCommands.RESYNCH.toString(),
                                                  "Found invalid number for ECR counts: \"" + ecr_ + "\"",
                                                  ex);
                }

                final String extL1ID_ = super.getValue("/" + Command.TOP_HEADER + "/" + ResynchCmd.RESYNCH_EL1ID_TAG);
                try {
                    this.extendedL1ID = Long.parseLong(extL1ID_);
                }
                catch(final NumberFormatException ex) {
                    throw new BadCommandException(FSMCommands.RESYNCH.toString(),
                                                  "Found invalid number for the extended L1 ID: \"" + extL1ID_ + "\"",
                                                  ex);
                }

                
                final List<String> mod_ = super.getValues("/" + Command.TOP_HEADER + "/" + ResynchCmd.RESYNCH_MODULES_TAG,
                                                          Command.VALUE_TAG);
                this.modules = mod_.toArray(new String[mod_.size()]);
            }
            catch(final XPathExpressionException ex) {
                throw new BadCommandException(FSMCommands.RESYNCH.toString(), "The command could not be decoded", ex);
            }
        }
        
        /**
         * Constructor
         * 
         * @param ecrNumber The ECR counts
         * @param extendedL1ID The last valid extended L1 ID
         * @throws BadCommandException Some error occurred building the command object
         */
        public ResynchCmd(final long ecrNumber, final long extendedL1ID) throws BadCommandException {
            this(ecrNumber, extendedL1ID, new String[] {});
        }
        
        /**
         * Constructor
         * 
         * @param ecrNumber The ECR counts
         * @param extendedL1ID The last valid extended L1 ID
         * @param modules Modules to be synchronized
         * @throws BadCommandException Some error occurred building the command object
         * @throws IllegalArgumentException <em>modules</em> cannot be null
         */
        public ResynchCmd(final long ecrNumber, final long extendedL1ID, final String[] modules) throws BadCommandException, IllegalArgumentException {
            super(FSMCommands.RESYNCH, false);

            if(modules == null) {
                throw new IllegalArgumentException("The array of modules cannot be null");
            }

            super.addParameter(new String[] {ResynchCmd.RESYNCH_ECR_TAG}, Long.toString(ecrNumber));
            super.addParameter(new String[] {ResynchCmd.RESYNCH_EL1ID_TAG}, Long.toString(extendedL1ID));
            super.addParameters(new String[] {ResynchCmd.RESYNCH_MODULES_TAG}, modules);

            this.ecrCounts = ecrNumber;
            this.extendedL1ID = extendedL1ID;
            this.modules = modules;
        }

        /**
         * It returns the last ECR counter
         * 
         * @return The last ECR counter
         */
        public long getECRCount() {
            return this.ecrCounts;
        }

        /**
         * It returns the last valid extended L1 ID
         * <p>
         * Note: Only the first 32 least significative bits are meaningful.
         *       Indeed the extended L1 ID is a 32 bit word: 8 bits ECR + 24 bits EC.
         * <p>
         * Note: A value of zero should be considered as invalid
         * 
         * @return The last valid extended L1 ID
         */
        public long getExtendedL1ID() {
            return this.extendedL1ID;
        }
        
        /**
         * It returns a list of modules to be resynchronized
         * 
         * @return A list of modules to be resynchronized
         */
        public String[] getModules() {
            return this.modules;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = prime * result + Arrays.hashCode(this.modules);
            result = prime * result + Objects.hash(Long.valueOf(this.ecrCounts), Long.valueOf(this.extendedL1ID));
            return result;
        }

        /**
         * Two {@link ResynchCmd} commands are the same if they refer to the same number of ECR counts, extended L1ID and list of modules
         */
        @Override
        public boolean equals(Object obj) {
            if(this == obj) {
                return true;
            }
            if(!super.equals(obj)) {
                return false;
            }
            if(this.getClass() != obj.getClass()) {
                return false;
            }
            final ResynchCmd other = (ResynchCmd) obj;
            return this.ecrCounts == other.ecrCounts && this.extendedL1ID == other.extendedL1ID && Arrays.equals(this.modules, other.modules);
        }
    }

    /**
     * Class encapsulating a command to ask a segment's controller to test the functionality of some child applications
     * 
     * @see RCCommands#TESTAPP
     */
    public static final class TestAppCmd extends Command {
        private final static String TESTAPP_CMD_APPS_TAG = "_test_app_names_";
        private final static String TESTAPP_CMD_SCOPE_TAG = "_test_scope_";
        private final static String TESTAPP_CMD_LEVEL_TAG = "_test_level_";

        private final String[] applications;
        private final Scope scope;
        private final int level;

        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @throws BadCommandException The command description could not be decoded
         */
        private TestAppCmd(final String cmdString) throws BadCommandException {
            super(RCCommands.TESTAPP, cmdString);

            // Need a map from 'toString()' to 'name()' so that the enum can be found
            // even if the two methods do not return the same string
            final Map<String, String> scopeMap = new HashMap<String, String>();
            for(final Scope s : Scope.values()) {
                scopeMap.put(s.toString(), s.name());
            }

            try {
                final String scope_ = super.getValue("/" + Command.TOP_HEADER + "/" + TestAppCmd.TESTAPP_CMD_SCOPE_TAG);
                try {
                    this.scope = Enum.valueOf(Scope.class, scopeMap.get(scope_));
                }
                catch(final NullPointerException | IllegalArgumentException ex) {
                    throw new BadCommandException(RCCommands.TESTAPP.toString(), "Found invald test scope: \"" + scope_ + "\"", ex);
                }

                final String level_ = super.getValue("/" + Command.TOP_HEADER + "/" + TestAppCmd.TESTAPP_CMD_LEVEL_TAG);
                try {
                    this.level = Integer.parseInt(level_);
                }
                catch(final NumberFormatException ex) {
                    throw new BadCommandException(RCCommands.TESTAPP.toString(), "Found invald test level: " + level_, ex);
                }

                final List<String> apps_ = super.getValues("/" + Command.TOP_HEADER + "/" + TestAppCmd.TESTAPP_CMD_APPS_TAG,
                                                           Command.VALUE_TAG);
                this.applications = apps_.toArray(new String[apps_.size()]);
            }
            catch(final XPathExpressionException ex) {
                throw new BadCommandException(RCCommands.TESTAPP.toString(), "The command could not be decoded", ex);
            }
        }

        /**
         * Constructor
         * 
         * @param applications The names of applications to be tested
         * @param scope The test's scope
         * @param level The test's level
         * @throws BadCommandException Some error occurred building the command object
         * @throws IllegalArgumentException <em>applications</em> or <em>scope</em> is null
         */
        public TestAppCmd(final String[] applications, final Scope scope, final int level) throws BadCommandException, IllegalArgumentException {
            super(RCCommands.TESTAPP);

            if((applications == null) || (scope == null)) {
                throw new IllegalArgumentException("The array of applications and the scope cannot be null");
            }

            super.addParameters(new String[] {TestAppCmd.TESTAPP_CMD_APPS_TAG}, applications);
            super.addParameter(new String[] {TestAppCmd.TESTAPP_CMD_LEVEL_TAG}, Integer.toString(level));
            super.addParameter(new String[] {TestAppCmd.TESTAPP_CMD_SCOPE_TAG}, scope.toString());

            this.applications = applications;
            this.scope = scope;
            this.level = level;
        }

        /**
         * Constructor
         * <p>
         * The test's level is set to 1 and the test's scope is set to {@link TestManager.Test.Scope#Any}.
         * 
         * @param applications The names of applications to be tested
         * @throws BadCommandException Some error occurred building the command object
         * @throws IllegalArgumentException <em>applications</em> is null
         */
        public TestAppCmd(final String[] applications) throws BadCommandException, IllegalArgumentException {
            this(applications, Scope.Any, 1);
        }

        /**
         * It returns the names of child applications to be tested
         * 
         * @return The names of child applications to be tested
         */
        public String[] getApplications() {
            return this.applications;
        }

        /**
         * It returns the test's scope
         * 
         * @return The test's scope
         */
        public Scope scope() {
            return this.scope;
        }

        /**
         * It returns the test's level
         * 
         * @return The test's level
         */
        public int level() {
            return this.level;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = (prime * result) + Arrays.hashCode(this.applications);
            result = (prime * result) + this.level;
            result = (prime * result) + ((this.scope == null) ? 0 : this.scope.hashCode());
            return result;
        }

        /**
         * Two {@link TestAppCmd} commands are equals if they have the same list of applications to be tested with both the same scope and
         * level.
         */
        @Override
        public boolean equals(final Object obj) {
            if(this == obj) {
                return true;
            }
            if(!super.equals(obj)) {
                return false;
            }
            if(!(obj instanceof TestAppCmd)) {
                return false;
            }
            final TestAppCmd other = (TestAppCmd) obj;
            if(!Arrays.equals(this.applications, other.applications)) {
                return false;
            }
            if(this.level != other.level) {
                return false;
            }
            if(this.scope != other.scope) {
                return false;
            }
            return true;
        }
    }

    /**
     * Class encapsulating a command to ask a segment's controller to dynamically restart some of its child controllers
     * 
     * @see RCCommands#DYN_RESTART
     */
    public static final class DynRestartCmd extends Command {
        private final static String DYNRESTART_CMD_SEGMENTS_TAG = "_ctrl_names_";

        private final String[] segments;

        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @throws BadCommandException The command description could not be decoded
         */
        private DynRestartCmd(final String cmdString) throws BadCommandException {
            super(RCCommands.DYN_RESTART, cmdString);

            if(super.supportsNotification() == true) {
                final String cmdName = RCCommands.DYN_RESTART.toString();
                throw new BadCommandException(cmdName,
                                              "Cannot create command \""
                                                  + cmdName
                                                  + "\": the command is configured to support asynchronous notification on completion but it does not");
            }

            try {
                final List<String> segs_ = super.getValues("/" + Command.TOP_HEADER + "/" + DynRestartCmd.DYNRESTART_CMD_SEGMENTS_TAG,
                                                           Command.VALUE_TAG);
                this.segments = segs_.toArray(new String[segs_.size()]);
            }
            catch(final XPathExpressionException ex) {
                throw new BadCommandException(RCCommands.DYN_RESTART.toString(), "The command could not be decoded", ex);
            }
        }

        /**
         * Constructor
         * 
         * @param segments The names of segments to be dynamically restarted
         * @throws BadCommandException Some error occurred building the command object
         * @throws IllegalArgumentException If <em>segments</em> is null
         */
        public DynRestartCmd(final String[] segments) throws BadCommandException, IllegalArgumentException {
            super(RCCommands.DYN_RESTART);

            if(segments == null) {
                throw new IllegalArgumentException("The array of segments cannot be null");
            }

            super.addParameters(new String[] {DynRestartCmd.DYNRESTART_CMD_SEGMENTS_TAG}, segments);

            try {
                super.replaceParameter("/" + Command.TOP_HEADER + "/" + Command.NOTIFY_SUPPORTED_TAG, Boolean.toString(false));
            }
            catch(final XPathExpressionException ex) {
                final String cmdName = RCCommands.DYN_RESTART.toString();
                throw new BadCommandException(cmdName, "Cannot create command \"" + cmdName + "\": " + ex.getMessage(), ex);
            }

            this.segments = segments;
        }

        /**
         * It returns the names of segments to be dynamically restarted
         * 
         * @return The names of segments to be dynamically restarted
         */
        public String[] getSegments() {
            return this.segments;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = (prime * result) + Arrays.hashCode(this.segments);
            return result;
        }

        /**
         * Two {@link DynRestartCmd} commands are equal if they refer to the same list of segments.
         */
        @Override
        public boolean equals(final Object obj) {
            if(this == obj) {
                return true;
            }
            if(!super.equals(obj)) {
                return false;
            }
            if(!(obj instanceof DynRestartCmd)) {
                return false;
            }
            final DynRestartCmd other = (DynRestartCmd) obj;
            if(!Arrays.equals(this.segments, other.segments)) {
                return false;
            }
            return true;
        }
    }

    /**
     * Class encapsulating a command to ask a segment's controller to put in/out of membership some child applications or a leaf application
     * to enable/disable some components
     * 
     * @see RCCommands#ENABLE
     * @see RCCommands#DISABLE
     */
    public static final class ChangeStatusCmd extends Command {
        private final static String CHANGESTATUS_CMD_CMPNTS_TAG = "_components_";
        private final static String CHANGESTATUS_CMD_ENABLE_DISABLE_TAG = "_is_enable_";

        private final boolean isIn;
        private final String[] components;

        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @throws BadCommandException The command description could not be decoded
         */
        private ChangeStatusCmd(final String cmdString) throws BadCommandException {
            super(cmdString);

            try {
                final String in = super.getValue("/" + Command.TOP_HEADER + "/" + ChangeStatusCmd.CHANGESTATUS_CMD_ENABLE_DISABLE_TAG);
                if(in.isEmpty() == true) {
                    throw new BadCommandException(RCCommands.ENABLE.toString(),
                                                  "The command could not be decoded: impossible to determine if it is an "
                                                      + RCCommands.ENABLE + " or " + RCCommands.DISABLE + " command");
                }

                this.isIn = Boolean.parseBoolean(in);

                final List<String> comps_ = super.getValues("/" + Command.TOP_HEADER + "/" + ChangeStatusCmd.CHANGESTATUS_CMD_CMPNTS_TAG,
                                                            Command.VALUE_TAG);
                this.components = comps_.toArray(new String[comps_.size()]);
            }
            catch(final XPathExpressionException ex) {
                throw new BadCommandException(RCCommands.ENABLE.toString(), "The command could not be decoded", ex);
            }
        }

        /**
         * Constructor
         * 
         * @param enable <em>false</em> if components have to be disabled or applications have to be put out of membership
         * @param components Components to enable/disable (or applications to be put in/out of membership)
         * @throws BadCommandException Some error occurred building the command object
         * @throws IllegalArgumentException If <em>components</em> is null
         */
        public ChangeStatusCmd(final boolean enable, final String[] components) throws BadCommandException, IllegalArgumentException {
            super(enable == true ? RCCommands.ENABLE : RCCommands.DISABLE);

            if(components == null) {
                throw new IllegalArgumentException("The array of components cannot be null");
            }

            super.addParameter(new String[] {ChangeStatusCmd.CHANGESTATUS_CMD_ENABLE_DISABLE_TAG}, Boolean.toString(enable));
            super.addParameters(new String[] {ChangeStatusCmd.CHANGESTATUS_CMD_CMPNTS_TAG}, components);

            this.isIn = enable;
            this.components = components;
        }

        /**
         * It returns <em>true</em> if this command is meant to enable or disable components
         * 
         * @return <em>true</em> if this command is meant to enable or disable components
         */
        public boolean status() {
            return this.isIn;
        }

        /**
         * It returns the components to be enabled or disabled
         * 
         * @return The components to be enabled or disabled
         */
        public String[] getComponents() {
            return this.components;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = (prime * result) + Arrays.hashCode(this.components);
            result = (prime * result) + (this.isIn ? 1231 : 1237);
            return result;
        }

        /**
         * Two {@link ChangeStatusCmd} commands are equal if both refer to the same enable/disable action for the same list of components.
         */
        @Override
        public boolean equals(final Object obj) {
            if(this == obj) {
                return true;
            }
            if(!super.equals(obj)) {
                return false;
            }
            if(!(obj instanceof ChangeStatusCmd)) {
                return false;
            }
            final ChangeStatusCmd other = (ChangeStatusCmd) obj;
            if(!Arrays.equals(this.components, other.components)) {
                return false;
            }
            if(this.isIn != other.isIn) {
                return false;
            }
            return true;
        }
    }

    // Not public
    static final class ApplicationStatusCmd extends Command {
        private final static String APP_NAME_TAG = "_app_name_";
        private final static String APP_STATE_TAG = "_app_state_";
        private final static String APP_IS_TR_TAG = "_app_is_transitioning_";
        private final static String APP_ERROR_TAG = "_app_error_reasons_";
        private final static String APP_BAD_CHLD_TAG = "_app_bad_children_";
        private final static String APP_IS_BUSY_TAG = "_app_is_busy_";
        private final static String APP_IS_FULLINIT_TAG = "_app_is_fullinit_";
        private final static String APP_LAST_TR_CMD_TAG = "_app_last_transition_cmd_";
        private final static String APP_CURR_TR_CMD_TAG = "_app_current_transition_cmd";
        private final static String APP_LAST_TR_TIME_TAG = "_app_last_transition_time_";
        private final static String APP_LAST_CMD_TAG = "_app_last_cmd_";

        private final String appName;
        private final ApplicationState appState;
        private final boolean isTransitioning;
        private final List<String> errorReasons;
        private final List<String> badChildren;
        private final boolean isBusy;
        private final boolean isFullyInitialized;
        private final long lastTransitionDuration;
        private final TransitionCmd lastTransitionCmd;
        private final TransitionCmd currentTransitionCmd;
        private final Command lastCmd;
        
        ApplicationStatusCmd(final String cmdString) throws BadCommandException {
            super(RCCommands.UPDATE_CHILD_STATUS, cmdString);
            
            try {
                this.appName = super.getValue("/" + Command.TOP_HEADER + "/" + ApplicationStatusCmd.APP_NAME_TAG);
                this.appState = ApplicationState.valueOf(super.getValue("/" + Command.TOP_HEADER + "/" + ApplicationStatusCmd.APP_STATE_TAG));
                this.isTransitioning = Boolean.parseBoolean(super.getValue("/" + Command.TOP_HEADER + "/" + ApplicationStatusCmd.APP_IS_TR_TAG));
                this.errorReasons = super.getValues("/" + Command.TOP_HEADER + "/" + ApplicationStatusCmd.APP_ERROR_TAG, Command.VALUE_TAG);
                this.badChildren = super.getValues("/" + Command.TOP_HEADER + "/" + ApplicationStatusCmd.APP_BAD_CHLD_TAG, Command.VALUE_TAG);
                this.isBusy = Boolean.parseBoolean(super.getValue("/" + Command.TOP_HEADER + "/" + ApplicationStatusCmd.APP_IS_BUSY_TAG));
                this.isFullyInitialized = Boolean.parseBoolean(super.getValue("/" + Command.TOP_HEADER + "/" + ApplicationStatusCmd.APP_IS_FULLINIT_TAG));
                this.lastTransitionDuration = Long.parseLong(super.getValue("/" + Command.TOP_HEADER + "/" + ApplicationStatusCmd.APP_LAST_TR_TIME_TAG));               
                this.lastTransitionCmd = TransitionCmd.class.cast(super.getNestedCommand("/" + Command.TOP_HEADER + "/" + ApplicationStatusCmd.APP_LAST_TR_CMD_TAG));
                this.currentTransitionCmd = TransitionCmd.class.cast(super.getNestedCommand("/" + Command.TOP_HEADER + "/" + ApplicationStatusCmd.APP_CURR_TR_CMD_TAG));
                this.lastCmd = super.getNestedCommand("/" + Command.TOP_HEADER + "/" + ApplicationStatusCmd.APP_LAST_CMD_TAG);
            }
            catch(final XPathExpressionException | TransformerException ex) {
                throw new BadCommandException(RCCommands.UPDATE_CHILD_STATUS.toString(), "The command could not be decoded", ex);
            }
        }

        public String getAppName() {
            return this.appName;
        }

        public ApplicationState getAppState() {
            return this.appState;
        }

        public boolean isTransitioning() {
            return this.isTransitioning;
        }

        public List<String> getErrorReasons() {
            return this.errorReasons;
        }

        public List<String> getBadChildren() {
            return this.badChildren;
        }

        public boolean isBusy() {
            return this.isBusy;
        }

        public boolean isFullyInitialized() {
            return this.isFullyInitialized;
        }

        public long getLastTransitionDuration() {
            return this.lastTransitionDuration;
        }

        public TransitionCmd getLastTransitionCmd() {
            return this.lastTransitionCmd;
        }

        public TransitionCmd getCurrentTransitionCmd() {
            return this.currentTransitionCmd;
        }

        public Command getLastCmd() {
            return this.lastCmd;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = (prime * result) + ((this.appName == null) ? 0 : this.appName.hashCode());
            result = (prime * result) + ((this.appState == null) ? 0 : this.appState.hashCode());
            result = (prime * result) + ((this.badChildren == null) ? 0 : this.badChildren.hashCode());
            result = (prime * result) + ((this.currentTransitionCmd == null) ? 0 : this.currentTransitionCmd.hashCode());
            result = (prime * result) + ((this.errorReasons == null) ? 0 : this.errorReasons.hashCode());
            result = (prime * result) + (this.isBusy ? 1231 : 1237);
            result = (prime * result) + (this.isFullyInitialized ? 1231 : 1237);
            result = (prime * result) + (this.isTransitioning ? 1231 : 1237);
            result = (prime * result) + ((this.lastCmd == null) ? 0 : this.lastCmd.hashCode());
            result = (prime * result) + ((this.lastTransitionCmd == null) ? 0 : this.lastTransitionCmd.hashCode());
            result = (prime * result) + (int) (this.lastTransitionDuration ^ (this.lastTransitionDuration >>> 32));
            return result;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(final Object obj) {
            if(this == obj) {
                return true;
            }
            if(!super.equals(obj)) {
                return false;
            }
            if(this.getClass() != obj.getClass()) {
                return false;
            }
            final ApplicationStatusCmd other = (ApplicationStatusCmd) obj;
            if(this.appName == null) {
                if(other.appName != null) {
                    return false;
                }
            } else if(!this.appName.equals(other.appName)) {
                return false;
            }
            if(this.appState != other.appState) {
                return false;
            }
            if(this.badChildren == null) {
                if(other.badChildren != null) {
                    return false;
                }
            } else if(!this.badChildren.equals(other.badChildren)) {
                return false;
            }
            if(this.currentTransitionCmd == null) {
                if(other.currentTransitionCmd != null) {
                    return false;
                }
            } else if(!this.currentTransitionCmd.equals(other.currentTransitionCmd)) {
                return false;
            }
            if(this.errorReasons == null) {
                if(other.errorReasons != null) {
                    return false;
                }
            } else if(!this.errorReasons.equals(other.errorReasons)) {
                return false;
            }
            if(this.isBusy != other.isBusy) {
                return false;
            }
            if(this.isFullyInitialized != other.isFullyInitialized) {
                return false;
            }
            if(this.isTransitioning != other.isTransitioning) {
                return false;
            }
            if(this.lastCmd == null) {
                if(other.lastCmd != null) {
                    return false;
                }
            } else if(!this.lastCmd.equals(other.lastCmd)) {
                return false;
            }
            if(this.lastTransitionCmd == null) {
                if(other.lastTransitionCmd != null) {
                    return false;
                }
            } else if(!this.lastTransitionCmd.equals(other.lastTransitionCmd)) {
                return false;
            }
            if(this.lastTransitionDuration != other.lastTransitionDuration) {
                return false;
            }
            return true;
        }
    }
    
    /**
     * Class encapsulating a command to ask a segment's controller to start some child applications
     * 
     * @see RCCommands#STARTAPP
     */
    public static final class StartAppsCmd extends Command {
        private final static String APP_TAG = "_applications_";
        private final static String IS_START_TAG = "_is_start_";
        private final static String IS_RESTART_TAG = "_is_restart_";

        private final String[] apps;

        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @throws BadCommandException The command description could not be decoded
         */
        private StartAppsCmd(final String cmdString) throws BadCommandException {
            super(RCCommands.STARTAPP, cmdString);

            try {
                final List<String> apps_ = super.getValues("/" + Command.TOP_HEADER + "/" + StartAppsCmd.APP_TAG, Command.VALUE_TAG);
                this.apps = apps_.toArray(new String[apps_.size()]);
            }
            catch(final XPathExpressionException ex) {
                throw new BadCommandException(RCCommands.STARTAPP.toString(), "The command could not be decoded", ex);
            }
        }

        /**
         * Constructor
         * 
         * @param applications The names of the applications to be started
         * @throws BadCommandException Some error occurred building the command object
         * @throws IllegalArgumentException If <em>applications</em> is null
         */
        public StartAppsCmd(final String[] applications) throws BadCommandException, IllegalArgumentException {
            super(RCCommands.STARTAPP);

            if(applications == null) {
                throw new IllegalArgumentException("The array of applications cannot be null");
            }

            super.addParameter(new String[] {StartAppsCmd.IS_START_TAG}, Boolean.toString(true));
            super.addParameter(new String[] {StartAppsCmd.IS_RESTART_TAG}, Boolean.toString(false));
            super.addParameters(new String[] {StartAppsCmd.APP_TAG}, applications);

            this.apps = applications;
        }

        /**
         * It returns the list of applications to be started
         * 
         * @return The list of applications to be started
         */
        public String[] getApplications() {
            return this.apps;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = (prime * result) + Arrays.hashCode(this.apps);
            return result;
        }

        /**
         * Two {@link StartAppsCmd} commands are equal if they refer to the same list of applications.
         */
        @Override
        public boolean equals(final Object obj) {
            if(this == obj) {
                return true;
            }
            if(!super.equals(obj)) {
                return false;
            }
            if(!(obj instanceof StartAppsCmd)) {
                return false;
            }
            final StartAppsCmd other = (StartAppsCmd) obj;
            if(!Arrays.equals(this.apps, other.apps)) {
                return false;
            }
            return true;
        }
    }

    /**
     * Class encapsulating a command to ask a segment's controller to stop some child applications
     * 
     * @see RCCommands#STOPAPP
     */
    public static final class StopAppsCmd extends Command {
        private final static String APP_TAG = "_applications_";
        private final static String IS_START_TAG = "_is_start_";
        private final static String IS_RESTART_TAG = "_is_restart_";

        private final String[] apps;

        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @throws BadCommandException The command description could not be decoded
         */
        private StopAppsCmd(final String cmdString) throws BadCommandException {
            super(RCCommands.STOPAPP, cmdString);

            try {
                final List<String> apps_ = super.getValues("/" + Command.TOP_HEADER + "/" + StartAppsCmd.APP_TAG, Command.VALUE_TAG);
                this.apps = apps_.toArray(new String[apps_.size()]);
            }
            catch(final XPathExpressionException ex) {
                throw new BadCommandException(RCCommands.STOPAPP.toString(), "The command could not be decoded", ex);
            }
        }

        /**
         * Constructor
         * 
         * @param applications List of applications to be stopped
         * @throws BadCommandException Some errors occurred building the command object
         * @throws IllegalArgumentException If <em>applications</em> is null
         */
        public StopAppsCmd(final String[] applications) throws BadCommandException, IllegalArgumentException {
            super(RCCommands.STOPAPP);

            if(applications == null) {
                throw new IllegalArgumentException("The array of applications cannot be null");
            }

            super.addParameter(new String[] {StopAppsCmd.IS_START_TAG}, Boolean.toString(false));
            super.addParameter(new String[] {StopAppsCmd.IS_RESTART_TAG}, Boolean.toString(false));
            super.addParameters(new String[] {StopAppsCmd.APP_TAG}, applications);

            this.apps = applications;
        }

        /**
         * It returns the list of applications to be stopped
         * 
         * @return The list of applications to be stopped
         */
        public String[] getApplications() {
            return this.apps;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = (prime * result) + Arrays.hashCode(this.apps);
            return result;
        }

        /**
         * Two {@link StopAppsCmd} commands are equal if they refer to the same list of applications.
         */
        @Override
        public boolean equals(final Object obj) {
            if(this == obj) {
                return true;
            }
            if(!super.equals(obj)) {
                return false;
            }
            if(!(obj instanceof StopAppsCmd)) {
                return false;
            }
            final StopAppsCmd other = (StopAppsCmd) obj;
            if(!Arrays.equals(this.apps, other.apps)) {
                return false;
            }
            return true;
        }
    }

    /**
     * Class encapsulating a command to ask a segment's controller to restart some child applications
     * 
     * @see RCCommands#RESTARTAPP
     */
    public static final class RestartAppsCmd extends Command {
        private final static String APP_TAG = "_applications_";
        private final static String IS_START_TAG = "_is_start_";
        private final static String IS_RESTART_TAG = "_is_restart_";

        private final String[] apps;

        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @throws BadCommandException The command description could not be decoded
         */
        private RestartAppsCmd(final String cmdString) throws BadCommandException {
            super(RCCommands.RESTARTAPP, cmdString);

            try {
                final List<String> apps_ = super.getValues("/" + Command.TOP_HEADER + "/" + StartAppsCmd.APP_TAG, Command.VALUE_TAG);
                this.apps = apps_.toArray(new String[apps_.size()]);
            }
            catch(final XPathExpressionException ex) {
                throw new BadCommandException(RCCommands.RESTARTAPP.toString(), "The command could not be decoded", ex);
            }
        }

        /**
         * Constructor
         * 
         * @param applications List of applications to be restarted
         * @throws BadCommandException Some error occurred building the command object
         * @throws IllegalArgumentException If <em>applications</em> is null
         */
        public RestartAppsCmd(final String[] applications) throws BadCommandException, IllegalArgumentException {
            super(RCCommands.RESTARTAPP);

            if(applications == null) {
                throw new IllegalArgumentException("The array of applications cannot be null");
            }

            super.addParameter(new String[] {RestartAppsCmd.IS_START_TAG}, Boolean.toString(false));
            super.addParameter(new String[] {RestartAppsCmd.IS_RESTART_TAG}, Boolean.toString(true));
            super.addParameters(new String[] {RestartAppsCmd.APP_TAG}, applications);

            this.apps = applications;
        }

        /**
         * It returns the list of applications to be restarted
         * 
         * @return The list of applications to be restarted
         */
        public String[] getApplications() {
            return this.apps;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = (prime * result) + Arrays.hashCode(this.apps);
            return result;
        }

        /**
         * Two {@link RestartAppsCmd} commands are equal if they refer to the same list of applications.
         */
        @Override
        public boolean equals(final Object obj) {
            if(this == obj) {
                return true;
            }
            if(!super.equals(obj)) {
                return false;
            }
            if(!(obj instanceof RestartAppsCmd)) {
                return false;
            }
            final RestartAppsCmd other = (RestartAppsCmd) obj;
            if(!Arrays.equals(this.apps, other.apps)) {
                return false;
            }
            return true;
        }
    }

    /**
     * Class encapsulating an user defined command.
     * 
     * @see RCCommands#USER
     */
    public static final class UserCmd extends Command {
        private final static String USR_CMD_NAME_TAG = "_user_cmd_name_";
        private final static String USR_CMD_PARAMS_TAG = "_user_cmd_parameters_";
        private final static String USR_CMD_IS_BROADCAST_TAG = "_user_cmd_brdct_";

        private final String cmdName;
        private final String[] params;
        private final AtomicBoolean isBroadcast = new AtomicBoolean();

        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @param placeholder Dummy argument used only to identify this constructor
         * @throws BadCommandException The command description could not be decoded
         */
        private UserCmd(final String cmdString, final Void placeholder) throws BadCommandException {
            super(RCCommands.USER, cmdString);

            try {
                this.cmdName = super.getValue("/" + Command.TOP_HEADER + "/" + UserCmd.USR_CMD_NAME_TAG);
                if(this.cmdName.isEmpty() == true) {
                    throw new BadCommandException(RCCommands.USER.toString(), "The user command name is empty");
                }

                final List<String> params_ = super.getValues("/" + Command.TOP_HEADER + "/" + UserCmd.USR_CMD_PARAMS_TAG, Command.VALUE_TAG);
                this.params = params_.toArray(new String[params_.size()]);
                
                this.isBroadcast.set(Boolean.valueOf(super.getValue("/" + Command.TOP_HEADER + "/" + UserCmd.USR_CMD_IS_BROADCAST_TAG)).booleanValue());
            }
            catch(final XPathExpressionException ex) {
                throw new BadCommandException(RCCommands.USER.toString(), "The command could not be decoded", ex);
            }
        }

        /**
         * Constructor
         * 
         * @param cmdName The name of the user command
         * @param parameters The parameters of the user command
         * @throws BadCommandException Some error occurred building the command object
         * @throws IllegalArgumentException If <em>cmdName</em> is null or empty, or <em>parameters</em> is null
         */
        public UserCmd(final String cmdName, final String[] parameters) throws BadCommandException, IllegalArgumentException {
            super(RCCommands.USER);

            if((cmdName == null) || (cmdName.isEmpty() == true) || (parameters == null)) {
                throw new IllegalArgumentException("The name of the command and the array of parameters must be valid and not null");
            }

            super.addParameter(new String[] {UserCmd.USR_CMD_IS_BROADCAST_TAG}, Boolean.FALSE.toString());
            super.addParameter(new String[] {UserCmd.USR_CMD_NAME_TAG}, cmdName);
            super.addParameters(new String[] {UserCmd.USR_CMD_PARAMS_TAG}, parameters);

            this.cmdName = cmdName;
            this.params = parameters;
            this.isBroadcast.set(false);
        }

        /**
         * Constructor
         * <p>
         * Using this constructor implies having an user-defined command without any parameter
         * 
         * @param cmdName The name of the user command
         * @throws BadCommandException Some error occurred building the command object
         */
        public UserCmd(final String cmdName) throws BadCommandException {
            this(cmdName, new String[] {});
        }

        /**
         * It returns the name of the user-defined command
         * 
         * @return The name of the user-defined command
         */
        public String getCommandName() {
            return this.cmdName;
        }

        /**
         * It returns the parameters of the user-defined command
         * 
         * @return The parameters of the user-defined command
         */
        public String[] getParameters() {
            return this.params;
        }

        /**
         * It returns whether the received user command comes from a wider broadcast and it is not directly sent
         * to the application.
         * 
         * @return <em>true</em> if the command comes from a wider broadcast
         */
        public boolean isBroadcast() {
            return this.isBroadcast.get();
        }
        
        /**
         * It sets whether the received user command comes from a wider broadcast and it is not directly sent
         * to the application.
         * 
         * @param value <em>true</em> if the command comes from a wider broadcast
         * @throws XPathExpressionException Error updating the command property
         */
        void isBroadcast(final boolean value) throws XPathExpressionException {
            this.replaceParameter("/" + Command.TOP_HEADER + "/" + UserCmd.USR_CMD_IS_BROADCAST_TAG, Boolean.valueOf(value).toString());
            this.isBroadcast.set(value);
        }
        
        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = (prime * result) + ((this.cmdName == null) ? 0 : this.cmdName.hashCode());
            result = (prime * result) + Arrays.hashCode(this.params);
            return result;
        }

        /**
         * Two {@link UserCmd} commands are equal if they have the same name and the same list of options
         */
        @Override
        public boolean equals(final Object obj) {
            if(this == obj) {
                return true;
            }
            if(!super.equals(obj)) {
                return false;
            }
            if(!(obj instanceof UserCmd)) {
                return false;
            }
            final UserCmd other = (UserCmd) obj;
            if(this.cmdName == null) {
                if(other.cmdName != null) {
                    return false;
                }
            } else if(!this.cmdName.equals(other.cmdName)) {
                return false;
            }
            if(!Arrays.equals(this.params, other.params)) {
                return false;
            }
            return true;
        }
    }

    /**
     * Class encapsulating a command to ask a segment's controller to gracefully exit
     * 
     * @see RCCommands#EXIT
     */
    public static final class ExitCmd extends Command {
        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @throws BadCommandException The command description could not be decoded
         */
        private ExitCmd(final String cmdString) throws BadCommandException {
            super(RCCommands.EXIT, cmdString);

            if(super.supportsNotification() == true) {
                final String cmdName = RCCommands.EXIT.toString();
                throw new BadCommandException(cmdName,
                                              "Cannot create command \""
                                                  + cmdName
                                                  + "\": the command is configured to support asynchronous notification on completion but it does not");
            }
        }

        /**
         * Constructor
         * 
         * @throws BadCommandException Some error occurred building the command object
         */
        public ExitCmd() throws BadCommandException {
            super(RCCommands.EXIT);

            try {
                super.replaceParameter("/" + Command.TOP_HEADER + "/" + Command.NOTIFY_SUPPORTED_TAG, Boolean.toString(false));
            }
            catch(final XPathExpressionException ex) {
                final String cmdName = RCCommands.EXIT.toString();
                throw new BadCommandException(cmdName, "Cannot create command \"" + cmdName + "\": " + ex.getMessage(), ex);
            }
        }
    }

    /**
     * Class encapsulating a command to ask a segment's controller to ignore any error condition (i.e., to put out of membership any
     * applications causing the controller to be in an error state)
     * 
     * @see RCCommands#IGNORE_ERROR
     */
    public static final class IgnoreErrorCmd extends Command {
        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @throws BadCommandException The command description could not be decoded
         */
        private IgnoreErrorCmd(final String cmdString) throws BadCommandException {
            super(RCCommands.IGNORE_ERROR, cmdString);

            if(super.supportsNotification() == true) {
                final String cmdName = RCCommands.IGNORE_ERROR.toString();
                throw new BadCommandException(cmdName,
                                              "Cannot create command \""
                                                  + cmdName
                                                  + "\": the command is configured to support asynchronous notification on completion but it does not");
            }
        }

        /**
         * Constructor
         * 
         * @throws BadCommandException Some error occurred building the command object
         */
        public IgnoreErrorCmd() throws BadCommandException {
            super(RCCommands.IGNORE_ERROR);

            try {
                super.replaceParameter("/" + Command.TOP_HEADER + "/" + Command.NOTIFY_SUPPORTED_TAG, Boolean.toString(false));
            }
            catch(final XPathExpressionException ex) {
                final String cmdName = RCCommands.IGNORE_ERROR.toString();
                throw new BadCommandException(cmdName, "Cannot create command \"" + cmdName + "\": " + ex.getMessage(), ex);
            }
        }
    }

    /**
     * Class encapsulating a command to ask an application to publish information about its status
     * 
     * @see RCCommands#PUBLISH
     */
    public static final class PublishCmd extends Command {
        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @throws BadCommandException The command description could not be decoded
         */
        private PublishCmd(final String cmdString) throws BadCommandException {
            super(RCCommands.PUBLISH, cmdString);
        }

        /**
         * Constructor
         * 
         * @throws BadCommandException Some error occurred building the command object
         */
        public PublishCmd() throws BadCommandException {
            super(RCCommands.PUBLISH);
        }
    }

    /**
     * Class encapsulating a command to ask an application to change the interval between two information status publications
     * 
     * @see RCCommands#CHANGE_PROBE_INTERVAL
     */
    public static final class ChangePublishIntervalCmd extends Command {
        static private final String CHANGE_PUBLISH_INTERVAL_TAG = "_probe_interval_";

        private final int newInterval;

        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @throws BadCommandException The command description could not be decoded
         */
        private ChangePublishIntervalCmd(final String cmdString) throws BadCommandException {
            super(RCCommands.CHANGE_PROBE_INTERVAL, cmdString);

            String interval_ = "";
            try {
                interval_ = super.getValue("/" + Command.TOP_HEADER + "/" + ChangePublishIntervalCmd.CHANGE_PUBLISH_INTERVAL_TAG);
                this.newInterval = Integer.parseInt(interval_);
            }
            catch(final NumberFormatException ex) {
                throw new BadCommandException(RCCommands.CHANGE_PROBE_INTERVAL.toString(),
                                              "Found invalid publish interval: " + interval_,
                                              ex);
            }
            catch(final XPathExpressionException ex) {
                throw new BadCommandException(RCCommands.CHANGE_PROBE_INTERVAL.toString(), "The command could not be decoded", ex);
            }
        }

        /**
         * Constructor
         * 
         * @param seconds The new time interval in seconds (a value of zero disables the regular publishing)
         * @throws BadCommandException Some error occurred building the command object
         * @throws IllegalArgumentException The value of <em>seconds</em> is less than zero
         */
        public ChangePublishIntervalCmd(final int seconds) throws BadCommandException, IllegalArgumentException {
            super(RCCommands.CHANGE_PROBE_INTERVAL);

            if(seconds < 0) {
                throw new IllegalArgumentException("The publish interval must be greater than zero");
            }

            super.addParameter(new String[] {ChangePublishIntervalCmd.CHANGE_PUBLISH_INTERVAL_TAG}, Integer.toString(seconds));

            this.newInterval = seconds;
        }

        /**
         * It returns the new publication interval
         * 
         * @return The new publication interval
         */
        public int getNewInterval() {
            return this.newInterval;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = (prime * result) + this.newInterval;
            return result;
        }

        /**
         * Two {@link ChangePublishIntervalCmd} commands are equal if they have the same new interval
         */
        @Override
        public boolean equals(final Object obj) {
            if(this == obj) {
                return true;
            }
            if(!super.equals(obj)) {
                return false;
            }
            if(!(obj instanceof ChangePublishIntervalCmd)) {
                return false;
            }
            final ChangePublishIntervalCmd other = (ChangePublishIntervalCmd) obj;
            if(this.newInterval != other.newInterval) {
                return false;
            }
            return true;
        }
    }

    /**
     * Class encapsulating a command to ask a Run Control application to publish its statistics
     * 
     * @see RCCommands#PUBLISHSTATS
     */
    public static final class PublishFullStatsCmd extends Command {
        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @throws BadCommandException The command description could not be decoded
         */
        private PublishFullStatsCmd(final String cmdString) throws BadCommandException {
            super(RCCommands.PUBLISHSTATS, cmdString);
        }

        /**
         * Constructor
         * 
         * @throws BadCommandException Some error occurred building the command object
         */
        public PublishFullStatsCmd() throws BadCommandException {
            super(RCCommands.PUBLISHSTATS);
        }
    }

    /**
     * Class encapsulating a command to ask an application to change the interval between two statistics publications
     * 
     * @see RCCommands#CHANGE_FULLSTATS_INTERVAL
     */
    public static final class ChangeFullStatsIntervalCmd extends Command {
        private final static String CHANGE_FULLSTAT_INTERVAL_TAG = "_fullstat_interval_";

        private final int newInterval;

        /**
         * Constructor.
         * <p>
         * To be used in order to build a command starting from its (string) description.
         * 
         * @param cmdString Encoded string describing the command and all its properties
         * @throws BadCommandException The command description could not be decoded
         */
        private ChangeFullStatsIntervalCmd(final String cmdString) throws BadCommandException {
            super(RCCommands.CHANGE_FULLSTATS_INTERVAL, cmdString);

            String interval_ = "";
            try {
                interval_ = super.getValue("/" + Command.TOP_HEADER + "/" + ChangeFullStatsIntervalCmd.CHANGE_FULLSTAT_INTERVAL_TAG);
                this.newInterval = Integer.parseInt(interval_);
            }
            catch(final NumberFormatException ex) {
                throw new BadCommandException(RCCommands.CHANGE_FULLSTATS_INTERVAL.toString(), "Found invalid publish interval: "
                                                                                               + interval_, ex);
            }
            catch(final XPathExpressionException ex) {
                throw new BadCommandException(RCCommands.CHANGE_FULLSTATS_INTERVAL.toString(), "The command could not be decoded", ex);
            }
        }

        /**
         * Constructor
         * 
         * @param seconds The new time interval in seconds (a value of zero disables the regular publishing)
         * @throws BadCommandException Some error occurred building the command object or the value of <em>seconds</em> is less than zero
         */
        public ChangeFullStatsIntervalCmd(final int seconds) throws BadCommandException {
            super(RCCommands.CHANGE_FULLSTATS_INTERVAL);

            if(seconds < 0) {
                throw new BadCommandException(RCCommands.CHANGE_FULLSTATS_INTERVAL.toString(),
                                              "The publish interval must be greater than zero");
            }

            super.addParameter(new String[] {ChangeFullStatsIntervalCmd.CHANGE_FULLSTAT_INTERVAL_TAG}, Integer.toString(seconds));

            this.newInterval = seconds;
        }

        public int getNewInterval() {
            return this.newInterval;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = (prime * result) + this.newInterval;
            return result;
        }

        /**
         * Two {@link ChangeFullStatsIntervalCmd} commands are equal if they have the same new interval
         */
        @Override
        public boolean equals(final Object obj) {
            if(this == obj) {
                return true;
            }
            if(!super.equals(obj)) {
                return false;
            }
            if(!(obj instanceof ChangeFullStatsIntervalCmd)) {
                return false;
            }
            final ChangeFullStatsIntervalCmd other = (ChangeFullStatsIntervalCmd) obj;
            if(this.newInterval != other.newInterval) {
                return false;
            }
            return true;
        }
    }

    /**
     * Constructor.
     * <p>
     * Sub-classes should use this constructor only when a command has to be build from its string (XML) representation.
     * <p>
     * The {@link Command#Command(RCCommands, String)} constructor should be used when the command is known in advance.
     * 
     * @param cmdString The string (XML) representation of the command
     * @throws BadCommandException Some error occurred parsing the command description
     */
    private Command(final String cmdString) throws BadCommandException {
        this(Command.parseCommand(cmdString), cmdString);
    }

    /**
     * Constructor.
     * <p>
     * Sub-classes should use this constructor only when a command has to be build from its string (XML) representation.
     * 
     * @param command The command to build
     * @param cmdString The string (XML) representation of the command
     * @throws BadCommandException Some error occurred parsing the command description
     */
    private Command(final RCCommands command, final String cmdString) throws BadCommandException {
        // Get the command
        this.command = command;

        // Get the UUID
        try (final StringReader sr = new StringReader(cmdString)) {
            this.xmlDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(sr));

            this.uuid = this.getValue("/" + Command.TOP_HEADER + "/" + Command.CMD_ID_TAG);
            if(this.uuid.isEmpty() == true) {
                throw new BadCommandException(this.command.toString(), "Cannot find the command's UUID");
            }
        }
        catch(final XPathExpressionException | SAXException | IOException | ParserConfigurationException ex) {
            throw new BadCommandException(this.command.toString(), "Failed to parse the command string", ex);
        }
    }

    /**
     * Constructor
     * 
     * @param command The command to be sent
     * @throws BadCommandException Some error occurred building the command object
     */
    private Command(final RCCommands command) throws BadCommandException {
        this.command = command;

        try {
            this.xmlDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

            // Root
            final Element rootElement = this.xmlDoc.createElement(Command.TOP_HEADER);
            this.xmlDoc.appendChild(rootElement);

            // The command name
            final Element cmdNameNode = this.xmlDoc.createElement(Command.CMD_NAME_TAG);
            cmdNameNode.appendChild(this.xmlDoc.createTextNode(command.toString()));
            rootElement.appendChild(cmdNameNode);

            // The command UID
            this.uuid = UUID.randomUUID().toString();

            final Element cmdUIDlement = this.xmlDoc.createElement(Command.CMD_ID_TAG);
            cmdUIDlement.appendChild(this.xmlDoc.createTextNode(this.uuid));
            rootElement.appendChild(cmdUIDlement);

            // Flag for remote notification
            final Element remNotElement = this.xmlDoc.createElement(Command.NOTIFY_SUPPORTED_TAG);
            remNotElement.appendChild(this.xmlDoc.createTextNode(Boolean.toString(true)));
            rootElement.appendChild(remNotElement);
        }
        catch(final ParserConfigurationException ex) {
            final String cmdName = command.toString();
            throw new BadCommandException(cmdName, "Cannot create command \"" + cmdName + "\": " + ex.getMessage(), ex);
        }
    }

    /**
     * It builds a full command object from its (string) representation.
     * <p>
     * If needed the returned Command object can be casted to the proper run-time type.
     * 
     * @param cmdString The string encoding the description of the command and of all its properties
     * @return A Command object with proper run-time type
     * @throws BadCommandException Some error occurred building the command
     * @throws IllegalArgumentException If <em>cmdString</em> is empty or null
     */
    public static Command buildCommand(final String cmdString) throws BadCommandException, IllegalArgumentException {
        if((cmdString == null) || (cmdString.isEmpty() == true)) {
            throw new IllegalArgumentException("The string describing the command cannot be empty or null");
        }

        final Command fullCmd;

        final RCCommands cmd = Command.parseCommand(cmdString);
        switch(cmd) {
            case CHANGE_FULLSTATS_INTERVAL:
                fullCmd = new ChangeFullStatsIntervalCmd(cmdString);
                break;
            case CHANGE_PROBE_INTERVAL:
                fullCmd = new ChangePublishIntervalCmd(cmdString);
                break;
            case DISABLE:
            case ENABLE:
                fullCmd = new ChangeStatusCmd(cmdString);
                break;
            case DYN_RESTART:
                fullCmd = new DynRestartCmd(cmdString);
                break;
            case EXIT:
                fullCmd = new ExitCmd(cmdString);
                break;
            case IGNORE_ERROR:
                fullCmd = new IgnoreErrorCmd(cmdString);
                break;
            case MAKE_TRANSITION: {
                final TransitionCmd trCmd = new TransitionCmd(cmdString);
                final FSMCommands fsmCmd = trCmd.getFSMCommand();
                switch(fsmCmd) {
                    case USERBROADCAST:
                        fullCmd = new UserBroadCastCmd(cmdString);
                        break;
                    case RESYNCH:
                        fullCmd = new ResynchCmd(cmdString);
                        break;
                    case SUB_TRANSITION:
                        fullCmd = new SubTransitionCmd(cmdString);
                        break;
                    default:
                        fullCmd = trCmd;
                }
            }
                break;
            case PUBLISH:
                fullCmd = new PublishCmd(cmdString);
                break;
            case PUBLISHSTATS:
                fullCmd = new PublishFullStatsCmd(cmdString);
                break;
            case RESTARTAPP:
                fullCmd = new RestartAppsCmd(cmdString);
                break;
            case STARTAPP:
                fullCmd = new StartAppsCmd(cmdString);
                break;
            case STOPAPP:
                fullCmd = new StopAppsCmd(cmdString);
                break;
            case TESTAPP:
                fullCmd = new TestAppCmd(cmdString);
                break;
            case USER:
                fullCmd = new UserCmd(cmdString, (Void) null);
                break;
            case UPDATE_CHILD_STATUS:
                fullCmd = new ApplicationStatusCmd(cmdString);
                break;
            default:
                throw new BadCommandException(cmd.toString(), "Command not supported");
        }

        return fullCmd;
    }

    /**
     * It returns the command represented by this object
     * 
     * @return The command represented by this object
     */
    public RCCommands getCommand() {
        return this.command;
    }

    /**
     * It returns the UUID of the command represented by this object
     * 
     * @return The UUID of the command represented by this object
     */
    public String uuid() {
        return this.uuid;
    }

    /**
     * It returns the name of the command to sent (i.e., {@link RCCommands#toString()})
     * 
     * @return The name of the command to sent
     */
    @Override
    public String toString() {
        return this.command.toString();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((this.command == null) ? 0 : this.command.hashCode());
        return result;
    }

    /**
     * Two {@link Command} commands are considered equals if they refer to the same {@link RCCommands} command.
     * <p>
     * Sub-classes should properly override this in order to add checks on specific command properties.
     * <p>
     * The command's UUID is not taken into account here: a check on it may be used by the caller to enforce the equality relationship (be
     * aware that the UUID is not used for the evaluation of the hash code neither).
     */
    @Override
    public boolean equals(final Object obj) {
        if(this == obj) {
            return true;
        }
        if(obj == null) {
            return false;
        }
        if(!(obj instanceof Command)) {
            return false;
        }
        final Command other = (Command) obj;
        if(this.command != other.command) {
            return false;
        }
        return true;
    }

    /**
     * It serializes this object to an XML representation
     * 
     * @return A string representation of this object
     * @throws TransformerException Some error occurred serializing the command
     */
    public String serialize() throws TransformerException {
        final StringWriter out = new StringWriter();

        final Transformer xmlTrans = TransformerFactory.newInstance().newTransformer();
        xmlTrans.setOutputProperty(OutputKeys.INDENT, "yes");
        xmlTrans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        final DOMSource domSrc = new DOMSource(this.xmlDoc);
        final StreamResult str = new StreamResult(out);
        xmlTrans.transform(domSrc, str);

        return out.toString();
    }

    /**
     * It adds a parameter to the description of the command
     * <p>
     * The XML representation will look like:
     * 
     * <pre>
     * {@code
     * <path>value</path>
     * }
     * </pre>
     * 
     * @param path The path of the parameter
     * @param value The value of the parameter
     */
    protected void addParameter(final String[] path, final String value) {
        final Node root = this.xmlDoc.getFirstChild();

        Node lastNode = root;
        for(final String p : path) {
            final Element e = this.xmlDoc.createElement(p);
            lastNode.appendChild(e);
            lastNode = e;
        }

        lastNode.appendChild(this.xmlDoc.createTextNode(value));
    }

    /**
     * It replaces a command parameter with the new value <em>value</em>
     * 
     * @param valuePath The path of the parameter
     * @param value The new value
     * @throws XPathExpressionException The replacement failed (e.g., the parameter could not be found)
     */
    protected void replaceParameter(final String valuePath, final String value) throws XPathExpressionException {
        final XPath xpath = XPathFactory.newInstance().newXPath();
        final Node n = (Node) xpath.evaluate(valuePath, this.xmlDoc, XPathConstants.NODE);
        n.setTextContent(value);
    }

    /**
     * It adds multiple parameters to the description of the command
     * <p>
     * The XML representation will look like:
     * 
     * <pre>
     * {@code
     * <path>
     *  <name_1>value_1</name_1>
     *  <name_2>value_2</name_2>
     *  ...
     *  <name_N>value_2</name_N>
     * </path>
     * }
     * </pre>
     * 
     * @param path The path of the parameters
     * @param values The names (keys of the map) and values of parameters
     */
    protected void addParameters(final String[] path, final Map<String, String> values) {
        final Node root = this.xmlDoc.getFirstChild();

        Node lastNode = root;
        for(final String p : path) {
            final Element e = this.xmlDoc.createElement(p);
            lastNode.appendChild(e);
            lastNode = e;
        }

        for(final Map.Entry<String, String> me : values.entrySet()) {
            final Element e = this.xmlDoc.createElement(me.getKey());
            e.appendChild(this.xmlDoc.createTextNode(me.getValue()));
            lastNode.appendChild(e);
        }
    }

    /**
     * It adds a multi-value parameter to the description of the command *
     * <p>
     * The XML representation will look like:
     * 
     * <pre>
     * {@code
     * <path>
     *  <_param_>value_1</_param_>
     *  <_param_>value_2</_param_>
     *  ...
     *  <_param_>value_N</_param_>
     * </path>
     * }
     * </pre>
     * 
     * @param path The path of the parameters
     * @param values The values of the parameter
     */
    protected void addParameters(final String[] path, final String[] values) {
        final Node rootNode = this.xmlDoc.getFirstChild();

        Node lastNode = rootNode;
        for(final String p : path) {
            final Element e = this.xmlDoc.createElement(p);
            lastNode.appendChild(e);
            lastNode = e;
        }

        for(final String v : values) {
            final Element e = this.xmlDoc.createElement(Command.VALUE_TAG);
            e.appendChild(this.xmlDoc.createTextNode(v));
            lastNode.appendChild(e);
        }
    }

    /**
     * It adds a nested command as a parameter to this command
     * 
     * @param path The path of the command
     * @param cmd The command
     */
    protected void addCommand(final String[] path, final Command cmd) {
        final Node rootNode = this.xmlDoc.getFirstChild();

        Node lastNode = rootNode;
        for(final String p : path) {
            final Element e = this.xmlDoc.createElement(p);
            lastNode.appendChild(e);
            lastNode = e;
        }

        final Node cmdNode = cmd.xmlDoc.getFirstChild();

        final Node importedNode = this.xmlDoc.importNode(cmdNode, true);
        lastNode.appendChild(importedNode);
    }

    /**
     * It builds the nested command found at path <em>valuePath</em>.
     * <p>
     * This method only supports one nested level.
     * 
     * @param valuePath The path the nested command is found at (or null if not found)
     * @return The {@link Command} object representing the nested command
     * @throws BadCommandException The command could not be built because its description was not consistent
     * @throws XPathExpressionException Problems with the command description parsing
     * @throws TransformerException Problems with the command description parsing
     */
    protected Command getNestedCommand(final String valuePath) throws BadCommandException, XPathExpressionException, TransformerException {
        final StringWriter out = new StringWriter();

        final XPath xpath = XPathFactory.newInstance().newXPath();

        // Get the TOP_HEADER (it is the new root of the nested command)
        final Node node = (Node) xpath.evaluate(valuePath + "/" + Command.TOP_HEADER, this.xmlDoc, XPathConstants.NODE);
        if(node == null) {
            return null;
        }
        
        // Here we dump the content of the XML node to a string, so that
        // the the nested command can be built as a regular command
        final Transformer xmlTrans = TransformerFactory.newInstance().newTransformer();
        final DOMSource domSrc = new DOMSource(node);
        final StreamResult str = new StreamResult(out);
        xmlTrans.transform(domSrc, str);
                               
        return Command.buildCommand(out.toString());
    }

    /**
     * It returns the value of the property defined at path <em>valuePath</em>
     * 
     * @param valuePath The path of the property to find
     * @return The value of the property defined at path <em>valuePath</em>
     * @throws XPathExpressionException Error in XML parsing
     */
    final protected String getValue(final String valuePath) throws XPathExpressionException {
        final XPath xpath = XPathFactory.newInstance().newXPath();
        return xpath.evaluate(valuePath + "/text()", this.xmlDoc);
    }

    /**
     * It returns all the values of the property defined at path <em>nodePath</em>
     * 
     * @param nodePath The path of the property to find
     * @param valueName The id of the property
     * @return The values of the property defined at path <em>nodePath</em>
     * @throws XPathExpressionException Error in XML parsing
     */
    protected List<String> getValues(final String nodePath, final String valueName) throws XPathExpressionException {
        final XPath xpath = XPathFactory.newInstance().newXPath();

        final List<String> values = new ArrayList<>();

        final Node node = Node.class.cast(xpath.evaluate(nodePath, this.xmlDoc, XPathConstants.NODE));
        if(node != null) {
            final XPath xpath_ = XPathFactory.newInstance().newXPath();
            final NodeList nl = NodeList.class.cast(xpath_.evaluate(valueName, node, XPathConstants.NODESET));
            if(nl != null) {
                final int num = nl.getLength();
                for(int i = 0; i < num; ++i) {
                    final Node n = nl.item(i);
                    values.add(n.getTextContent());
                }
            }
        }

        return values;
    }

    /**
     * @brief It adds the identifier (i.e., CORBA reference) of the command sender
     * @param id The identifier (i.e., CORBA reference) of the command sender
     */
    void commandSender(final String id) {
        this.addParameter(new String[] {Command.REMOTE_REFERENCE_TAG}, id);
    }

    /**
     * It returns <em>true</em> if the command supports the asynchronous remote notification on completion
     * 
     * @return <em>true</em> if the command supports the asynchronous remote notification on completion
     */
    boolean supportsNotification() {
        try {
            return Boolean.parseBoolean(this.getValue("/" + Command.TOP_HEADER + "/" + Command.NOTIFY_SUPPORTED_TAG));
        }
        catch(final XPathExpressionException ex) {
            // If the property is not defined then assume that the feature is supported
            ex.printStackTrace();

            return true;
        }
    }

    /**
     * It returns <em>true</em> if this command has already been sent
     * 
     * @return <em>true</em> if this command has already been sent
     */
    boolean isAlreadySent() {
        return this.alreadySent;
    }

    /**
     * It sets whether this command has already been sent or not
     */
    void setAlreadySent() {
        this.alreadySent = true;
    }

    /**
     * It parses the string representation of a command in order to return the encoded {@link RCCommands} command
     * 
     * @param cmdString The string representation of the command
     * @return The encoded command
     * @throws BadCommandException Errors parsing the command description
     */
    private static RCCommands parseCommand(final String cmdString) throws BadCommandException {
        String result = "";

        try (final StringReader sr = new StringReader(cmdString)) {
            final Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(sr));

            final XPath xpath = XPathFactory.newInstance().newXPath();
            result = xpath.evaluate("/" + Command.TOP_HEADER + "/" + Command.CMD_NAME_TAG + "/text()", doc);

            return RCCommands.valueOf(result);
        }
        catch(final IllegalArgumentException ex) {
            final String msg;
            if(result.isEmpty() == true) {
                msg = "Cannot find the command name in the command string representation";
            } else {
                msg = result + " is not a valid command";
            }

            throw new BadCommandException(result.isEmpty() == true ? "INVALID" : result, msg, ex);
        }
        catch(final XPathExpressionException | SAXException | IOException | ParserConfigurationException ex) {
            throw new BadCommandException(result.isEmpty() == true ? "INVALID" : result, "Failed to parse the command string", ex);
        }
    }
}
