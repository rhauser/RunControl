package daq.rc.test;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import daq.rc.ApplicationState;
import daq.rc.internal.FSM_STATE;


public class ApplicationStateTest {
    @Test
    public final void test() {
        final ApplicationState[] values = ApplicationState.values();
        final FSM_STATE[] checks = FSM_STATE.values();
                
        assertArrayEquals(Arrays.stream(values).map(item -> { return item.toString();}).toArray(String[]::new),
                          Arrays.stream(checks).map(item -> { return item.toString();}).toArray(String[]::new));        
    }
}
