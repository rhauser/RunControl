package daq.rc.test;

import static org.junit.Assert.*;

import java.util.EnumSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import TM.Test.Scope;
import daq.rc.Command;
import daq.rc.Command.ChangeStatusCmd;
import daq.rc.Command.FSMCommands;
import daq.rc.Command.RCCommands;
import daq.rc.Command.TransitionCmd;
import daq.rc.Command.UserCmd;
import daq.rc.internal.FSM_COMMAND;
import daq.rc.internal.RC_COMMAND;


public class CommandTest {
    @Before
    public void setUp() {
        System.loadLibrary("rc_JItemCtrl");
    }
    
    @Test
    public final void test() throws Exception {        
        // Check FSM commands are all implemented
        {
            final Set<String> values = EnumSet.allOf(FSMCommands.class).stream().map(item -> { return item.toString(); }).collect(Collectors.toSet()); // Pure Java
            final Set<String> checks = EnumSet.allOf(FSM_COMMAND.class).stream().map(item -> { return item.toString(); }).collect(Collectors.toSet()); // From C++

            assertTrue(values.containsAll(checks));    
        }
        
        // Check RC commands are all implemented
        {
            final Set<String> values = EnumSet.allOf(RCCommands.class).stream().map(item -> { return item.toString(); }).collect(Collectors.toSet()); // Pure Java
            final Set<String> checks = EnumSet.allOf(RC_COMMAND.class).stream().map(item -> { return item.toString(); }).collect(Collectors.toSet()); // From C++

            assertTrue(values.containsAll(checks));        
        }
        
        // Check consistency of Java commands
        {
            //
            // TransitionCmd
            //
            Command rcCmd = new TransitionCmd(FSMCommands.CONFIG);
            Command copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy));
            // Consistency with C++ implementation
            final daq.rc.internal.TransitionCmd trCPP = new daq.rc.internal.TransitionCmd(rcCmd.serialize());
            assertEquals(((TransitionCmd)rcCmd).getFSMCommand().toString(), trCPP.fsmCommand());
            assertEquals(rcCmd.uuid(), trCPP.uid());
            assertEquals(rcCmd.toString(), trCPP.name());

            //
            // ChangeStatusCmd
            //
            rcCmd = new ChangeStatusCmd(true, new String[] {"1", "2", "3"});
            copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy));
            // Consistency with C++ implementation
            final daq.rc.internal.ChangeStatusCmd chCPP = new daq.rc.internal.ChangeStatusCmd(rcCmd.serialize());
            assertEquals(Boolean.valueOf(((ChangeStatusCmd)rcCmd).status()), Boolean.valueOf(chCPP.isEnable()));
            assertArrayEquals((((ChangeStatusCmd)rcCmd).getComponents()), chCPP.components().toArray(new String[] {}));
            assertEquals(rcCmd.uuid(), chCPP.uid());
            assertEquals(rcCmd.toString(), chCPP.name());

            //
            // StartAppsCmd
            //
            rcCmd = new Command.StartAppsCmd(new String[] {"11", "22", "33"});
            copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy));
            // Consistency with C++ implementation
            final daq.rc.internal.StartStopAppCmd startCPP = new daq.rc.internal.StartStopAppCmd(rcCmd.serialize());
            assertTrue(startCPP.isStart());
            assertArrayEquals(((Command.StartAppsCmd)rcCmd).getApplications(), startCPP.arguments().toArray(new String[] {}));
            assertEquals(rcCmd.uuid(), startCPP.uid());
            assertEquals(rcCmd.toString(), startCPP.name());

            //
            // StopAppsCmd
            //
            rcCmd = new Command.StopAppsCmd(new String[] {"111", "222", "333"});
            copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy));
            // Consistency with C++ implementation
            final daq.rc.internal.StartStopAppCmd stopCPP = new daq.rc.internal.StartStopAppCmd(rcCmd.serialize());
            assertFalse(stopCPP.isStart());
            assertArrayEquals(((Command.StopAppsCmd) rcCmd).getApplications(), stopCPP.arguments().toArray(new String[] {}));
            assertEquals(rcCmd.uuid(), stopCPP.uid());
            assertEquals(rcCmd.toString(), stopCPP.name());

            //
            // RestartAppsCmd
            // 
            rcCmd = new Command.RestartAppsCmd(new String[] {"1111", "2222", "3333"});
            copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy) == true);
            // Consistency with C++ implementation
            final daq.rc.internal.StartStopAppCmd restartCPP = new daq.rc.internal.StartStopAppCmd(rcCmd.serialize());
            assertTrue(restartCPP.isRestart());
            assertArrayEquals(((Command.RestartAppsCmd) rcCmd).getApplications(), restartCPP.arguments().toArray(new String[] {}));
            assertEquals(rcCmd.uuid(), restartCPP.uid());
            assertEquals(rcCmd.toString(), restartCPP.name());

            //
            // ExitCmd
            // 
            rcCmd = new Command.ExitCmd();
            copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy));
            // Consistency with C++ implementation
            final daq.rc.internal.ExitCmd exitCPP = new daq.rc.internal.ExitCmd(rcCmd.serialize());
            assertEquals(rcCmd.uuid(), exitCPP.uid());
            assertEquals(rcCmd.toString(), exitCPP.name());

            //
            // IgnoreErrorCmd
            // 
            rcCmd = new Command.IgnoreErrorCmd();
            copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy));
            // Consistency with C++ implementation
            final daq.rc.internal.IgnoreErrorCmd ignoreCPP = new daq.rc.internal.IgnoreErrorCmd(rcCmd.serialize());
            assertEquals(rcCmd.uuid(), ignoreCPP.uid());
            assertEquals(rcCmd.toString(), ignoreCPP.name());

            //
            // PublishCmd
            //
            rcCmd = new Command.PublishCmd();
            copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy));
            // Consistency with C++ implementation
            final daq.rc.internal.PublishCmd pubCPP = new daq.rc.internal.PublishCmd(rcCmd.serialize());
            assertEquals(rcCmd.uuid(), pubCPP.uid());
            assertEquals(rcCmd.toString(), pubCPP.name());

            //
            // ChangePublishIntervalCmd
            // 
            rcCmd = new Command.ChangePublishIntervalCmd(101);
            copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy));
            // Consistency with C++ implementation
            final daq.rc.internal.ChangePublishIntervalCmd chPubCPP = new daq.rc.internal.ChangePublishIntervalCmd(rcCmd.serialize());
            assertEquals(((Command.ChangePublishIntervalCmd) rcCmd).getNewInterval(), chPubCPP.publishInterval());
            assertEquals(rcCmd.uuid(), chPubCPP.uid());
            assertEquals(rcCmd.toString(), chPubCPP.name());

            //
            // PublishFullStatsCmd
            //            
            rcCmd = new Command.PublishFullStatsCmd();
            copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy));
            // Consistency with C++ implementation
            final daq.rc.internal.PublishStatsCmd pubStatsCPP = new daq.rc.internal.PublishStatsCmd(rcCmd.serialize());
            assertEquals(rcCmd.uuid(), pubStatsCPP.uid());
            assertEquals(rcCmd.toString(), pubStatsCPP.name());

            //
            // ChangeFullStatsIntervalCmd
            //
            rcCmd = new Command.ChangeFullStatsIntervalCmd(102);
            copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy));
            // Consistency with C++ implementation
            final daq.rc.internal.ChangeFullStatIntervalCmd chStatsCPP = new daq.rc.internal.ChangeFullStatIntervalCmd(rcCmd.serialize());
            assertEquals(((Command.ChangeFullStatsIntervalCmd) rcCmd).getNewInterval(), chStatsCPP.fullStatInterval());
            assertEquals(rcCmd.uuid(), chStatsCPP.uid());
            assertEquals(rcCmd.toString(), chStatsCPP.name());

            //
            // UserCmd
            //
            rcCmd = new Command.UserCmd("name", new String[] {"11111", "22222", "33333"});
            copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy));
            // Consistency with C++ implementation
            final daq.rc.internal.UserCmd userCPP = new daq.rc.internal.UserCmd(rcCmd.serialize());
            assertEquals(((Command.UserCmd) rcCmd).getCommandName(), userCPP.commandName());
            assertArrayEquals(((Command.UserCmd) rcCmd).getParameters(), userCPP.commandParameters().toArray(new String[] {}));
            assertEquals(rcCmd.uuid(), userCPP.uid());
            assertEquals(rcCmd.toString(), userCPP.name());

            //
            // UserBroadCastCmd
            //
            rcCmd = new Command.UserBroadCastCmd((UserCmd) rcCmd);
            copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy));
            // Consistency with C++ implementation
            final daq.rc.internal.UserBroadcastCmd userBrCPP = new daq.rc.internal.UserBroadcastCmd(rcCmd.serialize());
            assertEquals(rcCmd.uuid(), userBrCPP.uid());
            assertEquals(rcCmd.toString(), userBrCPP.name());

            //
            // SubTransitionCmd
            // 
            rcCmd = new Command.SubTransitionCmd("SUB_1", FSMCommands.CONFIGURE);
            copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy));
            // Consistency with C++ implementation
            final daq.rc.internal.SubTransitionCmd subCPP = new daq.rc.internal.SubTransitionCmd(rcCmd.serialize());
            assertEquals(((Command.SubTransitionCmd) rcCmd).getSubTransitionName(), subCPP.subTransition());
            assertEquals(((Command.SubTransitionCmd) rcCmd).getMainTransitionCommand().toString(), subCPP.mainTransitionCmd());
            assertEquals(rcCmd.uuid(), subCPP.uid());
            assertEquals(rcCmd.toString(), subCPP.name());

            //
            // ResynchCmd
            //
            rcCmd = new Command.ResynchCmd(0, 1, new String[] {"MOD_1", "MOD_2", "MOD_3"});
            copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy));
            // Consistency with C++ implementation
            final daq.rc.internal.ResynchCmd resCPP = new daq.rc.internal.ResynchCmd(rcCmd.serialize());
            assertEquals(((Command.ResynchCmd) rcCmd).getECRCount(), resCPP.ecrCounts());
            assertEquals(((Command.ResynchCmd) rcCmd).getExtendedL1ID(), resCPP.extendedL1ID());            
            assertArrayEquals(((Command.ResynchCmd) rcCmd).getModules(), resCPP.modules().toArray(new String[] {}));            
            assertEquals(rcCmd.uuid(), resCPP.uid());
            assertEquals(rcCmd.toString(), resCPP.name());

            //
            // TestAppCmd
            //
            rcCmd = new Command.TestAppCmd(new String[] {"APP_1", "APP_2", "APP_3"}, Scope.Any, 3);
            copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy));
            // Consistency with C++ implementation
            final daq.rc.internal.TestAppCmd testCPP = new daq.rc.internal.TestAppCmd(rcCmd.serialize());
            assertArrayEquals(((Command.TestAppCmd) rcCmd).getApplications(), testCPP.applications().toArray(new String[] {}));            
            assertEquals(rcCmd.uuid(), testCPP.uid());
            assertEquals(rcCmd.toString(), testCPP.name());

            //
            // DynRestartCmd
            //
            rcCmd = new Command.DynRestartCmd(new String[] {"SEG_1", "SEG_2", "SEG_3"});
            copy = Command.buildCommand(rcCmd.serialize());

            assertTrue(rcCmd.equals(copy));
            // Consistency with C++ implementation
            final daq.rc.internal.DynRestartCmd dynCPP = new daq.rc.internal.DynRestartCmd(rcCmd.serialize());
            assertArrayEquals(((Command.DynRestartCmd) rcCmd).getSegments(), dynCPP.controllers().toArray(new String[] {}));            
            assertEquals(rcCmd.uuid(), dynCPP.uid());
            assertEquals(rcCmd.toString(), dynCPP.name());
        }
    }
}
