package daq.rc.test;

import org.junit.internal.TextListener;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.RunWith;
import org.junit.runner.notification.Failure;
import org.junit.runners.Suite;


public class RunAllTests {
    @RunWith(Suite.class)
    @Suite.SuiteClasses({
        ApplicationStateTest.class,
        CommandTest.class
    })

    public class MyTestSuite {
    }

    public static void main(final String[] args) {
        final JUnitCore junit = new JUnitCore();
        junit.addListener(new TextListener(System.out));
        final Result result = junit.run(MyTestSuite.class);
        
        System.out.println("Finished. Result: Failures: " +
            result.getFailureCount() + ". Ignored: " +
            result.getIgnoreCount() + ". Tests run: " +
            result.getRunCount() + ". Time: " +
            result.getRunTime() + "ms.");
        
        if(result.getFailureCount() > 0) {
            System.err.println("\n--- Here are the failures");
            for(final Failure f : result.getFailures()) {
                System.out.println(f.getTestHeader() + ":\n" + f.getTrace());
            }
        }
        
        System.exit(result.getFailureCount());
    }
}
