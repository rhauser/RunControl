package daq.rc;

import java.util.List;

import daq.rc.Command.ResynchCmd;
import daq.rc.Command.SubTransitionCmd;
import daq.rc.Command.TransitionCmd;
import daq.rc.Command.UserCmd;
import daq.rc.internal.FSM_COMMAND;
import daq.rc.internal.RC_COMMAND;


/**
 * Main interface to the run control framework. Each method corresponds to a state transition or a command the application may receive.
 * <p>
 * The default implementation of any single method does not perform any action. Users may implement only needed methods.
 * <p>
 * Any error should be reported throwing and {@link ers.Issue} exception; that will cause the exception to enter an error state.
 * <p>
 * Note: for any given {@link JControllable} instance, actions are never executed concurrently, apart the {@link #publish()} and
 * {@link #publishFullStats()} actions that may be concurrent to the execution of other commands and/or transitions.
 */
public interface JControllable {
    /**
     * It is called regularly with delays defined in the OKS configuration (<em>ProbeInterval</em>).
     * <p>
     * It is also called when the application receives the {@link RC_COMMAND#PUBLISH} command. The regular timer is started as soon as the
     * {@link FSM_COMMAND#CONNECT} transition is completed and it is stopped as soon as the {@link FSM_COMMAND#DISCONNECT} transition is
     * done.
     * <p>
     * Note: it may be called concurrently with other methods (and with itself).
     * 
     * @throws ers.Issue An error occurred during the execution
     * @see {@link CommandSender#publish(String)}
     * @see {@link CommandSender#changePublishInterval(String, int)}
     */
    @SuppressWarnings("unused")
    default public void publish() throws ers.Issue {
    }

    /**
     * It is called regularly with delays defined in the OKS configuration (<em>FullStatisticsInterval</em>).
     * <p>
     * It is also called when the application receives the {@link RC_COMMAND#PUBLISHSTATS} command. The regular timer is started as soon as
     * the {@link FSM_COMMAND#CONNECT} transition is completed and it is stopped as soon as the {@link FSM_COMMAND#DISCONNECT} transition is
     * done.
     * <p>
     * Note: it may be called concurrently with other methods (and with itself).
     * 
     * @throws ers.Issue An error occurred during the execution
     * @see {@link CommandSender#publishFullStats(String)}
     * @see {@link CommandSender#changeFullStatsInterval(String, int)}.
     */
    @SuppressWarnings("unused")
    default public void publishFullStats() throws ers.Issue {
    }

    /**
     * It is called when the application receives the {@link RC_COMMAND#USER} command.
     * <p>
     * That command may also come from the parent controller in case of command broadcasting.
     * 
     * @param usrCmd The user-defined command to execute
     * @throws ers.Issue An error occurred during the execution
     * @see {@link CommandSender#userCommand(String, String, String[])}
     */
    @SuppressWarnings("unused")
    default public void user(final UserCmd usrCmd) throws ers.Issue {
    }

    /**
     * It is called when the application receives the {@link RC_COMMAND#ENABLE} command.
     *
     * @param components The components to enable
     * @throws ers.Issue An error occurred during the execution
     * @see {@link CommandSender#changeStatus(String, boolean, String[])}
     */
    @SuppressWarnings("unused")
    default public void enable(final List<String> components) throws ers.Issue {
    }

    /**
     * It is called when the application receives the {@link RC_COMMAND#DISABLE} command.
     * 
     * @param components The components to disable
     * @throws ers.Issue An error occurred during the execution
     * @see {@link CommandSender#changeStatus(String, boolean, String[])}
     */
    @SuppressWarnings("unused")
    default public void disable(final List<String> components) throws ers.Issue {
    }

    /**
     * It is called when the application is asked to exit.
     * <p>
     * Note: timeouts are defined in the configuration with respect to the time an application may take in order to exit. When the timeout
     * elapses, the application is killed with the POSIX SIGKILL. That means timeouts should be carefully adapted in order to let the
     * application complete any needed action before exiting. At the same time very large timeouts would create inefficiencies related to
     * longer transition time.
     * <p>
     * Note: it is good practice to stop any on-going action (<em>i.e.</em>, anything still being executed in any method of this interface)
     * when this method is called, otherwise the process may have troubles to exit.
     *
     * @param state The current FSM state
     */
    default public void onExit(final ApplicationState state) {
    }

    /**
     * Method called when the application is asked to perform the {@link FSM_COMMAND#CONFIGURE} transition
     * 
     * @param cmd Object containing information about the transition to perform
     * @throws ers.Issue Some error occurred during the execution of the state transition
     * @see {@link CommandSender#makeTransition(String, daq.rc.Command.FSMCommands)}
     */
    @SuppressWarnings("unused")
    default public void configure(final TransitionCmd cmd) throws ers.Issue {
    }

    /**
     * Method called when the application is asked to perform the {@link FSM_COMMAND#CONNECT} transition
     * 
     * @param cmd Object containing information about the transition to perform
     * @throws ers.Issue Some error occurred during the execution of the state transition
     * @see {@link CommandSender#makeTransition(String, daq.rc.Command.FSMCommands)}
     */
    @SuppressWarnings("unused")
    default public void connect(final TransitionCmd cmd) throws ers.Issue {
    }

    /**
     * Method called when the application is asked to perform the {@link FSM_COMMAND#START} transition
     * 
     * @param cmd Object containing information about the transition to perform
     * @throws ers.Issue Some error occurred during the execution of the state transition
     * @see {@link CommandSender#makeTransition(String, daq.rc.Command.FSMCommands)}
     */
    @SuppressWarnings("unused")
    default public void prepareForRun(final TransitionCmd cmd) throws ers.Issue {
    }

    /**
     * Method called when the application is asked to perform the {@link FSM_COMMAND#STOPROIB} transition
     * 
     * @param cmd Object containing information about the transition to perform
     * @throws ers.Issue Some error occurred during the execution of the state transition
     * @see {@link CommandSender#makeTransition(String, daq.rc.Command.FSMCommands)}
     */
    @SuppressWarnings("unused")
    default public void stopROIB(final TransitionCmd cmd) throws ers.Issue {
    }

    /**
     * Method called when the application is asked to perform the {@link FSM_COMMAND#STOPDC} transition
     * 
     * @param cmd Object containing information about the transition to perform
     * @throws ers.Issue Some error occurred during the execution of the state transition
     * @see {@link CommandSender#makeTransition(String, daq.rc.Command.FSMCommands)}
     */
    @SuppressWarnings("unused")
    default public void stopDC(final TransitionCmd cmd) throws ers.Issue {
    }

    /**
     * Method called when the application is asked to perform the {@link FSM_COMMAND#STOPHLT} transition
     * 
     * @param cmd Object containing information about the transition to perform
     * @throws ers.Issue Some error occurred during the execution of the state transition
     * @see {@link CommandSender#makeTransition(String, daq.rc.Command.FSMCommands)}
     */
    @SuppressWarnings("unused")
    default public void stopHLT(final TransitionCmd cmd) throws ers.Issue {
    }

    /**
     * Method called when the application is asked to perform the {@link FSM_COMMAND#STOPRECORDING} transition
     * 
     * @param cmd Object containing information about the transition to perform
     * @throws ers.Issue Some error occurred during the execution of the state transition
     * @see {@link CommandSender#makeTransition(String, daq.rc.Command.FSMCommands)}
     */
    @SuppressWarnings("unused")
    default public void stopRecording(final TransitionCmd cmd) throws ers.Issue {
    }

    /**
     * Method called when the application is asked to perform the {@link FSM_COMMAND#STOPGATHERING} transition
     * 
     * @param cmd Object containing information about the transition to perform
     * @throws ers.Issue Some error occurred during the execution of the state transition
     * @see {@link CommandSender#makeTransition(String, daq.rc.Command.FSMCommands)}
     */
    @SuppressWarnings("unused")
    default public void stopGathering(final TransitionCmd cmd) throws ers.Issue {
    }

    /**
     * Method called when the application is asked to perform the {@link FSM_COMMAND#STOPARCHIVING} transition
     * 
     * @param cmd Object containing information about the transition to perform
     * @throws ers.Issue Some error occurred during the execution of the state transition
     * @see {@link CommandSender#makeTransition(String, daq.rc.Command.FSMCommands)}
     */
    @SuppressWarnings("unused")
    default public void stopArchiving(final TransitionCmd cmd) throws ers.Issue {
    }

    /**
     * Method called when the application is asked to perform the {@link FSM_COMMAND#DISCONNECT} transition
     * <p>
     * NOTE: During this transition the periodic timers are paused: the transition will not be considered as done until the action is
     * properly completed. It is then good practice to introduce some mechanism allowing {@link #publish()} and {@link #publishFullStats()}
     * to return as soon as the {@link FSM_COMMAND#DISCONNECT} transition is being performed: this would help in reducing the transition
     * time.
     * 
     * @param cmd Object containing information about the transition to perform
     * @throws ers.Issue Some error occurred during the execution of the state transition
     * @see {@link CommandSender#makeTransition(String, daq.rc.Command.FSMCommands)}
     */
    @SuppressWarnings("unused")
    default public void disconnect(final TransitionCmd cmd) throws ers.Issue {
    }

    /**
     * Method called when the application is asked to perform the {@link FSM_COMMAND#UNCONFIGURE} transition
     * 
     * @param cmd Object containing information about the transition to perform
     * @throws ers.Issue Some error occurred during the execution of the state transition
     * @see {@link CommandSender#makeTransition(String, daq.rc.Command.FSMCommands)}
     */
    @SuppressWarnings("unused")
    default public void unconfigure(final TransitionCmd cmd) throws ers.Issue {
    }

    /**
     * Method called when the application is asked to perform the {@link FSM_COMMAND#SUB_TRANSITION} transition
     * 
     * @param cmd Object containing information about the transition to perform
     * @throws ers.Issue Some error occurred during the execution of the state transition
     * @see {@link CommandSender#subTransition(String, String, daq.rc.Command.FSMCommands)}
     */
    @SuppressWarnings("unused")
    default public void subTransition(final SubTransitionCmd cmd) throws ers.Issue {
    }

    /**
     * Method called when the application is asked to perform the {@link FSM_COMMAND#RESYNCH} transition
     * 
     * @param cmd Object containing information about the transition to perform
     * @throws ers.Issue Some error occurred during the execution of the state transition
     * @see {@link CommandSender#resynch(String, int, String[])}
     */
    @SuppressWarnings("unused")
    default public void resynch(final ResynchCmd cmd) throws ers.Issue {
    }
}
