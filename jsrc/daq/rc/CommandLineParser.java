package daq.rc;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;


/**
 * Utility class to parse the command line options of a Run Control application.
 * <p>
 * When executed in the run control framework, command line options are automatically set. Using this class, the user can transparently
 * parse the options and use them to build the {@link JItemCtrl} object.
 */

public class CommandLineParser {
    private final boolean isInteractive;
    private final boolean helpSwitch;
    private final String parentName;
    private final String segmentName;
    private final String partitionName;
    private final String applicationName;

    /**
     * This exception is thrown by the {@link CommandLineParser} constructor when the command line options include the "help" switch.
     * <p>
     * The exception's message contains the help message with the list of valid command line arguments.
     */
    public static class HelpRequested extends Exception {
        private static final long serialVersionUID = 8929134969136041707L;

        /**
         * Constructor.
         * 
         * @param helpMessage The help message
         */
        public HelpRequested(final String helpMessage) {
            super(helpMessage);
        }
    }

    /**
     * Constructor.
     * <p>
     * The <em>tdaq.partition</em> and <em>tdaq.appname</em> properties are set.
     * 
     * @param args The arguments as they are passed to the main function
     * @throws ParseException Some error occurred parsing the command line options
     * @throws HelpRequested Thrown when the "help" switch is present in the command line
     */
    public CommandLineParser(final String[] args) throws ParseException, HelpRequested {
        // Create all the needed command line options
        final Options opts = new Options();
        opts.addOption("p", "partition", true, "Name of the partition (TDAQ_PARTITION)");
        opts.addOption("P", "parent", true, "Name of the parent controller");
        opts.addOption("s", "segment", true, "Name of the segment");
        opts.addOption("n", "name", true, "Name of the application (TDAQ_APPLICATION_NAME)");
        opts.addOption("i", "interactive", false, "Flag for running in interactive mode (default is false)");
        opts.addOption("h", "help", false, "Print help message");

        // Collect all the command line tokens
        final List<String> tokens = Stream.of("-p",
                                              "-partition",
                                              "-P",
                                              "-parent",
                                              "-s",
                                              "-segment",
                                              "-n",
                                              "-name",
                                              "-i",
                                              "-interactive",
                                              "-h",
                                              "-help").collect(Collectors.toCollection(ArrayList::new));

        // The parser will stop when an unknown token is met
        // Here we loop until all the valid options are parsed
        // Parsing is considered as done when the list of left-over arguments
        // does not contain any of the known options
        final Properties props = new Properties();
        String[] toBeparsed = args;
        CommandLine cmdLine = null;
        final org.apache.commons.cli.CommandLineParser parser = new DefaultParser();
        do {
            // Parsing
            cmdLine = parser.parse(opts, toBeparsed, true);

            // Collect parsed options
            for(final Option op : opts.getOptions()) {
                final String opName = op.getOpt();
                if(cmdLine.hasOption(opName) == true) {
                    // Be careful if the argument is a flag without any value!
                    // Properties do not allow null as a value
                    props.setProperty(opName, (op.hasArg() == true) ? cmdLine.getOptionValue(opName) : "");
                }
            }

            // Now check any option that has not been parsed
            // Eventually remove the first unknown option
            final List<String> leftOver = cmdLine.getArgList();
            if(leftOver.isEmpty() == false) {
                leftOver.remove(0);
            }

            toBeparsed = leftOver.toArray(new String[leftOver.size()]);
        }
        while(Collections.disjoint(cmdLine.getArgList(), tokens) == false);

        // Handle the case the help is requested
        this.helpSwitch = props.containsKey("h");
        if(this.helpSwitch == true) {
            final StringWriter helpMessage = new StringWriter();

            final HelpFormatter hf = new HelpFormatter();
            hf.printHelp(new PrintWriter(helpMessage, true),
                         80,
                         "myapp",
                         "Basic command line options for a run control application:",
                         opts,
                         0,
                         0,
                         "Note: the environment variable TDAQ_APPLICATION_NAME is mandatory; TDAQ_DB is needed for database access.",
                         true);

            throw new HelpRequested(helpMessage.toString());
        }

        this.isInteractive = props.containsKey("i");

        this.parentName = props.getProperty("P", this.isInteractive == true ? "NOTAV" : null);
        if(this.parentName == null) {
            throw new ParseException("The application's parent name has not been defined");
        }

        this.segmentName = props.getProperty("s", this.isInteractive == true ? "NOTAV" : null);
        if(this.segmentName == null) {
            throw new ParseException("The application's segment name has not been defined");
        }

        this.partitionName =
                           props.getProperty("p",
                                             this.isInteractive == true ? (System.getenv("TDAQ_PARTITION") == null ? "NOTAV"
                                                                                                                   : System.getenv("TDAQ_PARTITION"))
                                                                        : System.getenv("TDAQ_PARTITION"));
        if(this.partitionName == null) {
            throw new ParseException("The partition name has not been defined");
        }

        System.setProperty("tdaq.partition", this.partitionName);

        this.applicationName =
                             props.getProperty("n",
                                               this.isInteractive == true ? (System.getenv("TDAQ_APPLICATION_NAME") == null ? "Generic RunControl Application"
                                                                                                                            : System.getenv("TDAQ_APPLICATION_NAME"))
                                                                          : System.getenv("TDAQ_APPLICATION_NAME"));
        if(this.applicationName == null) {
            throw new ParseException("The application's name has not been defined");
        }

        System.setProperty("tdaq.appname", this.applicationName);
    }

    /**
     * It returns <em>true</em> if the application is asked to run in interactive mode
     * 
     * @return <em>true</em> if the application is asked to run in interactive mode
     */
    public boolean isInteractive() {
        return this.isInteractive;
    }

    /**
     * The name of the application's parent controller
     * 
     * @return The name of the application's parent controller
     */
    public String getParentName() {
        return this.parentName;
    }

    /**
     * The name of the segment the application belongs to
     * 
     * @return The name of the segment the application belongs to
     */
    public String getSegmentName() {
        return this.segmentName;
    }

    /**
     * The name of the partition the application will be executed
     * 
     * @return The name of the partition the application will be executed
     */
    public String getPartitionName() {
        return this.partitionName;
    }

    /**
     * The name of the application, as it will appear in the run control tree
     * 
     * @return The name of the application, as it will appear in the run control tree
     */
    public String getApplicationName() {
        return this.applicationName;
    }
}
