/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package daq.rc.internal;

public class ResynchCmd extends TransitionCmd {
  private transient long swigCPtr;

  protected ResynchCmd(long cPtr, boolean cMemoryOwn) {
    super(rc_JItemCtrlJNI.ResynchCmd_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ResynchCmd obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  @SuppressWarnings("deprecation")
  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        rc_JItemCtrlJNI.delete_ResynchCmd(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public ResynchCmd(String commandDescription) {
    this(rc_JItemCtrlJNI.new_ResynchCmd__SWIG_0(commandDescription), true);
  }

  public ResynchCmd(long ecrCount, long extendedL1ID, StringVector modules) {
    this(rc_JItemCtrlJNI.new_ResynchCmd__SWIG_1(ecrCount, extendedL1ID, StringVector.getCPtr(modules), modules), true);
  }

  public ResynchCmd(long ecrCount, long extendedL1ID) {
    this(rc_JItemCtrlJNI.new_ResynchCmd__SWIG_2(ecrCount, extendedL1ID), true);
  }

  public ResynchCmd(long ecrCount) {
    this(rc_JItemCtrlJNI.new_ResynchCmd__SWIG_3(ecrCount), true);
  }

  public long ecrCounts() {
    return rc_JItemCtrlJNI.ResynchCmd_ecrCounts__SWIG_0(swigCPtr, this);
  }

  public void ecrCounts(long ecr) {
    rc_JItemCtrlJNI.ResynchCmd_ecrCounts__SWIG_1(swigCPtr, this, ecr);
  }

  public long extendedL1ID() {
    return rc_JItemCtrlJNI.ResynchCmd_extendedL1ID__SWIG_0(swigCPtr, this);
  }

  public void extendedL1ID(long extendedL1ID) {
    rc_JItemCtrlJNI.ResynchCmd_extendedL1ID__SWIG_1(swigCPtr, this, extendedL1ID);
  }

  public StringVector modules() {
    return new StringVector(rc_JItemCtrlJNI.ResynchCmd_modules__SWIG_0(swigCPtr, this), true);
  }

  public void modules(StringVector mods) {
    rc_JItemCtrlJNI.ResynchCmd_modules__SWIG_1(swigCPtr, this, StringVector.getCPtr(mods), mods);
  }

  public String toString() {
    return rc_JItemCtrlJNI.ResynchCmd_toString(swigCPtr, this);
  }

  public StringVector arguments() {
    return new StringVector(rc_JItemCtrlJNI.ResynchCmd_arguments(swigCPtr, this), true);
  }

}
