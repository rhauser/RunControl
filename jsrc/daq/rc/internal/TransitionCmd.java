/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package daq.rc.internal;

public class TransitionCmd extends RunControlBasicCommand {
  private transient long swigCPtr;

  protected TransitionCmd(long cPtr, boolean cMemoryOwn) {
    super(rc_JItemCtrlJNI.TransitionCmd_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(TransitionCmd obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  @SuppressWarnings("deprecation")
  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        rc_JItemCtrlJNI.delete_TransitionCmd(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public TransitionCmd(String commandDescription) {
    this(rc_JItemCtrlJNI.new_TransitionCmd__SWIG_0(commandDescription), true);
  }

  public TransitionCmd(FSM_COMMAND cmd, boolean isRestart) {
    this(rc_JItemCtrlJNI.new_TransitionCmd__SWIG_1(cmd.swigValue(), isRestart), true);
  }

  public TransitionCmd(FSM_COMMAND cmd) {
    this(rc_JItemCtrlJNI.new_TransitionCmd__SWIG_2(cmd.swigValue()), true);
  }

  public String fsmCommand() {
    return rc_JItemCtrlJNI.TransitionCmd_fsmCommand__SWIG_0(swigCPtr, this);
  }

  public void fsmCommand(FSM_COMMAND cmd) {
    rc_JItemCtrlJNI.TransitionCmd_fsmCommand__SWIG_1(swigCPtr, this, cmd.swigValue());
  }

  public String fsmSourceState() {
    return rc_JItemCtrlJNI.TransitionCmd_fsmSourceState__SWIG_0(swigCPtr, this);
  }

  public void fsmSourceState(FSM_STATE state) {
    rc_JItemCtrlJNI.TransitionCmd_fsmSourceState__SWIG_1(swigCPtr, this, state.swigValue());
  }

  public String fsmDestinationState() {
    return rc_JItemCtrlJNI.TransitionCmd_fsmDestinationState__SWIG_0(swigCPtr, this);
  }

  public void fsmDestinationState(FSM_STATE state) {
    rc_JItemCtrlJNI.TransitionCmd_fsmDestinationState__SWIG_1(swigCPtr, this, state.swigValue());
  }

  public boolean isRestart() {
    return rc_JItemCtrlJNI.TransitionCmd_isRestart__SWIG_0(swigCPtr, this);
  }

  public void isRestart(boolean isControllerRestart) {
    rc_JItemCtrlJNI.TransitionCmd_isRestart__SWIG_1(swigCPtr, this, isControllerRestart);
  }

  public StringVector subTransitions() {
    return new StringVector(rc_JItemCtrlJNI.TransitionCmd_subTransitions__SWIG_0(swigCPtr, this), true);
  }

  public void subTransitions(FSMCommandToStringVector subTrMap) {
    rc_JItemCtrlJNI.TransitionCmd_subTransitions__SWIG_1(swigCPtr, this, FSMCommandToStringVector.getCPtr(subTrMap), subTrMap);
  }

  public boolean subTransitionsDone() {
    return rc_JItemCtrlJNI.TransitionCmd_subTransitionsDone__SWIG_0(swigCPtr, this);
  }

  public void subTransitionsDone(boolean done) {
    rc_JItemCtrlJNI.TransitionCmd_subTransitionsDone__SWIG_1(swigCPtr, this, done);
  }

  public boolean skipSubTransitions() {
    return rc_JItemCtrlJNI.TransitionCmd_skipSubTransitions__SWIG_0(swigCPtr, this);
  }

  public void skipSubTransitions(boolean skip) {
    rc_JItemCtrlJNI.TransitionCmd_skipSubTransitions__SWIG_1(swigCPtr, this, skip);
  }

  public String toString() {
    return rc_JItemCtrlJNI.TransitionCmd_toString(swigCPtr, this);
  }

  public StringVector arguments() {
    return new StringVector(rc_JItemCtrlJNI.TransitionCmd_arguments(swigCPtr, this), true);
  }

}
