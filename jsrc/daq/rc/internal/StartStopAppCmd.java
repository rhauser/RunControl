/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package daq.rc.internal;

public class StartStopAppCmd extends RunControlBasicCommand {
  private transient long swigCPtr;

  protected StartStopAppCmd(long cPtr, boolean cMemoryOwn) {
    super(rc_JItemCtrlJNI.StartStopAppCmd_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(StartStopAppCmd obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  @SuppressWarnings("deprecation")
  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        rc_JItemCtrlJNI.delete_StartStopAppCmd(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  static public class is_start_t {
    private transient long swigCPtr;
    protected transient boolean swigCMemOwn;
  
    protected is_start_t(long cPtr, boolean cMemoryOwn) {
      swigCMemOwn = cMemoryOwn;
      swigCPtr = cPtr;
    }
  
    protected static long getCPtr(is_start_t obj) {
      return (obj == null) ? 0 : obj.swigCPtr;
    }
  
    @SuppressWarnings("deprecation")
    protected void finalize() {
      delete();
    }
  
    public synchronized void delete() {
      if (swigCPtr != 0) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          rc_JItemCtrlJNI.delete_StartStopAppCmd_is_start_t(swigCPtr);
        }
        swigCPtr = 0;
      }
    }
  
    public is_start_t() {
      this(rc_JItemCtrlJNI.new_StartStopAppCmd_is_start_t(), true);
    }
  
  }

  static public class is_stop_t {
    private transient long swigCPtr;
    protected transient boolean swigCMemOwn;
  
    protected is_stop_t(long cPtr, boolean cMemoryOwn) {
      swigCMemOwn = cMemoryOwn;
      swigCPtr = cPtr;
    }
  
    protected static long getCPtr(is_stop_t obj) {
      return (obj == null) ? 0 : obj.swigCPtr;
    }
  
    @SuppressWarnings("deprecation")
    protected void finalize() {
      delete();
    }
  
    public synchronized void delete() {
      if (swigCPtr != 0) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          rc_JItemCtrlJNI.delete_StartStopAppCmd_is_stop_t(swigCPtr);
        }
        swigCPtr = 0;
      }
    }
  
    public is_stop_t() {
      this(rc_JItemCtrlJNI.new_StartStopAppCmd_is_stop_t(), true);
    }
  
  }

  static public class is_restart_t {
    private transient long swigCPtr;
    protected transient boolean swigCMemOwn;
  
    protected is_restart_t(long cPtr, boolean cMemoryOwn) {
      swigCMemOwn = cMemoryOwn;
      swigCPtr = cPtr;
    }
  
    protected static long getCPtr(is_restart_t obj) {
      return (obj == null) ? 0 : obj.swigCPtr;
    }
  
    @SuppressWarnings("deprecation")
    protected void finalize() {
      delete();
    }
  
    public synchronized void delete() {
      if (swigCPtr != 0) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          rc_JItemCtrlJNI.delete_StartStopAppCmd_is_restart_t(swigCPtr);
        }
        swigCPtr = 0;
      }
    }
  
    public is_restart_t() {
      this(rc_JItemCtrlJNI.new_StartStopAppCmd_is_restart_t(), true);
    }
  
  }

  public static StartStopAppCmd.is_start_t getIs_start_app_cmd() {
    long cPtr = rc_JItemCtrlJNI.StartStopAppCmd_is_start_app_cmd_get();
    return (cPtr == 0) ? null : new StartStopAppCmd.is_start_t(cPtr, false);
  }

  public static StartStopAppCmd.is_stop_t getIs_stop_app_cmd() {
    long cPtr = rc_JItemCtrlJNI.StartStopAppCmd_is_stop_app_cmd_get();
    return (cPtr == 0) ? null : new StartStopAppCmd.is_stop_t(cPtr, false);
  }

  public static StartStopAppCmd.is_restart_t getIs_restart_app_cmd() {
    long cPtr = rc_JItemCtrlJNI.StartStopAppCmd_is_restart_app_cmd_get();
    return (cPtr == 0) ? null : new StartStopAppCmd.is_restart_t(cPtr, false);
  }

  public StartStopAppCmd(String commandDescription) {
    this(rc_JItemCtrlJNI.new_StartStopAppCmd__SWIG_0(commandDescription), true);
  }

  public StartStopAppCmd(StartStopAppCmd.is_start_t tag, StringVector applications) {
    this(rc_JItemCtrlJNI.new_StartStopAppCmd__SWIG_1(StartStopAppCmd.is_start_t.getCPtr(tag), tag, StringVector.getCPtr(applications), applications), true);
  }

  public StartStopAppCmd(StartStopAppCmd.is_stop_t tag, StringVector applications) {
    this(rc_JItemCtrlJNI.new_StartStopAppCmd__SWIG_2(StartStopAppCmd.is_stop_t.getCPtr(tag), tag, StringVector.getCPtr(applications), applications), true);
  }

  public StartStopAppCmd(StartStopAppCmd.is_restart_t tag, StringVector applications) {
    this(rc_JItemCtrlJNI.new_StartStopAppCmd__SWIG_3(StartStopAppCmd.is_restart_t.getCPtr(tag), tag, StringVector.getCPtr(applications), applications), true);
  }

  public String currentState() {
    return rc_JItemCtrlJNI.StartStopAppCmd_currentState__SWIG_0(swigCPtr, this);
  }

  public void currentState(FSM_STATE state) {
    rc_JItemCtrlJNI.StartStopAppCmd_currentState__SWIG_1(swigCPtr, this, state.swigValue());
  }

  public String destinationState() {
    return rc_JItemCtrlJNI.StartStopAppCmd_destinationState__SWIG_0(swigCPtr, this);
  }

  public void destinationState(FSM_STATE state) {
    rc_JItemCtrlJNI.StartStopAppCmd_destinationState__SWIG_1(swigCPtr, this, state.swigValue());
  }

  public String transitionCommand() {
    return rc_JItemCtrlJNI.StartStopAppCmd_transitionCommand__SWIG_0(swigCPtr, this);
  }

  public void transitionCommand(FSM_COMMAND cmd) {
    rc_JItemCtrlJNI.StartStopAppCmd_transitionCommand__SWIG_1(swigCPtr, this, cmd.swigValue());
  }

  public boolean isStart() {
    return rc_JItemCtrlJNI.StartStopAppCmd_isStart(swigCPtr, this);
  }

  public boolean isRestart() {
    return rc_JItemCtrlJNI.StartStopAppCmd_isRestart(swigCPtr, this);
  }

  public String toString() {
    return rc_JItemCtrlJNI.StartStopAppCmd_toString(swigCPtr, this);
  }

  public StringVector arguments() {
    return new StringVector(rc_JItemCtrlJNI.StartStopAppCmd_arguments(swigCPtr, this), true);
  }

}
