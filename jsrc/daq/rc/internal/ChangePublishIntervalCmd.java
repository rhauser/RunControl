/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.1
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package daq.rc.internal;

public class ChangePublishIntervalCmd extends RunControlBasicCommand {
  private transient long swigCPtr;

  protected ChangePublishIntervalCmd(long cPtr, boolean cMemoryOwn) {
    super(rc_JItemCtrlJNI.ChangePublishIntervalCmd_SWIGUpcast(cPtr), cMemoryOwn);
    swigCPtr = cPtr;
  }

  protected static long getCPtr(ChangePublishIntervalCmd obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  @SuppressWarnings("deprecation")
  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        rc_JItemCtrlJNI.delete_ChangePublishIntervalCmd(swigCPtr);
      }
      swigCPtr = 0;
    }
    super.delete();
  }

  public ChangePublishIntervalCmd(String commandDescription) {
    this(rc_JItemCtrlJNI.new_ChangePublishIntervalCmd__SWIG_0(commandDescription), true);
  }

  public ChangePublishIntervalCmd(long newInterval) {
    this(rc_JItemCtrlJNI.new_ChangePublishIntervalCmd__SWIG_1(newInterval), true);
  }

  public long publishInterval() {
    return rc_JItemCtrlJNI.ChangePublishIntervalCmd_publishInterval__SWIG_0(swigCPtr, this);
  }

  public void publishInterval(long newInterval) {
    rc_JItemCtrlJNI.ChangePublishIntervalCmd_publishInterval__SWIG_1(swigCPtr, this, newInterval);
  }

  public String toString() {
    return rc_JItemCtrlJNI.ChangePublishIntervalCmd_toString(swigCPtr, this);
  }

  public StringVector arguments() {
    return new StringVector(rc_JItemCtrlJNI.ChangePublishIntervalCmd_arguments(swigCPtr, this), true);
  }

}
