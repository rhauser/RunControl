package daq.rc;

/**
 * Class describing a reason why a segment's controller in in an error state.
 * <p>
 * Error elements are identified by the Expert System and then passed to the controller.
 */
public class ErrorElement {
    private final String applicationName;
    private final String errorDescription;
    private final String errorType;

    /**
     * Constructor.
     * 
     * @param applicationName Name of controller's child application causing the error
     * @param errorDescription Human readable description of the error
     * @param errorType The error type
     */
    public ErrorElement(final String applicationName, final String errorDescription, final String errorType) {
        this.applicationName = applicationName;
        this.errorDescription = errorDescription;
        this.errorType = errorType;
    }

    /**
     * It returns the name of the child application causing the error
     * 
     * @return The name of the child application causing the error
     */
    public String getApplicationName() {
        return this.applicationName;
    }

    /**
     * It returns an human readable description of the error
     * 
     * @return An human readable description of the error
     */
    public String getErrorDescription() {
        return this.errorDescription;
    }

    /**
     * It returns the error type
     * 
     * @return The error type
     */
    public String getErrorType() {
        return this.errorType;
    }
}
