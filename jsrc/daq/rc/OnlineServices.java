package daq.rc;

import java.util.concurrent.atomic.AtomicReference;

import config.Configuration;
import daq.rc.RCException.OnlineServicesFailure;


/**
 * Utility class to access some online services (mainly the configuration service).
 * <p>
 * In order to get a valid instance of this class, the {@link #initialize(String, String)} method must be called. Please, note that the
 * initialization method is called by the {@link JItemCtrl} constructor(s).
 * <p>
 * NOTE: The TDAQ_APPLICATION_NAME environment variable must be set, otherwise the initialization will fail.
 */

public class OnlineServices {
    private static final AtomicReference<OnlineServices> INSTANCE = new AtomicReference<>();
    private static final String TDAQ_APPLICATION_NAME = "TDAQ_APPLICATION_NAME";
    private static final String TDAQ_APPLICATION_OBJECT_ID = "TDAQ_APPLICATION_OBJECT_ID";

    private final AtomicReference<Configuration> configuration = new AtomicReference<>();
    private final AtomicReference<dal.Partition> dalPartition = new AtomicReference<>();
    private final String partitionName;
    private final String segmentName;
    private final String applicationName;

    /**
     * Constructor.
     * 
     * @param partitionName The name of the partition
     * @param segmentName The name of the segment
     * @throws OnlineServicesFailure Some error occurred during initialization
     */
    private OnlineServices(final String partitionName, final String segmentName) throws OnlineServicesFailure {
        this.partitionName = partitionName;
        this.segmentName = segmentName;
        this.applicationName = System.getenv(OnlineServices.TDAQ_APPLICATION_NAME);
        if(this.applicationName == null) {
            throw new OnlineServicesFailure("The manadatory \"" + OnlineServices.TDAQ_APPLICATION_NAME
                                            + "\" environment variable is not defined");
        }
    }

    /**
     * This method must be called before getting the reference to an instance of this class.
     * <p>
     * It is safe to call this method more than once.
     * 
     * @param partitionName The name of the partition
     * @param segmentName The name of the segment
     * @throws OnlineServicesFailure Some error occurred during initialization
     * @see OnlineServices#instance()
     */
    public static void initialize(final String partitionName, final String segmentName) throws OnlineServicesFailure {
        if((partitionName == null) || (partitionName.isEmpty() == true)) {
            throw new IllegalArgumentException("The name of the partition cannot be null or empty");
        }

        if((segmentName == null) || (segmentName.isEmpty() == true)) {
            throw new IllegalArgumentException("The name of the segment cannot be null or empty");
        }

        OnlineServices.INSTANCE.compareAndSet(null, new OnlineServices(partitionName, segmentName));
    }

    /**
     * It returns the reference to an instance of this class.
     * 
     * @return The reference to an instance of this class
     * @throw IllegalStateException Exception thrown when the initialization method has not been called
     * @see OnlineServices#initialize(String, String)
     */
    public static OnlineServices instance() {
        final OnlineServices that = OnlineServices.INSTANCE.get();
        if(that == null) {
            throw new IllegalStateException("Not initialized");
        }

        return that;
    }

    /**
     * It returns a reference to the {@link dal.Partition} object representing the current partition.
     * 
     * @return A reference to the {@link dal.Partition} object representing the current partition
     * @throws OnlineServicesFailure Some error occurred accessing the configuration service
     */
    public dal.Partition getPartition() throws OnlineServicesFailure {
        this.initConfiguration();
        return this.dalPartition.get();
    }

    /**
     * It returns a reference to the {@link Configuration} object representing the current system configuration.
     * 
     * @return A reference to the {@link Configuration} object representing the current system configuration
     * @throws OnlineServicesFailure Some error occurred accessing the configuration service
     */
    public Configuration getConfiguration() throws OnlineServicesFailure {
        this.initConfiguration();
        return this.configuration.get();
    }

    /**
     * It returns a reference to the {@link dal.Segment} object representing the segment the application belongs to.
     * 
     * @return A reference to the {@link dal.Segment} object representing the segment the application belongs to
     * @throws OnlineServicesFailure Some error occurred accessing the configuration service or the application belongs to a template
     *             segment
     */
    public dal.Segment getSegment() throws OnlineServicesFailure {
        this.initConfiguration();

        try {
            final dal.Partition p = this.dalPartition.get();
            return p.get_segment(this.segmentName);
        }
        catch(final config.ConfigException | NullPointerException ex) {
            throw new OnlineServicesFailure("Cannot retrieve segment with name \"" + this.segmentName + "\" from configuration: " + ex, ex);
        }
    }

    /**
     * It returns a reference to the {@link dal.RunControlApplicationBase} object representing the current application.
     * <p>
     * Note: any run control application must be described in the database by a class inheriting from <em>RunControlApplicationBase</em>
     * 
     * @return A reference to the {@link dal.RunControlApplicationBase} object representing the current application
     * @throws OnlineServicesFailure Some error occurred accessing the configuration service
     */
    public dal.RunControlApplicationBase getApplication() throws OnlineServicesFailure {
        this.initConfiguration();

        final String appObjectId =
                                 System.getenv(OnlineServices.TDAQ_APPLICATION_OBJECT_ID) != null ? System.getenv(OnlineServices.TDAQ_APPLICATION_OBJECT_ID)
                                                                                                  : this.applicationName;

        try {
            final dal.RunControlApplicationBase app = dal.RunControlApplicationBase_Helper.get(this.configuration.get(), appObjectId);
            if(app == null) {
                throw new OnlineServicesFailure("Cannot find application of class \"RunControlApplicationBase\" with name \"" + appObjectId
                                                + "\"; got a null reference");
            }

            return app;
        }
        catch(config.SystemException | config.NotFoundException ex) {
            throw new OnlineServicesFailure("Cannot retrieve application \"" + appObjectId + "\" from the database: " + ex, ex);
        }
    }

    /**
     * It returns the name of the application.
     * 
     * @return The name of the application
     */
    public String getApplicationName() {
        return this.applicationName;
    }

    /**
     * It returns the name of the segment the application belongs to.
     * 
     * @return The name of the application the segment belongs to
     */
    public String getSegmentName() {
        return this.segmentName;
    }

    /**
     * It returns the name of the current partition.
     * 
     * @return The name of the current partition.
     */
    public String getPartitionName() {
        return this.partitionName;
    }

    // Lazy initialization for the configuration and partition object
    // They are the most expensive objects to build
    private void initConfiguration() throws OnlineServicesFailure {
        Configuration conf = this.configuration.get();
        if(conf == null) {
            synchronized(this) {
                conf = this.configuration.get();
                if(conf == null) {
                    try {
                        // Create configuration
                        conf = new Configuration("");

                        // Register converters
                        final dal.Partition p = dal.Partition_Helper.get(conf, this.partitionName);
                        if(p != null) {
                            conf.register_converter(new dal.SubstituteVariables(p));
                        } else {
                            throw new RCException.OnlineServicesFailure("Cannot find partition \"" + this.partitionName
                                                                        + "\" in the database, got a null reference");
                        }

                        this.dalPartition.set(p);
                        this.configuration.set(conf);
                    }
                    catch(final config.ConfigException ex) {
                        throw new RCException.OnlineServicesFailure("Some error occurred accessing the configuration: " + ex, ex);
                    }
                }
            }
        }
    }
}
