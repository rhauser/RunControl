/**
 * This package contains classes allowing java applications to interact with Run Control applications.
 * <p>
 * It provides:
 * <ul>
 * <li>Utility class to send commands to remote Run Control applications: {@link daq.rc.CommandSender};
 * <li>Classes representing commands to be sent to Run Control applications: {@link daq.rc.Command} and derived classes;
 * <li>Enumeration for all the FSM commands (i.e., FSM transitions): {@link daq.rc.Command.FSMCommands};
 * <li>Enumeration for all the available FSM states an application may assume: {@link daq.rc.ApplicationState};
 * <li>Enumeration for all the available Run Control commands: {@link daq.rc.Command.RCCommands};
 * <li>Enumeration for the statuses an application may assume from a Process Management point of view: {@link daq.rc.ProcessStatus};
 * <li>Entry point class in order to create run control state aware applications: {@link daq.rc.JItemCtrl};
 * <li>Class encapsulating actions to be executed by a run control application: {@link daq.rc.JControllable};
 * <li>Utility class to have easy access to some online services (e.g., the Configuration service): {@link daq.rc.OnlineServices};
 * <li>Utility class to parse command line options passed by the run control framework to a run control application: {@link daq.rc.CommandLineParser}.
 * </ul>
 * <p>
 * Users shall never use any class in the daq.rc.internal package!
 * <p>
 * All the exception thrown by classes of this package inherit from {@link daq.rc.RCException}.
 */
package daq.rc;