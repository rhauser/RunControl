package daq.rc;

import org.omg.CORBA.SystemException;

import rc.CannotExecute;


/**
 * Abstract class for all the exceptions thrown by methods in the Run Control library.
 */
public abstract class RCException extends java.lang.Exception {
    private static final long serialVersionUID = -5008598457609915535L;

    /**
     * Exception thrown when some problem occurs creating or initializing the {@link JItemCtrl}
     */
    public static class ItemCtrlException extends RCException {
        private static final long serialVersionUID = -4842369066581711601L;

        ItemCtrlException(final String reason) {
            super(reason);
        }
        
        ItemCtrlException(final String reason, final Throwable cause) {
            super(reason, cause);
        }
    }
    
    /**
     * Exception thrown in case of failure accessing the online services using the {@link OnlineServices} class
     */
    public static class OnlineServicesFailure extends RCException {
        private static final long serialVersionUID = 5174890860041397309L;

        OnlineServicesFailure(final String reason) {
            super(reason);
        }
        
        OnlineServicesFailure(final String reason, final Throwable cause) {
            super(reason, cause);
        }
    }
    
    /**
     * Exception thrown when user credentials (used to authenticate the command sender) cannot be acquired
     */
    public static class BadUserCredentials extends RCException {
        private static final long serialVersionUID = 2212984966613417356L;

        BadUserCredentials(final String reason) {
            super(reason);
        }
        
        BadUserCredentials(final String reason, final Throwable cause) {
            super(reason, cause);
        }
    }
    
    /**
     * Exception thrown when the reference of a Run Control application cannot be found in IPC
     */
    public static class IPCLookupException extends RCException {
        private static final long serialVersionUID = -2927115382841335162L;
        private final String applicationName;

        /**
         * Constructor
         * 
         * @param applicationName The name of the application
         */
        IPCLookupException(final String applicationName) {
            this(applicationName, null);
        }

        /**
         * Constructor
         * 
         * @param applicationName The name of the application
         * @param cause The cause of this exception
         */
        IPCLookupException(final String applicationName, final Throwable cause) {
            super("The application \"" + applicationName + "\" has not been found in IPC", cause);
            this.applicationName = applicationName;
        }

        /**
         * It returns the name of the application whose reference could not be found in IPC
         * 
         * @return The name of the application whose reference could not be found in IPC
         */
        public String applicationName() {
            return this.applicationName;
        }
    }

    /**
     * Exception thrown when a command object cannot be properly built
     */
    public static class BadCommandException extends RCException {
        private static final long serialVersionUID = 8534896899728903728L;
        private final String cmdName;

        /**
         * Constructor
         * 
         * @param cmd The name of the command that could not be built
         * @param message The error message
         */
        BadCommandException(final String cmd, final String message) {
            this(cmd, message, null);
        }

        /**
         * Constructor
         * 
         * @param cmd The command that could not be built
         * @param message The error message
         * @param cause The cause of this exception
         */
        BadCommandException(final String cmd, final String message, final Throwable cause) {
            super(message, cause);
            this.cmdName = cmd;
        }

        /**
         * It returns the name of the command that could not be built
         * 
         * @return The name of the command that could not be built
         */
        public String getCommand() {
            return this.cmdName;
        }
    }

    /**
     * Exception thrown when the execution of a command sent to a remote Run Control application failed
     */
    public static abstract class ExecutionException extends RCException {
        private static final long serialVersionUID = 8534896899728903728L;
        private final String commandName;
        private final String serverName;

        /**
         * Constructor
         * 
         * @param command The name of the command
         * @param serverName The name of the remote application
         * @param message The error message
         * @param cause The cause of this exception
         */
        ExecutionException(final String command, final String serverName, final String message, final Throwable cause) {
            super(message, cause);
            this.commandName = command;
            this.serverName = serverName;
        }

        /**
         * It returns the name of the command
         * 
         * @return The name of the command
         */
        public String getCommand() {
            return this.commandName;
        }

        /**
         * It returns the name of the application the command was sent to
         * 
         * @return The name of the application the command was sent to
         */
        public String getServerName() {
            return this.serverName;
        }
    }

    /**
     * Exception thrown when a command could not be executed because the remote application was busy This does not mean that a certain
     * command will never be executed when the application is already executing a different command. It just means that a certain command
     * could not be executed when the application is already executing a <b>specific</b> different command.
     */
    public static class ApplicationBusy extends ExecutionException {
        private static final long serialVersionUID = -3047787889933456059L;
        private final CannotExecute cause;

        /**
         * Constructor
         * 
         * @param command The name of the command
         * @param server The name of the application the command was sent to
         * @param cause The cause of failure
         */
        ApplicationBusy(final String command, final String server, final CannotExecute cause) {
            super(command, server, "The command \"" + command + "\" could not be executed by \"" + server + "\" because it was busy", cause);
            this.cause = cause;
        }

        @Override
        public synchronized CannotExecute getCause() {
            return this.cause;
        }
    }

    /**
     * Exception thrown when a command could not be executed because the remote application was in an error state
     */
    public static class ApplicationInError extends ExecutionException {
        private static final long serialVersionUID = 8955960516903734075L;
        private final CannotExecute cause;

        /**
         * Constructor
         * 
         * @param command The name of the command
         * @param server The name of the application the command was sent to
         * @param cause The cause of the failure
         */
        ApplicationInError(final String command, final String server, final CannotExecute cause) {
            super(command, server, "The command \"" + command + "\" could not be executed by \"" + server
                                   + "\" because it was in an error state", cause);
            this.cause = cause;
        }

        @Override
        public synchronized CannotExecute getCause() {
            return this.cause;
        }
    }

    /**
     * Exception thrown when command could not be executed because it was not allowed (e.g., by the Access Management system)
     */
    public static class CommandNotAllowed extends ExecutionException {
        private static final long serialVersionUID = -1318592967022805933L;
        private final CannotExecute cause;

        /**
         * Constructor
         * 
         * @param command The name of the command
         * @param serverName The name of the application the command was sent to
         * @param cause The cause of the failure
         */
        CommandNotAllowed(final String command, final String serverName, final CannotExecute cause) {
            super(command, serverName, "The execution of command \"" + command + " has not been allowed by \"" + serverName + "\": "
                                       + cause.error_message, cause);
            this.cause = cause;
        }

        @Override
        public synchronized CannotExecute getCause() {
            return this.cause;
        }
    }

    /**
     * Exception thrown when command could not be executed because of an unexpected error
     */
    public static class Unexpected extends ExecutionException {
        private static final long serialVersionUID = 1450853409474702246L;
        private final CannotExecute cause;

        /**
         * Constructor
         * 
         * @param command The name of the command
         * @param serverName The name of the application the command was sent to
         * @param cause The cause of the failure
         */
        Unexpected(final String command, final String serverName, final CannotExecute cause) {
            super(command, serverName, "Unexpected error by \"" + serverName + "\" while executing command \"" + command + "\": "
                                       + cause.error_message, cause);
            this.cause = cause;
        }

        @Override
        public synchronized CannotExecute getCause() {
            return this.cause;
        }
    }

    /**
     * Exception thrown when command could not be executed because the remote application could not be contacted
     */
    public static class CORBAException extends ExecutionException {
        private static final long serialVersionUID = 7677196054957892952L;
        private final SystemException cause;

        /**
         * Constructor
         * 
         * @param command The name of the command
         * @param serverName The name of the application the command was sent to
         * @param cause The cause of the failure
         */
        CORBAException(final String command, final String server, final SystemException cause) {
            super(command, server, "Failed contacting \"" + server + "\". Reason: " + cause, cause);
            this.cause = cause;
        }

        @Override
        public synchronized SystemException getCause() {
            return this.cause;
        }
    }

    /**
     * Constructor
     * 
     * @param reason Error message
     */
    RCException(final String reason) {
        super(reason);
    }

    /**
     * Constructor
     * 
     * @param reason Error message
     * @param cause The cause of the failure
     */
    RCException(final String reason, final Throwable cause) {
        super(reason, cause);
    }
}
