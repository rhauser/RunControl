package daq.rc.example;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.cli.ParseException;

import daq.rc.ApplicationState;
import daq.rc.Command.ResynchCmd;
import daq.rc.Command.SubTransitionCmd;
import daq.rc.Command.TransitionCmd;
import daq.rc.Command.UserCmd;
import daq.rc.CommandLineParser;
import daq.rc.CommandLineParser.HelpRequested;
import daq.rc.JControllable;
import daq.rc.JItemCtrl;
import daq.rc.OnlineServices;
import daq.rc.RCException.ItemCtrlException;
import daq.rc.RCException.OnlineServicesFailure;
import ers.Issue;


// Look at the RunControl/bin/jrc_example_application script for an example on how to launch this java application
public class RCExampleApplication {
    // Exception extending ers.Issue in order to report errors during state transitions
    static class TransitionFailure extends Issue {
        private static final long serialVersionUID = 8962663373144988455L;

        public TransitionFailure(final String message) {
            super(message);
        }

        public TransitionFailure(final String message, final Exception reason) {
            super(message, reason);
        }
    }

    // This interface defines the actions the application will execute
    static class MyControllable implements JControllable {
        @Override
        public void publish() throws Issue {
            ers.Logger.log("Publishing...");
        }

        @Override
        public void publishFullStats() throws Issue {
            ers.Logger.log("Publishing full stats...");
        }

        @Override
        public void user(final UserCmd usrCmd) throws Issue {
            ers.Logger.log("Executed user command " + usrCmd.getCommandName());
        }

        @Override
        public void enable(final List<String> components) throws Issue {
            ers.Logger.log("Enabled components " + Arrays.toString(components.toArray()));
        }

        @Override
        public void disable(final List<String> components) throws Issue {
            ers.Logger.log("Disabled components " + Arrays.toString(components.toArray()));
        }

        @Override
        public void onExit(final ApplicationState state) {
            ers.Logger.log("Exiting while in state " + state.name());
        }

        @Override
        public void configure(final TransitionCmd cmd) throws Issue {
            // Let's retrieve some information from the configuration
            try {
                final OnlineServices os = OnlineServices.instance();

                final String dbAppUID = os.getApplication().UID();
                final String appName = os.getApplicationName();

                System.out.println("This application name is " + appName);
                System.out.println("The UID of the corresponding application object in the DB is " + dbAppUID);
            }
            catch(final OnlineServicesFailure ex) {
                throw new TransitionFailure("Cannot retrieve configuration information: " + ex.getMessage(), ex);
            }

            this.logTransition(cmd);
        }

        @Override
        public void connect(final TransitionCmd cmd) throws Issue {
            this.logTransition(cmd);
        }

        @Override
        public void prepareForRun(final TransitionCmd cmd) throws Issue {
            this.logTransition(cmd);
        }

        @Override
        public void stopROIB(final TransitionCmd cmd) throws Issue {
            this.logTransition(cmd);
        }

        @Override
        public void stopDC(final TransitionCmd cmd) throws Issue {
            this.logTransition(cmd);
        }

        @Override
        public void stopHLT(final TransitionCmd cmd) throws Issue {
            this.logTransition(cmd);
        }

        @Override
        public void stopRecording(final TransitionCmd cmd) throws Issue {
            this.logTransition(cmd);
        }

        @Override
        public void stopGathering(final TransitionCmd cmd) throws Issue {
            this.logTransition(cmd);
        }

        @Override
        public void stopArchiving(final TransitionCmd cmd) throws Issue {
            this.logTransition(cmd);
        }

        @Override
        public void disconnect(final TransitionCmd cmd) throws Issue {
            this.logTransition(cmd);
        }

        @Override
        public void unconfigure(final TransitionCmd cmd) throws Issue {
            this.logTransition(cmd);
        }

        @Override
        public void subTransition(final SubTransitionCmd cmd) throws Issue {
            this.logTransition(cmd);
        }

        @Override
        public void resynch(final ResynchCmd cmd) throws Issue {
            this.logTransition(cmd);
        }

        private void logTransition(final TransitionCmd cmd) {
            ers.Logger.log("Executed transition " + cmd.getFSMCommand().name());
        }
    }

    // This is the main
    public static void main(final String[] argv) {
        try {
            // Parse the command line options using the provided utility class
            final CommandLineParser cmdLine = new CommandLineParser(argv);

            // Create the JItemCtrl
            final JItemCtrl ic = new JItemCtrl(cmdLine.getApplicationName(),
                                               cmdLine.getParentName(),
                                               cmdLine.getSegmentName(),
                                               cmdLine.getPartitionName(),
                                               new MyControllable(),
                                               cmdLine.isInteractive());

            // Initialize the run control framework
            ic.init();

            // This blocks: only now the application will be seen up for the process management
            ic.run();

            System.exit(0);
        }
        catch(final HelpRequested ex) {
            // The exception's message contains the help
            // Here some extra information may be printed (i.e., in case of additional specific command line arguments)
            System.out.println(ex.getMessage());

            System.exit(0);
        }
        catch(final ParseException | ItemCtrlException ex) {
            // Some error occurred
            ex.printStackTrace();

            System.exit(-1);
        }
    }
}
