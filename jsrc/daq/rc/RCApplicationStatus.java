package daq.rc;

import java.util.List;

import daq.rc.Command.ApplicationStatusCmd;
import daq.rc.Command.TransitionCmd;


/**
 * Class representing the current status of a Run Control application
 */
public class RCApplicationStatus {
    private final ApplicationStatusCmd statusCmd;

    /**
     * @see daq.rc.CommandSender#getApplicationStatus(String)
     */
    RCApplicationStatus(final ApplicationStatusCmd statusCmd) {
        this.statusCmd = statusCmd;
    }

    /**
     * It returns the name of the application
     * 
     * @return The name of the application
     */
    public String getAppName() {
        return this.statusCmd.getAppName();
    }

    /**
     * It returns the application's FSM state
     * 
     * @return The application's FSM state
     */
    public ApplicationState getAppState() {
        return this.statusCmd.getAppState();
    }

    /**
     * It returns <em>true</em> if the application is in the middle of an FSM transition
     * 
     * @return <em>true</em> if the application is in the middle of an FSM transition
     */
    public boolean isTransitioning() {
        return this.statusCmd.isTransitioning();
    }

    /**
     * It returns the list of reasons why the application is in an error state
     * <p>
     * The returned list is empty if the application is not in an error state
     * 
     * @return The list of reasons why the application is in an error state
     */
    public List<String> getErrorReasons() {
        return this.statusCmd.getErrorReasons();
    }

    /**
     * It returns the list of children causing the application to be in an error state. It is empty if the application is not in an error
     * state or when the error state is caused by an "internal" failure.
     * <p>
     * This has a meaning only for the controller applications.
     * 
     * @return The list of children causing the application to be in an error state
     */
    public List<String> getBadChildren() {
        return this.statusCmd.getBadChildren();
    }

    /**
     * It returns <em>true</em> if the application is busy in executing any command
     * 
     * @return <em>true</em> if the application is busy in executing any command
     */
    public boolean isBusy() {
        return this.statusCmd.isBusy();
    }

    /**
     * It returns <em>true</em> if the application is fully initialized
     * 
     * @return <em>true</em> if the application is fully initialized
     */
    public boolean isFullyInitialized() {
        return this.statusCmd.isFullyInitialized();
    }

    /**
     * It return the duration (in milliseconds) of the last executed FSM transition
     * 
     * @return The duration (in milliseconds) of the last executed FSM transition
     */
    public long getLastTransitionDuration() {
        return this.statusCmd.getLastTransitionDuration();
    }

    /**
     * It returns the last executed transition command
     * 
     * @return The last executed transition command, or null if no command has been executed yet
     */
    public TransitionCmd getLastTransitionCmd() {
        return this.statusCmd.getLastTransitionCmd();
    }

    /**
     * It returns the transition command being currently executed
     * 
     * @return The transition command being currently executed, or null if no transition is on-going
     */
    public TransitionCmd getCurrentTransitionCmd() {
        return this.statusCmd.getCurrentTransitionCmd();
    }

    /**
     * It returns the last executed command (not being a transition command)
     * 
     * @return The last executed command (not being a transition command), or null if no command has been executed yet
     */
    public Command getLastCmd() {
        return this.statusCmd.getLastCmd();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((this.statusCmd == null) ? 0 : this.statusCmd.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if(this == obj) {
            return true;
        }
        if(obj == null) {
            return false;
        }
        if(this.getClass() != obj.getClass()) {
            return false;
        }
        final RCApplicationStatus other = (RCApplicationStatus) obj;
        if(this.statusCmd == null) {
            if(other.statusCmd != null) {
                return false;
            }
        } else if(!this.statusCmd.equals(other.statusCmd)) {
            return false;
        }
        return true;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "RCApplicationStatus [getAppName()=" + this.getAppName() + ", getAppState()=" + this.getAppState() + ", isTransitioning()="
               + this.isTransitioning() + ", getErrorReasons()=" + this.getErrorReasons() + ", getBadChildren()=" + this.getBadChildren()
               + ", isBusy()=" + this.isBusy() + ", isFullyInitialized()=" + this.isFullyInitialized() + ", getLastTransitionDuration()="
               + this.getLastTransitionDuration() + ", getLastTransitionCmd()=" + this.getLastTransitionCmd()
               + ", getCurrentTransitionCmd()=" + this.getCurrentTransitionCmd() + ", getLastCmd()=" + this.getLastCmd() + "]";
    }
}
