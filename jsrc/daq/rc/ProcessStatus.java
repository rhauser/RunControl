package daq.rc;

/**
 * Enumeration for all the statuses a (physical) process may be with respect to Process Management system.
 */
public enum ProcessStatus {
    /**
     * Starting status
     * <p>
     * The parent controller sets this status for the process at the very beginning, when it still does not know whether the process is up
     * or not.
     */
    NOTAV,

    /**
     * A process is in this status when it is no more up but the controller cannot know why (i.e., it may be just exited or failed to
     * start).
     * <p>
     * This may happen when a controller, after a restart, asks the Process Management system about the status of a process and this one is
     * no more up.
     */
    ABSENT,

    /**
     * A process is in this status when the Process Manager system has been asked to start it but the process is not up yet.
     * <p>
     * If the application is configured with an <em>InitTimeout</em> greater than zero, it will stay in this status until the PMG sync
     * notification is done.
     */
    REQUESTED,

    /**
     * This is the status of a successfully started process
     */
    UP,

    /**
     * The process is in this status when the Process Management system has been asked to kill it, but it is still up (i.e., it is going to
     * exit soon).
     */
    TERMINATING,

    /**
     * The process is no more up
     * <p>
     * The process exited spontaneously or because of a signal.
     */
    EXITED,

    /**
     * The process failed to start
     */
    FAILED;
}
