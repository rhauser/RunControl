/*
 * FSMReactions.h
 *
 *  Created on: Jun 11, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/ItemCtrl/FSMReactions.h
 */

#ifndef DAQ_RC_FSMREACTIONS_H_
#define DAQ_RC_FSMREACTIONS_H_

#include "RunControl/Common/RunControlTransitionActions.h"
#include "RunControl/ItemCtrl/ControllableDispatcher.h"

#include <memory>


namespace daq { namespace rc {

class TransitionCmd;
class SubTransitionCmd;
class UserBroadcastCmd;
class ResynchCmd;

/**
 * @brief Class implementing actions to be executed during FSM transitions
 *
 * This class uses the defined daq::rc::ControllableDispatcher in order to let
 * all the defined daq::rc::Controllable items perform the needed actions.
 */
class FSMReactions : public RunControlTransitionActions {
    public:
        /**
         * @brief Constructor
         *
         * @param ctrlList The list of daq::rc::Controllable items performing FSM actions
         * @param dispatcher The daq::rc::ControllableDispatcher used to dispatch commands to the
         *                   daq::rc::Controllable items
         */
        FSMReactions(const ControllableDispatcher::ControllableList& ctrlList,
                     const std::shared_ptr<ControllableDispatcher>& dispatcher);

        void configure(const TransitionCmd& cmd) override;
        void connect(const TransitionCmd& cmd) override;
        void prepareForRun(const TransitionCmd& cmd) override;
        void stopROIB(const TransitionCmd& cmd) override;
        void stopDC(const TransitionCmd& cmd) override;
        void stopHLT(const TransitionCmd& cmd) override;
        void stopRecording(const TransitionCmd& cmd) override;
        void stopGathering(const TransitionCmd& cmd) override;
        void stopArchiving(const TransitionCmd& cmd) override;
        void disconnect(const TransitionCmd& cmd) override;
        void unconfigure(const TransitionCmd& cmd) override;
        void subTransition(const SubTransitionCmd& cmd) override;
        void resynch(const ResynchCmd& cmd) override;

        /**
         * @brief It executes the defined user command on the daq::rc::Controllable items
         *
         * @param cmd The command object
         */
        void userBroadcast(const UserBroadcastCmd& cmd) override;

    private:
        const ControllableDispatcher::ControllableList m_ctrl_list;
        const std::shared_ptr<ControllableDispatcher> m_ctrl_dispatcher;
};

}}


#endif /* DAQ_RC_FSMREACTIONS_H_ */
