/*
 * ItemCtrlStatus.h
 *
 *  Created on: Jun 11, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/ItemCtrl/ItemCtrlStatus.h
 */

#ifndef DAQ_RC_ITEMCTRLSTATUS_H_
#define DAQ_RC_ITEMCTRLSTATUS_H_

#include "RunControl/Common/Clocks.h"

#include <string>
#include <vector>
#include <memory>


namespace daq { namespace rc {

enum class FSM_STATE : unsigned int;
class TransitionCmd;
class RunControlBasicCommand;

/**
 * @brief Class collecting information about the status of the daq::rc::ItemCtrl
 *
 * @note This class is not thread safe. In the current implementation the consistency of
 *       this class is guarded by da::rc::ItemCtrlImpl.
 */
class ItemCtrlStatus {
    public:
        /**
         * @brief Constructor
         */
        ItemCtrlStatus();

        /**
         * @brief Default copy constructor
         */
        ItemCtrlStatus(const ItemCtrlStatus&) = default;

        /**
         * @brief Default copy assignment operator
         */
       ItemCtrlStatus& operator=(const ItemCtrlStatus&) = default;

       /**
        * @brief Destructor
        */
        ~ItemCtrlStatus();

        /**
         * @brief It returns the current FSM state
         * @return The current FSM state
         */
        FSM_STATE getCurrentState() const;

        /**
         * @brief It returns @em true if an FSM transition is being executed
         * @return @em true if an FSM transition is being executed
         */
        bool isTransitioning() const;

        /**
         * @brief It returns @em true if some commands or an FSM transition is being executed
         * @return @em true if some commands or an FSM transition is being executed
         */
        bool isBusy() const;

        /**
         * @brief It returns @em true if the application is in an error state
         * @return @em true if the application is in an error state
         */
        bool isError() const;

        /**
         * @brief It returns @em true if the starting phase has been completed (i.e., the application has reached the proper
         *        FSM state)
         * @return @em true if the starting phase has been completed
         */
        bool isFullyInitialized() const;

        /**
         * @brief It returns the reason(s) why the application is in an error state
         * @return The reason(s) why the application is in an error state
         */
        std::vector<std::string> getErrorReasons() const;

        /**
         * @brief It returns the time (in milliseconds) needed to perform the last FSM transition
         * @return The time (in milliseconds) needed to perform the last FSM transition
         */
        infotime_t getLastTransitionTime() const;

        /**
         * @brief It returns the time tag (in nanoseconds) of the last information update
         *
         * @note If interested in the wall time, then look at daq::rc::ItemCtrlStatus::getInfoWallTime
         *
         * @return The time tag (in nanoseconds) of the last information update
         */
        infotime_t getInfoTimeTag() const;

        /**
         * @brief It returns the wall time tag (in nanoseconds) of the last information update
         *
         * @note If interested in a monotonic time tag, then look at daq::rc::ItemCtrlStatus::getInfoTimeTag
         *
         * @return The wall time tag (in nanoseconds) of the last information update
         */
        long long getInfoWallTime() const;

        /**
         * @brief It returns the last executed transition command
         * @return The last executed transition command (it can be @em nullptr if
         *         no transition has been executed yet)
         */
        std::shared_ptr<TransitionCmd> getLastTransitionCmd() const;

        /**
         * @brief It returns the transition command currently being executed
         * @return The transition command currently being executed (it can be @em nullptr if
         *         no transition is on-going)
         */
        std::shared_ptr<TransitionCmd> getCurrentTransitionCmd() const;

        /**
         * @brief It returns the last executed command (not transition); it can be @em nullptr
         * @return The last executed command (not transition)
         */
        std::shared_ptr<RunControlBasicCommand> getLastCmd() const;

        /**
         * @brief It sets the current FSM state
         * @param state The current FSM state
         */
        void setCurrentState(FSM_STATE state);

        /**
         * @brief It sets whether an FSM transition is on-going or not
         * @param transitioning @em true if an FSM transition is being executed
         */
        void setTransitioning(bool transitioning);

        /**
         * @brief It sets whether a command (not transition) is being executed or not
         * @param executingCmd @em true if a command (not transition) is being executed
         */
        void setExecutingCmd(bool executingCmd);

        /**
         * @brief It sets whether the application is busy performing the regular publishing or not
         * @param publishing @em true if the application is busy performing the regular publishing
         */
        void setPublishing(bool publishing);

        /**
         * @brief It sets whether the application is busy performing the regular statistics publishing or not
         * @param publishing @em true if the application is busy performing the regular statistics publishing
         */
        void setPublishingStats(bool publishing);

        /**
         * @brief It sets the reasons why the application is in error state
         *
         * If @em errorReasons is empty, then the error state is cleared
         *
         * @param errorReasons The reasons why the application is in error state
         */
        void setErrorReasons(const std::vector<std::string>& errorReasons);

        /**
         * @brief It sets the time (in milliseconds) takes to perform the last FSM transition
         * @param time The time (in milliseconds) takes to perform the last FSM transition
         */
        void setLastTransitionTime(infotime_t time);

        /**
         * @brief It sets the last executed transition command
         * @param trCmd The last executed transition command
         */
        void setLastTransitionCmd(const std::shared_ptr<TransitionCmd>& trCmd);

        /**
         * @brief It sets the currently executed transition command (it can be @em nullptr) if
         *        there is no transition on-going
         * @param trCmd The currently executed transition command
         */
        void setCurrentTransitionCmd(const std::shared_ptr<TransitionCmd>& trCmd);

        /**
         * @brief It sets the last executed command (not transition)
         * @param cmd The last executed command (not transition)
         */
        void setLastCmd(const std::shared_ptr<RunControlBasicCommand>& cmd);

        /**
         * @brief It sets whether the starting phase has been completed or not
         * @param done @em true if the starting phase has been completed
         */
        void setFullyInitialized(bool done);

    protected:
        /**
         * @@brief It updates the time stamp tags any time an information item is modified
         */
        void updateTimeStamps();

        /**
         * @brief It returns the current wall (system) time (in nanoseconds)
         * @return The current wall (system) time (in nanoseconds)
         */
        static long long wallTime();

        /**
         * @brief It returns a monotonic time (in nanoseconds)
         * @return A monotonic time (in nanoseconds)
         */
        static infotime_t timeTag();

    private:
        FSM_STATE m_fsm_state;
        bool m_transitioning;
        bool m_executing_cmd;
        bool m_publishing;
        bool m_publishing_stats;
        std::vector<std::string> m_error_reasons;
        infotime_t m_last_tr_time;
        infotime_t m_time_tag;
        long long m_wall_time;
        std::shared_ptr<TransitionCmd> m_last_tr_cmd;
        std::shared_ptr<TransitionCmd> m_current_tr_cmd;
        std::shared_ptr<RunControlBasicCommand> m_last_cmd;
        bool m_fully_init;
};

}}

#endif /* DAQ_RC_ITEMCTRLSTATUS_H_ */
