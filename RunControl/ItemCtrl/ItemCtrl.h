/*
 * ItemCtrl.h
 *
 *  Created on: Jun 10, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/ItemCtrl/ItemCtrl.h
 */

#ifndef DAQ_RC_ITEMCTRL_H_
#define DAQ_RC_ITEMCTRL_H_

#include "RunControl/ItemCtrl/ControllableDispatcher.h"

#include <string>
#include <memory>


namespace daq { namespace rc {

class ItemCtrlImpl;
class Controllable;
class CmdLineParser;

/**
 * @brief Entry point class for (leaf) applications that can be steered by the Run Control system.
 *
 * The Run Control application is ready to receive commands from the Run Control system only after the ItemCtrl::init() and
 * ItemCtrl::run() methods are called (in the reported order).
 *
 * @note This class uses the <em>PMG sync</em> mechanism in order to notify the Process Management system when the application is ready.
 *       Only at that moment the physical process will be reported as up and running. The <em>PMG sync</em> is done once
 *       ItemCtrl::run() is called.
 *       \n
 * @note This class installs a signal handler for the SIGINT and SIGTERM signals, allowing the application to gracefully exit
 *       when one of those commands is received.
 *       \n
 * @remark All the constructors of this class perform a proper initialization of the daq::rc::OnlineServices class.
 *         \n
 * @remark When the ItemCtrl is started in interactive mode, it is suggested to use the daq::rc::CmdLineParser class for
 *         parsing the command line options. In this case the daq::rc::CmdLineParser will take care of properly setting
 *         the environment needed by the interactive mode.
 *         \n
 * @attention Because of the usage of the <em>PMG sync</em> mechanism, any Run Control application has to be described in the OKS database
 *            with an @em InitTimeout value grater than zero. The timeout can be tuned taking into account the amount of work to be done
 *            before ItemCtrl::run() is called.
 *
 * @see daq::rc::OnlineServices::initialize
 * @see <a href="https://gitlab.cern.ch/atlas-tdaq-software/RunControl/blob/master/bin/rc_example_application.cxx">This link</a>
 *      to have a complete example on how to use this class.
 */
class ItemCtrl {
    public:
        /**
         * @brief Constructor.
         *
         * @param name The name of this Run Control application (<em>i.e.</em>, the same that will be used for IPC publication)
         * @param parent The name of the parent segment's controller
         * @param segment The name of the segment this application belongs to
         * @param partition The name of the partition
         * @param ctrl A daq::rc::Controllable instance (it cannot be @em null)
         * @param isInteractive @em true if the application should run in interactive way (default is @em false)
         * @param dispatcher A controllable's actions dispatcher (default is daq::rc::DefaultDispatcher)
         *
         * @throws daq::rc::ItemCtrlInitializationFailed Errors creating the internal data structures
         */
        ItemCtrl(const std::string& name,
                 const std::string& parent,
                 const std::string& segment,
                 const std::string& partition,
                 const std::shared_ptr<Controllable>& ctrl,
                 bool isInteractive = false,
                 const std::shared_ptr<ControllableDispatcher>& dispatcher = std::shared_ptr<ControllableDispatcher>(new DefaultDispatcher()));

        /**
         * @overload ItemCtrl(const CmdLineParser&, const std::shared_ptr<Controllable>&, const std::shared_ptr<ControllableDispatcher>& dispatcher)
         *
         * @param cmdLine Command line parser object
         * @param ctrl A daq::rc::Controllable instance (it cannot be @em null)
         * @param dispatcher A controllable's actions dispatcher (default is daq::rc::DefaultDispatcher)
         *
         * @throws daq::rc::ItemCtrlInitializationFailed Errors creating the internal data structures
         */
        ItemCtrl(const CmdLineParser& cmdLine,
                 const std::shared_ptr<Controllable>& ctrl,
                 const std::shared_ptr<ControllableDispatcher>& dispatcher = std::shared_ptr<ControllableDispatcher>(new DefaultDispatcher()));

        /**
         * @brief Constructor.
         *
         * @param name The name of this Run Control application (<em>i.e.</em>, the same that will be used for IPC publication)
         * @param parent The name of the parent segment's controller
         * @param segment The name of the segment this application belongs to
         * @param partition The name of the partition
         * @param ctrlList A list of daq::rc::Controllable instances (none of them can be @em null)
         * @param isInteractive @em true if the application should run in interactive way (default is @em false)
         * @param dispatcher A controllable's actions dispatcher (default is daq::rc::DefaultDispatcher)
         *
         * @throws daq::rc::ItemCtrlInitializationFailed Errors creating the internal data structures
         */
        ItemCtrl(const std::string& name,
                 const std::string& parent,
                 const std::string& segment,
                 const std::string& partition,
                 const ControllableDispatcher::ControllableList& ctrlList,
                 bool isInteractive = false,
                 const std::shared_ptr<ControllableDispatcher>& dispatcher = std::shared_ptr<ControllableDispatcher>(new DefaultDispatcher()));

        /**
         * @overload ItemCtrl(const CmdLineParser&, const ControllableDispatcher::ControllableList&, const std::shared_ptr<ControllableDispatcher>&)
         *
         * @param cmdLine Command line parser object
         * @param ctrlList A list of daq::rc::Controllable instances (none of them can be @em null)
         * @param dispatcher A controllable's actions dispatcher (default is daq::rc::DefaultDispatcher)
         *
         * @throws daq::rc::ItemCtrlInitializationFailed Errors creating the internal data structures
         */
        ItemCtrl(const CmdLineParser& cmdLine,
                 const ControllableDispatcher::ControllableList& ctrlList,
                 const std::shared_ptr<ControllableDispatcher>& dispatcher = std::shared_ptr<ControllableDispatcher>(new DefaultDispatcher()));


        /**
         * @brief Copy constructor: deleted.
         */
        ItemCtrl(const ItemCtrl&) = delete;

        /**
         * @brief Default move constructor.
         */
        ItemCtrl(ItemCtrl&&) = default;

        /**
         * @brief Copy assignment operator: deleted.
         */
        ItemCtrl& operator=(const ItemCtrl&) = delete;

        /**
         * @brief Default move assignment operator.
         */
        ItemCtrl& operator=(ItemCtrl&&) = default;

        /**
         * @brief Destructor.
         */
        ~ItemCtrl();

        /**
         * @brief It initializes the Run Control framework (<em>i.e.</em>, the IPC system is initialized and
         *        the IPC publication is done).
         *
         * @note The process will not be up for the Process Management system until ItemCtrl::run() is called.
         *
         * @throws daq::rc::ItemCtrlInitializationFailed The initialization failed (this shall be considered a @em FATAL error)
         */
        void init();

        /**
         * @brief This method should be called when this Run Control application is ready to start its operations
         *        (<em>i.e.</em>, it is ready to receive and execute external commands).
         *
         * At this stage the Process Management system will report this process as up and running.
         *
         * @pre ItemCtrl::init() has been called
         *
         * @warning This method blocks and will return only when this application is asked to exit via a call to ItemCtrl::shutdown() or
         *          it receives a SIGINT or SIGTERM signal.
         */
        void run();

        /**
         * @brief It asks this application to exit.
         *
         * @post ItemCtrl::run() returns
         */
        void shutdown();

    private:
        std::unique_ptr<ItemCtrlImpl> m_impl;
};

}}


#endif /* DAQ_RC_ITEMCTRL_H_ */
