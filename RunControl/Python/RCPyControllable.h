#ifndef DAQ_RC_ITEMCTRL_RCPYCONTROLLABLE_H_
#define DAQ_RC_ITEMCTRL_RCPYCONTROLLABLE_H_

/**
 * @file RunControl/Python/RCPyControllable.h
 */

// Include this first in order to avoid compilation
// warning on slc6 about "_XOPEN_SOURCE redefined"
#include <boost/python.hpp>

#include "RunControl/Common/Controllable.h"
#include "RunControl/FSM/FSMStates.h"

#include <string>
#include <vector>

#include <Python.h>

namespace daq { namespace rc {

class PyToolkit;
class TransitionCmd;
class ResynchCmd;
class UserCmd;
class SubTransitionCmd;

class pyControllable : public daq::rc::Controllable {
public:
    ~pyControllable() noexcept;

    void configure(const daq::rc::TransitionCmd&);
    void connect(const daq::rc::TransitionCmd&);
    void prepareForRun(const daq::rc::TransitionCmd&);

    void stopROIB(const daq::rc::TransitionCmd&);
    void stopDC(const daq::rc::TransitionCmd&);
    void stopHLT(const daq::rc::TransitionCmd&);
    void stopRecording(const daq::rc::TransitionCmd&);
    void stopGathering(const daq::rc::TransitionCmd&);
    void stopArchiving(const daq::rc::TransitionCmd&);

    void disconnect(const daq::rc::TransitionCmd&);
    void unconfigure(const daq::rc::TransitionCmd&);
    void subTransition(const daq::rc::SubTransitionCmd&);
    void resynch(const daq::rc::ResynchCmd&);

    void publish();
    void publishFullStats();
    void user(const daq::rc::UserCmd&);
    void disable(const std::vector<std::string>&);
    void enable(const std::vector<std::string>&);
    void onExit(daq::rc::FSM_STATE fsm_state) noexcept;

private:
    friend class PyToolkit;
    pyControllable(const std::string& modName,
                   const std::string& className);
    
    std::string parse_python_exception();
    
    boost::python::object* baseClass;
    daq::rc::Controllable* _objectHandle;
};

}}  //daq::rc

#endif
