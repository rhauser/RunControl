/*
 * Controller.h
 *
 *  Created on: Dec 5, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/Controller.h
 */

#ifndef DAQ_RC_CONTROLLER_H_
#define DAQ_RC_CONTROLLER_H_

#include <string>
#include <memory>

namespace daq { namespace rc {

class RunController;
class UserRoutines;
class CmdLineParser;

/**
 * @brief Class representing a segment's controller.
 *
 * A segment's controller can be customized passing to it an instance of the daq::rc::UserRoutines class whose actions
 * will be executed before a transition is dispatched and propagated to child applications.
 *
 * Running a controller is straightforward: after the creation of a Controller object, it is enough to call Controller::init() and
 * then Controller::run() to have a working controller.
 *
 * @note The controller uses the <em>PMG sync</em> mechanism in order to notify the Process Management system when it is ready.
 *       Only at that moment the physical process will be reported as up and running. The <em>PMG sync</em> is done once
 *       Controller::run() is called.
 *       \n
 * @note This class installs a signal handler for the SIGINT and SIGTERM signals, allowing the controller to gracefully exit
 *       when one of those commands is received.
 *       \n
 * @remark All the constructors of this class perform a proper initialization of the daq::rc::OnlineServices class.
 *
 * @attention Because of the usage of the <em>PMG sync</em> mechanism, any custom controller has to be described in the OKS database
 *            with an @em InitTimeout value grater than zero. The timeout can be tuned taking into account the amount of work to be done
 *            before Controller::run() is called.
 *
 * @see daq::rc::OnlineServices::initialize
 * @see <a href="https://gitlab.cern.ch/atlas-tdaq-software/RunControl/blob/master/bin/rc_controller.cxx">This link</a> to have an example on how to use this class.
 */
class Controller {
    public:
        /**
         * @brief Constructor.
         *
         * @param name The name of this segment's controller (that is the name published in IPC)
         * @param parent The name of the parent of this segment's controller
         * @param partition The name of the partition this controller runs in
         * @param segment The name of the segment this controller belongs to (<em>i.e.</em>, the segment it controls)
         * @param userActions daq::rc::UserRoutines User actions (cannot be @em null)
         *
         * @throws daq::rc::ControllerInitializationFailed The controller cannot be initialized
         */
        Controller(const std::string& name,
                   const std::string& parent,
                   const std::string& partition,
                   const std::string& segment,
                   const std::shared_ptr<UserRoutines>& userActions);

        /**
         * @overload Controller(const CmdLineParser& cmdLine, const std::shared_ptr<UserRoutines>& userActions)
         *
         * @param cmdLine Command line parser object
         * @param userActions User actions (cannot be @em null)
         *
         * @throws daq::rc::ControllerInitializationFailed The controller cannot be initialized
         */
        Controller(const CmdLineParser& cmdLine, const std::shared_ptr<UserRoutines>& userActions);

        /**
         * @brief Constructor.
         *
         * Use this constructor when no daq::rc::UserRoutines are needed.
         *
         * @param name The name of this segment's controller (that is the name published in IPC)
         * @param parent The name of the parent of this segment's controller
         * @param partition The name of the partition this controller runs in
         * @param segment The name of the segment this controller belongs to (<em>i.e.</em>, the segment it controls)
         *
         * @throws daq::rc::ControllerInitializationFailed The controller cannot be initialized
         */
        Controller(const std::string& name,
                   const std::string& parent,
                   const std::string& partition,
                   const std::string& segment);

        /**
         * @overload Controller(const CmdLineParser& cmdLine)
         *
         * @param cmdLine Command line parser object
         *
         * @throws daq::rc::ControllerInitializationFailed The controller cannot be initialized
         */
        Controller(const CmdLineParser& cmdLine);

        /**
         * @brief Copy constructor: deleted.
         */
        Controller(const Controller&) = delete;

        /**
         * @brief Default move constructor.
         */
        Controller(Controller&&) = default;

        /**
         * @brief Copy assignment operator: deleted.
         */
        Controller& operator=(const Controller&) = delete;

        /**
         * @brief Default move assignment operator.
         */
        Controller& operator=(Controller&&) = default;

        /**
         * @brief Destructor.
         */
        ~Controller();

        /**
         * @brief It initializes the controller (<em>i.e.</em>, internal structures are initialized, the configuration is read,
         *        the IPC publication is done).
         *
         * @note The process will not be up for the Process Management system until Controller::run() is called.
         *
         * @throws daq::rc::ControllerInitializationFailed The controller's initialization failed (this shall be considered as a @em FATAL error)
         */
        void init();

        /**
         * @brief When this method is called, it means that the controller is ready and it will start its operations.
         *
         * At this stage the Process Management system will report this process as up and running.
         *
         * @pre Controller::init() has been called
         *
         * @warning This method blocks and will return only when this controller is asked to exit via a call to Controller::shutdown() or
         *          it receives a SIGINT or SIGTERM signal.
         */
        void run();

        /**
         * @brief It asks this controller to exit.
         *
         * @post Controller::run() returns
         */
        void shutdown();

    private:
        std::unique_ptr<RunController> m_impl;
};

}}

#endif /* DAQ_RC_CONTROLLER_H_ */
