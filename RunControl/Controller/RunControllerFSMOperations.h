/*
 * RunControllerFSMOperations.h
 *
 *  Created on: Jan 9, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/RunControllerFSMOperations.h
 */

#ifndef DAQ_RC_RUNCONTROLLERFSMOPERATIONS_H_
#define DAQ_RC_RUNCONTROLLERFSMOPERATIONS_H_

#include "RunControl/Common/RunControlTransitionActions.h"
#include "RunControl/Common/CommandSender.h"

#include <memory>
#include <atomic>

namespace daq { namespace rc {

class ApplicationList;
class ApplicationController;
class DVSController;
class TransitionCmd;
class UserBroadcastCmd;
class SubTransitionCmd;
class ResynchCmd;
class ThreadPool;
class InformationPublisher;

/**
 * @brief Class implementing actions executed by a controller during FSM transitions
 *
 * Note that some secondary actions are executed by the RunController and the RootController
 * in call-back functions called directly by the FSM (<em>e.g.,</em> when a transition is initiated or
 * completed). Here the bulk actions are executed and they deal mainly with command propagation
 * to child applications and proper start/stop of applications.
 *
 * Note also that the RootController implements some UserRoutines.
 *
 * All the transition methods but RunControllerFSMOperations::reloadDB are implemented calling
 * RunControllerFSMOperations::performTransition.
 *
 * @attention Any thrown exception will cause the transition to abort and will be intercepted
 *            by the FSM that will call the corresponding call-back function (daq::rc::RunController::fsmTransitionException
 *            and daq::rc::RootController::fsmTransitionException).
 *
 * @see daq::rc::RootRoutines
 * @see daq::rc::RunController
 * @see daq::rc::RootController
 * @see daq::rc::RunController::fsmTransitioning
 * @see daq::rc::RunController::fsmTransitionDone
 * @see daq::rc::RootController::fsmTransitioning
 * @see daq::rc::RootController::fsmTransitionDone
 */
class RunControllerFSMOperations : public RunControlTransitionActions {
    public:
        /**
         * @brief Constructor
         *
         * @param appCtrl Pointer to the daq::rc::ApplicationController (used to start/stop processes)
         * @param dvsCtrl Pointer to the daq::rc::DVSController (notified when a reload of the database transition happens)
         * @param appList Pointer to the daq::rc::ApplicationList (updated when a reload of the database transition happens)
         * @param infoPublisher Pointer to the daq::rc::InformationPublisher
         *
         * @throws daq::rc::ThreadPoolError Errors creating the internal thread pool used for command dispatching
         */
        RunControllerFSMOperations(const std::shared_ptr<ApplicationController>& appCtrl,
                                   const std::shared_ptr<DVSController>& dvsCtrl,
                                   const std::shared_ptr<ApplicationList>& appList,
                                   const std::shared_ptr<InformationPublisher>& infoPublisher);

        /**
         * @brief Destructor
         */
        virtual ~RunControllerFSMOperations() noexcept;


        /**
         * @brief Copy constructor: deleted
         */
        RunControllerFSMOperations(const RunControllerFSMOperations&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
        RunControllerFSMOperations& operator=(const RunControllerFSMOperations&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        RunControllerFSMOperations(RunControllerFSMOperations&&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        RunControllerFSMOperations& operator=(RunControllerFSMOperations&&) = delete;

        virtual void enterFSM(const TransitionCmd&) override;
        virtual void initialize(const TransitionCmd&) override;
        virtual void shutdown(const TransitionCmd&) override;

        virtual void configure(const TransitionCmd&) override;
        virtual void connect(const TransitionCmd&) override;
        virtual void prepareForRun(const TransitionCmd&) override;
        virtual void stopROIB(const TransitionCmd&) override;
        virtual void stopDC(const TransitionCmd&) override;
        virtual void stopHLT(const TransitionCmd&) override;
        virtual void stopRecording(const TransitionCmd&) override;
        virtual void stopGathering(const TransitionCmd&) override;
        virtual void stopArchiving(const TransitionCmd&) override;
        virtual void disconnect(const TransitionCmd&) override;
        virtual void unconfigure(const TransitionCmd&) override;

        virtual void subTransition(const SubTransitionCmd&) override;

        virtual void userBroadcast(const UserBroadcastCmd&) override;

        /**
         * @brief Method implementing all the actions to be performed during a reload of the database
         *
         * During a reload of the database the following actions are done (in order):
         * @li The new application list is created;
         * @li Removed, added and kept applications are computed;
         * @li The removed application are stopped;
         * @li The new and old application lists are swapped;
         * @li The daq::rc::DVSController is asked to reload the database as well;
         * @li The new added applications are started.
         *
         * @note In the current implementation the reload of the database is allowed only in the
         *       daq::rc::FSM_STATE::NONE state: it means this action will be executed by the
         *       RootController only. Nevertheless is has been decided to implement the transition's
         *       action here for uniformity and to add some flexibility if, in future, the database
         *       reload is allowed in states other than NONE. Actions that will be anyway done only by
         *       the RootController have been added to the daq::rc::RootController class (look at the
         *       FSM call-back methods)
         *
         * @param cmd The command object representing the FSM transition to be done
         *
         * @throws daq::rc::DBReloadFailure Something went wrong during the reload of the database
         *
         * @see daq::rc::RootController::configCallback
         * @see daq::rc::ApplicationList::swap
         */
        virtual void reloadDB(const TransitionCmd& cmd) override;

        virtual void resynch(const ResynchCmd&) override;

        /**
         * @brief It interrupts any transition activity
         *
         * @attention The interrupted state will be removed only when this method is properly
         *            called again
         *
         * @param shouldInterrupt @em true if the transition actions should be interrupted
         */
        virtual void interrupt(bool shouldInterrupt) override;

    protected:
        /**
         * @brief Method implementing actions to be takes during an FSM transition
         *
         * During a transition:
         * @li The daq::rc::ApplicationController is asked to stop all the needed applications;
         * @li The daq::rc::ApplicationController is asked to start all the needed applications;
         * @li The transition command is propagated to all the child Run Control and/or controller applications;
         * @li Started applications are waited to reach the proper state;
         * @li Applications that received the transition command are waited to reach the proper state.
         *
         * @param cmd The command representing the transition to be done
         * @param toApplications Whether or not the transition should be propagated to child Run Control applications
         * @param toControllers Whether or not the transition should be propagated to child controllers
         *
         * @throws daq::rc::DependencyCycle Cycle in application's start or shutdown dependencies (hint of bad configuration)
         * @throws daq::rc::BadCommand The transition command is corrupted or mal-formed
         * @throws daq::rc::TransitionInterrupted The transition has been interrupted
         *
         * @note Commands are not propagated for the RunControllerFSMOperations::enterFSM and
         *       RunControllerFSMOperations::initialize methods (where applications are just started)
         */
        void performTransition(const TransitionCmd& cmd, bool toApplications = true, bool toControllers = true);

        /**
         * @brief It checks the interrupted state
         *
         * @param cmd The transition command corresponding to the on-going transition
         *
         * @throws daq::rc::TransitionInterrupted Transition activities are interrupted
         */
        void interrupted(const TransitionCmd& cmd) const;

    private:
        std::atomic<bool> m_is_interrupted;
        const std::shared_ptr<ApplicationController> m_app_ctrl;
        const std::shared_ptr<DVSController> m_dvs_ctrl;
        const std::shared_ptr<ApplicationList> m_app_list;
        const std::shared_ptr<InformationPublisher> m_info_publisher;
        CommandSender m_cmd_sender;
        const std::unique_ptr<ThreadPool> m_tp;
};

}}

#endif /* DAQ_RC_RUNCONTROLLERFSMOPERATIONS_H_ */
