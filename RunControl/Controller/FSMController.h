/*
 * FSMController.h
 *
 *  Created on: Aug 31, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/FSMController.h
 */

#ifndef DAQ_RC_FSMCONTROLLER_H_
#define DAQ_RC_FSMCONTROLLER_H_

#include "RunControl/FSM/FSM.h"
#include "RunControl/FSM/FSMDefs.h"
#include "RunControl/Common/Algorithms.h"

#include <boost/signals2/connection.hpp>

#include <memory>
#include <functional>
#include <tuple>

namespace std {
    class exception;
}

namespace daq { namespace rc {

class RunControlTransitionActions;
class UserRoutines;
class FSMActions;
class TransitionCmd;
enum class FSM_STATE : unsigned int;
enum class FSM_COMMAND : unsigned int;

/**
 * @brief Component bridging the controller and the FSM
 *
 * The controller does not interact directly with the FSM, but uses an instance of this class to do it.
 * It is possible to retrieve information from the FSM in two different ways:
 * @li push - The @em register methods can be used to register call-backs so that clients are properly notified about FSM activities;
 * @li pull - The FSMController::isBusy and FSMController::status methods can be used to have information about the FSM.
 *
 * @note All the call-back methods are never called concurrently.
 *
 * @note When the FSM is asked to perform state transitions via the FSMController::makeTransition or FSMController::goToState, exceptions
 *       thrown by the executed actions are not propagated to the caller that can receive notification only through the call-back
 *       registered via the FSMController::registerOnTransitionExceptionCallback method.
 *
 * @see daq::rc::FSM - The implementation of the FSM
 */
class FSMController {
	public:
        /**
         * @brief Constructor
         *
         * @note The FSM is not initiated yet. The FSMController cannot be asked to interact with the FSM until
         *       FSMController::init is called
         *
         * @param rc The controller's actions to be executed during FSM transitions
         * @param ur The daq::rc::UserRoutines to be executed during FSM transitions
         */
		FSMController(const std::shared_ptr<RunControlTransitionActions>& rc, const std::shared_ptr<UserRoutines>& ur);

		/**
		 * @brief Destructor
		 */
		~FSMController();

        /**
         * @brief Copy constructor: deleted
         */
		FSMController(const FSMController&) = delete;

        /**
         * @brief Move constructor: deleted
         */
		FSMController(FSMController&&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
		FSMController& operator=(const FSMController&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
		FSMController& operator=(FSMController&&) = delete;

		/**
		 * @brief It creates and initializes the FSM.
		 *
		 * @tparam InitialState The FSM initial state
		 */
		template<FSM_STATE InitialState>
		void init() {
		    namespace ph = std::placeholders;

			m_fsm.reset(new FSM<InitialState>(m_rc_actions, m_user_functions));

			m_fsm->registerOnFSMBusyCallback(std::bind(&FSMController::busy, this, ph::_1));
			m_fsm->registerOnTransitioningCallback(std::bind(&FSMController::transitioning, this, ph::_1));
			m_fsm->registerOnTransitionDoneCallback(std::bind(&FSMController::transitionDone, this, ph::_1));
			m_fsm->registerOnNoTransitionCallback(std::bind(&FSMController::noTransition, this, ph::_1));
			m_fsm->registerOnTransitionExceptionCallback(std::bind(&FSMController::transitionException, this, ph::_1, ph::_2));
		}

		/**
		 * @brief It starts the FSM operations (the FSM enters its initial state)
		 *
		 * This method should be called after the FSMController::init and before asking the FSM to perform
		 * any state transition.
		 *
		 * @note To be called only once
		 *
		 * @param isRestart @em true if the controller is restarting after a crash
		 */
		void start(bool isRestart);

		/**
		 * @brief It interrupts the FSM operations and any on-going transition
		 *
		 * The interrupted state will persist until this method is called again with @em false as a parameter
		 *
		 * @param shouldInterrupt @em true if the FSM operations should be interrupted
		 */
		void interruptTransition(bool shouldInterrupt);

		/**
		 * @brief It asks the FSM to perform the transition described in the @em cmd command
		 *
		 * @note This method returns only once the transition has been completed (unless the
		 *       FSM is already busy; in that case this method returns immediately)
		 *
		 * @note If the @em cmd command is valid but it does not correspond to any transition
		 *       given the current FSM state, then no exceptions are thrown but the caller can
		 *       receive a notification via the call-back eventually registered with
		 *       FSMController::registerOnNoTransitionCallback
		 *
		 * @attention @em cmd must at least define the corresponding daq::rc::FSM_COMMAND
		 *
		 * @param cmd The command describing the transition to be done
		 * @return @em false if the transition cannot be executed because the FSM is already busy
		 *
		 * @throws daq::rc::TransitionNotAllowed The corresponding FSM command is not valid (<em>i.e.</em>, it is daq::rc::FSM_COMMAND::INIT_FSM
		 *                                       or daq::rc::FSM_COMMAND::STOP_FSM)
		 * @throws daq::rc::EventNotProcessed The command is rejected by the FSM
		 * @throws daq::rc::BadCommand The transition command described by @em cmd is not a valid daq::rc::FSM_COMMAND
		 *
		 * @see FSMController::isCommandValid
		 */
		bool makeTransition(const std::shared_ptr<TransitionCmd>& cmd);

		/**
		 * @brief It asks the FSM to perform the needed state transitions to reach the @em state final state
		 *
		 * @param state The final state to reach
		 * @param isRestart @em true if the controller is restarting after a crash
		 * @param st Sub-transitions that should be executed
		 *
		 * @throws daq::rc::TransitionInterrupted The FSM operations have been interrupted
		 * @throws daq::rc::InvalidState The FSM state @em state is not valid given the FSM initial state
		 *                               (e.g., the daq::rc::FSM_STATE::NONE state is not valid for an FSM
		 *                               whose initial state is daq::rc::FSM_STATE::INITIAL)
		 * @throws daq::rc::FSMPathError The @em state state cannot be reached given the current FSM state
		 * @throws Any exception coming from actions executed during FSM transitions
		 *
		 * @see daq::rc::FSMGraph
		 */
		void goToState(FSM_STATE state, bool isRestart, const Algorithms::SubtransitionMap& st = {{}});

		/**
		 * @brief It returns information about the status of the FSM
		 *
		 * @return A tuple with (in order): the current FSM state, the FSM destination state
		 *         (equal to the current state if not transitioning), the last executed transition
		 */
        std::tuple<FSM_STATE, FSM_STATE, FSM_COMMAND> status() const;

        /**
         * @brief It returns @em true if the FSM is already performing a state transition
         *
         * @return @em true if the FSM is already performing a state transition
         */
		bool isBusy() const;

		/**
		 * @brief It checks if @em cmd is a valid command for the FSM
		 *
		 * Here valid means that the command is part of the set of allowed FSM events:
		 * @li daq::rc::FSM_COMMAND::INIT_FSM and daq::rc::FSM_COMMAND::STOP_FSM are not valid because reserved for initialization
		 *     and termination of the FSM;
		 * @li Other commands may not be valid because referring to transitions that do not exist given an initial state
		 *     (<em>e.g.</em>, daq::rc::FSM_COMMAND::SHUTDOWN is not valid for an FSM whose initial state is daq::rc::FSM_STATE::INITIAL)
		 *
		 * @note This command may be used before calling FSMController::makeTransition when an early detection of a bad
		 *       command is needed.
		 *
		 * @note If the command is part of the set of allowed FSM events but the corresponding transition cannot be performed
		 *       given the current state (<em>i.e.</em>, trying to perform the daq::rc::FSM_COMMAND::START in the daq::rc:FSM_STATE::INITIAL
		 *       state), then this method will return @em true anyway.
		 *
		 * @param cmd The FSM command
		 * @return @em true if the FSM command is valid
		 */
		bool isCommandValid(FSM_COMMAND cmd) const;

		/**
		 * @brief Type for a token corresponding to a registered call-back
		 */
		typedef boost::signals2::connection CallbackToken;

		/**
		 * @brief It registers a call-back to be called any time the busy state of the FSM changes
		 *
		 * @param cb The call-back
		 * @return A token corresponding to the registered call-back (to be used to remove the call-back itself)
		 */
		CallbackToken registerOnFSMBusyCallback(const FSMDefs::OnFSMBusyType& cb);

		/**
		 * @brief It registers a call-back to be called when a state transition begins
		 *
		 * Even if the "busy" call-back is received, there is no guarantee that this call-back will be executed
		 * as well; indeed the event may be rejected by the FSM and then the "no transition" call-back will be
		 * executed followed by the "no more busy" one.
		 *
		 * @param cb The call-back
		 * @return A token corresponding to the registered call-back (to be used to remove the call-back itself)
		 */
		CallbackToken registerOnTransitioningCallback(const FSMDefs::OnTransitioningSlotType& cb);

		/**
		 * @brief It registers a call-back to be called when a transition is successfully done
		 *
		 * @attention This call-back is not called when the transition is aborted because some
		 *            exception is thrown (in this case the "transition exception" call-back is called)
		 *
		 * @param cb The call-back
		 * @return A token corresponding to the registered call-back (to be used to remove the call-back itself)
		 */
		CallbackToken registerOnTransitionDoneCallback(const FSMDefs::OnTransitionDoneSlotType& cb);

		/**
		 * @brief It registers a call-back to be called when there is no transition corresponding to
		 *        a defined event given the current FSM state.
		 *
		 * @param cb The call-back
		 * @return A token corresponding to the registered call-back (to be used to remove the call-back itself)
		 */
		CallbackToken registerOnNoTransitionCallback(const FSMDefs::OnNoTransitionSlotType& cb);

		/**
		 * @brief It registers a call-back to be called when some exception is thrown during the execution
		 *        of a transition's action.
		 *
		 * @param cb The call-back
		 * @return A token corresponding to the registered call-back (to be used to remove the call-back itself)
		 */
		CallbackToken registerOnTransitionExceptionCallback(const FSMDefs::OnTransitionExceptionSlotType& cb);

		// Call-backs are usually removed during destruction
		// This method may be used if the removal has to be done before
		// NOTE: this method is not synchronized with the ones calling
		// the call-back functions (i.e., a call-back may be called in the
		// while and being executed after this method returned).
		// That is not needed now.
		/**
		 * @brief It removes a previously registered call-back
		 *
		 * @attention This method is not synchronized with the ones calling
         *            the call-back functions (<em>i.e.</em>, a call-back may be called in the
         *            while and being executed after this method returned).
		 *
		 * @param token The token corresponding to a previously registered call-back
		 */
		void removeCallback(const CallbackToken& token);

	protected:
		// Call-backs from the FSM: they just call the call-backs registered by clients
		void transitioning(const TransitionCmd& trCmd);
		void transitionDone(const TransitionCmd& trCmd);
		void noTransition(const TransitionCmd& trCmd);
		void transitionException(const TransitionCmd& trCmd, std::exception& ex);
		void busy(bool isBusy);

	private:
		const std::shared_ptr<RunControlTransitionActions> m_rc_actions;
		const std::shared_ptr<UserRoutines> m_user_functions;
		std::unique_ptr<FSMActions> m_fsm;

		FSMDefs::OnFSMBusy m_busy_signal;
		FSMDefs::OnTransitionDone m_transition_done_signal;
		FSMDefs::OnTransitioning m_transitioning_signal;
		FSMDefs::OnNoTransition m_no_transition_signal;
		FSMDefs::OnTransitionException m_transition_ex_signal;
};

}}

#endif /* DAQ_RC_FSMCONTROLLER_H_ */
