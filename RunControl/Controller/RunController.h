/*
 * RunController.h
 *
 *  Created on: Dec 5, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/RunController.h
 */

#ifndef DAQ_RC_RUNCONTROLLER_H_
#define DAQ_RC_RUNCONTROLLER_H_

#include "RunControl/Common/Clocks.h"
#include "RunControl/Common/CommandedApplication.h"
#include "RunControl/Common/Algorithms.h"
#include "RunControl/Controller/ApplicationController.h"
#include "RunControl/Controller/CommandController.h"
#include "RunControl/Controller/FSMController.h"

#include <list>
#include <string>
#include <memory>
#include <utility>
#include <shared_mutex>
#include <atomic>


namespace std {
    class exception;
}

namespace daq { namespace rc {

class ControllerApplication;
class UserRoutines;
class ApplicationList;
class CommandReceiver;
class CommandSender;
class ThreadPool;
class ScheduledThreadPool;
class ApplicationStatusCmd;
class ApplicationSelfInfo;
class TransitionCmd;
class InformationPublisher;
class Application;
class TaskBookkeeper;
class AMBridge;
class TransitionChecker;
class DVSController;
enum class FSM_STATE : unsigned int;
enum class FSM_COMMAND : unsigned int;

/**
 * @brief Core and main component of a controller
 *
 * This class implements the core functionality of a controller:
 * @li It receives external commands and deals with them;
 * @li It receives updates from child applications about their status;
 * @li It deals with timers (i.e., regularly called functions - as known as "probe" or "publish" methods);
 * @li It takes care of the start and stop procedures;
 * @li It manages all the other components.
 *
 * The execution of commands is delegated to the daq::rc::CommandController.
 *
 * Transition commands are an exception: since an FSM transition involves several components and it
 * is really a critical part of the whole application, the RunController takes care of it
 * with the help of the daq::rc::FSMController.
 *
 * Actions to be executed during an FSM transition are delegated to an instance of the
 * daq::rc::RunControllerFSMOperations class. Anyway some additional actions are taken by
 * the RunController in call-back methods notified by the FSM itself (RunController::fsmTransitioning,
 * RunController::fsmTransitionDone, RunController::fsmBusy, RunController::fsmNoTransition,
 * RunController::fsmTransitionException).
 *
 * @note Sub-classes have to implement the daq::rc::RunController::onInit where the FSM has to be properly
 *       initialized taking into account the FSM initial state (default initial state for a controller different
 *       than the RootController is daq::rc::FSM_STATE::INITIAL)
 *
 * This class is the implementation of the daq::rc::Controller class in the context of the @em pimpl idiom.
 */
class RunController : public CommandedApplication {
    public:
        /**
         * @brief Copy constructor: deleted
         */
        RunController(const RunController&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        RunController(RunController&&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
        RunController& operator=(const RunController&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        RunController& operator=(RunController&&) = delete;

        /**
         * @brief Destructor
         */
        virtual ~RunController() noexcept;

        /**
         * @brief It executes a transition upon external request
         *
         * @note Transitions are executed asynchronously
         *
         * @param ctx Context with information about the command sender
         * @param commandDescription The string representation of the FSM transition command to be executed
         *
         * @throws daq::rc::BadCommand @em commandDescription does not describe a valid command
         * @throws daq::rc::Busy The transition cannot be executed because the controller is shutting down or is busy
         * @throws daq::rc::InErrorState The transition cannot be executed because the controller is in error
         * @throws daq::rc::NotAllowed The execution of the command is not allowed by the Access Management system
         */
        virtual void makeTransition(const CommandedApplication::SenderContext& ctx,
                                    const std::string& commandDescription) override;

        /**
         * @brief It executes a command upon external request
         *
         * @note Commands are executed asynchronously (see the daq::rc::CommandController class for exceptions)
         *
         * @param ctx Context with information about the command sender
         * @param commandDescription The string representation of the command to be executed
         *
         * @throws daq::rc::BadCommand @em commandDescription does not describe a valid command
         * @throws daq::rc::Busy The command cannot be executed because the controller is busy
         * @throws daq::rc::NotAllowed The execution of the command is not allowed by the Access Management system
         */
        virtual void executeCommand(const CommandedApplication::SenderContext& ctx,
                                    const std::string& commandDescription) override;

        /**
         * @brief It sets the controller's error state upon external request
         *
         * @param ctx Context with information about the command sender
         * @param errorItems Error description
         *
         * @throws daq::rc::NotAllowed The execution of the command is not allowed by the Access Management system
         */
        virtual void setError(const CommandedApplication::SenderContext& ctx,
                              const CommandedApplication::ErrorItems& errorItems) override;

        /**
         * @brief It returns information about the status of this controller
         *
         * @return An encoded string describing the status of the controller
         *
         * @see daq::rc::ApplicationStatusCmd
         */
        virtual std::string status() override;

        /**
         * @brief It returns the status of one of the child applications
         *
         * @param childName The name of the child application
         * @return An encoded string describing the status of the child application
         *
         * @see daq::rc::ApplicationStatusCmd
         *
         * @throws daq::rc::UnknownChild No child with that name is found
         */
        virtual std::string childStatus(const std::string& childName) override;

        /**
         * @brief It updates the local information about a child application
         *
         * Updates usually come from children themselves
         *
         * @param childDescription An encoded string describing the status of the child application
         */
        virtual void updateChild(const std::string& childDescription) override;

        /**
         * @brief It returns the name of this controller
         *
         * @return The name of this controller
         */
        std::string id() override;

        /**
         * @brief It returns the name of the partition this controller belongs to
         *
         * @return The name of the partition this controller belongs to
         */
        std::string partition() override;

        /**
         * @brief It returns the name of the parent controller
         *
         * @return The name of the parent controller
         */
        const std::string& getParent() const;

        /**
         * @brief It returns the name of the segment this controller masters
         *
         * @return The name of the segment this controller masters
         */
        const std::string& getSegment() const;

    protected:
        /**
         * @brief Constructor
         *
         * @param name The name of this controller
         * @param parent The name of the parent controller
         * @param partition The name of the partition
         * @param segment The name of the controlled segment
         * @param userActions Pointer to the user code to be executed during FSM transitions
         * @param isInteractive @em true if the controller should run in interactive mode (not supported)
         *
         * @throws daq::rc::ControllerInitializationFailed Error in creating any of the controller's component
         */
        RunController(const std::string& name,
                      const std::string& parent,
                      const std::string& partition,
                      const std::string& segment,
                      std::shared_ptr<UserRoutines> userActions,
                      bool isInteractive);

        /**
         * @brief It initializes this controller
         *
         * Actions executed:
         * @li The application list is loaded;
         * @li The daq::rc::DVSController is asked to load the configuration;
         * @li The proper daq::rc::CommandReceiver is created;
         * @li The FSM is created and properly initialized;
         * @li All other internal data structures are initialized;
         * @li In case of daq::rc::CORBAReceiver, the IPC publication is done;
         *
         * @throws daq::rc::ControllerInitializationFailed Some error occurred trying to initialize
         *                                                 any of the controller's components
         *
         * @see daq::rc::RunController::onInit
         */
        void init();

        /**
         * @brief It start the controller activities
         *
         * The PMG sync is created so that the controller will appear as up and running to the
         * external world.
         *
         * This method blocks until daq::rc::RunController::shutdown is called.
         */
        void run();

        /**
         * @brief It causes the controller to stop and initiate the exit procedure.
         *
         * @see daq::rc::RunController::onExit
         */
        void shutdown();

        /**
         * @brief Called any time the busy state of the controller changes
         *
         * This is a call-back function registered to the daq::rc::CommandController
         *
         * @param isBusy @em true when the controller enters a busy state because some command
         *               is being executed.
         *
         * @see daq::rc::CommandController::registerOnBusyCallback
         */
        void busyWithCommands(bool isBusy);

        /**
         * @brief Called any time an application exits
         *
         * This is a call-back function registered to the daq::rc::ApplicationController
         *
         * @param app The application exited
         * @param globalExit @em true if the application exited because this controller is shutting down
         *
         * @see daq::rc::ApplicationController::registerOnApplicationExitCallback
         */
        void applicationExited(const std::shared_ptr<Application>& app, bool globalExit);

        /**
         * @brief It builds a command object representing the status of this controller
         *
         * @param myInfo This controller's status information
         * @return A command object representing the status of this controller
         */
        std::unique_ptr<ApplicationStatusCmd> buildApplicationStatusCmd(const ApplicationSelfInfo& myInfo) const;

        /**
         * @brief It updates the local information about the status of a child application
         *
         * @param childStatus A command object representing the status of the child application
         */
        void updateChild(const std::shared_ptr<const ApplicationStatusCmd>& childStatus);

        /**
         * @brief It implements the execution of a transition command
         *
         * @param cmd The transition command to be executed
         *
         * @throws daq::rc::BadCommand @em cmd is corrupted or mal-formed
         * @throws daq::rc::Busy The transition cannot be done because the controller is busy
         * @throws daq::rc::InErrorState The transition cannot be done because the controller is in error
         */
        void makeTransition(const std::shared_ptr<TransitionCmd>& cmd);

        /**
         * @brief It returns the glabal status of all the child applications
         *
         * This method is called during the controller's start operations and re-connects to all
         * already running applications.
         *
         * @return The FSM status of the children
         *
         * @throws daq::rc::BadChildren Children are not all in the same FSM state
         * @throws daq::rc::InterruptedException The execution has been interrupted
         */
        std::string getChildrenStatus();

        /**
         * @brief It returns a pointer to the object describing the status of this controller
         *
         * @return A pointer to the object describing the status of this controller
         */
        std::shared_ptr<ControllerApplication> thisControllerApplication() const;

        /**
         * @brief It provides information about the current status of the controller
         *
         * @return Information about the current status of the controller (the boolean element
         *         in the pair indicates whether the controller is exiting or not)
         */
        std::pair<bool, ApplicationSelfInfo> thisControllerApplicationStatus() const;

        /**
         * @brief It updates this controller's own information
         *
         * Needed when the database is reloaded to refresh the information extracted from the
         * configuration database
         *
         * @param newApp The object with the controller's fresh information from the database
         * @return A pointer to the new object describing the status of this controller
         *
         * @see daq::rc::Application::Update
         */
        std::shared_ptr<ControllerApplication> thisControllerApplication(ControllerApplication* newApp);

        /**
         * @brief It returns a reference to the daq::rc::ApplicationController instance
         *
         * @return A reference to the daq::rc::ApplicationController instance
         */
        ApplicationController& applicationController() const;

        /**
         * @brief It returns a reference to the daq::rc::InformationPublisher instance
         *
         * @return A reference to the daq::rc::InformationPublisher instance
         */
        InformationPublisher& infoPublisher() const;

        /**
         * @brief It returns a reference to the daq::rc::UserRoutines instance
         *
         * @return A reference to the daq::rc::UserRoutines instance
         */
        UserRoutines& userRoutines() const;

        /**
         * @brief It returns a reference to the daq::rc::FSMController instance
         *
         * @return A reference to the daq::rc::FSMController instance
         */
        FSMController& fsmController() const;

        /**
         * @brief It returns a reference to the daq::rc::DVSController instance
         *
         * @return A reference to the daq::rc::DVSController instance
         */
        DVSController& dvsController() const;

        /**
         * @brief It returns a reference to the daq::rc::ApplicationList instance
         *
         * @return A reference to the daq::rc::ApplicationList instance
         */
        const ApplicationList& applicationList() const;

        /**
         * @brief Method executed periodically (starting at the end of the daq::rc::FSM_COMMAND::CONNECT transition and
         *        stopping at the end of the daq::rc::FSM_COMMAND::DISCONNECT transition)
         *
         * It updates the parent controller and schedules tests for the child applications
         *
         * @return Always @em true
         *
         * @throws boost::thread_interrupted The execution has been interrupted (the exception needs to be propagated
         *                                   to the calling thread pool)
         *
         * @see daq::rc::ScheduledThreadPool
         */
        bool periodicPublish();

        /**
         * @brief Method executed periodically (starting at the end of the daq::rc::FSM_COMMAND::CONNECT transition and
         *        stopping at the end of the daq::rc::FSM_COMMAND::DISCONNECT transition)
         *
         * It periodically executes the daq::rc::UserRoutines::publishAction method (any exception will cause
         * a FATAL issue to be sent).
         *
         * @return Always @em true
         *
         * @throws boost::thread_interrupted The execution has been interrupted (the exception needs to be propagated
         *                                   to the calling thread pool)
         */
        bool periodicUserPublish();

        /**
         * @brief Method executed periodically (starting at the end of the daq::rc::FSM_COMMAND::CONNECT transition and
         *        stopping at the end of the daq::rc::FSM_COMMAND::DISCONNECT transition)
         *
         * It periodically executes the daq::rc::UserRoutines::publishStatisticsAction method (any exception will cause
         * a FATAL issue to be sent).
         *
         *
         * @return Always @em true
         *
         * @throws boost::thread_interrupted The execution has been interrupted (the exception needs to be propagated
         *                                   to the calling thread pool)
         */
        bool periodicUserPublishStats();

        /**
         * @brief The FSM is initialized
         */
        virtual void onInit();

        /**
         * @brief Actions performed when the controller is asked to EXIT
         *
         * Any command being executed is stopped and the daq::rc::ApplicationController is asked to kill
         * all the child applications.
         *
         * @see daq::rc::ApplicationController::exiting
         */
        virtual void onExit();

        /**
         * @brief Call-back from the daq::rc::FSMController
         *
         * Called any time the FSM busy status changes
         *
         * @param isBusy @em true if the FSM is busy
         */
        virtual void fsmBusy(bool isBusy);

        /**
         * @brief Call-back from the daq::rc::FSMController
         *
         * Called any time the FSM starts a transition
         *
         * @param trCmd The command representing the on-going transition
         */
        virtual void fsmTransitioning(const TransitionCmd& trCmd);

        /**
         * @brief Call-back from the daq::rc::FSMController
         *
         * Called any time the FSM completes a transition
         *
         * @param trCmd The command representing the on-going transition
         */
        virtual void fsmTransitionDone(const TransitionCmd& trCmd);

        /**
         * @brief Call-back from the daq::rc::FSMController
         *
         * Called when there is no transition corresponding to the given transition command
         * (i.e., the event sent to the FSM is formally valid but it is sent when the FSM is
         * not in the proper state)
         *
         * @param trCmd The command representing the transition
         */
        virtual void fsmNoTransition(const TransitionCmd& trCmd);

        /**
         * @brief Call-back from the daq::rc::FSMController
         *
         * Called any time an FSM transition is aborted because of an exception
         *
         * @param trCmd The command representing the on-going transition
         * @param ex The exception causing the transition to abort
         */
        virtual void fsmTransitionException(const TransitionCmd& trCmd, std::exception& ex);

        /**
         * @brief Executed when the transition is aborted because of an exception
         *
         * That is because the daq::rc::FSMController calls the @em fsmTransitionDone
         * call-back only when the transition is <b>successfully</b> done.
         *
         * @param trCmd the command representing the transition
         */
        virtual void fsmTransitionDoneAfterException(const TransitionCmd& trCmd);

        /**
         * @brief It returns a pointer to a command object encapsulating information about this controller's parent
         * @return A pointer to a command object encapsulating information about this controller's parent (it can be
         *         @em nullptr if the parent cannot be reached or does not exist).
         */
        virtual std::unique_ptr<const ApplicationStatusCmd> getParentStatus() const;

        /**
         * @brief It publishes IS information about the new status and updates this controller's parent
         *
         * @param myInfo Information about the status of this controller
         * @param actionTimeout This controller's action timeout
         * @param exitTimeout This controller's exit timeout
         */
        virtual void notifyStatusChange(const ApplicationSelfInfo& myInfo, int actionTimeout, int exitTimeout);

        /**
         * @brief It sends the ES a status update about itself and all its the children
         *
         * @param includeSV If @em true the update will include children's supervision data
         */
        void bulkUpdateToES(bool includeSV = false);

        /**
         * @brief It creates an object used to hold the information about the status of this controller
         *
         * @return A pointer to the new object holding information about this controller's status
         *
         * @throws daq::rc::ConfigurationIssue Information about this controller cannot be retrieved from
         *                                     the configuration database
         */
        static std::unique_ptr<ControllerApplication> createThisControllerApplication();

    private:
        friend class Controller;
        friend class ChildStatusTask;
        friend class ReachStateTask;
        friend class TransitionTask;
        friend class StartOperationsTask;

        mutable std::shared_mutex m_myapp_mutex;

        chrono::time_point<clock_t> m_tr_start;

        Algorithms::SubtransitionMap m_subtr_tree;
        const std::string m_app_name;
        const std::string m_parent;
        const std::string m_segment;
        const std::string m_partition;
        const bool m_is_interactive;
        std::atomic<bool> m_is_exiting;

        // NOTE: leave the InformationPublisher as one of the last objects to be destroyed.
        // It waits for the IS publications/removals to be done before exiting;
        // this in principle may take some time but will have a small impact
        // if it is one the last things to be done.

        // NOTE: the order of objects has a meaning and should be changed with care.
        // For instance thread pools should be the last ones to be created (and
        // the first one to be destroyed) because the tasks they execute may need
        // to access other data members

        // NOTE: if you have move the FSMController to be a shared pointer,
        // then give a look at the "removeCallback" method's doc for
        // possible synchronization issue
        std::shared_ptr<ControllerApplication> m_myself;
        std::list<FSMController::CallbackToken> m_fsm_ctrl_tokens;
        ApplicationController::OnApplicationExitSlotToken m_app_ctrl_token;
        CommandController::CallbackToken m_cmd_ctrl_token;
        const std::unique_ptr<AMBridge> m_am_bridge;
        const std::unique_ptr<TaskBookkeeper> m_tbk;
        const std::shared_ptr<InformationPublisher> m_info_publisher;
        const std::unique_ptr<CommandSender> m_cmd_sender;
        const std::shared_ptr<ApplicationList> m_app_list;
        const std::shared_ptr<DVSController> m_dvs_ctrl;
        const std::shared_ptr<ApplicationController> m_app_ctrl;
        const std::shared_ptr<UserRoutines> m_user_actions;
        const std::unique_ptr<ThreadPool> m_parentsender_tp;
        const std::unique_ptr<TransitionChecker> m_transition_checker;
        const std::unique_ptr<FSMController> m_fsm_ctrl;
        const std::unique_ptr<ScheduledThreadPool> m_scheduled_tp;
        const std::unique_ptr<CommandController> m_cmd_ctrl;
        const std::unique_ptr<ScheduledThreadPool> m_transition_timeout_timer;
        const std::unique_ptr<ThreadPool> m_transitions_tp;
        std::shared_ptr<CommandReceiver> m_cmd_recv;
};

}}

#endif /* DAQ_RC_RUNCONTROLLER_H_ */
