/*
 * ApplicationSelfInfo.h
 *
 *  Created on: Dec 14, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/ApplicationSelfInfo.h
 */

#ifndef DAQ_RC_APPLICATIONSELFINFO_H_
#define DAQ_RC_APPLICATIONSELFINFO_H_

#include "RunControl/Common/Clocks.h"

#include <memory>
#include <vector>
#include <string>

namespace daq { namespace rc {

class RunControlBasicCommand;
class TransitionCmd;
enum class FSM_STATE : unsigned int;

/**
 * @brief Class holding child application's dynamic information.
 *
 * This kind of information is filled using the updates coming directly from child applications.
 * This class is not thread safe and can actually be modified only by the daq::rc::Application class
 * (that implements the proper synchronization mechanism).
 *
 * @see daq::rc::Application
 * @see daq::rc::ApplicationSVInfo
 */
class ApplicationSelfInfo {
    public:
        /**
         * @brief Constructor
         */
        ApplicationSelfInfo();

        /**
         * @brief Constructor
         *
         * @param state The application's current FSM state
         * @param isTransitioning @em true if the application is performing a transition
         * @param errorResons Reasons why the application is in an error state
         * @param badChildren Children causing the application's error state (valid only for child controllers)
         * @param lastTrCmd The last executed transition command
         * @param currTrCmd The transition command being executed
         * @param lastTrTime Time needed to complete the last transition (in milliseconds)
         * @param lastCmd The last received command (not transition command)
         * @param isBusy @em true if the application is in a busy state
         * @param fullyInizialized @em true is the application is fully initialized (i.e., the start or restart
         *                         procedure has been completed)
         * @param infoTimeTag Time (in nanoseconds) information has been updated the last time - monotonic
         * @param wallTime Time (in nanoseconds) information has been updated the last time - system time
         */
        ApplicationSelfInfo(FSM_STATE state,
                            bool isTransitioning,
                            const std::vector<std::string>& errorResons,
                            const std::vector<std::string>& badChildren,
                            const std::shared_ptr<const TransitionCmd>& lastTrCmd,
                            const std::shared_ptr<const TransitionCmd>& currTrCmd,
                            unsigned int lastTrTime,
                            const std::shared_ptr<const RunControlBasicCommand>& lastCmd,
                            bool isBusy,
                            bool fullyInizialized,
                            infotime_t infoTimeTag,
                            long long wallTime);

        /**
         * @brief Default copy constructor
         */
        ApplicationSelfInfo(const ApplicationSelfInfo&) = default;

        /**
         * @brief Default move constructor
         */
        ApplicationSelfInfo(ApplicationSelfInfo&&) = default;

        /**
         * @brief Default copy assignment operator
         */
        ApplicationSelfInfo& operator=(const ApplicationSelfInfo&) = default;

        /**
         * @brief Default move assignment operator
         */
        ApplicationSelfInfo& operator=(ApplicationSelfInfo&&) = default;

        /**
         * @brief Destructor
         */
        ~ApplicationSelfInfo();

        /**
         * @brief It returns the application's current FSM state
         *
         * @return The application's current FSM state
         */
        FSM_STATE getFSMState() const;

        /**
         * @brief It returns @em true if the application is performing a transition
         *
         * @return @em true if the application is performing a transition
         */
        bool isTransitioning() const;

        /**
         * @brief It returns @em true if the application is in an error state
         *
         * @return @em true if the application is in an error state
         */
        bool isInternalError() const;

        /**
         * @brief It returns the reason why the application is in an error state
         *
         * @return The reason why the application is in an error state
         */
        std::vector<std::string> getErrorReasons() const;

        /**
         * @brief It returns the children causing the application's error state (valid only for child controllers)
         *
         * @return The children causing the application's error state (valid only for child controllers)
         */
        std::vector<std::string> getBadChildren() const;

        /**
         * @brief It returns the last executed transition command
         *
         * @return The last executed transition command (it may return @em nullptr when no transition commands have been executed yet)
         */
        std::shared_ptr<const TransitionCmd> getLastTransitionCommand() const;

        /**
         * @brief It returns the transition command being executed
         *
         * @return The transition command being executed (it may be @em nullptr if there is no transition on-going)
         */
        std::shared_ptr<const TransitionCmd> getCurrentTransitionCommand() const;

        /**
         * @brief It returns the time needed to complete the last transition (in milliseconds)
         *
         * @return The time needed to complete the last transition (in milliseconds)
         */
        infotime_t getLastTransitionTime() const;

        /**
         * @brief It returns the the last received command (not transition command)
         *
         * @return The last received command (not transition command). It may be @em nullptr if no command has been received yet
         */
        std::shared_ptr<const RunControlBasicCommand> getLastCommand() const;

        /**
         * @brief It returns @em true if the application is in a busy state
         *
         * @return @em true if the application is in a busy state
         */
        bool isBusy() const;

        /**
         * @brief It returns @em true is the application is fully initialized (i.e., the start or restart procedure has been completed)
         *
         * @return @em true is the application is fully initialized
         */
        bool isFullyInitialized() const;

        /**
         * @brief It returns the time (in nanoseconds, monotonic) this information has been updated last
         *
         * @return The time (in nanoseconds, monotonic) this information has been updated last
         */
        infotime_t getInfoTimeTag() const;

        /**
         * @brief It returns the time (in nanoseconds, system time) this information has been updated last
         *
         * @return The time (in nanoseconds, system time) this information has been updated last
         */
        long long getInfoWallTime() const;

    protected:
        friend class Application;

        /**
         * @brief It sets the application's current FSM state
         *
         * @param state The application's current FSM state
         */
        void setFSMState(FSM_STATE state);

        /**
         * @brief It sets whether the application is performing an FSM transition or not
         *
         * @param isTransitioning @em true if the application is performing a transition
         */
        void setTransitioning(bool isTransitioning);

        /**
         * @brief It sets the application's internal error state
         *
         * The error state can be cleared with an empty @em errorReasons
         *
         * @param errorReasons The reason why the application is in an error state
         * @param badChildren The children causing the application's error state (valid only for child controllers)
         */
        void setInternalError(const std::vector<std::string>& errorReasons, const std::vector<std::string>& badChildren);

        /**
         * @brief It sets the last executed transition command
         *
         * @param cmd The last executed transition command (it can be @em nullptr)
         */
        void setLastTransitionCommand(const std::shared_ptr<const TransitionCmd>& cmd);

        /**
         * @brief It sets the transition command being currently executed
         *
         * @param cmd The transition command being currently executed (it can be @em nullptr)
         */
        void setCurrentTransitionCommand(const std::shared_ptr<const TransitionCmd>& cmd);

        /**
         * @brief It sets the time (in milliseconds) needed to perform the last FSM transition
         *
         * @param time The time (in milliseconds) needed to perform the last FSM transition
         */
        void setLastTransitionTime(infotime_t time);

        /**
         * @brief It sets the last received command (not transition command)
         *
         * @param cmd The last received command (not transition command). It can be @em nullptr
         */
        void setLastCommand(const std::shared_ptr<const RunControlBasicCommand>& cmd);

        /**
         * @brief It sets whether the application is in a busy state
         *
         * @param isBusy @em true if the application is busy
         */
        void setBusy(bool isBusy);

        /**
         * @brief It sets whether the application has been fully initialized (i.e., the start or restart procedure has been completed)
         *
         * @param done @em true is the application is fully initialized
         */
        void setFullyInitialized(bool done);

        /**
         * @brief It updates the time stamps every time this information is updated
         */
        void updateTimeTags();

    private:
        FSM_STATE m_fsm_state;
        bool m_transitioning;
        std::vector<std::string> m_error_reasons;
        std::vector<std::string> m_bad_children;
        bool m_internal_error;
        std::shared_ptr<const TransitionCmd> m_last_tr_cmd;
        std::shared_ptr<const TransitionCmd> m_current_tr_cmd;
        infotime_t m_tr_time;
        std::shared_ptr<const RunControlBasicCommand> m_last_cmd;
        bool m_busy;
        bool m_fully_initialized;
        infotime_t m_time_tag;
        long long m_wall_time;
};

}}

#endif /* DAQ_RC_APPLICATIONSELFINFO_H_ */
