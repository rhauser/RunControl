/*
 * RunControlApplication.h
 *
 *  Created on: Nov 2, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/RunControlApplication.h
 */

#ifndef DAQ_RC_RUNCONTROLAPPLICATION_H_
#define DAQ_RC_RUNCONTROLAPPLICATION_H_

#include "RunControl/Controller/Application.h"

#include <string>

class Configuration;
namespace daq { namespace core {
	class Partition;
	class BaseApplication;
}}

namespace daq { namespace rc {

/**
 * @brief Class representing a child Run Control application
 *
 * Sub-classes may need to overwrite the value of the action timeout using the
 * RunControlApplication::setActionTimeout method (this must be done in the
 * constructor).
 */
class RunControlApplication : public Application {
	public:
        /**
         * @brief Constructor
         *
         * It adds specific start and restart parameters and environment variables used by leaf
         * run control applications to determine some information publishing intervals.
         *
         * @param db The ::Configuration object
         * @param p The daq::core::Partition object
         * @param appConfig The daq::core::BaseApplication object describing this application
         * @param isParentRoot @em true if the parent of this application is the RootControler
         *
         * @throws daq::rc::ConfigurationIssue Error accessing information from the configuration database
         */
        RunControlApplication(Configuration& db,
                              const daq::core::Partition& p,
                              const daq::core::BaseApplication& appConfig,
                              bool isParentRoot = false);

        /**
         * @brief Copy constructor: deleted
         */
        RunControlApplication(const RunControlApplication&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        RunControlApplication(RunControlApplication&&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
        RunControlApplication& operator=(const RunControlApplication& other) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        RunControlApplication& operator=(RunControlApplication&& other) = delete;

        /**
         * @brief Destructor
         */
		virtual ~RunControlApplication() noexcept;

        /**
         * @brief It returns daq::rc::ApplicationType::RUN_CONTROL
         *
         * @return daq::rc::ApplicationType::RUN_CONTROL
         */
		virtual ApplicationType type() const noexcept;

		/**
		 * @brief It returns the application's action timeout (in seconds)
		 *
		 * @return The application's action timeout (in seconds)
		 */
		unsigned int getActionTimeout() const;

		/**
		 * @brief It returns the application's probe timeout (in seconds) used to periodically publish some information
		 *
		 * A value of zero means that the periodic publication is disabled.
		 *
		 * @return The application's probe timeout (in seconds)
		 */
		unsigned int getProbeInterval() const;

		/**
		 * @brief It returns the application's timeout (in seconds) used to periodically publish some statistics information
		 *
		 * A value of zero means that the periodic publication is disabled.
		 *
		 * @return The application's timeout (in seconds) used to periodically publish some statistics information
		 */
		unsigned int getFullStatInterval() const;

	protected:
		/**
		 * @brief It sets the application's action timeout (in seconds)
		 *
		 * @param timeout The application's action timeout (in seconds)
		 */
		void setActionTimeout(unsigned int timeout);

	private:
		/**
		 * @brief It returns the command line parameters to be added to the ones retrieved from the
		 *        configuration database.
		 *
		 * The additional parameters include: the application's name, the name of the parent controller and
		 * the name of the segment.
		 *
		 * @return A string containing the additional parameters
		 */
		std::string additionalParameters() const;

		int m_action_timeout;
		unsigned int m_probe_interval;
		unsigned int m_fullstat_interval;

		static const std::string PROBE_ENV;
		static const std::string FULLSTAT_ENV;
};

}}

#endif /* DAQ_RC_RUNCONTROLAPPLICATION_H_ */
