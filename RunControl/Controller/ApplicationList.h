/*
 * ApplicationList.h
 *
 *  Created on: Nov 5, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/ApplicationList.h
 */

#ifndef DAQ_RC_APPLICATIONLIST_H_
#define DAQ_RC_APPLICATIONLIST_H_

#include "RunControl/Controller/Application.h"

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/mem_fun.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/tag.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/identity_fwd.hpp>
#include <boost/multi_index/indexed_by.hpp>

#include <string>
#include <memory>
#include <regex>
#include <map>
#include <set>
#include <shared_mutex>


class Configuration;
namespace daq { namespace core {
	class Partition;
}}

namespace daq { namespace rc {
    class ApplicationList;
}}

/**
 * @brief It swaps the content of two daq::rc::ApplicationList instances
 *
 * @param lhs The first daq::rc::ApplicationList instance
 * @param rhs The second daq::rc::ApplicationList instance
 */
void swap(daq::rc::ApplicationList& lhs, daq::rc::ApplicationList& rhs);

namespace daq { namespace rc {

namespace mi = boost::multi_index;

/**
 * @brief Class holding the list of all the applications to be menaged by the controller
 *
 * The list is created empty and is then filled caling the daq::rc::ApplicationList::load method.
 * This class is fully thread safe.
 *
 * @note The only way to update a daq::rc::ApplicationList is to swap its content with a second
 *       instance's content.
 *
 * @note Applications can be retrieved using regular expressions using
 *       daq::rc::ApplicationList::getApplications(const std::regex& expression) or
 *       getApplications(const std::set<std::string>& applications).
 */
class ApplicationList {
	public:

        /**
         * @brief Constructor
         */
		ApplicationList();

		/**
		 * @brief Move constructor
		 */
		ApplicationList(ApplicationList&& other);

		/**
		 * @brief Copy constructor
		 */
		ApplicationList(const ApplicationList& other);

		/**
		 * @brief Copy assignment operator using the "copy and swap" idiom
		 */
		ApplicationList& operator=(ApplicationList other);

		/**
		 * @brief Destructor
		 */
		~ApplicationList() noexcept;

		/**
		 * @brief It builds the list of applications reading the configuration database
		 *
		 * @param db The ::Configuration object
		 * @param part The partition
		 * @param segmentName The name of the segment the controller "controls"
		 *
		 * @throws daq::rc::ConfigurationIssue Some error occurred accessing the configuration database
		 */
		void load(Configuration& db, const daq::core::Partition& part, const std::string& segmentName);

		/**
		 * @brief It returns @em true is the application list is empty
		 *
		 * @return @em true is the application list is empty
		 */
		bool isEmpty() const;

		/**
		 * @brief It gets the application whose name is @em applicationName
		 *
		 * @param applicationName The name of the application to be looked for
		 * @return The application whose name is @em applicationName or @em nullptr if that application does not exist
		 */
		std::shared_ptr<Application> getApplication(const std::string& applicationName) const;

		/**
		 * @brief It gets the the applications with name matching the regular expression
		 *
		 * @param expression Regular expression applications names are matched with
		 * @return The set of applications whose names match the regular expression
		 */
		Application::ApplicationSet getApplications(const std::regex& expression) const;

		/**
		 * @brief Utility method to retrieve all the applications whose names match the string in @em applications
		 *
		 * @param applications The names of applications
		 * @return A container mapping application names to their corresponding Application objects. The mapped Application can be
		 *         a @em nullptr if the application is not found.
		 *
		 * @note Application names can be regular expression. In order to be treated as a reguler expression,
		 *       the lookup string must end with the @em "//r" characters. For instance, the @em "this is a regex" regular expression
		 *       will be treated as such only if composed as @em "this is a regex//r"
		 */
		std::map<std::string, std::shared_ptr<Application>> getApplications(const std::set<std::string>& applications) const;

		/**
		 * @brief It gets all the applications of a given type
		 *
		 * @param appType The type of the applications to look for
		 * @return The set of applications whose type is @em appType
		 */
		Application::ApplicationSet getApplications(ApplicationType appType) const;

		/**
		 * @brief It gets all the applications that are started (or stopped) at the same time
		 *
		 * @param when String representing the FSM transition applications should be started or stopped at
		 * @param atStart @em true if we are looking for applications to be started; @em false otherwise
		 * @return All the applications that are started (or stopped) at the same time
		 */
		Application::ApplicationSet getApplications(const std::string& when, bool atStart) const;

		/**
		 * @brief It gets all the applications of defined type that are started (or stopped) at the same time
		 *
		 * @param appType The type of the applications to look for
		 * @param when String representing the FSM transition applications should be started or stopped at
		 * @param atStart @em true if we are looking for applications to be started; @em false otherwise
		 * @return All the applications of the @em appType type that are started (or stopped) at the same time
		 */
		Application::ApplicationSet getApplications(ApplicationType appType, const std::string& when, bool atStart) const;

		/**
		 * @brief It gets all the applications
		 *
		 * @return All the applications
		 */
		Application::ApplicationSet getAllApplications() const;

		/**
		 * @brief It swap the contents of this instance with the contents of the @em other instance
		 *
		 * Used during the reload of the configuration database in order to reflect any configuration update.
		 *
		 * @param other The instance to the swapped with this
		 */
		void swap(ApplicationList& other);

	private:
		friend void ::swap(ApplicationList& lhs, ApplicationList& rhs);

		struct name_key {};
		struct start_key {};
		struct type_start_key {};
		struct stop_type_key {};

		typedef mi::multi_index_container<
					std::shared_ptr<Application>,
					mi::indexed_by<
						mi::hashed_unique<mi::tag<name_key>, mi::const_mem_fun<Application, std::string, &Application::getName>>,
						mi::ordered_non_unique<mi::tag<start_key>, mi::const_mem_fun<Application, std::string, &Application::getStartAt>>,
						mi::ordered_non_unique<mi::tag<type_start_key>,
						                       mi::composite_key<
						                          std::shared_ptr<Application>,
											  	  mi::const_mem_fun<Application, ApplicationType, &Application::type>,
											  	  mi::const_mem_fun<Application, std::string, &Application::getStartAt>
											   >
						>,
						mi::ordered_non_unique<mi::tag<stop_type_key>,
											   mi::composite_key<
											      std::shared_ptr<Application>,
                                                  mi::const_mem_fun<Application, std::string, &Application::getStopAt>,
											  	  mi::const_mem_fun<Application, ApplicationType, &Application::type>
											   >
						>
				  > // indexed_by
		> ApplicationContainer;

		ApplicationContainer m_app_list;
		mutable std::shared_mutex m_mutex;
};

}}

#endif /* DAQ_RC_APPLICATIONLIST_H_ */
