/*
 * ApplicationSupervisorInfo.h
 *
 *  Created on: Nov 13, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/ApplicationSupervisorInfo.h
 */

#ifndef DAQ_RC_APPLICATIONSUPERVISORINFO_H_
#define DAQ_RC_APPLICATIONSUPERVISORINFO_H_

#include "RunControl/Common/Clocks.h"

#include <tmgr/tmresult.h>

#include <boost/bimap.hpp>

#include <string>

namespace daq { namespace rc {

class Application;

/**
 * @brief Enumeration for the application status from a process management perspective
 */
enum class APPLICATION_STATUS : unsigned int {
	NOTAV = 1,  //!< NOTAV - This is the initial state (or when it is not possible to retrieve the information from the PMG)
	ABSENT,     //!< ABSENT - The application is no more running but it is not possible to determine why
	REQUESTED,  //!< REQUESTED - The PMG has been requested to start the application
	UP,         //!< UP - The application is up and running
	TERMINATING,//!< TERMINATING - The PMG has been asked to kill the process
	EXITED,     //!< EXITED - The application exited
	FAILED      //!< FAILED - The application failed to be started
};

/**
 * @brief Utility class to convert daq::rc::APPLICATION_STATUS items to string and viceversa
 */
class ApplicationStates {
    public:
        /**
         * @brief Default onstructor: deleted
         */
        ApplicationStates() = delete;

        /**
         * @brief Destructor: deleted
         */
        ~ApplicationStates() = delete;

        /**
         * @brief String representation of the daq::rc::APPLICATION_STATUS::NOTAV item
         */
        static const std::string NOTAV_STATE;

        /**
         * @brief String representation of the daq::rc::APPLICATION_STATUS::ABSENT item
         */
        static const std::string ABSENT_STATE;

        /**
         * @brief String representation of the daq::rc::APPLICATION_STATUS::REQUESTED item
         */
        static const std::string REQUESTED_STATE;

        /**
         * @brief String representation of the daq::rc::APPLICATION_STATUS::UP item
         */
        static const std::string UP_STATE;

        /**
         * @brief String representation of the daq::rc::APPLICATION_STATUS::TERMINATING item
         */
        static const std::string TERMINATING_STATE;

        /**
         * @brief String representation of the daq::rc::APPLICATION_STATUS::EXITED item
         */
        static const std::string EXITED_STATE;

        /**
         * @brief String representation of the daq::rc::APPLICATION_STATUS::FAILED item
         */
        static const std::string FAILED_STATE;

        /**
         * @brief It translates a daq::rc::APPLICATION_STATUS to a string
         *
         * @param status The application's status
         * @return The string representation of @em status
         */
        static std::string statusToString(APPLICATION_STATUS status);

        /**
         * @brief It translated a string representation of the application's status to the proper
         *        daq::rc::APPLICATION_STATUS item
         *
         * @param status A string representation of the application's status
         * @return The daq::rc::APPLICATION_STATUS item corresponding to @em status
         *
         * @throws daq::rc::InvalidStatus The @em status string does not match any daq::rc::APPLICATION_STATUS item
         */
        static APPLICATION_STATUS stringToStatus(const std::string& status);

    private:
        typedef boost::bimap<APPLICATION_STATUS, std::string> bm_type;
        static bm_type STATUS_MAP;
};

/**
 * @brief Class holding child application's dynamic information.
 *
 * This kind of information is filled by the parent controller using mainly information coming from the PMG system.
 * This class is not thread safe and can actually be modified only by the daq::rc::Application class
 * (that implements the proper synchronization mechanism).
 *
 * @see daq::rc::Application
 * @see daq::rc::ApplicationSelfInfo
 */
class ApplicationSVInfo {
	public:
        /**
         * @brief Default constructor
         */
        ApplicationSVInfo();

        /**
         * @brief Constructor
         *
         * @param appStatus The application's status
         * @param membership @em true if the application is not out of membership
         * @param runningHost The name of the host where the application is running on
         * @param isRestarting @em true if the application is being restarted
         * @param notResponding @em true if the application is not responding to commands
         * @param pmgHandle The PMG handle of the corresponding process
         * @param lastId The ID (from the PMG handle) of the last executed process
         * @param exitCode The application's exit code
         * @param exitSignal The application's exit signal
         * @param forceLookup If @em true then the PMG will be asked about the status of the process regardless the locally cached information
         * @param testResult The result of functionality tests executed for the application
         * @param infoTimeTag Time (in nanoseconds) information has been updated the last time - monotonic
         * @param infoWallTime Time (in nanoseconds) information has been updated the last time - system time
         */
        ApplicationSVInfo(APPLICATION_STATUS appStatus,
                          bool membership,
                          const std::string& runningHost,
                          bool isRestarting,
                          bool notResponding,
                          const std::string& pmgHandle,
                          unsigned int lastId,
                          int exitCode,
                          int exitSignal,
                          bool forceLookup,
                          daq::tmgr::TestResult testResult,
                          infotime_t infoTimeTag,
                          long long infoWallTime);

        /**
         * @brief Default copy constructor
         */
        ApplicationSVInfo(const ApplicationSVInfo&) = default;

        /**
         * @brief Default move constructor
         */
        ApplicationSVInfo(ApplicationSVInfo&&) = default;

        /**
         * @brief Default copy assignement operator
         */
        ApplicationSVInfo& operator=(const ApplicationSVInfo&) = default;

        /**
         * @brief Default move assignment operator
         */
        ApplicationSVInfo& operator=(ApplicationSVInfo&&) = default;

        /**
         * @brief Destructor
         */
        ~ApplicationSVInfo();

        /**
         * @brief It returns @em true if the application is not out of membership
         *
         * @return @em true if the application is not out of membership
         */
		bool isMembership() const;

		/**
		 * @brief It returns @em true if the application is being restarted
		 *
		 * @return @em true if the application is being restarted
		 */
		bool isRestarting() const;

		/**
		 * @brief It returns the name of the host where the application is running on
		 *
		 * @return The name of the host where the application is running on
		 */
		std::string getRunningHost() const;

		/**
		 * @brief It returns the application's status
		 *
		 * @return The application's status
		 */
		APPLICATION_STATUS getStatus() const;

		/**
		 * @brief It returns @em true if the application is not responding to commands
		 *
		 * @return @em true if the application is not responding to commands
		 */
		bool isNotResponding() const;

		/**
		 * @brief It returns the PMG handle of the corresponding process
		 *
		 * @return The PMG handle of the corresponding process
		 */
		std::string getHandle() const;

		/**
		 * @brief It returns the ID (from the PMG handle) of the last executed process
		 *
		 * @return The ID (from the PMG handle) of the last executed process
		 */
		unsigned int getLastID() const;

		/**
		 * @brief It returns the application's exit code
		 *
		 * @return The application's exit code
		 */
		int getExitCode() const;

		/**
		 * @brief It returns the application's exit signal
		 *
		 * @return The application's exit signal
		 */
		int getExitSignal() const;

		/**
		 * @brief It returns @em true if the PMG will be asked about the status of the process
		 *        regardless the locally cached information
		 *
		 * @return @em true if the PMG will be asked about the status of the process
		 */
		bool isForceLookup() const;

		/**
		 * @brief It returns the result of functionality tests executed for the application
		 *
		 * @return The result of functionality tests executed for the application
		 */
		daq::tmgr::TestResult getTestStatus() const;

		/**
		 * @brief It returns the time (in nanoseconds, monotonic) this information has been updated last
		 *
		 * @return The time (in nanoseconds, monotonic) this information has been updated last
		 */
		infotime_t getInfoTimeTag() const;

		/**
		 * @brief It returns the time (in nanoseconds, system time) this information has been updated last
		 *
		 * @return The time (in nanoseconds, system time) this information has been updated last
		 */
		long long getInfoWallTime() const;

	protected:
		friend class Application;

		/**
		 * @brief It sets the application's current membership status
		 *
		 * @param membership @em false if the application is out of membership
		 */
		void setMembership(bool membership);

		/**
		 * @brief It sets whether the application is being restarted or not
		 *
		 * @param restarting @em true if the application is being restarted
		 */
		void setRestarting(bool restarting);

		/**
		 * @brief It sets the name of the host the application is running on
		 *
		 * @param runningHost The name of the host the application is running on
		 */
		void setRunningHost(const std::string& runningHost);

		/**
		 * @brief It sets the application's current status
		 *
		 * @param status The application's current status
		 */
		void setStatus(APPLICATION_STATUS status);

		/**
		 * @brief It sets whether the application is responding to commands or not
		 *
		 * @param notResponding @em true if the application is not able to react to commands
		 */
		void setNotResponding(bool notResponding);

		/**
		 * @brief It sets the PMG handle of the corresponding process
		 *
		 * @param pmgHandle The PMG handle of the corresponding process
		 */
		void setHandle(const std::string& pmgHandle);

		/**
		 * @brief It sets the ID (from the PMG handle) of the last executed process
		 *
		 * @param id The ID (from the PMG handle) of the last executed process
		 */
		void setLastID(unsigned int id);

		/**
		 * @brief It sets the application's exit code
		 *
		 * @param exCode The application's exit code
		 */
		void setExitCode(int exCode);

		/**
		 * @brief It sets the application's exit signal
		 *
		 * @param signal The application's exit signal
		 */
		void setExitSignal(int signal);

		/**
		 * @brief It sets whether the PMG should be asked about the status of the process
         *        regardless the locally cached information
		 *
		 * @param force @em true if the PMG should be asked about the status of the process
		 */
		void setForceLookup(bool force);

		/**
		 * @brief It sets the result of functionality tests executed for the application
		 *
		 * @param status The result of functionality tests executed for the application
		 */
		void setTestStatus(daq::tmgr::TestResult status);

		/**
		 * @brief It updates the time stamps every time this information is updated
		 */
		void updateTimeTags();

	private:
		APPLICATION_STATUS m_status;
		bool m_membership;
		std::string m_running_host;
		bool m_restarting;
		bool m_not_responding;
		std::string m_pmg_handle;
		unsigned int m_last_id;
		int m_exit_code;
		int m_exit_signal;
		bool m_force_lookup;
		daq::tmgr::TestResult m_test_status;
		infotime_t m_time_tag;
		long long m_wall_time;
};

}}

#endif /* DAQ_RC_APPLICATIONSUPERVISORINFO_H_ */
