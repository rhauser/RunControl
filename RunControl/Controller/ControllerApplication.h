/*
 * ControllerApplication.h
 *
 *  Created on: Nov 2, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/ControllerApplication.h
 */

#ifndef DAQ_RC_CONTROLLERAPPLICATION_H_
#define DAQ_RC_CONTROLLERAPPLICATION_H_

#include "RunControl/Controller/RunControlApplication.h"
#include "RunControl/Controller/Application.h"
#include "RunControl/Common/Algorithms.h"

#include <string>
#include <map>

class Configuration;
namespace daq { namespace core {
	class Partition;
	class BaseApplication;
}}

namespace daq { namespace rc {

/**
 * @brief Class representing a child controller application
 */
class ControllerApplication : public RunControlApplication {
	public:
        /**
         * @brief Constructor
         *
         * Exit and actions timeouts are properly computed.
         *
         * @param db The ::Configuration object
         * @param p The daq::core::Partition object
         * @param appConfig The daq::core::BaseApplication object describing this application
         * @param isParentRoot @em true if the parent of this application is the RootControler
         *
         * @throws daq::rc::ConfigurationIssue Error accessing information from the configuration database
         */
        ControllerApplication(Configuration& db,
                              const daq::core::Partition& p,
                              const daq::core::BaseApplication& appConfig,
                              bool isParentRoot = false);

        /**
         * @brief Copy constructor: deleted
         */
		ControllerApplication(const ControllerApplication&) = delete;

		/**
		 * @brief Move constructor: deleted
		 */
		ControllerApplication(ControllerApplication&&) = delete;

		/**
		 * @brief Copy assignment operator: deleted
		 */
		ControllerApplication& operator=(const ControllerApplication&) = delete;

		/**
		 * @brief Move assignment operator: deleted
		 */
        ControllerApplication& operator=(ControllerApplication&&) = delete;

        /**
         * @brief Destructor
         */
		~ControllerApplication() noexcept;

		/**
		 * @brief It returns daq::rc::ApplicationType::CHILD_CONTROLLER
		 *
		 * @return daq::rc::ApplicationType::CHILD_CONTROLLER
		 */
		ApplicationType type() const noexcept;

		/**
		 * @brief It returns the sub-transitions defined for the segment this controller application controls
		 *
		 * @return The sub-transitions defined for the segment this controller application controls
		 */
		const Algorithms::SubtransitionMap& subTransitions() const;

	private:
		Algorithms::SubtransitionMap m_subtransitions;
};

}}

#endif /* DAQ_RC_CONTROLLERAPPLICATION_H_ */
