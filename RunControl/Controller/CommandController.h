/*
 * CommandController.h
 *
 *  Created on: Feb 19, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/CommandController.h
 */

#ifndef DAQ_RC_COMMANDCONTROLLER_H_
#define DAQ_RC_COMMANDCONTROLLER_H_

#include "RunControl/Common/RunControlBasicCommand.h"

#include <boost/signals2/signal.hpp>
#include <boost/signals2/connection.hpp>

#include <mutex>
#include <memory>
#include <map>
#include <functional>
#include <utility>
#include <shared_mutex>
#include <string>


namespace daq { namespace rc {

class ApplicationController;
class DVSController;
class ApplicationList;
class InformationPublisher;
class ChangeStatusCmd;
class StartStopAppCmd;
class ChangePublishIntervalCmd;
class ChangeFullStatIntervalCmd;
class PublishCmd;
class PublishStatsCmd;
class ThreadPool;
class TaskBookkeeper;
class ScheduledThreadPool;
class UserRoutines;
class TestAppCmd;
enum class RC_COMMAND : unsigned int;
enum class FSM_STATE : unsigned int;

/**
 * @brief Component whose main task is to execute commands when requested by the daq::rc::RunController class instance.
 *
 * This class also manages the proper execution of concurrent commands. For this reason the CommandController::grantResource
 * method has to be invoked every time a command is executed outside the context of this class (<em>e.g.</em>, this is the case of
 * the commands for FSM transitions that are managed directly by the daq::rc::RunController class). The command allocates a
 * resource that has to be freed when the execution of the command is completed.
 *
 * The CommandController::registerOnBusyCallback method can be used to register call-back functions to be called any time
 * the busy state changes. Clients will then receive a notification when the first command is received and when the last
 * command has been executed.
 *
 * @remark It is a daq::rc::CommandController's task to eventually notify a remote command sender when the execution of that command
 *         has been done. Note that this is done only when needed and that by default no notifications are sent to remote
 *         command senders. The information about whether a sender should be notified or not is encoded in the command object.
 *         \n
 * @note The execution of commands is completely asynchronous unless specified.
 *
 * @see RunControlBasicCommand::notConcurrentWith
 * @see RunControlBasicCommand::senderReference
 */
class CommandController {
    public:
        /**
         * @brief Type representing a resource allocated for the execution of a command
         *
         * The easiest and safest way to release the resource is just to let it go out of scope.
         *
         * A resource can also be copied (even if it is not recommended) and will be released
         * when no more instances of it are alive.
         */
        typedef std::shared_ptr<void> CommandResource;

        /**
         * @brief Constructor
         *
         * @param appCtrl Pointer to the daq::rc::ApplicationController used to start/stop/restart applications
         * @param appList Pointer to the daq::rc::ApplicationList
         * @param dvsCtrl Pointer to the daq::rc::DVSController used to test the functionality of applications
         * @param infoPub Pointer to the daq::rc::InformationPublisher used to publish changes in the application's status
         * @param ur Pointer to the daq::rc::UserRoutines
         * @param schedTP Pointer to the daq::rc::ScheduledThreadPool used for executing regular timers
         *
         * @throws daq::rc::ThreadPoolError The internal thread pool for command execution could not be created
         */
        CommandController(const std::shared_ptr<ApplicationController>& appCtrl,
                          const std::shared_ptr<const ApplicationList>& appList,
                          const std::shared_ptr<DVSController>& dvsCtrl,
                          const std::shared_ptr<InformationPublisher>& infoPub,
                          const std::shared_ptr<UserRoutines>& ur,
                          ScheduledThreadPool& schedTP);

        /**
         * @brief Destructor
         */
        ~CommandController();

        /**
         * @brief Default copy constructor: deleted
         */
        CommandController(const CommandController&) = delete;

        /**
         * @brief Default copy assignment operator: deleted
         */
        CommandController& operator=(const CommandController&) = delete;

        /**
         * @brief Default move constructor: deleted
         */
        CommandController(CommandController&&) = delete;

        /**
         * @brief Default move assignment operator: deleted
         */
        CommandController& operator=(CommandController&&) = delete;

        /**
         * @brief It will try to allocate the needed resources for the execution of the @em cmd command.
         *
         * If the command is allowed to be executed, then it is a caller responsibility to properly release
         * the resource when the command is done.
         *
         * @warning Not releasing the resource may cause the busy state to be never cleared.
         *          \n
         * @warning The release of a resource is equivalent to acknowledge that the corresponding command
         *          has been executed. That means failures in releasing the resource will also imply
         *          that eventually command senders will never receive any notification.
         *
         * @param cmd The command to be executed.
         * @return A pair whose first element is a boolean indicating whether the command execution is allowed or not
         *         and whose second element is the resource allocated to the command being executed (the resource is valid
         *         only if the execution of the command is allowed)
         */
        std::pair<bool, CommandResource> grantResource(const std::shared_ptr<RunControlBasicCommand>& cmd);

        /**
         * @brief It changes the membership of some applications as defined in the @em cmd command
         *
         * @note This command is synchronous
         *
         * @param cmd The command to be executed
         */
        void executeCommand(const std::shared_ptr<const ChangeStatusCmd>& cmd);

        /**
         * @brief It starts/stops/restarts applications as defined in the @em cmd command.
         *
         * @param cmd The command to be executed
         *
         * @throws daq::rc::NotAllowed The execution of the command is not allowed (some not-compatible command is still being executed)
         */
        void executeCommand(const std::shared_ptr<const StartStopAppCmd>& cmd);

        /**
         * @brief It changes the publish interval as defined inm the @em cmd command
         *
         * @param cmd The command to be executed
         *
         * @throws daq::rc::NotAllowed The execution of the command is not allowed (some not-compatible command is still being executed)
         */
        void executeCommand(const std::shared_ptr<const ChangePublishIntervalCmd>& cmd);

        /**
         * @brief It changes the interval for publishing full statistics information as defined in the @em cmd command
         *
         * @param cmd The command to be executed
         *
         * @throws daq::rc::NotAllowed The execution of the command is not allowed (some not-compatible command is still being executed)
         */
        void executeCommand(const std::shared_ptr<const ChangeFullStatIntervalCmd>& cmd);

        /**
         * @brief It executes a daq::rc::PublishCmd command
         *
         * @param cmd The command to be executed
         *
         * @throws daq::rc::NotAllowed The execution of the command is not allowed (some not-compatible command is still being executed)
         */
        void executeCommand(const std::shared_ptr<const PublishCmd>& cmd);

        /**
         * @brief It executes a daq::rc::PublishStatsCmd command
         *
         * @param cmd The command to be executed
         *
         * @throws daq::rc::NotAllowed The execution of the command is not allowed (some not-compatible command is still being executed)
         */
        void executeCommand(const std::shared_ptr<const PublishStatsCmd>& cmd);

        /**
         * @brief It executes functionality tests for applications as defined in the @em cmd command
         *
         * @param cmd The command to be executed
         *
         * @throws daq::rc::NotAllowed The execution of the command is not allowed (some not-compatible command is still being executed)
         */
        void executeCommand(const std::shared_ptr<const TestAppCmd>& cmd);

        /**
         * @brief It interrupts all the task being currently executed
         */
        void interruptCurrentTasks();

        typedef boost::signals2::signal<void (bool isBusy)> OnBusy;

        /**
         * @brief Type for a call-back function to be called when the busy state changes
         *
         * The signature of the function is <em>void (bool isBusy)</em>, where the <em>isBusy</em>
         * argument is @em true when the application is busy executing some command
         *
         * @see CommandController::registerOnBusyCallback
         */
        typedef OnBusy::slot_type OnBusySlotType;

        /**
         * @brief Type for a token to be used to remove the call-back registration
         *
         * @see CommandController::removeCallback
         */
        typedef boost::signals2::connection CallbackToken;

        /**
         * @brief It registers a call-back used to be notified when there is a change in the busy state
         *
         * @param cb The call-back function
         * @return A token to be used to un-register the call-back function
         */
        CallbackToken registerOnBusyCallback(const OnBusySlotType& cb);

        // Call-backs are usually removed during destruction
        // This method may be used if the removal has to be done before
        // NOTE: this method is not synchronized with the ones calling
        // the call-back functions (i.e., a call-back may be called in the
        // while and being executed after this method returned).
        // That is not needed now.
        /**
         * @brief It removes a previously registered call-back
         * @param t The token obtained when the call-back was registered
         */
        void removeCallback(const CallbackToken& t);

    protected:
        /**
         * @brief It allocates the needed resources for a command
         *
         * @param cmd The command to be executed
         *
         * @throws daq::rc::NotAllowed The execution of the command is not allowed
         */
        void allocateCommandResource(const std::shared_ptr<const RunControlBasicCommand>& cmd);

        /**
         * @brief It releases the resources allocated to a command
         *
         * @param cmd The command that has been executed
         */
        void releaseCommandResource(const std::shared_ptr<const RunControlBasicCommand>& cmd);

        /**
         * @brief When requested, it notifies the sender of a command as soon as that command has been executed
         *
         * @param senderReference CORBA reference of the command sender
         * @param cmd The object representing the executed command
         *
         * @see daq::rc::RunControlBasicCommand::senderReference
         */
        void notifyCommandSender(const std::string& senderReference, const std::shared_ptr<const RunControlBasicCommand>& cmd);

        /**
         * @overload CommandController::notifyCommandSender(const std::shared_ptr<RunControlBasicCommand>& cmd
         */
        void notifyCommandSender(const std::shared_ptr<const RunControlBasicCommand>& cmd);

    private:
        friend class CommandTask;

        std::shared_mutex m_sub_mutex;
        std::mutex m_res_mutex;
        OnBusy m_busy_sig;
        const std::unique_ptr<TaskBookkeeper> m_tbk;
        std::map<RC_COMMAND, RunControlBasicCommand::ConstCommandSet> m_commands;
        const std::shared_ptr<const ApplicationList> m_app_list;
        const std::shared_ptr<DVSController> m_dvs_ctrl;
        const std::shared_ptr<ApplicationController> m_app_ctrl;
        const std::shared_ptr<UserRoutines> m_user_actions;
        const std::shared_ptr<InformationPublisher> m_info_publisher;
        std::once_flag m_notifier_tp_flag;
        std::unique_ptr<ThreadPool> m_notifier_tp;
        const std::reference_wrapper<ScheduledThreadPool> m_scheduled_actions;
        const std::unique_ptr<ThreadPool> m_command_executor_tp;
};

}}

#endif /* DAQ_RC_COMMANDCONTROLLER_H_ */
