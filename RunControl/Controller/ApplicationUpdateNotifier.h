/*
 * ApplicationUpdateNotifier.h
 *
 *  Created on: May 24, 2024
 *      Author: avolio
 */

#ifndef DAQ_RC_CONTROLLER_APPLICATIONUPDATENOTIFIER_H_
#define DAQ_RC_CONTROLLER_APPLICATIONUPDATENOTIFIER_H_

#include <boost/signals2/signal.hpp>

#include <string>
#include <memory>
#include <mutex>

namespace daq { namespace rc {

class Application;
class ApplicationSVInfo;
class ApplicationSelfInfo;
class AUNDeleter;

/**
 * @brief Utility class used to get notifications about changes in the status of the child applications.
 *
 * Call-backs can be registered in order to get notified about any change in the application status using the <b>registerCallback</b> method.
 * Similarly, call-backs can be removed using the <b>removeCallback</b> method.
 *
 * References to instances of this class can be retrieved using the <b>instance</b> static method.
 */
class ApplicationUpdateNotifier {
    public:
        typedef boost::signals2::signal<void(const std::string&, const ApplicationSVInfo&, const ApplicationSelfInfo&)> OnApplicationUpdate;

        /**
         * @brief Typedef for a call-back function with signature <b>void (const std::string&, const ApplicationSVInfo&, const ApplicationSelfInfo&)</b> where
         *        the arguments are the name of the application and the two data structure holding application information.
         */
        typedef OnApplicationUpdate::slot_type Callback;

        /**
         * @brief Token used to handle the subscription call-back.
         */
        typedef boost::signals2::connection CallbackToken;

        /**
         * @brief Scoped token used to handle the subscription call-back.
         *
         * The corresponding call-back is removed when the token goes out of scope.
         */
        typedef boost::signals2::scoped_connection ScopedCallbackToken;

    public:
        ApplicationUpdateNotifier(const ApplicationUpdateNotifier&) = delete;
        ApplicationUpdateNotifier(ApplicationUpdateNotifier&&) = delete;
        ApplicationUpdateNotifier& operator=(const ApplicationUpdateNotifier&) = delete;
        ApplicationUpdateNotifier& operator=(ApplicationUpdateNotifier&&) = delete;

        /**
         * @brief It returns a reference to an instance of this class.
         *
         * @return A reference to an instance of this class.
         */
        static ApplicationUpdateNotifier& instance();

        /**
         * @brief Use this method to register a function to be called any time the status of a child application changes.
         *
         * @param cb  A call-back function with signature <b>void (const std::string&, const ApplicationSVInfo&, const ApplicationSelfInfo&)</b> where
         *            the arguments are the name of the application and the two data structure holding application information.
         *
         * @return A token to be used to remove the call-back registration.
         */
        CallbackToken registerCallback(const Callback& cb);

        /**
         * @brief It removes a previously registered call-back.
         *
         * @param token The token identifying a previously registered call-back.
         */
        void removeCallback(const CallbackToken& token);

        /**
         * @brief It removes a previously registered call-back.
         *
         * Overloaded method to be used with a <b>ScopedCallbackToken</b> in order to remove the corresponding call-back before the token goes out of scope.
         */
        void removeCallback(ScopedCallbackToken& token);

        /**
         * @brief Utility function to change a simple token into a scoped one.
         *
         * @param token The token identifying a previously registered call-back.
         */
        static ScopedCallbackToken scopedToken(const CallbackToken& token);

    protected:
        /**
         * @brief Method called to notify all the subscribers about any change in the application status.
         *
         * @param name The name of the updated application
         * @param svInfo Application information
         * @param selfInfo Application information
         */
        void notify(const std::string& name, const ApplicationSVInfo& svInfo, const ApplicationSelfInfo& selfInfo);

    private:
        friend class AUNDeleter;
        friend class Application; // This is the place where "notify" is called

        ApplicationUpdateNotifier();
        ~ApplicationUpdateNotifier();

        OnApplicationUpdate m_app_update_signal;

        static std::once_flag m_init_flag;
        static std::unique_ptr<ApplicationUpdateNotifier, AUNDeleter> m_self;
};

}}

#endif /* DAQ_RC_CONTROLLER_APPLICATIONUPDATENOTIFIER_H_ */
