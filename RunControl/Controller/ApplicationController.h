/*
 * ApplicationController.h
 *
 *  Created on: Nov 12, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/ApplicationController.h
 */

#ifndef DAQ_RC_APPLICATIONCONTROLLER_H_
#define DAQ_RC_APPLICATIONCONTROLLER_H_

#include "RunControl/Controller/Application.h"
#include "RunControl/Common/ObjectPool.h"

#include <boost/signals2/signal.hpp>

#include <string>
#include <vector>
#include <set>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <utility>
#include <functional>
#include <atomic>
#include <shared_mutex>

namespace daq { namespace pmg {
    class Process;
    class Handle;
}}

namespace daq { namespace rc {

class ApplicationList;
class TransitionCmd;
class StartStopAppCmd;
class InformationPublisher;
class ThreadPool;
class DVSController;

/**
 * @brief Component taking care of the interaction with the Process Management system.
 *
 * This component takes care of starting, stopping and monitoring the lifetime of processes. It is possible
 * to request this component to start/stop a very well defined set of applications or just to properly start/stop
 * applications when a state transition happens.
 *
 * The ApplicationController updates the status of the application (see daq::rc::ApplicationSVInfo) as soon as it
 * is notified by the PMG system. Additionally it uses the daq::rc::DVSController to test the functionality of any
 * application as soon as it has reached the daq::rc::APPLICATION_STATUS::RUNNING status (tests are performed with
 * the "any" scope and at level 1). When an application fails to start, a "precondition" test is done on it in order
 * to verify if it could not be started because of some up-stream issue.
 *
 * The ApplicationController::registerOnApplicationExitCallback method can be used to register call-backs in order
 * to be notified when an application is no more up.
 *
 * @note Instances of this class can be created only via the ApplicationController::create static function.
 *
 */
class ApplicationController : public std::enable_shared_from_this<ApplicationController> {
	public:
        /**
         * @brief It creates and instance of this class.
         *
         * @param appList The list of applications the controller is responsible for
         * @param dvsCtrl The controller's component taking care of testing application's functionality
         * @param infoPublisher The controller's component taking care of information publishing (to IS and to the Expert System)
         * @return A shared pointer holding a new instance of this class
         *
         * @throws daq::rc::PMGSystemError The PMG sub-system could not be initialized
         * @throws daq::rc::ThreadPoolError Failure in creating the internally used thread pool
         */
        static std::shared_ptr<ApplicationController> create(const std::shared_ptr<ApplicationList>& appList,
                                                             const std::shared_ptr<DVSController>& dvsCtrl,
                                                             const std::shared_ptr<InformationPublisher>& infoPublisher);

        /**
         * @brief Move constructor: deleted
         */
		ApplicationController(ApplicationController&&) = delete;

		/**
		 * @brief Copy constructor: deleted
		 */
		ApplicationController(const ApplicationController&) = delete;

		/**
		 * @brief Copy assignment operator: deleted
		 */
		ApplicationController& operator=(const ApplicationController&) = delete;

		/**
		 * @brief Move assignment operator: deleted
		 */
		ApplicationController& operator=(ApplicationController&&) = delete;

		/**
		 * @brief It will check the existence of the @em app application both on the primary and the back-up hosts.
		 *
		 * This method does not rely on any locally cached information about the status of the process.
		 * The application will be considered as "connected" if its status is different than PMGProcessState_SIGNALED,
		 * PMGProcessState_EXITED, PMGProcessState_SYNCERROR, PMGProcessState_FAILED.
		 *
		 * If more than one alive instance of the same application is found, then all of them will be properly killed.
		 *
		 * @param app The application to look for
		 * @return A pair whose first element is a boolean defining whether or not the application was found, and whose
		 *         second element is a list of names of hosts found in a bad shape.
		 *
		 * @see ApplicationController::isRunningSomewhere
		 *
		 * @throws daq::rc::ApplicationReconnectionFailure Errors occurred interacting with the PMG system
		 */
        std::pair<bool, std::set<std::string>> tryToConnect(const std::shared_ptr<Application>& app);

        /**
         * @brief It resets the status of the @em app application to daq::rc::APPLICATION_STATUS::ABSENT.
         *
         * The reset is done only if the current status of the application is daq::rc::APPLICATION_STATUS::FAILED or
         * daq::rc::APPLICATION_STATUS::EXITED.
         *
         * @param app The application whose status has to be reset
         * @return @em true if the action has been done
         */
        bool resetAppStatus(const std::shared_ptr<Application>& app);

        /**
         * @brief It starts applications configured to be started during a well defined FSM transition
         *
         * Applications are started following a well-defined order:
         * @li daq::rc::ApplicationType::ONLINE_SEGMENT_INFRASTRUCTURE;
         * @li daq::rc::ApplicationType::INFRASTRUCTURE;
         * @li daq::rc::ApplicationType::CHILD_CONTROLLER, daq::rc::ApplicationType::RUN_CONTROL and daq::rc::ApplicationType::STANDARD.
         *
         * This method returns only when all the applications are started. An application is considered as started when:
         * @li Its status is daq::rc::APPLICATION_STATUS::UP or daq::rc::APPLICATION_STATUS::EXITED (the EXITED status can be reached only if the
         *     application was in the UP status before). The check is done in a loop so the UP status may be missed;
         * @li It it out of membership.
         *
         * Applications out of membership are not started, but they are taken into account for start dependencies (if application A depends on application
         * B and application B is not started because out of membership, the A will fail to start).
         *
         * This method can be interrupted calling ApplicationController::interrupt.
         *
         * @param cmd The command representing the transition
         * @return A list of all the applications that should be started during the transition (regardless their membership)
         *
         * @see ApplicationAlgorithms::isStartCompleted
         *
         * @throws daq::rc::DependencyCycle The applications to be started for the FSM transition have a cycle in their start dependencies
         * @throws daq::rc::BadCommand The @em cmd command is mal-formed or corrupted
         * @throws daq::rc::InterruptedException The action has been interrupted
         */
		Application::ApplicationSet startApps(const TransitionCmd& cmd);

		/**
		 * @brief It stops applications configured to be stopped during a well defined FSM transition
		 *
		 * Applications are stopped following a well-defined order:
         * @li daq::rc::ApplicationType::CHILD_CONTROLLER, daq::rc::ApplicationType::RUN_CONTROL and daq::rc::ApplicationType::STANDARD.
         * @li daq::rc::ApplicationType::INFRASTRUCTURE;
         * @li daq::rc::ApplicationType::ONLINE_SEGMENT_INFRASTRUCTURE;
		 *
		 * This method returns only when all the applications are in the daq::rc::APPLICATION_STATUS::EXITED, daq::rc::APPLICATION_STATUS::FAILED
		 * or daq::rc::APPLICATION_STATUS::ABSENT status. The application's membership is not taken into account because at the end of the
		 * transition all the applications have to be exited.
		 *
		 * This method can be interrupted calling ApplicationController::interrupt.
		 *
		 * @param cmd The command representing the transition
		 *
		 * @see ApplicationAlgorithms::isStopCompleted
		 *
		 * @throws daq::rc::DependencyCycle The applications to be stopped for the FSM transition have a cycle in their shutdown dependencies
         * @throws daq::rc::BadCommand The @em cmd command is mal-formed or corrupted
         * @throws daq::rc::InterruptedException The action has been interrupted
		 */
		void stopApps(const TransitionCmd& cmd);

		/**
		 * @brief It starts all the applications described in the @em cmd command
		 *
		 * This method returns when all the applications have either been started (<em>i.e.</em>, they are now in the daq::rc::APPLICATION_STATUS::UP or
		 * daq::rc::APPLICATION_STATUS::EXITED status) or just could not be started.
		 *
		 * When applications of different types are started, the same order as specified in ApplicationController::startApps(const daq::rc::TransitionCmd& cmd)
		 * is followed.
		 *
		 * The application's membership is not taken into account because it is completely valid to request to start an out of membership application.
		 *
		 * This method can be interrupted calling ApplicationController::interrupt.
		 *
		 * @see ApplicationAlgorithms::isStartCompleted
		 *
		 * @param cmd Command containing the needed information about the applications to be started
		 * @return A list of all the applications that have been successfully started (<em>i.e.</em>, they have reached the daq::rc::APPLICATION_STATUS::UP status)
		 *
		 * @throws daq::rc::DependencyCycle The applications to be started have a cycle in their start dependencies
		 * @throws daq::rc::BadCommand The @em cmd command is mal-formed or corrupted
		 * @throws daq::rc::InterruptedException The action has been interrupted
		 */
		Application::ApplicationSet startApps(const StartStopAppCmd& cmd);

		/**
		 * @brief It stops all the applications described in the @em cmd command
		 *
		 * This method returns when all the applications have either been stopped (<em>i.e.</em>, they are now in the daq::rc::APPLICATION_STATUS::EXITED,
         * daq::rc::APPLICATION_STATUS::FAILED or daq::rc::APPLICATION_STATUS::ABSENT status) or just could not be stopped.
		 *
		 * When applications of different types are stopped, the same order as specified in ApplicationController::stopApps(const daq::rc::TransitionCmd& cmd)
         * is followed.
		 *
		 * The application's membership is not taken into account because it is completely valid to request to stop an out of membership application.
		 *
		 * This method can be interrupted calling ApplicationController::interrupt.
		 *
		 * @see ApplicationAlgorithms::isStopCompleted
		 *
		 * @param cmd Command containing the needed information about the applications to be stopped
		 * @return A list of all the applications that have been successfully stopped
		 *
		 * @throws daq::rc::DependencyCycle The applications to be stopped have a cycle in their shutdown dependencies
         * @throws daq::rc::BadCommand The @em cmd command is mal-formed or corrupted
         * @throws daq::rc::InterruptedException The action has been interrupted
		 */
		Application::ApplicationSet stopApps(const StartStopAppCmd& cmd);

		/**
		 * @brief It restarts all the applications described in the @em cmd command
		 *
		 * This method returns when all the applications have been either restarted or just could not be restarted. The conditions for an application
		 * to be considered restarted are the same as the ones for ApplicationsController::startApps(const daq::rc::StartStopAppCmd& cmd).
		 *
		 * Note that when an application is stopped (before being started again) its shutdown dependencies are not taken into account. Start dependencies, on
		 * the other side, are always taken into account. That is done because in a complex system (with several dependencies) it would be almost
		 * impossible to restart any application.
		 *
		 * When applications of different types are restarted, the same order as specified in ApplicationController::startApps(const daq::rc::TransitionCmd& cmd)
         * is followed.
		 *
		 * The application's membership is not taken into account because it is completely valid to request to restart an out of membership application.
		 *
		 * This method can be interrupted calling ApplicationController::interrupt.
		 *
		 * @param cmd Command containing the needed information about the applications to be restarted
		 * @return A list of all the applications that have been successfully restarted
		 *
		 * @see ApplicationAlgorithms::isStartCompleted
		 *
		 * @throws daq::rc::DependencyCycle The applications to be started have a cycle in their start dependencies
         * @throws daq::rc::BadCommand The @em cmd command is mal-formed or corrupted
         * @throws daq::rc::InterruptedException The action has been interrupted
		 */
		Application::ApplicationSet restartApps(const StartStopAppCmd& cmd);

		/**
		 * @brief It kills all the child applications of this controller
		 *
		 * This method should be called only when the controller is going to exit and all the applications have to
		 * be stopped. This method cannot be interrupted and will return only when all the applications are no more
		 * running (the same conditions for ApplicationController::stopApps(const daq::rc::StartStopAppCmd& cmd) apply).
		 *
		 * @throws daq::rc::DependencyCycle The applications to be stopped have a cycle in their shutdown dependencies
		 */
		void exiting();

		/**
		 * @brief It interrupts the execution of any on-going action
		 *
		 * The interrupted state will stay until this method is called with @em shouldInterrupt set to @em false.
		 *
		 * @param shouldInterrupt If @em true any on-going actions is interrupted
		 */
		void interrupt(bool shouldInterrupt);

		typedef boost::signals2::signal<void (const std::shared_ptr<Application>&, bool)> OnApplicationExit;

        /**
         * @brief Typedef for the application on-exit callback
         *
         * The call-back must be any function of type <em>void (const std::shared_ptr<Application>& app, bool exiting)</em> where:
         * @li @em app is the application that exited;
         * @li @em exiting is @em true if the application exited because the controller is exiting (i.e., ApplicationController::exiting
         *     is being executed).
         */
		typedef OnApplicationExit::slot_type OnApplicationExitSlotType;

		/**
		 * @brief Typedef for the token identifying the application on-exit call-back
		 */
		typedef boost::signals2::connection OnApplicationExitSlotToken;

		/**
		 * @brief It registers a call-back to be called every time a process exits
		 *
		 * The call-back is called when a notification by the PMG is received about a process
		 * being in one of its end-state (from a PMG point of view).
		 *
		 * @param cb The call-back to be called
		 * @return A token to be used in order to remove the call-back
		 *
		 * @see ApplicationController::removeOnApplicationExitCallback
		 */
		OnApplicationExitSlotToken registerOnApplicationExitCallback(const OnApplicationExitSlotType& cb);

		/**
		 * @brief It removes a previously registered call-back.
		 *
		 * @param token The token returned by ApplicationController::registerOnApplicationExitCallback
		 */
		void removeOnApplicationExitCallback(const OnApplicationExitSlotToken& token);

	protected:
		/**
		 * @brief It stops the @em app application
		 *
		 * This method returns as soon as the command to stop the application is sent to the PMG.
		 *
		 * Note that the PMG will be asked to stop a process only if it is in the daq::rc::APPLICATION_STATUS::UP or
		 * daq::rc::APPLICATION_STATUS::REQUESTED (with an init timeout greater than zero) state.
		 *
		 * @param app The application to be stopped
		 * @param checkDependencies If @em true the @em app's shutdown dependencies are checked against other applications
		 * @return @em true if the PMG has been asked to stop the process; @em false means that the process is not running
		 *         anymore or the PMG refused to kill it because still in the REQUESTED state.
		 *
		 * @throws daq::rc::ApplicationStopFailure The application could not be stopped (e.g., failure in contacting the PMG)
		 */
        bool stopApp(const std::shared_ptr<Application>& app, bool checkDependencies);

        /**
         * @brief It starts the @em app application
         *
         * This method returns as soon as the command to start the application is sent to the PMG.
         *
         * Note that the PMG will be asked to start the process only if it is in the daq::rc::APPLICATION_STATUS::EXITED,
         * daq::rc::APPLICATION_STATUS::FAILED or daq::rc::APPLICATION_STATUS::ABSENT status.
         *
         * @param app The application to be started
         * @param checkDependencies If @em true the @em app's start dependencies are checked against other applications
         * @return @em true if the PMG has been asked to start the process; @em false means that the process is already up.
         *
         * @throws daq::rc::ApplicationStartingFailure The application could not be started (e.g., failure in contacting the PMG)
         */
        bool startApp(const std::shared_ptr<Application>& app, bool checkDependencies);

        /**
         * @brief It restarts the @em app application
         *
         * @param app The application to be restarted
         *
         * @throws daq::rc::ApplicationRestartingFailure The application could not be restarted
         * @throws daq::rc::InterruptedException The action has been interrupted
         */
        void restartApp(const std::shared_ptr<Application>& app);

        /**
         * @brief It checks the interrupted status
         *
         * @throws daq::rc::InterruptedException When the actions of the ApplicationController are interrupted
         *
         * @see daq::rc::ApplicationController::isInterrupted
         */
        void interrupted() const;

        /**
         * @brief It checks the interrupted status
         *
         * @return @em true if this ApplicationController is interrupted
         *
         * @see daq::rc::ApplicationController::interrupted
         */
        bool isInterrupted() const;

        /**
         * @brief Called to notify all the subscribers that an application exited
         *
         * @param app The exited application
         * @param exiting @em true if the controller is performing its exiting procedure
         */
        void signalApplicationExited(const std::shared_ptr<Application>& app, bool exiting);

        /**
         * @brief Call-back function receiving notifications from the PMG system
         *
         * @param ac Weak pointer to this ApplicationController instance
         * @param p Pointer to the daq::pmg::Process object describing the application this call-back refers to
         * @param other Arbitrary patrameter
         */
        static void pmgCallback(const std::weak_ptr<ApplicationController>& ac, std::shared_ptr<daq::pmg::Process> p, void* other);

        /**
         * @brief Typedef for a mutex/conditional variable pair
         */
        typedef std::pair<std::recursive_mutex, std::condition_variable_any> SyncPair;

        /**
         * @brief It returns the mutex and conditional variable pair associated to a defined application
         *
         * @param app The application
         * @return The mutex and conditional variable pair associated to the @em app application
         */
        std::shared_ptr<SyncPair> applicationSyncPair(const std::shared_ptr<const Application>& app);

        /**
         * @brief It waits for a condition to be satisfied for a set of application
         *
         * This method can be interrupted calling ApplicationController::interrupt
         *
         * @param applications The set of applications the condition should be satisfied for
         * @param condition The condition to verify
         *
         * @throws daq::rc::InterruptedException If the execution of this method has been interrupted
         *
         * @see daq::rc::ApplicationController::waitForCondition
         */
        void waitForCondition_i(std::vector<std::shared_ptr<Application>> applications, const std::function<bool (const Application&)>& condition) const;

        /**
         * @brief It waits for a condition to be satisfied for a set of application
         *
         * This method cannot be interrupted
         *
         * @param applications The set of applications the condition should be satisfied for
         * @param condition The condition to verify
         *
         * @see daq::rc::ApplicationController::waitForCondition_i
         */
        void waitForCondition(std::vector<std::shared_ptr<Application>> applications, const std::function<bool (const Application&)>& condition) const;

        /**
         * @brief It checks that start or shutdown dependencies for an application are satisfied
         *
         * @param app The application whose dependencies should be verified
         * @param isStart @em true if the start dependencies should be verified, @em false otherwise
         *
         * @throws daq::rc::DependenciesFailure If start or shutdown dependencies are not fulfilled (e.g., a dependent application
         *                                      should be up but it is not).
         */
        void checkDependencies(const std::shared_ptr<const Application>& app, bool isStart) const;

        /**
         * @brief It checks if an application is already running on some host
         *
         * This method asks directly the PMGs running on the primary and back-up hosts.
         *
         * Note that if the PMG is not running on some host, then the assumption that the application is not running
         * either is done.
         *
         * @param app The application to check
         * @return A pair made up of all the daq::pmg::Handle handles of found processes and a set of hosts where the
         *         PMG is not properly working
         *
         * @throws daq::rc::ApplicationLookupFailure Errors occurred asking the PMGs.
         */
        std::pair<std::vector<std::shared_ptr<daq::pmg::Handle>>, std::set<std::string>> isRunningSomewhere(const std::shared_ptr<Application>& app) const;

        /**
         * @brief It actually asks the PMG to start a process
         *
         * First the application's primary host is used and then, in case of failure, the back-up hosts.
         *
         * @param app The application to be started
         * @param badHosts Hosts where the application cannot be started
         *
         * @throws daq::rc::ApplicationLaunchFailure The application could not be started (neither on the primary or any back-up host)
         */
        void launch(const std::shared_ptr<Application>& app, const std::set<std::string>& badHosts);

	private:
        friend class StopAppTask;
        friend struct AppCtrlDeleter;

        // throws daq::rc::PMGSystemError, daq::rc::ThreadPoolError
        ApplicationController(const std::shared_ptr<ApplicationList>& appList,
                              const std::shared_ptr<DVSController>& dvsCtrl,
                              const std::shared_ptr<InformationPublisher>& infoPublisher);

        ~ApplicationController() noexcept;

	private:
		ObjectPool<SyncPair> m_app_sync;
		std::shared_mutex m_sig_mutex;
		std::atomic<bool> m_exiting;
		std::atomic<bool> m_interrupted;
		OnApplicationExit m_app_exit_sig;
		const std::shared_ptr<ApplicationList> m_app_list;
        const std::shared_ptr<InformationPublisher> m_info_publisher;
        const std::shared_ptr<DVSController> m_dvs_ctrl;
        const std::unique_ptr<ThreadPool> m_lookup_tp;
        const std::unique_ptr<ThreadPool> m_start_stop_tp;
        static const std::unique_ptr<ThreadPool> m_pmg_cbk_tp;
};

}}

#endif /* DAQ_RC_APPLICATIONCONTROLLER_H_ */
