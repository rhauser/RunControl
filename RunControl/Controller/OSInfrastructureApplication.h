/*
 * OSInfrastructureApplication.h
 *
 *  Created on: Nov 2, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/OSInfrastructureApplication.h
 */

#ifndef DAQ_RC_OSINFRASTRUCTUREAPPLICATION_H_
#define DAQ_RC_OSINFRASTRUCTUREAPPLICATION_H_

#include "RunControl/Controller/InfrastructureApplication.h"
#include "RunControl/Controller/Application.h"

class Configuration;
namespace daq { namespace core {
	class Partition;
	class BaseApplication;
}}

namespace daq { namespace rc {

/**
 * @brief Class representing a child infrastructure application belonging to the Online segment
 */
class OSInfrastructureApplication : public InfrastructureApplication {
	public:
        /**
         * @brief Constructor
         *
         * This kind of applications are always children of the RootController.
         *
         * This is the only class deriving from daq::rc::Application that overwrites
         * the FSM transitions applications are started at.
         * Indeed for the RootController infrastructure applications are started at
         * daq::rc::FSM_COMMAND::INITIALIZE and stopped at daq::rc::FSM_COMMAND::SHUTDOWN while
         * online segment infrastructure applications are instead started at daq::rc::FSM_COMMAND::INIT_FMS
         * and stopped at daq::rc::FSM_COMMAND::STOP_FSM.
         *
         * @param db The ::Configuration object
         * @param p The daq::core::Partition object
         * @param appConfig The daq::core::BaseApplication object describing this application
         *
         * @throws daq::rc::ConfigurationIssue Error accessing information from the configuration database
         */
        OSInfrastructureApplication(Configuration& db,
                                    const daq::core::Partition& p,
                                    const daq::core::BaseApplication& appConfig);

        /**
         * @brief Copy constructor: deleted
         */
        OSInfrastructureApplication(const OSInfrastructureApplication&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        OSInfrastructureApplication(OSInfrastructureApplication&&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
        OSInfrastructureApplication& operator=(const OSInfrastructureApplication& other) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        OSInfrastructureApplication& operator=(OSInfrastructureApplication&& other) = delete;

        /**
         * @brief Destructor
         */
		~OSInfrastructureApplication() noexcept;

        /**
         * @brief It returns daq::rc::ApplicationType::ONLINE_SEGMENT_INFRASTRUCTURE
         *
         * @return daq::rc::ApplicationType::ONLINE_SEGMENT_INFRASTRUCTURE
         */
		ApplicationType type() const noexcept;
};

}}

#endif /* DAQ_RC_OSINFRASTRUCTUREAPPLICATION_H_ */
