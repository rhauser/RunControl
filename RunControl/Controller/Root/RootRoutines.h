/*
 * RootRoutines.h
 *
 *  Created on: Jun 20, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/Controller/Root/RootRoutines.h
 */

#ifndef DAQ_RC_ROOTROUTINES_H_
#define DAQ_RC_ROOTROUTINES_H_

#include "RunControl/Common/UserRoutines.h"

#include <memory>


namespace daq { namespace rc {

class TransitionCmd;
class RunParameters;
class ActiveTime;

/**
 * @brief User routines for the RootController (to be executed before a transition is propagated
 *        through out the RC tree)
 *
 * @note Some additional actions are performed in RootController::fsmTransitioning (because for
 *       some transitions there are no user routines) and in RootController::fsmTransitionDone
 *       (because they need to be done at the end of the transition)
 */
class RootRoutines : public UserRoutines {
    public:

        /**
         * @brief Constructor
         *
         * @param runParams daq::rc::RunParameters instance managing the publication of the run parameters
         * @param activeTime daq::rc::ActiveTime instance managing the calculation of time the Root Controller is
         *                   in the daq::rc::FSM_STATE::RUNNING state
         */
        RootRoutines(const std::shared_ptr<RunParameters>& runParams,
                     const std::shared_ptr<ActiveTime>& activeTime);

        /**
         * @brief Copy constructor: deleted
         */
        RootRoutines(const RootRoutines&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
        RootRoutines& operator=(const RootRoutines&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        RootRoutines(RootRoutines&&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        RootRoutines& operator=(RootRoutines&&) = delete;

        /**
         * @brief Destructor
         */
        ~RootRoutines() noexcept;

        /**
         * @brief It starts the daq::rc::ActiveTime and the daq::rc::RunParameters
         *
         * @param cmd The command representing the current FSM transition
         *
         * @throws daq::rc::RunNumber Error retrieving the run number from the run number service
         */
        void prepareAction(const TransitionCmd& cmd) override;

        /**
         * @brief It stops the daq::rc::ActiveTime and the daq::rc::RunParameters
         *
         * @param cmd The command representing the current FSM transition
         */
        void stopArchivingAction(const TransitionCmd& cmd) override;

        /**
         * @brief Utility method implementing RootController::stopArchivingAction
         *
         * @param isRestart If @em true it means that the RootController has been restated after a crash: in
         *                  this case only a sub-set of actions will be performed
         */
        void stop(bool isRestart);

        /**
         * @brief It returns always returns @em true giving the possibility to retry in case of failures
         *
         * @return Always @em true
         *
         * @see daq::rc::UserRoutines::clearAction
         */
        bool clearAction() noexcept override;

        /**
         * @brief It returns a reference to the daq::rc::RunParameters instance
         *
         * @return A reference to the daq::rc::RunParameters instance
         */
        RunParameters& getRunParams() const;

        /**
         * @brief It returns a reference to the daq::rc::ActiveTime instance
         *
         * @return A reference to the daq::rc::ActiveTime instance
         */
       ActiveTime& getActiveTime() const;

    private:
        const std::shared_ptr<RunParameters> m_run_params;
        const std::shared_ptr<ActiveTime> m_active_time;
};

}}


#endif /* DAQ_RC_ROOTROUTINES_H_ */
