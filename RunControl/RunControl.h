/*
 * RunControl.h
 *
 *  Created on: Jun 27, 2013
 *      Author: avolio
 */

#ifndef DAQ_RC_RUNCONTROL_H_
#define DAQ_RC_RUNCONTROL_H_

/**
 * @namespace daq::rc
 * @brief Namespace for the Run Control
 */

/**
 * @page RCMain Overview of the Run Control Package
 *
 * @tableofcontents
 *
 * All the data structures in this package are defined in the @em daq::rc namespace. The main header @em RunControl/RunControl.h
 * includes all the other header files available for users, even though specific header inclusion is recommended.
 *
 * Link to code examples can be found at the and of this page.
 *
 * @section RCLeaf Main Classes for Leaf Applications
 * For the development of Run Control leaf applications the main classes to be used are:
 * @li daq::rc::ItemCtrl: the entry point to the Run Control framework (header file is @em RunControl/ItemCtrl/ItemCtrl.h);
 * @li daq::rc::Controllable: the interface to implement in order to execute proper actions when the
 *     application receives a command (header file is @em RunControl/Common/Controllable.h);
 * @li daq::rc::ControllableDispatcher: class implementing the policy to be used to call actions on daq::rc::Controllable
 *     instances (header file is @em RunControl/ItemCtrl/ControllableDispatcher.h).
 *
 * Applications using the daq::rc::ItemCtrl should link against @em librc_ItemCtrl.so.
 *
 * @section RCCtrl Main Classes for Segment's Controller Customization
 * In order to customize a segment's controller, the main classes to be used are:
 * @li daq::rc::Controller: the class representing a segment's controller (header file is @em RunControl/Controller/Controller.h);
 * @li daq::rc::UserRoutines: interface to be implemented in order to customize the controller's behavior (header file is @em RunControl/Common/UserRoutines.h).
 *
 * Applications using the daq::rc::Controller class should link against @em librc_Controller.so.
 *
 * @section RCFSM FSM States and Commands (Transitions)
 * The two header files @em RunControl/FSM/FSMStates.h and @em RunControl/FSM/FSMCommands.h contain classes describing the FSM states and transitions.
 * In particular:
 * @li daq::rc::FSM_STATE: enumeration of all the FSM states;
 * @li daq::rc::FSM_COMMAND: enumeration of all the FSM transitions;
 * @li daq::rc::FSMStates: utility class with functions converting FSM state enumeration items to their string representations and viceversa;
 * @li daq::rc::FSMCommands: utility class with functions converting FSM transition enumeration items to their string representations and viceversa.
 *
 * Shared libraries to link against are @em librc_FSMStates.so and @em librc_FSMCommands.so (both @em librc_ItemCtrl.so and @em librc_Controller.so are
 * already linked against them).
 *
 * @section RCCmd Run Control Commands
 * Commands dispatched to controllers and leaf applications are described as <em>command objects</em>, bringing with them some context (<em>e.g.</em>,
 * the source and destination states of a transition, etc.). They all inherit from the same daq::rc::RunControlBasicCommand class and are defined
 * in the @em RunControl/Common/RunControlCommands.h header file. The same header defines also:
 * @li daq::rc::RC_COMMAND: enumeration for all the Run Control commands;
 * @li daq::rc::RunControlCommands: utility class with functions converting Run Control command enumeration items to their string representations and viceversa.
 *
 * The shared library to link against is @em librc_RCCommands.so.
 *
 * Additionally the daq::rc::CommandSender class (in header file @em RunControl/Common/CommandSender.h) is provided in order to
 * send commands to any Run Control application. In this case the shared library to link against is @em librc_Commander.so.
 *
 * @remark A command line utility (@em rc_sender) is provided to send commands to Run Control applications. Refer to the utility help
 *         for additional details.
 *
 * @section RCUtility Utility Classes
 * @li daq::rc::CmdLineParser: it helps in easily creating the @em main(...) function for leaf applications and custom controllers
 *                             taking care of properly parsing the command line options (header file is @em RunControl/Common/CmdLineParser.h
 *                             and the corresponding shared library to link is @em librc_CmdLine.so);
 * @li daq::rc::OnlineServices: it gives easy access to the IPC partition and to the Configuration (header file is @em RunControl/Common/OnlineServices.h
 *                              and the corresponding shared library to link is @em librc_OnlSvc.so).
 *
 * Both @em librc_ItemCtrl.so and @em librc_Controller.so are already linked against @em librc_CmdLine.so and @em librc_OnlSvc.so.
 *
 * @section RCExceptions Exceptions
 * All the exceptions thrown by methods and functions of this library inherit from @em daq::rc::Exception. The are described in the two header
 * files @em RunControl/Common/Exceptions.h and @em RunControl/Common/UserExceptions.h, with the latter historically describing exceptions
 * issued by different sub-systems and not by the Run Control core.
 *
 * @section RCExamples Examples
 * <a href="https://gitlab.cern.ch/atlas-tdaq-software/RunControl/blob/master/bin/rc_example_application.cxx">This link</a>
 * contains an example of a simple leaf application: in particular it includes a simple implementation of the daq::rc::Controllable
 * interface and shows how to instantiate and start the daq::rc::ItemCtrl and how to deal with the command line parameters.
 * \n
 * <a href="https://gitlab.cern.ch/atlas-tdaq-software/RunControl/blob/master/bin/rc_controller.cxx">This link</a>
 * contains an example of a simple controller (actually it is the code for the default controller, without any customization).
 *
 * @attention Remember that when an application is executed in the Run Control environment, it has to accept some very well defined
 *            command line parameters. Look at the daq::rc::CmdLineParser documentation for additional details.
 */

// "Utility" headers
#include <RunControl/Common/CmdLineParser.h>
#include <RunControl/Common/Clocks.h>
#include <RunControl/Common/OnlineServices.h>

// Headers for the FSM
#include <RunControl/FSM/FSMStates.h>
#include <RunControl/FSM/FSMCommands.h>

// Headers for commands
#include <RunControl/Common/RunControlBasicCommand.h>
#include <RunControl/Common/RunControlCommands.h>
#include <RunControl/Common/CommandSender.h>
#include <RunControl/Common/CommandNotifier.h>

// Headers for the controller
#include <RunControl/Controller/Controller.h>
#include <RunControl/Common/UserRoutines.h>

// Headers for leaf applications
#include <RunControl/ItemCtrl/ItemCtrl.h>
#include <RunControl/ItemCtrl/ControllableDispatcher.h>
#include <RunControl/Common/Controllable.h>

// Headers for exceptions
#include <RunControl/Common/Exceptions.h>
#include <RunControl/Common/UserExceptions.h>


#endif /* DAQ_RC_RUNCONTROL_H_ */
