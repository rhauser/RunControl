/*
 * Algorithms.h
 *
 *  Created on: Jun 17, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/Common/Algorithms.h
 */

#ifndef DAQ_RC_ALGORITHMS_H_
#define DAQ_RC_ALGORITHMS_H_

#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/Exceptions.h"

#include <config/Configuration.h>
#include <dal/Partition.h>
#include <dal/Segment.h>
#include <dal/OnlineSegment.h>
#include <dal/Component.h>
#include <dal/SubTransition.h>
#include <dal/util.h>

#include <boost/lexical_cast.hpp>

#include <string>
#include <vector>
#include <deque>
#include <map>
#include <list>
#include <memory>
#include <iterator>


namespace daq { namespace rc {

/**
 * @brief Utility class implementing some algorithms
 */
class Algorithms {
    public:
        /**
         * @brief Map from a daq::rc::FSM_COMMAND to a vector of sub-transitions
         */
        typedef std::map<FSM_COMMAND, std::vector<std::string>> SubtransitionMap;

        /**
         * @brief It computes the sub-transitions defined for a given segment
         *
         * @param seg The segment
         * @param subTrMap The map to be filled with sub-transitions
         *
         * @throws daq::rc::ConfigurationIssue Errors retrieving information from the configuration database
         */
        static void subTransitions(const daq::core::Segment& seg, Algorithms::SubtransitionMap& subTrMap) {
            try {
                const auto& subTransitions = seg.get_SubTransitions();
                for(const daq::core::SubTransition* st : subTransitions) {
                    const std::string& mainTrStr = st->get_MainTransition();
                    try {
                        FSM_COMMAND mainTr = FSMCommands::stringToCommand(st->get_MainTransition());
                        const auto& subSteps = st->get_Substeps();
                        for(const auto& stt : subSteps) {
                            subTrMap[mainTr].push_back(stt);
                        }
                    }
                    catch(daq::rc::BadCommand& ex) {
                        ers::error(daq::rc::BadSubtransition(ERS_HERE, "the main transition is not valid", mainTrStr, ex));
                    }
                }
            }
            catch(daq::config::Exception& ex) {
                const std::string& msg = std::string("Cannot retrieve the list of sub-transitions for segment \"") +  seg.UID() + "\" from the database";
                const std::string& reason = ex.message();
                throw daq::rc::ConfigurationIssue(ERS_HERE, msg + ": " + reason, reason, ex);
            }
        }

        /**
         * @brief For a given segment it calculates the sub-transitions defined in all the parent segments
         *
         * @param db The ::Configuration object
         * @param p The daq::core::Partition object
         * @param seg The daq::core::Segemnt object
         * @param segFullName The full name of the segment
         * @return A map filled with all the sub-transitions
         *
         * @throws daq::rc::ConfigurationIssue Errors retrieving information from the configuration database
         */
        static Algorithms::SubtransitionMap subTransitionsFromParents(Configuration& db,
                                                                      const daq::core::Partition& p,
                                                                      const daq::core::Segment& seg,
                                                                      const std::string& segFullName)
        {
            Algorithms::SubtransitionMap subTrMap;

            try {
                const daq::core::OnlineSegment* os = p.get_OnlineInfrastructure();
                if(os->UID() != segFullName) {
                    Algorithms::subTransitions(*os, subTrMap);
                }

                std::list<std::vector<const daq::core::Component*>> paths;
                seg.get_parents(p, paths);

                const unsigned int numOfPaths = paths.size();
                if(numOfPaths == 1) {
                    const auto& parents = paths.front();
                    for(const daq::core::Component* component : parents) {
                        const daq::core::Segment* parentSegment = db.cast<daq::core::Segment>(component);
                        if(parentSegment != nullptr) {
                            Algorithms::subTransitions(*parentSegment, subTrMap);
                        }
                    }
                } else if(numOfPaths > 1) {
                    const std::string& msg = std::string("The segment \"") + segFullName + "\" seems to have more than one direct parent!";
                    throw daq::rc::ConfigurationIssue(ERS_HERE, msg);
                }
            }
            catch(daq::config::Exception& ex) {
                const std::string& msg = std::string("Cannot retrieve the list of sub-transitions for segment \"") +  segFullName + "\" from the database";
                const std::string& reason = ex.message();
                throw daq::rc::ConfigurationIssue(ERS_HERE, msg + ": " + reason, reason, ex);
            }
            catch(daq::core::AlgorithmError& ex) {
                const std::string& msg = std::string("Cannot retrieve the list of sub-transitions for segment \"") +  segFullName + "\" from the database";
                const std::string& reason = ex.message();
                throw daq::rc::ConfigurationIssue(ERS_HERE, msg + ": " + reason, reason, ex);
            }

            return subTrMap;
        }

        /**
         * @brief It builds the proper command object starting from a command item and its arguments.
         *
         * Useful for command line parsing in utility applications.
         *
         * @param command The command
         * @param arguments The command's arguments
         * @return The command object representing the command
         *
         * @throws daq::rc::BadCommand The command is not supported or has bad arguments
         */
        static std::unique_ptr<RunControlBasicCommand> buildCommand(RC_COMMAND command, const std::deque<std::string>& arguments) {
            std::unique_ptr<RunControlBasicCommand> rcCmd;

            if(command == RC_COMMAND::MAKE_TRANSITION) {
                if(arguments.empty() == true) {
                    throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::MAKE_TRANSITION_CMD, "the transition command has not been specified");
                }

                // This will throw if the string does not represent a good FSM command
                const FSM_COMMAND fsmCmd = FSMCommands::stringToCommand(arguments[0]);

                // Use the remaining elements as arguments
                const auto firstArg = ++(arguments.cbegin());
                const auto& lastArg = arguments.cend();
                const auto numOfArgs = std::distance(firstArg, lastArg);

                switch(fsmCmd) {
                    case FSM_COMMAND::USERBROADCAST:
                    {
                        if(numOfArgs == 0) {
                            const std::string& msg = std::string("For the ") + FSMCommands::USERBROADCAST_CMD + " command, the user command has to be defined";
                            throw daq::rc::BadCommand(ERS_HERE, FSMCommands::USERBROADCAST_CMD, msg);
                        } else {
                            const UserCmd usrCmd(*firstArg, std::vector<std::string>(std::next(firstArg), lastArg));
                            rcCmd.reset(new UserBroadcastCmd(usrCmd));
                        }

                        break;
                    }
                    case FSM_COMMAND::SUB_TRANSITION:
                    {
                        if(numOfArgs < 2) {
                            const std::string& msg = std::string("For the ") + FSMCommands::SUB_TRANSITION_CMD +
                                                    " command, the sub-transition name and the main transition command have to be defined";
                            throw daq::rc::BadCommand(ERS_HERE, FSMCommands::SUB_TRANSITION_CMD, msg);
                        } else {
                            try {
                                const FSM_COMMAND mainCmd = FSMCommands::stringToCommand(*std::next(firstArg));
                                rcCmd.reset(new SubTransitionCmd(*firstArg, mainCmd));
                            }
                            catch(daq::rc::BadCommand& ex) {
                                const std::string& msg = std::string("the specified main command \"") + *std::next(firstArg) + "\" is not a valid FSM command";
                                throw daq::rc::BadCommand(ERS_HERE, FSMCommands::SUB_TRANSITION_CMD, msg, ex);
                            }
                        }

                        break;
                    }
                    case FSM_COMMAND::RESYNCH:
                    {
                        if(numOfArgs == 0) {
                            const std::string& msg = std::string("For the ") + FSMCommands::RESYNCH_CMD + " command, the ECR number has to be defined";
                            throw daq::rc::BadCommand(ERS_HERE, FSMCommands::RESYNCH_CMD, msg);
                        } else {
                            try {
                                std::uint32_t ecr = boost::lexical_cast<std::uint32_t>(*firstArg);

                                std::uint32_t el1id = 0;
                                auto it = std::next(firstArg);
                                if(it != lastArg) {
                                    try {
                                        el1id = boost::lexical_cast<std::uint32_t>(*it);
                                    }
                                    catch(boost::bad_lexical_cast&) {
                                        // The extended L1 ID is not defined
                                        it = firstArg;
                                    }
                                } else {
                                    it = firstArg;
                                }

                                rcCmd.reset(new ResynchCmd(ecr,
                                                           el1id,
                                                           std::vector<std::string>(std::next(it), lastArg)));
                            }
                            catch(boost::bad_lexical_cast& ex) {
                                const std::string& msg = *firstArg + " is not a valid ECR number";
                                throw daq::rc::BadCommand(ERS_HERE, FSMCommands::RESYNCH_CMD, msg, ex);
                            }
                        }

                        break;
                    }
                    default:
                    {
                        std::unique_ptr<TransitionCmd> trCmd(new TransitionCmd(fsmCmd));
                        if((numOfArgs != 0) && (*firstArg == "NO_SUBTR")) {
                            trCmd->skipSubTransitions(true);
                        }

                        rcCmd.reset(trCmd.release());

                        break;
                    }
                }
            } else {
                switch(command) {
                    case RC_COMMAND::TESTAPP:
                    {
                        if(arguments.empty() == true) {
                            const std::string msg = "No applications to be tested have been specified";
                            throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::TESTAPP_CMD, msg);
                        }

                        std::unique_ptr<TestAppCmd> tac;
                        if(arguments.size() >= 3) {
                            // Test's scope and level can only be defined in the number of arguments is greater than 3
                            // <application list> <scope> <level>
                            try {
                                auto it = --arguments.end();

                                // Check whether the last argument can be casted to a number
                                const int l = boost::lexical_cast<int>(*it);

                                // The scope is the previous one
                                const std::string& s = *(--it);

                                tac.reset(new TestAppCmd(std::vector<std::string>(arguments.begin(), it)));
                                tac->scope(s);
                                tac->level(l);
                            }
                            catch(boost::bad_lexical_cast&) {
                                // The last argument is not the test's level; then all the arguments are applications
                                tac.reset(new TestAppCmd(std::vector<std::string>(arguments.begin(), arguments.end())));
                            }
                        } else {
                            tac.reset(new TestAppCmd(std::vector<std::string>(arguments.begin(), arguments.end())));
                        }

                        rcCmd.reset(tac.release());

                        break;
                    }
                    case RC_COMMAND::USER:
                    {
                        if(arguments.empty() == true) {
                            const std::string& msg = std::string("For the ") + RunControlCommands::USER_CMD + " command, the user command name has to be defined";
                            throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::USER_CMD, msg);
                        }

                        rcCmd.reset(new UserCmd(arguments[0], std::vector<std::string>(++(arguments.begin()), arguments.end())));

                        break;
                    }
                    case RC_COMMAND::ENABLE:
                    {
                        if(arguments.empty() == true) {
                            const std::string msg = "No components to be enabled have been specified";
                            throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::ENABLE_CMD, msg);
                        }

                        rcCmd.reset(new ChangeStatusCmd(true, std::vector<std::string>(arguments.begin(), arguments.end())));

                        break;
                    }
                    case RC_COMMAND::DISABLE:
                    {
                        if(arguments.empty() == true) {
                            const std::string msg = "No components to be disabled have been specified";
                            throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::DISABLE_CMD, msg);
                        }

                        rcCmd.reset(new ChangeStatusCmd(false, std::vector<std::string>(arguments.begin(), arguments.end())));

                        break;
                    }
                    case RC_COMMAND::STARTAPP:
                    {
                        if(arguments.empty() == true) {
                            const std::string msg = "No application to be started has been specified";
                            throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::STARTAPP_CMD, msg);
                        }

                        rcCmd.reset(new StartStopAppCmd(StartStopAppCmd::is_start_app_cmd, std::vector<std::string>(arguments.begin(), arguments.end())));

                        break;
                    }
                    case RC_COMMAND::STOPAPP:
                    {
                        if(arguments.empty() == true) {
                            const std::string msg = "No application to be stopped has been specified";
                            throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::STOPAPP_CMD, msg);
                        }

                        rcCmd.reset(new StartStopAppCmd(StartStopAppCmd::is_stop_app_cmd, std::vector<std::string>(arguments.begin(), arguments.end())));

                        break;

                    }
                    case RC_COMMAND::RESTARTAPP:
                    {
                        if(arguments.empty() == true) {
                            const std::string msg = "No application to be restarted has been specified";
                            throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::RESTARTAPP_CMD, msg);
                        }

                        rcCmd.reset(new StartStopAppCmd(StartStopAppCmd::is_restart_app_cmd, std::vector<std::string>(arguments.begin(), arguments.end())));

                        break;
                    }
                    case RC_COMMAND::PUBLISH:
                    {
                        rcCmd.reset(new PublishCmd());

                        break;
                    }
                    case RC_COMMAND::CHANGE_PROBE_INTERVAL:
                    {
                        if(arguments.empty() == true) {
                            const std::string msg = "The new publishing interval has not been defined";
                            throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::CHANGE_PROBE_INTERVAL_CMD, msg);
                        }

                        try {
                            rcCmd.reset(new ChangePublishIntervalCmd(boost::lexical_cast<unsigned int>(arguments[0])));
                        }
                        catch(boost::bad_lexical_cast& ex) {
                            const std::string& msg = std::string("The defined publishing interval is not correct: ") + ex.what();
                            throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::CHANGE_PROBE_INTERVAL_CMD, msg, ex);
                        }

                        break;
                    }
                    case RC_COMMAND::PUBLISHSTATS:
                    {
                        rcCmd.reset(new PublishStatsCmd());

                        break;
                    }
                    case RC_COMMAND::CHANGE_FULLSTATS_INTERVAL:
                    {
                        if(arguments.empty() == true) {
                            const std::string msg = "The new full-stats interval has not been defined";
                            throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::CHANGE_FULLSTATS_INTERVAL_CMD, msg);
                        }

                        try {
                            rcCmd.reset(new ChangeFullStatIntervalCmd(boost::lexical_cast<unsigned int>(arguments[0])));
                        }
                        catch(boost::bad_lexical_cast& ex) {
                            const std::string& msg = std::string("The defined interval for full-stats is not correct: ") + ex.what();
                            throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::CHANGE_FULLSTATS_INTERVAL_CMD, msg, ex);
                        }

                        break;
                    }
                    case RC_COMMAND::DYN_RESTART:
                    {
                        if(arguments.empty() == true) {
                            const std::string msg = "No segments for the dynamic restart have been defined";
                            throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::DYN_RESTART_CMD, msg);
                        }

                        rcCmd.reset(new DynRestartCmd(std::vector<std::string>(arguments.begin(), arguments.end())));

                        break;
                    }
                    case RC_COMMAND::IGNORE_ERROR:
                    {
                        rcCmd.reset(new IgnoreErrorCmd());

                        break;
                    }
                    case RC_COMMAND::EXIT:
                    {
                        rcCmd.reset(new ExitCmd());

                        break;
                    }
                    default:
                    {
                        // TODO: remember to complete it with any additional supported command
                        const std::string& commandName = RunControlCommands::commandToString(command);
                        const std::string& msg = commandName + " is not supported";
                        throw daq::rc::BadCommand(ERS_HERE, commandName, msg);

                        break;
                    }
                }
            }

            return rcCmd;
        }


    private:
        Algorithms() = delete;
        ~Algorithms() = delete;
};

}}

#endif /* DAQ_RC_ALGORITHMS_H_ */
