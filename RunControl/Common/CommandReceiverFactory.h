/*
 * CommandReceiverFactory.h
 *
 *  Created on: Oct 29, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Common/CommandReceiverFactory.h
 */

#ifndef DAQ_RC_COMMANDRECEIVERFACTORY_H_
#define DAQ_RC_COMMANDRECEIVERFACTORY_H_

#include <memory>

namespace daq { namespace rc {

/**
 * @brief Enumeration identifying different kinds of daq::rc::CommandReceiver
 */
enum class RECV_TYPE : unsigned int {
	CORBA,//!< CORBA - Receiver for commands sent over CORBA
	CMDLN //!< CMDLN - Receiver waiting for commands on the application's standard input (command line)
};

class CommandReceiver;
class CommandedApplication;

/**
 * @brief Factory class allowing to create daq::rc::CommandReceiver instances of different kinds
 */
class CommandReceiverFactory {
	public:
        /**
         * @brief Constructor: deleted
         */
        CommandReceiverFactory() = delete;

        /**
         * @brief Destructor: deleted
         */
        ~CommandReceiverFactory() = delete;

        /**
         * @brief It creates the proper daq::rc::CommandReceiver
         *
         * @param type The receiver type
         * @param ca reference to the application that will execute the received command
         *
         * @return A pointer to the proper daq::rc::CommandReceiver
         */
        static std::shared_ptr<CommandReceiver> create(RECV_TYPE type, CommandedApplication& ca);
};

}}

#endif /* DAQ_RC_COMMANDRECEIVERFACTORY_H_ */
