/*
 * CommandedApplication.h
 *
 *  Created on: Jun 7, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/Common/CommandedApplication.h
 */

#ifndef DAQ_RC_COMMANDEDAPPLICATION_H_
#define DAQ_RC_COMMANDEDAPPLICATION_H_

#include <string>
#include <vector>

namespace daq { namespace rc {

/**
 * @brief Pure interface representing an application able to execute external commands
 *
 * @see daq::rc::RunController
 * @see daq::rc::ItemCtrlImpl
 */
class CommandedApplication {
    public:
        /**
         * @brief Type describing error reasons
         *
         * Each error item is made up of an array of size three:
         * @li First element is the name of the application causing the error;
         * @li Second element is the description of the error;
         * @li Third element is the error type.
         */
        typedef std::vector<std::array<std::string, 3>> ErrorItems;

        /**
         * @brief Class used to identify the sender of a command
         */
        class SenderContext {
            public:
                /**
                 * @brief Constructor
                 *
                 * @param userName User name the sender application is running for
                 * @param hostName Name of the host where the sender application is running
                 * @param appId The identifier of the sender application (usually the application's name)
                 */
                SenderContext(const std::string& userName,
                              const std::string& hostName,
                              const std::string& appId)
                    : m_user_name(userName), m_host_name(hostName), m_application_id(appId)
                {
                }

                /**
                 * @brief It returns the user name the sender application is running for
                 *
                 * @return The user name the sender application is running for
                 */
                const std::string& getUserName() const {
                    return m_user_name;
                }

                /**
                 * @brief It returns the name of the host where the sender application is running
                 *
                 * @return The name of the host where the sender application is running
                 */
                const std::string& getHostName() const {
                    return m_host_name;
                }

                /**
                 * @brief It returns the identifier of the sender application (usually the application's name)
                 *
                 * @return The identifier of the sender application (usually the application's name)
                 */
                const std::string& getApplicationId() const {
                    return m_application_id;
                }

            private:
                const std::string m_user_name;
                const std::string m_host_name;
                const std::string m_application_id;
        };

    public:
        /**
         * @brief Destructor
         */
        virtual ~CommandedApplication() noexcept {;}

        /**
         * @brief This must return the name of the application executing the commands
         */
        virtual std::string id() = 0;

        /**
         * @brief This must return the name of the partition the application executing the commands belongs to
         */
        virtual std::string partition() = 0;

        /**
         * @brief Implementing classes have to implement proper execution of FSM transition commands
         *
         * In case of errors the following exception should be thrown:
         * @li daq::rc::Busy - The command cannot be executed because the application is busy;
         * @li daq::rc::NotAllowed - The command cannot be executed because not allowed;
         * @li daq::rc::InErrorState - The command cannot be executed because the application is in error;
         * @li Any other exception inheriting from ers::Issue.
         *
         * @param ctx Sender application's context
         * @param commandDescription String representation of the transition command
         *
         * @see daq::rc::RunControlBasicCommand
         * @see daq::rc::TransitionCmd
         */
        virtual void makeTransition(const CommandedApplication::SenderContext& ctx,
                                    const std::string& commandDescription) = 0;

        /**
         * @brief Implementing classes have to implement proper execution of commands (not transitions)
         *
         * In case of errors the following exception should be thrown:
         * @li daq::rc::Busy - The command cannot be executed because the application is busy;
         * @li daq::rc::NotAllowed - The command cannot be executed because not allowed;
         * @li daq::rc::InErrorState - The command cannot be executed because the application is in error;
         * @li Any other exception inheriting from ers::Issue.
         *
         * @param ctx Sender application's context
         * @param commandDescription String representation of the command
         *
         * @see daq::rc::RunControlBasicCommand
         */
        virtual void executeCommand(const CommandedApplication::SenderContext& ctx,
                                    const std::string& commandDescription) = 0;

        /**
         * @brief Implementing classes have to implement proper set/clear of the error status
         *
         * @param ctx Sender application's context
         * @param errorItems Items describing the error causes (if empty the error state should be cleared)
         */
        virtual void setError(const CommandedApplication::SenderContext& ctx,
                              const ErrorItems& errorItems) = 0;

        /**
         * @brief Implementing classes should return an encoded string representing the status of the application
         *
         * @return String representing the status of the application
         *
         * @see daq::rc::ApplicationStatusCmd
         */
        virtual std::string status() = 0;

        /**
         * @brief Implementing classes should return an encoded string representing the status of a child application
         *
         * If the child application is not known then an exception of type daq::rc::UnknownChild should be thrown
         *
         * @param childName Name of the child application
         * @return An encoded string representing the status of a child application
         *
         * @see daq::rc::ApplicationStatusCmd
         */
        virtual std::string childStatus(const std::string& childName) = 0;

        /**
         * @brief Implementing classes should update information about a child application
         * @param childDescription Encoded string representing the status of a child application
         *
         * @see daq::rc::ApplicationStatusCmd
         */
        virtual void updateChild(const std::string& childDescription) = 0;
};

}}


#endif /* DAQ_RC_COMMANDEDAPPLICATION_H_ */
