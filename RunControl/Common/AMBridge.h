/*
 * AMBridge.h
 *
 *  Created on: Jul 12, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/Common/AMBridge.h
 */

#ifndef DAQ_RC_AMBRIDGE_H_
#define DAQ_RC_AMBRIDGE_H_

#include <string>
#include <memory>

namespace daq { namespace am {
    class ServerInterrogator;
}}

namespace daq { namespace rc {

/**
 * @brief Class used to ask the Access Management system whether a command can be executed or not
 */
class AMBridge {
    public:
        /**
         * @brief Costructor
         *
         * @param partitionName The name of the partition
         */
        explicit AMBridge(const std::string& partitionName);

        /**
         * @brief Copy constructor: deleted.
         */
        AMBridge(const AMBridge&) = delete;

        /**
         * @brief Copy assignment operator: deleted.
         */
        AMBridge& operator=(const AMBridge&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        AMBridge(AMBridge&&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        AMBridge& operator=(AMBridge&&) = delete;

        /**
         * @brief Destructor
         */
        ~AMBridge();

        /**
         * @brief It checks whether a command can be executed
         *
         * @param user The user requesting the command
         * @param cmdName The name of the command
         *
         * @return @em true if the command can be executed
         */
        bool canExecuteCommand(const std::string& user, const std::string& cmdName);

    private:
        const std::string m_partition_name;
        const std::string m_process_owner;
        const std::unique_ptr<daq::am::ServerInterrogator> m_server_int;
};

}}


#endif /* DAQ_RC_AMBRIDGE_H_ */
