/*
 * CommandNotifierAdapter.h
 *
 *  Created on: May 12, 2014
 *      Author: avolio
 */

/**
 * @file RunControl/Common/CommandNotifierAdapter.h
 */

#ifndef DAQ_RC_COMMANDNOTIFIERADAPTER_H_
#define DAQ_RC_COMMANDNOTIFIERADAPTER_H_

#include "RunControl/Common/RunControlBasicCommand.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/Exceptions.h"

#include <rc/rc.hh>
#include <ers/ers.h>
#include <ipc/object.h>
#include <ipc/core.h>
#include <ipc/exceptions.h>

#include <functional>
#include <string>
#include <memory>


namespace daq { namespace rc {

/**
 * @brief Simple adapter class implementing the CORBA rc::sender interface used to receive a
 *        notification when a command has been executed by a remote Run Control application
 *
 * @warning Remember that instances of this class have to be always created on the heap and
 *          destroyed calling "_destroy()"
 */
class CommandNotifierAdapter : public IPCObject<POA_rc::sender> {
    public:
        /**
         * @brief Typedef for the call-back function executed when a command is completed
         */
        typedef std::function<void (const RunControlBasicCommand&)> Callback;

    public:
        /**
         * @brief Constructor
         *
         * @param cb The call-back function executed when a command is completed
         */
        CommandNotifierAdapter(Callback cb)
            : IPCObject<POA_rc::sender>(), m_cb(std::move(cb))
        {
        }

        /**
         * @brief Copy constructor: deleted
         */
        CommandNotifierAdapter(const CommandNotifierAdapter&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        CommandNotifierAdapter(CommandNotifierAdapter&&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
        CommandNotifierAdapter& operator=(const CommandNotifierAdapter&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        CommandNotifierAdapter& operator=(CommandNotifierAdapter&&) = delete;


        /**
         * @brief Implementation of the rc::sender IDL interface
         *
         * @param commandDescription String representation of the executed command
         */
        void commandExecuted(const char* commandDescription) override {
            if(m_cb != nullptr) {
                try {
                    const std::unique_ptr<RunControlBasicCommand>& cmd = RunControlCommands::factory(std::string(commandDescription));
                    m_cb(*cmd);
                }
                catch(daq::rc::BadCommand& ex) {
                    // This should never happen: it implies some data corruption while sending data through the network
                    ex.wrap_message("Cannot execute call-back, the command description seems to be corrupted: ",
                                    std::string(" (full command string is ") + commandDescription + ")");
                    ers::error(ex);
                }
            }
        }

        /**
         * @brief It returns the CORBA reference of this object
         *
         * @return The CORBA reference of this object
         *
         * @throws daq::rc::OnlineServicesFailure Some error occurred building the CORBA reference string
         */
        std::string reference() {
            try {
                return IPCCore::objectToString(_this(), IPCCore::Corbaloc);
            }
            catch(ipc::Exception& ex) {
                throw daq::rc::OnlineServicesFailure(ERS_HERE, "Some error occurred building the CORBA reference string: ", ex);
            }
        }

        /**
         * @brief Utility method used to notify a command sender about the execution a given command
         *
         * @param senderReference The CORBA reference of the command sender to be notified
         * @param cmd The completed command
         *
         * @throws daq::rc::CannotNotifyCommandSender Some error occurred notifying the command sender
         */
        static void notifyCommandSender(const std::string& senderReference, const std::shared_ptr<const RunControlBasicCommand>& cmd) {
            try {
                auto&& objPtr = IPCCore::stringToObject(senderReference);
                ::rc::sender_var snd = ::rc::sender::_narrow(objPtr);
                if(CORBA::is_nil(snd) == false) {
                    snd->commandExecuted(CORBA::string_dup(cmd->serialize().c_str()));
                } else {
                    throw daq::rc::CannotNotifyCommandSender(ERS_HERE,
                                                             senderReference,
                                                             cmd->toString(),
                                                             "got an invalid CORBA reference");
                }
            }
            catch(ipc::Exception& ex) {
                throw daq::rc::CannotNotifyCommandSender(ERS_HERE,
                                                         senderReference,
                                                         cmd->toString(),
                                                         "failed to obtain a proper CORBA reference to it",
                                                         ex);
            }
            catch(CORBA::Exception& ex) {
                throw daq::rc::CannotNotifyCommandSender(ERS_HERE,
                                                         senderReference,
                                                         cmd->toString(),
                                                         std::string("got CORBA exception \"") + ex._name() + "\" trying to contact it");
            }
        }

    private:
        const Callback m_cb;
};

}}


#endif /* DAQ_RC_COMMANDNOTIFIERADAPTER_H_ */
