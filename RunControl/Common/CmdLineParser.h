/*
 * CmdLineParser.h
 *
 *  Created on: Jun 4, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/Common/CmdLineParser.h
 */

#ifndef DAQ_RC_CMDLINEPARSER_H_
#define DAQ_RC_CMDLINEPARSER_H_

#include <boost/property_tree/ptree.hpp>

#include <string>

namespace daq { namespace rc {

/**
 * @brief Command line parser for Run Control applications.
 *
 * This class can be used to parse the command line parameters of a run control application.
 * All the parsing is done in the class's constructor: once the object is built users can have
 * easy access to the passed parameters and use it to build instances of Controller and ItemCtrl.
 *
 * Access to the usual "help" message (as a string) is allowed in two different ways:
 * @li Calling CmdLineParser::description();
 * @li Enabling the "help" command line switch in the constructor: in this case the constructor will throw daq::rc::CmdLineHelp
 *     when the <tt>-h</tt> switch is detected. The daq::rc::CmdLineHelp's message will return the "help" string.
 *
 * @see <a href="https://gitlab.cern.ch/atlas-tdaq-software/RunControl/blob/master/bin/rc_example_application.cxx">This link</a> to have an example on how to use this class.
 *
 * @note A Run Control application uses by default some well defined command line switches that should be considered as reserved
 *       (<em>i.e.</em>, they should not be redefined):
 *       @li <em>-p, --partition</em>: The name of the IPC partition;
 *       @li <em>-P, --parent</em>: The name of the parent controller;
 *       @li <em>-s, --segment</em>: The name of the segment the application belongs to;
 *       @li <em>-n, --name</em>: The application's name (as it would appear in IPC);
 *       @li <em>-d, --database</em>: Connection string to the database (<em>i.e.</em>, @em oks file or @em rdb server);
 *       @li <em>-i, --interactive</em>: It enables the interactive mode (for leaf applications only).
 *
 * @note @n The environment variables <tt>TDAQ_PARTITION</tt>, <tt>TDAQ_APPLICATION_NAME</tt> and <tt>TDAQ_DB</tt> are set to the corresponding values (if any) provided by the command line.
 *
 * @warning This class is not thread safe and access from different threads should be properly externally synchronized.
 *
 * @see ItemCtrl::ItemCtrl(const CmdLineParser&, const ControllableDispatcher::ControllableList&, const std::shared_ptr<ControllableDispatcher>&)
 * @see ItemCtrl::ItemCtrl(const CmdLineParser&, const std::shared_ptr<Controllable>&, const std::shared_ptr<ControllableDispatcher>&)
 *
 * @see Controller::Controller(const CmdLineParser&)
 * @see Controller::Controller(const CmdLineParser&, const std::shared_ptr<UserRoutines>&)
 */

class CmdLineParser {
    public:
        /**
         * The command line is parsed.
         *
         * @param argc The number of command line arguments (<em>i.e.</em>, the first argument of the main function)
         * @param argv The command line arguments (<em>i.e.</em>, the second argument of the main function)
         * @param haveHelpSwitch If @em true then the help command line switch (<em>i.e.</em>, <tt>-h</tt>) is enabled: in this case the daq::rc::CmdLineHelp
         *                       exception will be thrown if <tt>-h</tt> is specified as a command line option;
         *                       @n If @em false then the help command line switch is disabled (in this case the help message can be accessed via the CmdLineParser::description()
         *                       method).
         *
         * @throws daq::rc::CmdLineError Some error occurred parsing the command line (<em>e.g.</em>, some mandatory parameter is missing or
         *                               some parameter is not valid)
         * @throws daq::rc::CmdLineHelp This exception is thrown only if @em haveHelpSwitch is set to @em true
         *                              and the command line contains the <tt>-h</tt> switch
         */
        CmdLineParser(int argc, char** argv, bool haveHelpSwitch = false);

        /**
         * It returns the name of the IPC partition as defined by the command line options.
         *
         * @return The name of the IPC partition
         */
        std::string partitionName() const;

        /**
         * It returns the name (as shown in IPC) of the parent controller as defined by the command line options.
         *
         * @return The name of the parent controller
         */
        std::string parentName() const;

        /**
         * It returns the name of the segment the application belongs to as defined by the command line options.
         *
         * @return The name of the segment the application belongs to
         */
        std::string segmentName() const;

        /**
         * It return the name of the application (as it will be shown in IPC) as defined by the command line options.
         *
         * @return The name of the application
         */
        std::string applicationName() const;

        /**
         * It returns the connection string used to read the OKS database as defined by the command line options.
         *
         * @return The connection string used to read the OKS database
         */
        std::string database() const;

        /**
         * It returns @em true if the interactive mode is enabled by the command line options, @em false otherwise.
         *
         * @return @em true if the interactive mode is enabled
         */
        bool interactiveMode() const;

        /**
         * It returns the "help" message, showing the supported command line options and their meaning.
         *
         * @return The "help" message
         */
        const std::string& description() const;

    private:
        /**
         * Why using a property tree here? Because it helps to extend this class keeping its binary compatibility.
         * All the properties are saved to and retrieved from this container: whenever a new command line option has
         * to be introduced, it can be simply added to it, without adding any additional data member to this class.
         */
        boost::property_tree::ptree m_params;
        std::string m_description;

        static const std::string APPLICATION_NAME;
        static const std::string PARTITION_NAME;
        static const std::string PARENT_NAME;
        static const std::string SEGMENT_NAME;
        static const std::string INTERACTIVE_MODE;
        static const std::string DATABASE_NAME;
};

}}

#endif /* DAQ_RC_CMDLINEPARSER_H_ */
