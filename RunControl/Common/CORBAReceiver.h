/*
 * CORBAReceiver.h
 *
 *  Created on: Oct 1, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Common/CORBAReceiver.h
 */

#ifndef DAQ_RC_CORBARECEIVER_H_
#define DAQ_RC_CORBARECEIVER_H_

#include "RunControl/Common/CommandReceiver.h"

#include <rc/rc.hh>

#include <ipc/object.h>
#include <ipc/namedobject.h>
#include <owl/semaphore.h>

#include <string>
#include <memory>
#include <atomic>

namespace daq { namespace rc {

struct CORBAReceiverDeleter;
class CommandedApplication;

/**
 * @brief A command receiver able to receive commands over CORBA.
 *
 * @note Instances of this class should be created via the daq::rc::CommandReceiverFactory class.
 */
class CORBAReceiver : public CommandReceiver
{
    private:
        /**
         * @brief Adapter class implementing the CORBA interface.
         *
         * @warning Always create instances in the heap and destroy using the "_destroy()" method.
         */
        class IPCAdapter : public IPCNamedObject<POA_rc::commander, ::ipc::multi_thread> {
            public:
                /**
                 * @brief Constructor.
                 *
                 * @param ca The application actually executing the remote call
                 * @param sync Simple synchronizer
                 * @param sem Semaphore used to notify the shutdown (coming from a remote request)
                 */
                IPCAdapter(CommandedApplication& ca, std::atomic<bool>& sync, OWLSemaphore& sem);

                /**
                 * @brief Executed when a transition command is received
                 *
                 * This method is part of the rc::commander IDL interface.
                 *
                 * @param sc The sender's context
                 * @param commandDescription Encoded string representing the transition command
                 *
                 * @throws rc::CannotExecute Failed to execute the transition
                 *
                 * @see daq::rc::TransitionCmd
                 */
                void makeTransition(const ::rc::SenderContext& sc, const char* commandDescription) override;

                /**
                 * @brief Executed when a (not transition) command is received
                 *
                 * This method is part of the rc::commander IDL interface.
                 *
                 * @param sc The sender's context
                 * @param commandDescription Encoded string representing the command
                 *
                 * @throws rc::CannotExecute Failed to execute the command
                 *
                 * @see daq::rc::RunControlBasicCommand
                 */
                void executeCommand(const ::rc::SenderContext& sc, const char* commandDescription) override;

                /**
                 * @brief Executed when a request to provide self information is received
                 *
                 * This method is part of the rc::commander IDL interface.
                 *
                 * @param statusDescription Encoded (out) string describing the status of this application
                 *
                 * @see daq::rc::ApplicationStatusCmd
                 */
                void status(::CORBA::String_out statusDescription) override;

                /**
                 * @brief Executed when a request to provide information about a child application is received
                 *
                 * This method is part of the rc::commander IDL interface.
                 *
                 * @param childName The name of the child application
                 * @param statusDescription Encoded (out) string describing the status of the child application
                 *
                 * @throws rc::UnknownChild The child application is now known
                 *
                 * @see daq::rc::ApplicationStatusCmd
                 */
                void childStatus(const char* childName, ::CORBA::String_out statusDescription) override;

                /**
                 * @brief Executed when a status update from one of the child application is received
                 *
                 * This method is part of the rc::commander IDL interface.
                 *
                 * @param applicationDescription Encoded string describing the status of the child application
                 *
                 * @see daq::rc::ApplicationStatusCmd
                 */
                void updateChild(const char* applicationDescription) override;

                /**
                 * @brief Executed when a request to set the error state of this application is received
                 *
                 * This method is part of the rc::commander IDL interface.
                 *
                 * @param sc The sender's context
                 * @param errorItems Causes of the error state (no items means to clear the error state)
                 *
                 * @throws rc::CannotExecute Failed to execute the command
                 */
                void setError(const ::rc::SenderContext& sc, const ::rc::ErrorItemList& errorItems) override;

                void shutdown() override;
            private:
               /**
                * @brief Verify credentials.
                *
                * @param creds Input credentials.
                *
                * @throws daq::tokens::CannotVerifyToken
                *
                * @return User name.
                */
                std::string verify_credentials(std::string_view creds);

                CommandedApplication& m_commanded_application;
                std::atomic<bool>& m_sync;
                OWLSemaphore& m_sem;
        };

    public:
        /**
         * @brief Copy constructor: deleted.
         */
        CORBAReceiver(const CORBAReceiver&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        CORBAReceiver(CORBAReceiver&&) = delete;

        /**
         * @brief Copy assignment operator: deleted.
         */
        CORBAReceiver& operator=(const CORBAReceiver&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        CORBAReceiver& operator=(CORBAReceiver&&) = delete;

        /**
         * @brief It performs the IPC publication
         *
         * @throws daq::rc::OnlineServicesFailure The IPC publication failed
         */
        void init() override;

        /**
         * @brief It just locks until daq::rc::CORBAReceiver::shutdown is called
         */
        void start() override;

        /**
         * @brief It lets daq::rc::CORBAReceiver::start return
         */
        void shutdown() override;

        /**
         * @brief It deactivates the underlining CORBA object
         *
         * This action is usually executed in the destructor, but it is given as an
         * utility method as well.
         *
         * @warning Do not call more than once.
         */
        void deactivate() override;

    private:
        friend struct CORBAReceiverDeleter;
        friend class CommandReceiverFactory;

        /**
         * @brief It creates a new instance of this class
         *
         * @param ca The application that will execute the received commands
         * @return A pointer to a new instance of this class
         */
        static std::shared_ptr<CORBAReceiver> create(CommandedApplication& ca);

        /**
         * @brief Constructor
         *
         * @param ca The application that will execute the received commands
         */
        CORBAReceiver(CommandedApplication& ca);

        /**
         * @brief Destructor
         */
        ~CORBAReceiver() noexcept;

        OWLSemaphore m_sem;
        std::atomic<bool> m_sync;
        std::atomic<bool> m_deactivated;
        IPCAdapter* m_corba_obj;
};

}}

#endif /* DAQ_RC_CORBARECEIVER_H_ */
