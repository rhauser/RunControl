/*
 * ObjectPool.h
 *
 *  Created on: Dec 4, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/Common/ObjectPool.h
 */

#ifndef DAQ_RC_OBJECTPOOL_H_
#define DAQ_RC_OBJECTPOOL_H_

#include <tbb/concurrent_hash_map.h>

#include <string>
#include <memory>

namespace daq { namespace rc {

/**
 * @brief Utility class implementing an "infinite" object pool.
 *
 * Objects can be retrieved from the pool using simple string identifier.
 *
 * @tparam Type of the objects to be stored in the pool. Objects will be created on the heap and should
 *         have a valid default constructor
 *
 * @warning Objects are released by the pool only when ObjectPool::release is called
 *
 * @note This class is fully thread-safe
 */
template<typename T>
class ObjectPool {
    public:
        /**
         * @brief Default constructor
         */
        ObjectPool() = default;

        /**
         * @brief Copy constructor: deleted
         */
        ObjectPool(const ObjectPool&) = delete;

        /**
         * @brief Move constructor: deleted
         */
        ObjectPool(ObjectPool&&) = delete;

        /**
         * @brief Copy assignment operator: deleted
         */
        ObjectPool& operator=(const ObjectPool&) = delete;

        /**
         * @brief Move assignment operator: deleted
         */
        ObjectPool& operator=(ObjectPool&&) = delete;

        /**
         * @brief Default destructor
         */
        ~ObjectPool() = default;

        /**
         * @brief It returns a shared pointer to an instance of @em T corresponding to the @em name identifier
         *
         * If an instance for @em name does not exist, then a new one is created
         *
         * @param name Identifier of the object to be retrieved
         * @return A shared pointer to an instance of @em T
         *
         * @throws Any exception thrown by the @em T's default constructor
         */
        std::shared_ptr<T> get(const std::string& name) {
            // First look if the value is already there (a read lock is acquired)
            {
                typename Map::const_accessor constAcc;
                if(m_map.find(constAcc, name) == true) {
                    return constAcc->second;
                }
            }

            // If we are here then it means we need to add the value to the map
            do {
                {
                    // Try to insert the object (a write lock is acquired)
                    typename Map::accessor acc;
                    if(m_map.insert(acc, name) == true) {
                        (acc->second).reset(new T());
                        return acc->second;
                    }
                }

                {
                    // The object has not been inserted, some other thread did it
                    // Look into the map in order to get it (a read lock is acquired)
                    // If it fails, then it means some other thread called 'release'
                    // In this case continue the loop and try again
                    typename Map::const_accessor constAcc;
                    if(m_map.find(constAcc, name) == true) {
                        return constAcc->second;
                    }
                }
            } while(true);
        }

        /**
         * @brief The instance corresponding to @em name is released by this pool
         *
         * @note Any shared pointer returned by ObjectPool::get keeps its validity even after a call
         *       to this method (the pool has only a shared ownership of objects)
         *
         * @param name Identifier of the object to be released
         */
        void release(const std::string& name) {
            typename Map::accessor acc;
            if(m_map.find(acc, name) == true) {
                m_map.erase(acc);
            }
        }

    private:
        typedef tbb::concurrent_hash_map<std::string, std::shared_ptr<T>> Map;

        Map m_map;
};

}}

#endif /* DAQ_RC_OBJECTPOOL_H_ */
