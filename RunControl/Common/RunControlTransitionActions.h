/*
 * RunControlTransitionActions.h
 *
 *  Created on: Aug 30, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Common/RunControlTransitionActions.h
 */

#ifndef DAQ_RC_RUNCONTROLTRANSITIONACTIONS_H_
#define DAQ_RC_RUNCONTROLTRANSITIONACTIONS_H_

#include "RunControl/Common/Controllable.h"

namespace daq { namespace rc {

class TransitionCmd;
class UserBroadcastCmd;
class UserCmd;

/**
 * @brief Main interface to the FSM.
 *
 * This interface includes all the actions required by the FSM, including the ones
 * not exposed to the final users.
 *
 * All the methods have a default "empty" implementation.
 */
class RunControlTransitionActions : public TransitionActions {
	public:
        /**
         * @brief Destructor
         */
		virtual ~RunControlTransitionActions() noexcept {}

		// Additional FSM actions not exposed to users
		/**
		 * @brief Method called when the FSM is started
		 * @param cmd The transition command
		 */
        virtual void enterFSM(const TransitionCmd& cmd) { (void) cmd; }

        /**
         * @brief Method called when the daq::rc::FSM_COMMAND::INITIALIZE transition is performed
         * @param cmd The transition command
         */
		virtual void initialize(const TransitionCmd& cmd ) { (void) cmd; }

        /**
         * @brief Method called when the daq::rc::FSM_COMMAND::SHUTDOWN transition is performed
         * @param cmd The transition command
         */
		virtual void shutdown(const TransitionCmd& cmd) { (void) cmd; }

        /**
         * @brief Method called when the daq::rc::FSM_COMMAND::USERBROADCAST transition is performed
         * @param cmd The user-broadcast command
         */
		virtual void userBroadcast(const UserBroadcastCmd& cmd) { (void) cmd; }

        /**
         * @brief Method called when the daq::rc::FSM_COMMAND::RELOAD_DB transition is performed
         * @param cmd The database reload command
         */
        virtual void reloadDB(const TransitionCmd& cmd) { (void) cmd; }

        // Interrupt FSM actions if requested
        /**
         * @brief It interrupts or resumes the FSM actions
         * @param interrupt @em true if the FSM actions have to be interrupted
         */
        virtual void interrupt(bool interrupt) { (void) interrupt; };
};

}}

#endif /* DAQ_RC_RUNCONTROLTRANSITIONACTIONS_H_ */
