/*
 * UserRoutines.h
 *
 *  Created on: Aug 29, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Common/UserRoutines.h
 */

#ifndef DAQ_RC_USERROUTINES_H_
#define DAQ_RC_USERROUTINES_H_

namespace daq { namespace rc {

class TransitionCmd;

/**
 * @brief Interface allowing to extend the default behavior of a segment's controller.
 *
 * The customization of a segment's controller is intended to let the controller execute some
 * specific actions before commands are dispatched to child applications: that is mainly to
 * have a central point where segment-wide actions are performed.
 *
 * All the methods but UserRoutines::clearAction() have a default "empty" implementation, so that
 * derived classes can override only methods corresponding to the needed actions.
 *
 * @remark All the methods but UserRoutines::publishAction() and UserRoutines::publishStatisticsAction()
 *         are never executed concurrently.
 *
 * @warning Any error occurring during the execution of a command should be reported throwing an exception
 *          inheriting from ers::Issue: in such a case the framework will issue a FATAL error.
 *          \n
 * @warning When the controller is restarted after a failure (leaving all of its children up and running), it may
 *          be dangerous to execute user code. Please, check the return value of daq::rc::TransitionCmd::isRestart() const
 *          if the execution of some actions may hurt the system in such a situation.
 *          \n
 * @warning This interface should not be "abused" in order to use a controller as it was a leaf application. Indeed
 *          the controller has a structure by far more complex than a leaf application and needs more
 *          resources, making it less suitable to be executed on HW with limited resources (<em>e.g.</em>, SBCs).
 *          Moreover having several leaf-controllers will in general increase the time needed to boot the system.
 *          Leaf applications should be implemented using the daq::rc::Controllable interface.
 *
 * @see daq::rc::Controller, the main class to instantiate in order to start a controller
 */
class UserRoutines {
    public:
        /**
         * @brief Destructor.
         */
        virtual ~UserRoutines() noexcept {}

        /**
         * @brief Action called before the FSM is initiated (i.e., before the FSM enters
         *        its initial state). For a standard controller, that means before the
         *        actions to enter the daq::rc::FSM_STATE::INITIAL state are executed.
         *
         * @param cmd The corresponding transition command.
         */
        virtual void initAction(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Action called before the daq::rc::FSM_COMMAND::CONFIGURE is dispatched
         *        by the controller to its child applications.
         *
         * @param cmd The corresponding transition command.
         */
        virtual void configureAction(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Action called before the daq::rc::FSM_COMMAND::CONNECT is dispatched
         *        by the controller to its child applications.
         *
         * @param cmd The corresponding transition command.
         */
        virtual void connectAction(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Action called before the daq::rc::FSM_COMMAND::DISCONNECT is dispatched
         *        by the controller to its child applications.
         *
         * @attention During this transition the periodic timers are paused: the transition will not be considered
         *            as done until the action is properly completed. It is then good practice to introduce some
         *            mechanism allowing UserRoutines::publishAction() and UserRoutines::publishStatisticsAction()
         *            to return as soon as the daq::rc::FSM_COMMAND::DISCONNECT transition is being performed:
         *            this would help in reducing the transition time.
         *
         * @param cmd The corresponding transition command.
         */
        virtual void disconnectAction(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Action called before the daq::rc::FSM_COMMAND::START is dispatched
         *        by the controller to its child applications.
         *
         * @param cmd The corresponding transition command.
         */
        virtual void prepareAction(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Action called before the daq::rc::FSM_COMMAND::STOPROIB is dispatched
         *        by the controller to its child applications.
         *
         * @param cmd The corresponding transition command.
         */
        virtual void stopROIBAction(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Action called before the daq::rc::FSM_COMMAND::STOPDC is dispatched
         *        by the controller to its child applications.
         *
         * @param cmd The corresponding transition command.
         */
        virtual void stopDCAction(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Action called before the daq::rc::FSM_COMMAND::STOPHLT is dispatched
         *        by the controller to its child applications.
         *
         * @param cmd The corresponding transition command.
         */
        virtual void stopHLTAction(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Action called before the daq::rc::FSM_COMMAND::STOPRECORDING is dispatched
         *        by the controller to its child applications.
         *
         * @param cmd The corresponding transition command.
         */
        virtual void stopRecordingAction(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Action called before the daq::rc::FSM_COMMAND::STOPGATHERING is dispatched
         *        by the controller to its child applications.
         *
         * @param cmd The corresponding transition command.
         */
        virtual void stopGatheringAction(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Action called before the daq::rc::FSM_COMMAND::STOPARCHIVING is dispatched
         *        by the controller to its child applications.
         *
         * @param cmd The corresponding transition command.
         */
        virtual void stopArchivingAction(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Action called before the daq::rc::FSM_COMMAND::UNCONFIGURE is dispatched
         *        by the controller to its child applications.
         *
         * @param cmd The corresponding transition command.
         */
        virtual void unconfigureAction(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief It is called regularly with delays defined in the OKS configuration (@em ProbeInterval).
         *
         * It is also called when the controller receives the daq::rc::RC_COMMAND::PUBLISH command.
         * The regular timer is started as soon as the daq::rc::FSM_COMMAND::CONNECT transition is completed and
         * it is stopped as soon as the daq::rc::FSM_COMMAND::DISCONNECT transition is done.
         *
         * @attention It may be called concurrently with other methods (and with itself).
         */
        virtual void publishAction() {}

        /**
         * @brief It is called regularly with delays defined in the OKS configuration (@em FullStatisticsInterval).
         *
         * It is also called when the controller receives the daq::rc::RC_COMMAND::PUBLISHSTATS command.
         * The regular timer is started as soon as the daq::rc::FSM_COMMAND::CONNECT transition is completed and
         * it is stopped as soon as the daq::rc::FSM_COMMAND::DISCONNECT transition is done.
         *
         * @attention It may be called concurrently with other methods (and with itself).
         */
        virtual void publishStatisticsAction() {}

        /**
         * @brief This is called when an user action fails.
         *
         * If it returns @em true, the previous failure is considered as fixed and the action is re-tried.
         * Here is a logical schema showing how the controller will deal with a failure:
         * @code{.cpp}
           try {
               // First attempt
               prepareAction(...);
           }
           catch(ers::Issue&) {
               if(clearAction() == true) {
                   // Retry
                   try {
                       // Second attempt
                       prepareAction(...);
                   }
                   catch(ers::Issue&) {
                       // Second failure, white flag!
                       throw;
                   }
               } else {
                   // Nothing to retry
                   throw;
               }
           }
           @endcode
         *
         * @remark The default implementation returns @em false.
         *
         * @return @em true if the action can be re-tried after a first failure
         */
        virtual bool clearAction() noexcept { return false; }
};

}}

#endif /* DAQ_RC_USERROUTINES_H_ */
