/*
 * RunControlBasicCommand.h
 *
 *  Created on: Oct 3, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Common/RunControlBasicCommand.h
 */

#ifndef DAQ_RC_RUNCONTROLBASICCOMMAND_H_
#define DAQ_RC_RUNCONTROLBASICCOMMAND_H_

#include "RunControl/Common/Exceptions.h"

#include <ers/LocalContext.h>
#include <ers/ers.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/exceptions.hpp>
#include <boost/property_tree/ptree_fwd.hpp>
#include <boost/optional/optional.hpp>

#include <utility>
#include <string>
#include <vector>
#include <set>
#include <sstream>
#include <memory>
#include <mutex>
#include <random>

namespace daq { namespace rc {
    class RunControlBasicCommand;
}}

/**
 * @brief It swaps two daq::rc::RunControlBasicCommand objects.
 *
 * @param lhs The first object to swap
 * @param rhs The second object to swap
 */
void swap(daq::rc::RunControlBasicCommand& lhs, daq::rc::RunControlBasicCommand& rhs);

namespace daq { namespace rc {

enum class RC_COMMAND : unsigned int;

/**
 * @brief Base class used for the representation of all the Run Control commands.
 *
 * Sub-classes should override the following methods:
 * @li RunControlBasicCommand::toString() const - To have a human readable string representation of the full command;
 * @li RunControlBasicCommand::arguments() const - To have a list of arguments for this command;
 * @li RunControlBasicCommand::notConcurrentWith() const - To specify whether or not this command should be executed concurrently with other commands;
 * @li RunControlBasicCommand::doClone() const - Mandatory: to properly clone instances of this class;
 * @li RunControlBasicCommand::equals(const RunControlBasicCommand& other) const - To properly define the equality operator.
 *
 * Every command can be serialized to its string representation using the RunControlBasicCommand::serialize() method.
 * One of the provided constructors can be used to build a RunControlBasicCommand object out of its string representation.
 *
 * Derived classes should not declare any data member in order to describe command parameters or properties but shall use the @em parameter(...)
 * and @em parameters(...) methods provided by this base class in order to store and retrieve them. That is needed in order to have a
 * proper and consistent (de)serialization mechanism without any addition in the derived class. Moreover this allows to change parameters
 * and properties without breaking the class's binary compatibility.
 *
 * Parameters and properties can be set and retrieved using string identifiers and can be of any basic or string type.
 * This class allows multi-value parameters and properties.
 *
 * Nested commands are supported via the @em command(..) methods.
 *
 * @note Parameter's identifiers cannot contain white spaces or punctuation (underscores are valid instead).
 *       \n
 * @note By default a command is described to support asynchronous notification on completion. Derived classes may change this
 *       using the RunControlBasicCommand::notifySupported(bool supported) method.
 *
 * @attention It is not safe to access the same instance of this class from multiple threads without proper external synchronization.
 */
class RunControlBasicCommand {
    public:
        /**
         * @brief Type of the UID associated to each command.
         */
        typedef std::string uid_t;

    public:
        /**
         * Constructor.
         *
         * @param commandName The command to build
         */
        explicit RunControlBasicCommand(RC_COMMAND commandName);

        /**
         * @overload RunControlBasicCommand::RunControlBasicCommand(const std::string& commandDescription)
         *
         * @param commandDescription Full string representation (including all the command properties) of the command
         * @throws daq::rc::BadCommand The command description is not valid
         *
         * @note A valid @em commandDescription string is usually the one returned by RunControlBasicCommand::serialize()
         *       \n
         * @note If the RunControlBasicCommand has to be created with the proper run-time type, then the RunControlCommands::factory
         *       function should be used.
         */
        explicit RunControlBasicCommand(const std::string& commandDescription);

        /**
         * @brief Move constructor.
         */
        RunControlBasicCommand(RunControlBasicCommand&&) = default;

        /**
         * @brief Copy constructor.
         */
        RunControlBasicCommand(const RunControlBasicCommand&) = default;

        /**
         * @brief Assignment operator.
         */
        RunControlBasicCommand& operator=(RunControlBasicCommand other);

        /**
         * @brief Equality operator.
         *
         * It calls RunControlBasicCommand::equals(const RunControlBasicCommand& other) const that can
         * be overridden in derived classes in order to define proper equality conditions.
         *
         * @param other The command to be compared to this for equality
         * @return @em true if the two commands are equals
         */
        bool operator==(const RunControlBasicCommand& other) const;

        /**
         * @brief Destructor.
         */
        virtual ~RunControlBasicCommand() noexcept;

        /**
         * @brief It serialized this object to a string representation of it.
         *
         * @throws daq::rc::BadCommand The command is ill-formed and cannot be serialized
         * @return A string representation of this object
         */
        std::string serialize() const;

        /**
         * @brief It returns the name of this command.
         *
         * The name of this command is defined as the string representing the command passed to the
         * RunControlBasicCommand::RunControlBasicCommand(RC_COMMAND commandName) constructor.
         *
         * @return The name of this command.
         *
         * @see RunControlCommands::commandToString(RC_COMMAND cmd)
         */
        std::string name() const;

        /**
         * @brief It returns the UID of this command object.
         *
         * @return The UID of this command object
         */
        uid_t uid() const;

        /**
         * @brief It returns the CORBA reference of the remote command sender
         *
         * If the returned string is not empty, then it means that the remote sender
         * requests to be notified when the command has been executed.
         *
         * @return The CORBA reference of the remote command sender
         */
        std::string senderReference() const;

        /**
         * @brief It sets the CORBA reference of the remote command sender
         *
         * Setting the reference implies to send a notification to the remote
         * command sender as soon as the command has been executed
         *
         * @param reference The CORBA reference of the remote command sender
         */
        void senderReference(const std::string& reference);

        /**
         * @brief It returns @em true if the command supports asynchronous notification when executed
         *
         * @return @em true if the command supports asynchronous notification when executed
         */
        bool notifySupported() const;

        /**
         * @brief It returns a human readable string describing this command.
         *
         * @note This method has a complete different nature with respect to RunControlBasicCommand::serialize(): it is
         *       meant just to have a nice way to print the content of this object.
         *       /n The default implementation just calls RunControlCommand::name().
         *
         * @return A human readable string describing this command
         */
        virtual std::string toString() const;

        /**
         * @brief It returns a vector of strings representing the arguments of this command.
         *
         * @note The default implementation just returns an empty vector. It is a derived class's task to
         *       properly identify the command's arguments (<em>i.e.</em>, for a command representing some
         *       resources to be enabled or disabled, arguments may actually be the list of resources).
         *
         * @return A vector of strings representing the arguments of this command
         */
        virtual std::vector<std::string> arguments() const;

        /**
         * @brief It returns the list of commands this command cannot be executed concurrently with.
         *
         * @note It is used by the Run Control core in order to decide whether a command can be
         *       executed or not at a certain moment.
         *       \n
         *       The default implementation returns an empty set (<em>i.e.</em>, the command is compatible with
         *       all the other commands).
         *
         * @return The list of commands this command cannot be executed concurrently with
         */
        virtual std::set<RC_COMMAND> notConcurrentWith() const;

        /**
         * @brief It returns a new fresh instance of this class representing the same command and keeping
         *        all its properties and parameters.
         *
         * @attention This method calls RunControlBasicCommand::doClone() that shall be properly overridden in derived
         *            classes in order to let this method return an object with the right run time type.
         *
         * @return A clone of this object.
         */
        std::unique_ptr<RunControlBasicCommand> clone() const;

        /**
         * @brief It swaps this object with @em other.
         *
         * @param other The command to be swapped with this.
         */
        void swap(RunControlBasicCommand& other);

        /**
         * @brief Functor used as comparator in daq::rc::CommandSet.
         *
         * Instances of RunControlBasicCommand are ordered by their UIDs.
         */
        struct Comparator {
                bool operator()(const std::shared_ptr<RunControlBasicCommand>& e1,
                                const std::shared_ptr<RunControlBasicCommand>& e2) const
                {
                    return (e1->uid() < e2->uid());
                }
        };


        /**
         * @brief Functor used as comparator in daq::rc::ConstCommandSet.
         *
         * Instances of RunControlBasicCommand are ordered by their UIDs.
         */
        struct ConstComparator {
                bool operator()(const std::shared_ptr<const RunControlBasicCommand>& e1,
                                const std::shared_ptr<const RunControlBasicCommand>& e2) const
                {
                    return (e1->uid() < e2->uid());
                }
        };

        /**
         * @brief A set of shared pointers to RunControlBasicCommand objects.
         */
        typedef std::set<std::shared_ptr<RunControlBasicCommand>, RunControlBasicCommand::Comparator> CommandSet;

        /**
         * @brief A set of shared pointers to constant RunControlBasicCommand objects.
         */
        typedef std::set<std::shared_ptr<const RunControlBasicCommand>, RunControlBasicCommand::ConstComparator> ConstCommandSet;

    protected:
        /**
         * @brief It sets whether the command supports asynchronous completion notification or not
         *
         * @param supported @em true if the command supports asynchronous completion notification or not
         */
        void notifySupported(bool supported);

        /**
         * @brief It clones the current object (<em>i.e.</em>, it returns a new object representing this command and all
         *        its properties).
         *
         * @return A clone of this object.
         *
         * @attention The default implementation returns an object whose run time type is daq::rc::RunControlBasicCommand.
         *
         * @see daq::rc::RunControlBasicCommand::clone() const
         */
        virtual RunControlBasicCommand* doClone() const;

        /**
         * @brief It checks whether @em other and @em this represent the same command.
         *
         * @note In the default implementation two command objects are equals if the have the same name
         *       as returned by RunControlBasicCommand::name() const.
         *
         * @param other The command object to be checked against @em this
         * @return @em true if the two objects represent the same command
         *
         * @see RunControlBasicCommand::operator==(const RunControlBasicCommand& other) const
         */
        virtual bool equals(const RunControlBasicCommand& other) const;

        /**
         * @brief It returns the nested command whose identifier is @em cmdIdentifier.
         *
         * @tparam T The type of the nested command. It has to be a type derived from RunControlBasicCommand.
         * @param cmdIdentifier The identifier of the nested command (<em>i.e.</em>, the keyword used
         *        to store the nested command)
         *
         * @return The nested command
         *
         * @throws daq::rc::CommandParameterNotFound There is not parameter associated to the @em cmdIdentifier key
         * @throws daq::rc::CommandParameterInvalid The @em cmdIdentifier string is not a valid identifier
         * @throws Any Any exception thrown by the T's constructor
         */
        template<typename T>
        std::unique_ptr<T> command(const std::string& cmdIdentifier) const {
            return std::unique_ptr<T>(new T(commandRepresentation(cmdIdentifier)));
        }

        /**
         * @brief It returns a string representation of the nested command.
         *
         * @note The returned string is the same as the one obtained calling the RunControlBasicCommand::serialize()
         *       on the nested command object.
         *
         * @param cmdIdentifier The identifier of the nested command (<em>i.e.</em>, the keyword used
         *                      to store the nested command)
         * @return A string representation of the nested command (it may be empty)
         *
         * @throws daq::rc::CommandParameterNotFound There is not parameter associated to the @em cmdIdentifier key
         * @throws daq::rc::CommandParameterInvalid The @em cmdIdentifier string is not a valid identifier
         */
        std::string commandRepresentation(const std::string& cmdIdentifier) const;

        /**
         * @brief It sets a command (nested command) as a parameter of this command.
         *
         * @note If a nested command with the same identifier is already present, then it is overwritten.
         *
         * @param cmdIdentifier The identifier used to store the nested command
         * @param cmd The command to store
         *
         * @throws daq::rc::CommandParameterInvalid The @em cmdIdentifier string is not a valid identifier
         */
        void command(const std::string& cmdIdentifier, const RunControlBasicCommand& cmd);

        /**
         * @brief It returns the value of the parameter whose identifier is @em parName
         *
         * @tparam T The type of the parameter: it can be a basic type or a string
         * @param parName The parameter's identifier (<em>i.e.</em>, the one used to store the parameter)
         * @return The value of the parameter whose identifier is @em parName
         *
         * @throws daq::rc::CommandParameterNotFound There is not parameter associated to the @em parName key
         * @throws daq::rc::CommandParameterInvalid The @em parName string is not a valid identifier
         */
        template<typename T>
        T parameter(const std::string& parName) const {
            try {
                boost::optional<T> value = m_ptree.get_optional<T>(RunControlBasicCommand::TOP_HEADER + "." + parName);
                if(value) {
                    return value.get();
                } else {
                    throw daq::rc::CommandParameterNotFound(ERS_HERE,parName, name());
                }
            }
            catch(boost::property_tree::ptree_bad_data& ex) {
                throw daq::rc::CommandParameterInvalid(ERS_HERE, parName, name(), ex.what(), true, ex);
            }
        }

        /**
         * @brief It stores the value of a parameter using @em parName as an identifier.
         *
         * @note If the parameter is already defined, then its value is overwritten.
         *
         * @tparam T The type of the parameter: it can be a basic type or a string
         * @param parName The parameter's identifier
         * @param value The parameter's value
         *
         * @throws daq::rc::CommandParameterInvalid The @em parName string is not a valid identifier
         */
        template<typename T>
        void parameter(const std::string& parName, const T& value) {
            checkParameter(parName, false);

            try {
                m_ptree.put<T>(RunControlBasicCommand::TOP_HEADER + "." + parName, value);
            }
            catch(boost::property_tree::ptree_bad_data& ex) {
                throw daq::rc::CommandParameterInvalid(ERS_HERE, parName, name(), ex.what(), false, ex);
            }
        }

        /**
         * @brief It returns the list of values for parameter whose identifier is @em parName.
         *
         * @tparam T The type of the parameters: it can be a basic type or a string
         * @param parName The parameters's identifier
         * @return A vector containing the found parameters. The vector may be empty if @em parName cannot be found
         *         or the parameter has no values.
         *
         * @throws daq::rc::CommandParameterInvalid The @em parName string is not a valid identifier
         */
        template<typename T>
        std::vector<T> parameters(const std::string& parName) const {
            std::vector<T> values;

            try {
                boost::optional<const boost::property_tree::ptree&> st = m_ptree.get_child_optional(RunControlBasicCommand::TOP_HEADER + "." + parName);
                if(st) {
                    const boost::property_tree::ptree::const_iterator& begin = st.get().begin();
                    const boost::property_tree::ptree::const_iterator& end = st.get().end();
                    for(boost::property_tree::ptree::const_iterator it = begin; it != end; ++it) {
                        if(it->first == RunControlBasicCommand::VALUE_TAG) {
                            values.push_back(it->second.get_value<T>());
                        }
                    }
                } else {
                    ERS_DEBUG(3, "Parameter \"" << parName << "\" not found for command \"" << name() << "\"");
                }
            }
            catch(boost::property_tree::ptree_bad_data& ex) {
                throw daq::rc::CommandParameterInvalid(ERS_HERE, parName, name(), ex.what(), true, ex);
            }

            return values;
        }

		/**
		 * @brief It sets a list of values for the parameter identified by @em parName.
		 *
		 * If the @em parName parameter already exists, then its values are overwritten.
		 * This method can be used to store multi-value parameters.
		 *
		 * @tparam T The type of the parameters: it can be a basic type or a string
		 * @param parName The parameters's identifier
		 * @param values The values associated to the parameter
		 *
		 * @throws daq::rc::CommandParameterInvalid The @em parName string is not a valid identifier
		 */
        template<typename T>
        void parameters(const std::string& parName, const std::vector<T>& values) {
            checkParameter(parName, false);

            // Clear the sub-tree if it already exists
            boost::optional<boost::property_tree::ptree&> st = m_ptree.get_child_optional(RunControlBasicCommand::TOP_HEADER + "." + parName);
            if(st) {
                st.get().clear();
            }

            try {
                for(unsigned int i = 0; i != values.size(); ++i) {
                    m_ptree.add<T>(RunControlBasicCommand::TOP_HEADER + "." + parName + "." + RunControlBasicCommand::VALUE_TAG, values[i]);
                }
            }
            catch(boost::property_tree::ptree_bad_data& ex) {
                throw daq::rc::CommandParameterInvalid(ERS_HERE, parName, name(), ex.what(), false, ex);
            }
        }

		/**
		 * @brief It checks whether a parameter identifier is valid.
		 *
		 * @param parName The identifier
		 * @param isGet @em true if the check is done for a @em getter method
		 *
		 * @throws daq::rc::CommandParameterInvalid The @em parName string is not a valid identifier
		 */
        void checkParameter(const std::string& parName, bool isGet) const;

		/**
		 * @brief It generates the UID in order to uniquely identify the command.
		 *
		 * @return The command's UID
		 *
		 * @note This is thread safe.
		 */
        static uid_t generateUID();

    private:
        friend void ::swap(RunControlBasicCommand& lhs, RunControlBasicCommand& rhs);

        boost::property_tree::ptree m_ptree;

        static std::mutex m_rand_mutex;
        static std::mt19937 m_rand_gen;
        static std::once_flag m_init_flag;

        static const std::string TOP_HEADER;
        static const std::string CMD_NAME_TAG;
        static const std::string CMD_ID_TAG;
        static const std::string VALUE_TAG;
        static const std::string REMOTE_REFERENCE_TAG;
        static const std::string NOTIFY_SUPPORTED_TAG;
};

}}

#endif /* DAQ_RC_RUNCONTROLBASICCOMMAND_H_ */
