/*
 * Controllable.h
 *
 *  Created on: Aug 16, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Common/Controllable.h
 */

#ifndef DAQ_RC_CONTROLLABLE_H_
#define DAQ_RC_CONTROLLABLE_H_

#include <string>
#include <vector>


namespace daq { namespace rc {

class TransitionCmd;
class UserCmd;
class SubTransitionCmd;
class ResynchCmd;
enum class FSM_STATE : unsigned int;

/**
 * @brief Interface defining methods called when applications are asked to perform FSM state transitions.
 *
 * @warning Leaf application have to implement the daq::rc::Controllable interface and not this one directly.
 */
class TransitionActions {
    public:
        /**
         * @brief Destructor.
         */
        virtual ~TransitionActions() noexcept {}

        /**
         * @brief Method called when the application is asked to perform the daq::rc::FSM_COMMAND::CONFIGURE transition.
         *
         * @param cmd Object containing information about the transition to perform
         *
         * @see CommandSender::makeTransition(const std::string&, FSM_COMMAND) const
         * @see CommandSender::makeTransition(const std::string&, const TransitionCmd&) const
         */
        virtual void configure(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Method called when the application is asked to perform the daq::rc::FSM_COMMAND::CONNECT transition.
         *
         * @param cmd Object containing information about the transition to perform
         *
         * @see CommandSender::makeTransition(const std::string&, FSM_COMMAND) const
         * @see CommandSender::makeTransition(const std::string&, const TransitionCmd&) const
         */
        virtual void connect(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Method called when the application is asked to perform the daq::rc::FSM_COMMAND::START transition.
         *
         * This transition is also defined as the Start Of Run (SOR).
         *
         * @param cmd Object containing information about the transition to perform
         *
         * @see CommandSender::makeTransition(const std::string&, FSM_COMMAND) const
         * @see CommandSender::makeTransition(const std::string&, const TransitionCmd&) const
         */
        virtual void prepareForRun(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Method called when the application is asked to perform the daq::rc::FSM_COMMAND::STOPROIB transition.
         *
         * @param cmd Object containing information about the transition to perform
         *
         * @see CommandSender::makeTransition(const std::string&, FSM_COMMAND) const
         * @see CommandSender::makeTransition(const std::string&, const TransitionCmd&) const
         */
        virtual void stopROIB(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Method called when the application is asked to perform the daq::rc::FSM_COMMAND::STOPDC transition.
         *
         * @param cmd Object containing information about the transition to perform
         *
         * @see CommandSender::makeTransition(const std::string&, FSM_COMMAND) const
         * @see CommandSender::makeTransition(const std::string&, const TransitionCmd&) const
         */
        virtual void stopDC(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Method called when the application is asked to perform the daq::rc::FSM_COMMAND::STOPHLT transition.
         *
         * @param cmd Object containing information about the transition to perform
         *
         * @see CommandSender::makeTransition(const std::string&, FSM_COMMAND) const
         * @see CommandSender::makeTransition(const std::string&, const TransitionCmd&) const
         */
        virtual void stopHLT(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Method called when the application is asked to perform the daq::rc::FSM_COMMAND::STOPRECORDING transition.
         *
         * @param cmd Object containing information about the transition to perform
         *
         * @see CommandSender::makeTransition(const std::string&, FSM_COMMAND) const
         * @see CommandSender::makeTransition(const std::string&, const TransitionCmd&) const
         */
        virtual void stopRecording(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Method called when the application is asked to perform the daq::rc::FSM_COMMAND::STOPGATHERING transition.
         *
         * @param cmd Object containing information about the transition to perform
         *
         * @see CommandSender::makeTransition(const std::string&, FSM_COMMAND) const
         * @see CommandSender::makeTransition(const std::string&, const TransitionCmd&) const
         */
        virtual void stopGathering(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Method called when the application is asked to perform the daq::rc::FSM_COMMAND::STOPARCHIVING transition.
         *
         * This transition is also defined as the End Of Run (EOR).
         *
         * @param cmd Object containing information about the transition to perform
         *
         * @see CommandSender::makeTransition(const std::string&, FSM_COMMAND) const
         * @see CommandSender::makeTransition(const std::string&, const TransitionCmd&) const
         */
        virtual void stopArchiving(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Method called when the application is asked to perform the daq::rc::FSM_COMMAND::DISCONNECT transition.
         *
         * @attention During this transition the periodic timers are paused: the transition will not be considered
         *            as done until the action is properly completed. It is then good practice to introduce some
         *            mechanism allowing Controllable::publish() and Controllable::publishFullStats() to return as
         *            soon as the daq::rc::FSM_COMMAND::DISCONNECT transition is being performed: this would help in reducing
         *            the transition time.
         *
         * @param cmd Object containing information about the transition to perform
         *
         * @see CommandSender::makeTransition(const std::string&, FSM_COMMAND) const
         * @see CommandSender::makeTransition(const std::string&, const TransitionCmd&) const
         */
        virtual void disconnect(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Method called when the application is asked to perform the daq::rc::FSM_COMMAND::UNCONFIGURE transition.
         *
         * @param cmd Object containing information about the transition to perform
         *
         * @see CommandSender::makeTransition(const std::string&, FSM_COMMAND) const
         * @see CommandSender::makeTransition(const std::string&, const TransitionCmd&) const
         */
        virtual void unconfigure(const TransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Method called when the application is asked to perform a sub-transition.
         *
         * @note A sub-transition does not cause a change in the FSM state.
         *
         * @param cmd Object containing information about the sub-transition to perform
         */
        virtual void subTransition(const SubTransitionCmd& cmd) { (void)cmd; }

        /**
         * @brief Method called when an application is asked to perform a re-synchronization of the HW.
         *
         * @note This transition does not case a change in the FSM state.
         *
         * @param cmd Object containing information about the re-synchronization
         */
        virtual void resynch(const ResynchCmd& cmd) { (void)cmd; }
};

/**
 * @brief The @em Controllable interface is the main interface between final applications and the Run Control
 * system.
 *
 * Through this interface the application's behavior can be customized with respect to the execution
 * of FSM state transitions and Run Control commands.
 *
 * All the methods have a default "empty" implementation, so that derived classes can override only methods
 * corresponding to the needed actions.
 *
 * @note The methods of this interface are never executed concurrently, with just two exceptions:
 *       @li<em>daq::rc::Controllable::publish()</em> and <em>daq::rc::Controllable::publishFullStats()</em>: they are
 *          called regularly (starting from the daq::rc::FSM_STATE::CONNECTED state and with delays defined in
 *          the OKS application's configuration) and, potentially, at the same time other methods are executed (
 *          it should be also taken into account that, for instance, daq::rc::Controllable::publish() may be in
 *          execution when a daq::rc::RC_COMMAND::PUBLISH command is receiver, having as a result the concurrent
 *          execution of two daq::rc::Controllable::publish());
 *       @li<em>daq::rc::Controllable::onExit(FSM_STATE)</em>: it is called when the application is asked to exit
 *          and, in principle, concurrently with every other action.
 *
 * @warning Any error occurring during a state transition or the execution of a command should be reported
 *          throwing an exception inheriting from ers::Issue: in such a case the framework will issue a FATAL
 *          error.
 *
 * @attention <em>daq::rc::Controllable::publish()</em> and <em>daq::rc::Controllable::publishFullStats()</em> are
 *            meant for simple monitoring purposes: it should be carefully evaluated whether those actions should
 *            ever thrown any exception.
 *
 * @see daq::rc::ItemCtrl, the main class to instantiate in order to let an application be Run Control aware.
 * @see daq::rc::ControllableDispatcher
 */
class Controllable : public TransitionActions {
    public:
        /**
         * @brief Destructor.
         */
        virtual ~Controllable() noexcept {}

        /**
         * @brief It is called regularly with delays defined in the OKS configuration (@em ProbeInterval).
         *
         * It is also called when the application receives the daq::rc::RC_COMMAND::PUBLISH command.
         * The regular timer is started as soon as the daq::rc::FSM_COMMAND::CONNECT transition is completed and
         * it is stopped as soon as the daq::rc::FSM_COMMAND::DISCONNECT transition is done.
         *
         * @attention It may be called concurrently with other methods (and with itself).
         *
         * @see CommandSender::publish(const std::string&) const
         * @see CommandSender::changePublishInterval(const std::string&, unsigned int) const
         */
        virtual void publish() {}

        /**
         * @brief It is called regularly with delays defined in the OKS configuration (@em FullStatisticsInterval).
         *
         * It is also called when the application receives the daq::rc::RC_COMMAND::PUBLISHSTATS command.
         * The regular timer is started as soon as the daq::rc::FSM_COMMAND::CONNECT transition is completed and
         * it is stopped as soon as the daq::rc::FSM_COMMAND::DISCONNECT transition is done.
         *
         * @attention It may be called concurrently with other methods (and with itself).
         *
         * @see CommandSender::publishStats(const std::string&) const
         * @see CommandSender::changeFullStatsInterval(const std::string&, unsigned int) const
         */
        virtual void publishFullStats() {}

        /**
         * @brief It is called when the application receives the daq::rc::RC_COMMAND::USER command.
         *
         * That command may also come from the parent controller in case of command broadcasting.
         *
         * @param usrCmd The user-defined command to execute
         *
         * @see CommandSender::userCommand(const std::string&, const std::string&, const std::vector<std::string>&) const
         * @see CommandSender::userCommand(const std::string&, const UserCmd&) const
         */
        virtual void user(const UserCmd& usrCmd) { (void)usrCmd; }

        /**
         * @brief It is called when the application receives the daq::rc::RC_COMMAND::ENABLE command.
         *
         * @param components The components to enable
         *
         * @see CommandSender::changeStatus(const std::string&, bool, const std::vector<std::string>&) const
         * @see CommandSender::changeStatus(const std::string&, const ChangeStatusCmd&) const
         */
        virtual void enable(const std::vector<std::string>& components) { (void)components; }

        /**
         * @brief It is called when the application receives the daq::rc::RC_COMMAND::DISABLE command.
         *
         * @param components The components to disable
         *
         * @see CommandSender::changeStatus(const std::string&, bool, const std::vector<std::string>&) const
         * @see CommandSender::changeStatus(const std::string&, const ChangeStatusCmd&) const
         */
        virtual void disable(const std::vector<std::string>& components) { (void)components; }

        /**
         * @brief It is called when the application is asked to exit.
         *
         * @attention Timeouts are defined in the configuration with respect to the time an application
         *            may take in order to exit. When the timeout elapses the application is killed with
         *            the POSIX SIGKILL. That means timeouts should be carefully adapted in order to let
         *            the application complete any needed action before exiting.
         *            \n At the same time very large timeouts would create inefficiencies related to longer
         *            transition time.
         *            \n
         * @attention It is good practice to stop any on-going action (<em>i.e.</em>, anything still being executed
         *            in any method of this interface) when this method is called, otherwise the process may have
         *            troubles to exit.
         *
         * @param state The current FSM state
         */
        virtual void onExit(FSM_STATE state) noexcept { (void)state; }
};

}}

#endif /* DAQ_RC_CONTROLLABLE_H_ */
