/*
 * RunControlCommands.h
 *
 *  Created on: Sep 25, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/Common/RunControlCommands.h
 */

#ifndef DAQ_RC_RUNCONTROLCOMMANDS_H_
#define DAQ_RC_RUNCONTROLCOMMANDS_H_

#include <RunControl/Common/RunControlBasicCommand.h>
#include <RunControl/Common/Clocks.h>

#include <boost/bimap.hpp>

#include <string>
#include <vector>
#include <set>
#include <map>
#include <memory>
#include <functional>
#include <utility>
#include <cstdint>

namespace daq { namespace rc {

enum class FSM_COMMAND : unsigned int;
enum class FSM_STATE : unsigned int;

/**
 * @brief  Enumeration for all the commands that can be sent to a Run Control application.
 *
 * @see daq::rc::RunControlCommands::commandToString and daq::rc::RunControlCommands::stringToCommand to covert enumeration
 *      items to string and viceversa.
 *
 * @remark String representations of all the Run Control commands are provided by the daq::rc::RunControlCommands class. Do
 *         no use free strings because error prone and the resulting code will depend on any change in
 *         the string representation of the Run Control commands.
 *
 * @note Some commands may be executed only by segment controllers and not by leaf applications.
 *
 * @see daq::rc::RunControlBasicCommand
 * @see daq::rc::CommandSender
 * @see daq::rc::FSM_COMMAND
 */
enum class RC_COMMAND : unsigned int {
    MAKE_TRANSITION,          //!< Command to ask a Run Control application to perform an FSM transition
    ENABLE,                   //!< Command to ask a Run Control application to enable some components
    DISABLE,                  //!< Command to ask a Run Control application to disable some components
    IGNORE_ERROR,             //!< Command to ask a Run Control application to ignore the cause(s) of its error state
    STOPAPP,                  //!< Command to ask a segment controller to stop some child applications
    STARTAPP,                 //!< Command to ask a segment controller to start some child applications
    RESTARTAPP,               //!< Command to ask a segment controller to restart some child applications
    PUBLISH,                  //!< Command to ask a Run Control application to publish information about its status
    PUBLISHSTATS,             //!< Command to ask a Run Control application to publish some statistics about itself
    EXIT,                     //!< Command to ask a Run Control application to exit
    USER,                     //!< Command to ask a Run Control application to execute an user specific command
    DYN_RESTART,              //!< Command to ask a segment controller to dynamically restart one of its child controller
    CHANGE_PROBE_INTERVAL,    //!< Command to ask a Run Control application to change the delay between information publishing
    CHANGE_FULLSTATS_INTERVAL,//!< Command to ask a Run Control application to change the delay between statistics publishing
    UPDATE_CHILD_STATUS,      //!< Command to ask a segment controller to update the status of one of its child
    TESTAPP                   //!< Command to ask a segment controller to test a child application
};

/**
 * @brief Utility class providing string representations for all the Run Control commands and conversion functions
 *        from daq::rc::RC_COMMAND items to strings and viceversa.
 */
class RunControlCommands {
    public:
        /**
         * @brief Constructor: deleted.
         */
        RunControlCommands() = delete;

        /**
         * @brief Destructor: deleted.
         */
        ~RunControlCommands() = delete;

        static const std::string MAKE_TRANSITION_CMD;           //!< String representation of the daq::rc::RC_COMMAND::MAKE_TRANSITION
        static const std::string ENABLE_CMD;                    //!< String representation of the daq::rc::RC_COMMAND::ENABLE
        static const std::string DISABLE_CMD;                   //!< String representation of the daq::rc::RC_COMMAND::DISABLE
        static const std::string IGNORE_ERROR_CMD;              //!< String representation of the daq::rc::RC_COMMAND::IGNORE_ERROR
        static const std::string STOPAPP_CMD;                   //!< String representation of the daq::rc::RC_COMMAND::STOPAPP
        static const std::string STARTAPP_CMD;                  //!< String representation of the daq::rc::RC_COMMAND::STARTAPP
        static const std::string RESTARTAPP_CMD;                //!< String representation of the daq::rc::RC_COMMAND::RESTARTAPP
        static const std::string PUBLISH_CMD;                   //!< String representation of the daq::rc::RC_COMMAND::PUBLISH
        static const std::string PUBLISHSTATS_CMD;              //!< String representation of the daq::rc::RC_COMMAND::PUBLISHSTATS
        static const std::string EXIT_CMD;                      //!< String representation of the daq::rc::RC_COMMAND::EXIT
        static const std::string USER_CMD;                      //!< String representation of the daq::rc::RC_COMMAND::USER
        static const std::string DYN_RESTART_CMD;               //!< String representation of the daq::rc::RC_COMMAND::DYN_RESTART
        static const std::string CHANGE_PROBE_INTERVAL_CMD;     //!< String representation of the daq::rc::RC_COMMAND::CHANGE_PROBE_INTERVAL
        static const std::string CHANGE_FULLSTATS_INTERVAL_CMD; //!< String representation of the daq::rc::RC_COMMAND::CHANGE_FULLSTATS_INTERVAL
        static const std::string UPDATE_CHILD_STATUS_CMD;       //!< String representation of the daq::rc::RC_COMMAND::UPDATE_CHILD_STATUS
        static const std::string TESTAPP_CMD;                   //!< String representation of the daq::rc::RC_COMMAND::TESTAPP

        static const std::string REGEX_TOKEN;                   //!< Token used to flag a regular expression (e.g., applications to restart)

        /**
         * @brief Function providing conversion from a daq::rc::RC_COMMAND item to its string representation.
         *
         * The returned string is equal to one of the static strings defined in this class. For instance,
         * the following relationship will always evaluate to <em>true</em>:
         * @code{.cpp} RunControlCommands::commandToString(RC_COMMAND::ENABLE) == RunControlCommands::ENABLE_CMD @endcode
         *
         * @param cmd The Run Control command
         * @return A string representation of the Run Control command
         */
        static std::string commandToString(RC_COMMAND cmd);

        /**
         * @brief Function providing conversion from a string to the corresponding daq::rc::RC_COMMAND item.
         *
         * Relationships like the following one will always evaluate to <em>true</em>:
         * @code{.cpp} RunControlCommands::stringToCommand(RunControlCommands::ENABLE_CMD) == RC_COMMAND::ENABLE @endcode
         *
         * @param cmd The string representation of a Run Control command
         * @return The enumeration item corresponding to @em cmd
         *
         * @throws daq::rc::BadCommand The @em cmd string does not correspond to a valid Run Control command
         */
        static RC_COMMAND stringToCommand(const std::string& cmd);

        /**
         * @brief Factory function to build a proper command object starting from it string representation.
         *
         * @remark This is the function that has to be used in order to build the command object with the proper
         *         run-time type (so that, for instance, casting is possible).
         *
         * @param stringRepresentation A string representation of the command
         * @return The command object
         *
         * @throws daq::rc::BadCommand @em stringRepresentation cannot be de-serialized to a valid command object
         *
         * @see daq::rc::RunControlBasicCommand::serialize()
         * @see daq::rc::TransitionCmd::create(const std::string&)
         */
        static std::unique_ptr<RunControlBasicCommand> factory(const std::string& stringRepresentation);

    private:
        typedef std::function<std::unique_ptr<RunControlBasicCommand>(const std::string&)> factory_function;
        typedef boost::bimap<RC_COMMAND, std::string, boost::bimaps::with_info<factory_function>> bm_type;
        static const bm_type CMD_MAP;
};

/**
 * @brief Command object for the daq::rc::RC_COMMAND::EXIT command.
 *
 * The application that receives this command is asked to gracefully exit.
 *
 * @note This command does not support asynchronous notification on completion.
 *
 * @see daq::rc::CommandSender::exit(const std::string& recipient) const
 * @see daq::rc::Controllable::onExit(FSM_STATE)
 */
class ExitCmd: public RunControlBasicCommand {
    public:
        /**
         * @brief Constructor.
         *
         * @param commandDescription The string representation of the command
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         */
        explicit ExitCmd(const std::string& commandDescription);

        /**
         * @brief Constructor.
         */
        ExitCmd();

        /**
         * @brief This command cannot be executed concurrently with other daq::rc::RC_COMMAND::EXIT commands.
         */
        std::set<RC_COMMAND> notConcurrentWith() const override;

        /**
         * @brief It returns a new fresh instance of this class representing the same command and keeping
         *        all its properties and parameters.
         *
         * @return A clone of this object.
         */
        std::unique_ptr<ExitCmd> clone() const;

        // No need to override 'toString'
    protected:
        // No need to override 'equals' (this command has no additional attributes)
        ExitCmd* doClone() const override;
};

/**
 * @brief Command object for the daq::rc::RC_COMMAND::TESTAPP command.
 *
 * Command reserved to segment controllers: when a controller receives this command, it is asked to test the status
 * of some child applications.
 *
 * @see daq::rc::CommandSender::testApp(const std::string& recipient, const std::vector<std::string>& applications) const
 * @see daq::rc::CommandSender::testApp(const std::string& recipient, const TestAppCmd& cmd) const
 */
class TestAppCmd: public RunControlBasicCommand {
    public:
        /**
         * @brief Constructor.
         *
         * @param commandDescription The string representation of the command
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         */
        explicit TestAppCmd(const std::string& commandDescription);

        /**
         * @brief Constructor.
         *
         * The test's scope is set to daq::tm::Test::Scope::Any and the test's level is set to 1.
         *
         * @param applications The list of applications that should be tested
         *
         * @note Application names can be regular expressions but, in order to be treated as such, they have to end
         *       with the @em "//r" characters.
         */
        TestAppCmd(const std::vector<std::string>& applications);

        /**
         * @brief It sets the list of child applications to be tested.
         *
         * @param applications The list of child applications to be tested
         *
         * @note Application names can be regular expressions but, in order to be treated as such, they have to end
         *       with the @em "//r" characters.
         */
        void applications(const std::vector<std::string>& applications);

        /**
         * @brief It returns the list of child applications to be tested.
         *
         * @return The list of child applications to be tested
         *
         * @note The returned list may contain regular expressions, ending with the @em "//r"
         *       sequence of characters.
         */
        std::vector<std::string> applications() const;

        /**
         * It returns the test level.
         *
         * @return The test level
         */
        int level() const;

        /**
         * It sets the test level.
         *
         * @param testLevel The test level
         */
        void level(int testLevel);

        /**
         * It returns the test's scope.
         *
         * @return The test's scope
         */
        std::string scope() const;

        /**
         * It sets the test's scope.
         *
         * @attention Valid scopes are only the ones defined in the daq::tm::Test::Scope structure.
         *
         * @param testScope The test scope
         */
        void scope(const std::string& testScope);

        /**
         * @brief This command cannot be executed concurrently with other daq::rc::RC_COMMAND::EXIT commands.
         */
        std::set<RC_COMMAND> notConcurrentWith() const override;

        /**
         * @brief It returns a new fresh instance of this class representing the same command and keeping
         *        all its properties and parameters.
         *
         * @return A clone of this object.
         */
        std::unique_ptr<TestAppCmd> clone() const;

        std::string toString() const override;

        /**
         * @brief It returns the list of child applications to be tested.
         *
         * @return The list of child applications to be tested
         */
        std::vector<std::string> arguments() const override;

    protected:
        /**
         * @brief It checks whether @em other and this represent the same command.
         *
         * @param other The command object to compare with this one
         * @return @em true if @em other is of type @em daq::rc::TestAppCmd and holds the same
         *         list of child applications to be tested
         */
        bool equals(const RunControlBasicCommand& other) const override;

        TestAppCmd* doClone() const override;

    private:
        static const std::string TESTAPP_APP_NAMES;
        static const std::string TEST_APP_SCOPE;
        static const std::string TEST_APP_LEVEL;
};

/**
 * @brief Command object for the daq::rc::RC_COMMAND::DYN_RESTART command.
 *
 * Command reserved to segment controllers: when a controller receives this command, it is asked to dynamically
 * restart some of its child controllers.
 *
 * @note This command does not support asynchronous notification on completion.
 *
 * @see daq::rc::CommandSender::dynamicRestart(const std::string& recipient, const std::vector<std::string>& segments) const
 * @see daq::rc::CommandSender::dynamicRestart(const std::string& recipient, const DynRestartCmd& cmd) const
 */
class DynRestartCmd: public RunControlBasicCommand {
    public:
        /**
         * @brief Constructor.
         *
         * @param commandDescription The string representation of the command
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         */
        explicit DynRestartCmd(const std::string& commandDescription);

        /**
         * @brief Constructor.
         *
         * @param controllers The list of controllers that should be dynamically restarted.
         */
        DynRestartCmd(const std::vector<std::string>& controllers);

        /**
         * @brief It sets the list of child controllers to be dynamically restarted.
         *
         * @param controllers The list of child controllers
         */
        void controllers(const std::vector<std::string>& controllers);

        /**
         * @brief It returns the list of child controllers to be dynamically restarted.
         *
         * @return The list of child controllers to be dynamically restarted
         */
        std::vector<std::string> controllers() const;

        /**
         * @brief It returns a new fresh instance of this class representing the same command and keeping
         *        all its properties and parameters.
         *
         * @return A clone of this object.
         */
        std::unique_ptr<DynRestartCmd> clone() const;

        std::string toString() const override;

        /**
         * @brief It returns the list of child controllers to be dynamically restarted.
         *
         * @return The list of child controllers to be dynamically restarted
         */
        std::vector<std::string> arguments() const override;

    protected:
        /**
         * @brief It checks whether @em other and this represent the same command.
         *
         * @param other The command object to compare with this one
         * @return @em true if @em other is of type @em daq::rc::DynRestartCmd and holds the same
         *         list of child controllers to be restarted
         */
        bool equals(const RunControlBasicCommand& other) const override;

        DynRestartCmd* doClone() const override;

    private:
        static const std::string DYN_RESTART_CTRL_NAMES;
};

/**
 * @brief Command object for the daq::rc::RC_COMMAND::IGNORE_ERROR command.
 *
 * Command reserved to segment controllers: when a controller receives this command, it is asked to ignore any child
 * causing the error condition (if any).
 *
 * @note This command does not support asynchronous notification on completion.
 *
 * @see daq::rc::CommandSender::ignoreError(const std::string& recipient) const
 * @see daq::rc::CommandSender::setError(const std::string& recipient, const std::vector<CommandSender::ErrorElement>& errorReasons) const
 */
class IgnoreErrorCmd: public RunControlBasicCommand {
    public:
        /**
         * @brief Constructor.
         *
         * @param commandDescription The string representation of the command
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         */
        explicit IgnoreErrorCmd(const std::string& commandDescription);

        /**
         * @brief Constructor.
         */
        IgnoreErrorCmd();

        /**
         * @brief It returns a new fresh instance of this class representing the same command and keeping
         *        all its properties and parameters.
         *
         * @return A clone of this object.
         */
        std::unique_ptr<IgnoreErrorCmd> clone() const;

        // No need to override 'toString'
    protected:
        // No need to override 'equals' (this command has no additional attributes)
        IgnoreErrorCmd* doClone() const override;
};

/**
 * @brief Command object for the daq::rc::RC_COMMAND::PUBLISH command.
 *
 * The application that receives this command is asked to publish its information.
 *
 * @see daq::rc::CommandSender::publish(const std::string& recipient) const
 * @see daq::rc::Controllable::publish()
 * @see daq::rc::UserRoutines::publishAction()
 */
class PublishCmd: public RunControlBasicCommand {
    public:
        /**
         * @brief Constructor.
         *
         * @param commandDescription The string representation of the command
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         */
        explicit PublishCmd(const std::string& commandDescription);

        /**
         * @brief Constructor.
         */
        PublishCmd();

        /**
         * @brief This command cannot be executed concurrently with other daq::rc::RC_COMMAND::EXIT commands.
         */
        std::set<RC_COMMAND> notConcurrentWith() const override;

        /**
         * @brief It returns a new fresh instance of this class representing the same command and keeping
         *        all its properties and parameters.
         *
         * @return A clone of this object.
         */
        std::unique_ptr<PublishCmd> clone() const;

    protected:
        PublishCmd* doClone() const override;
};

/**
 * @brief Command object for the daq::rc::RC_COMMAND::CHANGE_PROBE_INTERVAL command.
 *
 * The application that receives this command is asked to change the interval between two
 * consecutive information publications.
 *
 * @see daq::rc::CommandSender::changePublishInterval(const std::string& recipient, unsigned int newInterval) const
 */
class ChangePublishIntervalCmd: public RunControlBasicCommand {
    public:
        /**
         * @brief Constructor.
         *
         * @param commandDescription The string representation of the command
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         */
        explicit ChangePublishIntervalCmd(const std::string& commandDescription);

        /**
         * @brief Constructor.
         *
         * @param newInterval The new publishing interval (in seconds)
         */
        explicit ChangePublishIntervalCmd(unsigned int newInterval);

        /**
         * @brief This command cannot be executed concurrently with other daq::rc::RC_COMMAND::MAKE_TRANSITION,
         *        daq::rc::RC_COMMAND::CHANGE_PROBE_INTERVAL and daq::rc::RC_COMMAND::EXIT commands.
         */
        std::set<RC_COMMAND> notConcurrentWith() const override;

        /**
         * @brief It returns the value of the new publishing interval.
         *
         * @return The value of the new publishing interval (in seconds)
         */
        unsigned int publishInterval() const;

        /**
         * @brief It sets the value of the new publishing interval.
         *
         * @param newInterval The value of the new publishing interval (in seconds)
         */
        void publishInterval(unsigned int newInterval);

        /**
         * @brief It returns a new fresh instance of this class representing the same command and keeping
         *        all its properties and parameters.
         *
         * @return A clone of this object.
         */
        std::unique_ptr<ChangePublishIntervalCmd> clone() const;

        std::string toString() const override;

        /**
         * @brief It returns the new publishing interval.
         *
         * @return The new publishing interval
         */
        std::vector<std::string> arguments() const override;

    protected:
        ChangePublishIntervalCmd* doClone() const override;

    private:
        static const std::string CHANGE_PUBLISH_INTERVAL_TAG;
};

/**
 * @brief Command object for the daq::rc::RC_COMMAND::PUBLISHSTATS command.
 *
 * The application that receives this command is asked to publish its statistics.
 *
 * @see daq::rc::CommandSender::publishStats(const std::string& recipient) const
 * @see daq::rc::Controllable::publishFullStats()
 * @see daq::rc::UserRoutines::publishStatisticsAction()
 */

class PublishStatsCmd: public RunControlBasicCommand {
    public:
        /**
         * @brief Constructor.
         *
         * @param commandDescription The string representation of the command
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         */
        explicit PublishStatsCmd(const std::string& commandDescription);

        /**
         * @brief Constructor.
         */
        PublishStatsCmd();

        /**
         * @brief This command cannot be executed concurrently with other daq::rc::RC_COMMAND::EXIT commands.
         */
        std::set<RC_COMMAND> notConcurrentWith() const override;

        /**
         * @brief It returns a new fresh instance of this class representing the same command and keeping
         *        all its properties and parameters.
         *
         * @return A clone of this object.
         */
        std::unique_ptr<PublishStatsCmd> clone() const;

    protected:
        PublishStatsCmd* doClone() const override;
};

/**
 * @brief Command object for the daq::rc::RC_COMMAND::CHANGE_FULLSTATS_INTERVAL command.
 *
 * The application that receives this command is asked to change the interval between two
 * consecutive statistics publications.
 *
 * @see daq::rc::CommandSender::changeFullStatsInterval(const std::string& recipient, unsigned int newInterval) const
 */
class ChangeFullStatIntervalCmd: public RunControlBasicCommand {
    public:
        /**
         * @brief Constructor.
         *
         * @param commandDescription The string representation of the command
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         */
        explicit ChangeFullStatIntervalCmd(const std::string& commandDescription);

        /**
         * @brief Constructor.
         *
         * @param newInterval The new publication interval (in seconds)
         */
        explicit ChangeFullStatIntervalCmd(unsigned int newInterval);

        /**
         * @brief This command cannot be executed concurrently with other daq::rc::RC_COMMAND::MAKE_TRANSITION,
         *        daq::rc::RC_COMMAND::CHANGE_PROBE_INTERVAL and daq::rc::RC_COMMAND::EXIT commands.
         */
        std::set<RC_COMMAND> notConcurrentWith() const override;

        /**
         * @brief It returns the new publishing interval
         *
         * @return The new publishing interval (in seconds)
         */
        unsigned int fullStatInterval() const;

        /**
         * @brief It sets the new publishing interval.
         *
         * @param newInterval The new publishing interval (in seconds)
         */
        void fullStatInterval(unsigned int newInterval);

        /**
         * @brief It returns a new fresh instance of this class representing the same command and keeping
         *        all its properties and parameters.
         *
         * @return A clone of this object.
         */
        std::unique_ptr<ChangeFullStatIntervalCmd> clone() const;

        std::string toString() const override;

        /**
         * @brief It returns as the new publish interval as the only argument.
         *
         * @return The new publish interval
         */
        std::vector<std::string> arguments() const override;

    protected:
        ChangeFullStatIntervalCmd* doClone() const override;

    private:
        static const std::string CHANGE_FULLSTAT_INTERVAL_TAG;
};

/**
 * @brief Command object for the daq::rc::RC_COMMAND::ENABLE and daq::rc::RC_COMMAND::DISABLE commands.
 *
 * The application that receives this command is asked to change the status of some components.
 *
 * @see daq::rc::CommandSender::changeStatus(const std::string& recipient, const ChangeStatusCmd& cmd) const
 * @see daq::rc::CommandSender::changeStatus(const std::string& recipient, bool enabled, const std::vector<std::string>& components) const
 * @see daq::rc::Controllable::enable(const std::vector<std::string>& components)
 * @see daq::rc::Controllable::disable(const std::vector<std::string>& components)
 */
class ChangeStatusCmd: public RunControlBasicCommand {
    public:
        /**
         * @brief Constructor.
         *
         * @param commandDescription The string representation of the command
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         */
        explicit ChangeStatusCmd(const std::string& commandDescription);

        /**
         * @brief Constructor.
         *
         * @param enable @em true if the component has to be enabled
         * @param componentName The name of the component to be enabled or disabled
         *
         * @note The component name can be a regular expressions but, in order to be treated as such, it has to end
         *       with the @em "//r" characters.
         */
        ChangeStatusCmd(bool enable, const std::string& componentName);

        /**
         * @brief Constructor
         *
         * @param enable @em true if the component has to be enabled
         * @param components List of components to be enabled or disabled
         *
         * @note Component names can be regular expressions but, in order to be treated as such, they have to end
         *       with the @em "//r" characters.
         */
        ChangeStatusCmd(bool enable, const std::vector<std::string>& components);

        /**
         * @brief It sets whether the components have to be enabled or disabled.
         *
         * @param isEnabled If @em true the components should be enabled
         */
        void enable(bool isEnabled);

        /**
         * @brief It returns @em true if the components should be enabled.
         *
         * @return @em true if the components should be enabled
         */
        bool isEnable() const;

        /**
         * @brief It returns the list of components to be enabled or disabled.
         *
         * @return The list of components to be enabled or disabled
         *
         * @note Component names can be regular expressions, ending with the @em "//r" characters.
         */
        std::vector<std::string> components() const;

        /**
         * @brief It sets the components that should be enabled or disabled
         *
         * @param components The components that should be enabled or disabled
         *
         * @note Component names can be regular expressions but, in order to be treated as such, they have to end
         *       with the @em "//r" characters.
         */
        void components(const std::vector<std::string>& components);

        /**
         * @brief It returns a new fresh instance of this class representing the same command and keeping
         *        all its properties and parameters.
         *
         * @return A clone of this object.
         */
        std::unique_ptr<ChangeStatusCmd> clone() const;

        std::string toString() const override;

        /**
         * @brief The returned arguments contain all the components to be enabled or disabled.
         *
         * @return The components to be enabled or disabled
         */
        std::vector<std::string> arguments() const override;

    protected:
        /**
         * @brief It checks whether @em other and this object represent the same command.
         *
         * @param other The command to be compared to this one
         * @return @em true if @em other is of type ChangeStatusCmd and it holds the same list
         *         of component to enable/disable and it refers to the same "enabling" or
         *         "disabling" action
         */
        bool equals(const RunControlBasicCommand& other) const override;
        ChangeStatusCmd* doClone() const override;

    private:
        static const std::string CHANGESTATUS_CMD_ENABLE_DISABLE_TAG;
        static const std::string CHANGESTATUS_CMD_CMPNTS_TAG;
};

/**
 * @brief Command object for the daq::rc::RC_COMMAND::USER command.
 *
 * The application that receives this command is asked to execute an user defined command.
 *
 * @note An user defined command is identified by a name and a list of parameters.
 *
 * @see daq::rc::CommandSender::userCommand(const std::string& recipient, const std::string& cmdName, const std::vector<std::string>& args) const
 * @see daq::rc::CommandSender::userCommand(const std::string& recipient, const UserCmd& userCmd) const
 * @see daq::rc::Controllable::user(const UserCmd& usrCmd)
 */
class UserCmd: public RunControlBasicCommand {
    public:
        /**
         * @brief Constructor.
         *
         * @param commandDescription The string representation of the command
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         */
        explicit UserCmd(const std::string& commandDescription);

        /**
         * @brief Constructor.
         *
         * @param commandName The name of this user command
         * @param params The parameters of this user command
         */
        UserCmd(const std::string& commandName, const std::vector<std::string>& params);

        /**
         * @brief It returns the current FSM state.
         *
         * @return The current FSM state
         *
         * @throws daq::rc::CommandParameterNotFound The current FSM state cannot be found in the command description. This method will
         *                                           never throw when passed to the daq::rc::Controllable interface.
         *
         */
        std::string currentFSMState() const;

        /**
         * @brief It sets the current FSM state.
         *
         * @param state The current FSM state
         */
        void currentFSMState(FSM_STATE state);

        /**
         * @brief It returns whether the received user command comes from a wider broadcast and it is not directly sent
         *        to the application.
         *
         * @return @em true if the command comes from a wider broadcast
         */
        bool isBroadcast() const;

        /**
         * @brief It returns the name of this user defined command.
         *
         * @return The name of this user defined command
         */
        std::string commandName() const;

        /**
         * @brief It sets the name of this user defined command.
         *
         * @param userCommandName The name of this user defined command
         */
        void commandName(const std::string& userCommandName);

        /**
         * @brief It returns the parameters of this user defined command.
         *
         * @return The parameters of this user defined command
         */
        std::vector<std::string> commandParameters() const;

        /**
         * @brief It sets the parameters for this user defined command.
         *
         * @param parameters The parameters for this user defined command
         */
        void commandParameters(const std::vector<std::string>& parameters);

        /**
         * @brief It returns a new fresh instance of this class representing the same command and keeping
         *        all its properties and parameters.
         *
         * @return A clone of this object.
         */
        std::unique_ptr<UserCmd> clone() const;

        std::string toString() const override;

        /**
         * @brief It returns the arguments of the full command.
         *
         * @return The list of arguments: the name of the user command followed by its arguments.
         *
         * @attention Use UserCmd::commandParameters() to have only the arguments of the underlining user command.
         */
        std::vector<std::string> arguments() const override;

    protected:
        friend struct CmdAccessor;

        /**
         * @brief It sets whether the received user command comes from a wider broadcast and it is not directly sent
         *        to the application.
         *
         * @param value @em true if the command comes from a wider broadcast
         */
        void isBroadcast(bool value);

        /**
         * @brief It checks whether @em other and this object represent the same command.
         *
         * @param other The command object to be compared with this one
         * @return @em true if @em other is of type UserCmd and represent the same user command as this object (that means
         *         the user commands have the same name and arguments)
         */
        bool equals(const RunControlBasicCommand& other) const override;

        UserCmd* doClone() const override;

    private:
        static const std::string USR_CMD_NAME_TAG;
        static const std::string USR_CMD_PARAMS_TAG;
        static const std::string USR_CMD_FSM_STATE_TAG;
        static const std::string USR_CMD_IS_BROADCAST;
};

// Forward declaration
class TransitionCmd;

/**
 * @brief Command object for the daq::rc::RC_COMMAND::UPDATE_CHILD_STATUS command.
 *
 * The segment's controller that receives this command will update the information about the corresponding child
 * (contained in this command object).
 *
 * @warning This command is sent to segment's controllers by their children and should never be sent manually!
 *
 * @remark This command is also used to retrieve the status of an application on-demand.
 *
 * @see daq::rc::CommandSender::updateChild(const std::string& recipient, const ApplicationStatusCmd& statusUpdateCmd) const
 * @see daq::rc::CommandSender::status(const std::string& recipient) const
 * @see daq::rc::CommandSender::childStatus(const std::string& recipient, const std::string& childName) const
 */
class ApplicationStatusCmd: public RunControlBasicCommand {
    public:
        /**
         * @brief Constructor.
         *
         * @param commandDescription The string representation of the command
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         */
        explicit ApplicationStatusCmd(const std::string& commandDescription);

        /**
         * @brief Constructor.
         *
         * @param appName The name of the application the information contained in this command object refers to
         * @param appState The FSM state of the application
         * @param transitioning @em true if the application is performing a state transition
         * @param busy @em true if the application is busy executing any command
         * @param fullyInitialized @em true if the application has already done the PMG synch
         * @param infoTimeTag Time tag - in nanoseconds - of the information (it may have no relationship at all with the clock time)
         * @param infoWallTime Wall time tag - in nanoseconds - of the information (<em>i.e.</em>, clock time)
         */
        ApplicationStatusCmd(const std::string& appName,
                             FSM_STATE appState,
                             bool transitioning,
                             bool busy,
                             bool fullyInitialized,
                             infotime_t infoTimeTag,
                             long long infoWallTime);

        /**
         * @brief It return the name of the application the information contained in this command object refers to.
         *
         * @return The the name of the application the information contained in this command object refers to
         */
        std::string applicationName() const;

        /**
         * @brief It sets the name of the application the information contained in this command object refers to
         *
         * @param appName The name of the application the information contained in this command object refers to
         */
        void applicationName(const std::string& appName);

        /**
         * @brief It returns the FSM state of the application.
         *
         * @return The FSM state of the application
         */
        std::string fsmState() const;

        /**
         * @brief It sets the FSM state of the application
         *
         * @param state The FSM state of the application
         */
        void fsmState(FSM_STATE state);

        /**
         * @brief It returns @em true if the application is in the middle of an FSM transition.
         *
         * @return @em true if the application is in the middle of an FSM transition
         */
        bool isTransitioning() const;

        /**
         * @brief It sets whether the application is in the middle of an FSM transition or not.
         *
         * @param isTransitioning @em true if the application is in the middle of an FSM transition
         */
        void isTransitioning(bool isTransitioning);

        /**
         * @brief It returns a list of reasons describing why the application is in an error state.
         *
         * If the application is not in error then an empty list is returned.
         *
         * @return A list of reasons describing why the application is in an error state
         */
        std::vector<std::string> errorReasons() const;

        /**
         * @brief It sets a list of reasons why the application is in error state.
         *
         * In order to describe an application that is not in an error state, the list should be empty
         *
         * @param errorReasons The list of reasons why the application is in error state
         */
        void errorReasons(const std::vector<std::string>& errorReasons);

        /**
         * @brief It returns the names of child applications causing the error state.
         *
         * If the error is internal to the application, then this method will return the name of the application
         * itself (as it happens for a leaf application).
         *
         * @return The names of child applications causing the error state
         */
        std::vector<std::string> badChildren() const;

        /**
         * @brief It sets a list of child applications causing the error state.
         *
         * The list may contain the application itself in case of an internal bad state.
         *
         * @param badChildren The list of child applications causing the error state
         */
        void badChildren(const std::vector<std::string>& badChildren);

        /**
         * @brief It returns @em true if the application is busy executing any command.
         *
         * @return @em true if the application is busy executing any command
         */
        bool isBusy() const;

        /**
         * @brief It sets the application's busy state.
         *
         * @param isBusy @em true if the application is busy executing any command
         */
        void isBusy(bool isBusy);

        /**
         * @brief It returns the last transition command executed by the application.
         *
         * @return The last transition command executed by the application
         *
         * @warning It may return @em null if the application has not performed any transition command yet.
         */
        std::unique_ptr<TransitionCmd> lastTransitionCommand() const;

        /**
         * @brief It sets the last transition command executed by the application.
         *
         * @param cmd The last transition command executed by the application
         */
        void lastTransitionCommand(const TransitionCmd& cmd);

        /**
         * @brief It returns the current transition command being executed by the application.
         *
         * @return The current transition command being executed by the application
         *
         * @warning It may return @em null if the application is not performing any FSM transition
         */
        std::unique_ptr<TransitionCmd> currentTransitionCommand() const;

        /**
         * @brief It sets the FSM transition currently performed by the application.
         *
         * @param cmd The FSM transition currently performed by the application
         */
        void currentTransitionCommand(const TransitionCmd& cmd);

        /**
         * @brief It returns the duration (in milliseconds) of the last transition.
         *
         * @return The duration (in milliseconds) of the last transition
         */
        infotime_t lastTransitionDuration() const;

        /**
         * @brief It sets the duration (in milliseconds) of the last transition.
         *
         * @param duration The duration (in milliseconds) of the last transition
         */
        void lastTransitionDuration(infotime_t duration);

        /**
         * @brief It returns the last executed command.
         *
         * @return The last executed command
         *
         * @warning It may return @em null if no command has been executed yet.
         */
        std::unique_ptr<RunControlBasicCommand> lastCommand() const;

        /**
         * @brief It sets the last command executed by the application.
         *
         * @param cmd The last command executed by the application
         */
        void lastCommand(const RunControlBasicCommand& cmd);

        /**
         * @brief It returns @em true if the application has already done the PMG synch.
         *
         * @return @em true if the application has already done the PMG synch
         */
        bool isFullyInitialized() const;

        /**
         * @brief It sets whether the application has already done the PMG synch or not.
         *
         * @param done If @em true the application has already done the PMG synch
         */
        void isFullyInitialized(bool done);

        /**
         * @brief It returns the time tag (in nanoseconds) of this status update command.
         *
         * @note This time tag may not have any relationship with the clock time.
         *
         * @return The time tag (in nanoseconds) of this status update command
         */
        infotime_t infoTimeTag() const;

        /**
         * @brief It sets the time (in nanoseconds) of this status update command.
         *
         * @param timeTag The time (in nanoseconds) of this status update command
         */
        void infoTimeTag(infotime_t timeTag);

        /**
         * @brief It returns the clock time (in nanoseconds) of this status update command.
         *
         * @return The clock time (in nanoseconds) of this status update command
         *
         */
        long long infoWallTime() const;

        /**
         * @brief It sets the clock time (in nanoseconds) of this status update command.
         *
         * @param wallTime The clock time (in nanoseconds) of this status update command
         */
        void infoWallTime(long long wallTime);

        /**
         * @brief It returns a new fresh instance of this class representing the same command and keeping
         *        all its properties and parameters.
         *
         * @return A clone of this object.
         */
        std::unique_ptr<ApplicationStatusCmd> clone() const;

        std::string toString() const override;

    protected:
        // 'equals' is not implemented because not needed for this kind of command
        ApplicationStatusCmd* doClone() const override;

    private:
        static const std::string APP_NAME_TAG;
        static const std::string APP_STATE_TAG;
        static const std::string APP_IS_TR_TAG;
        static const std::string APP_ERROR_TAG;
        static const std::string APP_BAD_CHLD_TAG;
        static const std::string APP_IS_BUSY_TAG;
        static const std::string APP_IS_FULLINIT_TAG;
        static const std::string APP_LAST_TR_CMD_TAG;
        static const std::string APP_CURR_TR_CMD_TAG;
        static const std::string APP_LAST_TR_TIME_TAG;
        static const std::string APP_LAST_CMD_TAG;
        static const std::string APP_INFO_TIME_TAG_TAG;
        static const std::string APP_INFO_WALL_TIME_TAG;
};

/**
 * @brief Command object for the daq::rc::RC_COMMAND::STOPAPP, daq::rc::RC_COMMAND::STARTAPP
 * and daq::rc::RC_COMMAND::RESTARTAPP commands.
 *
 * The segment's controller that receives this command is asked to start, stop or restart
 * some applications.
 *
 * @see daq::rc::CommandSender::startStopApplications(const std::string& recipient, const StartStopAppCmd& cmd) const
 * @see daq::rc::CommansSender::startApplications(const std::string& recipient, const std::vector<std::string>& applications) const
 * @see daq::rc::CommandSender::stopApplications(const std::string& recipient, const std::vector<std::string>& applications) const
 * @see daq::rc::CommandSender::restartApplications(const std::string& recipient, const std::vector<std::string>& applications) const
 */
class StartStopAppCmd: public RunControlBasicCommand {
    public:
        /**
         * @brief Structure used to tag a daq::rc::RC_COMMAND::STARTAPP command.
         */
        struct is_start_t {};

        /**
         * @brief Structure used to tag a daq::rc::RC_COMMAND::STOPAPP command.
         */
        struct is_stop_t {};

        /**
         * @brief Structure used to tag a daq::rc::RC_COMMAND::RESTARTAPP command.
         */
        struct is_restart_t {};

        static const is_start_t is_start_app_cmd;       //!< Tag object to identify a daq::rc::RC_COMMAND::STARTAPP command
        static const is_stop_t is_stop_app_cmd;         //!< Tag object to identify a daq::rc::RC_COMMAND::STOPAPP command
        static const is_restart_t is_restart_app_cmd;   //!< Tag object to identify a daq::rc::RC_COMMAND::RESTARTAPP command

    public:
        /**
         * @brief Constructor.
         *
         * @param commandDescription The string representation of the command
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         */
        explicit StartStopAppCmd(const std::string& commandDescription);

        /**
         * @brief Constructor for a daq::rc::RC_COMMAND::STARTAPP command object.
         *
         * @param tag Tag parameter
         * @param applications List of applications to be started
         *
         * @note Application names can be regular expressions but, in order to be treated as such, they have to end
         *       with the @em "//r" characters.
         */
        StartStopAppCmd(is_start_t tag, const std::vector<std::string>& applications);

        /**
         * @brief Constructor for a daq::rc::RC_COMMAND::STOPAPP command object.
         *
         * @param tag Tag parameter
         * @param applications List of applications to be stopped
         *
         * @note Application names can be regular expressions but, in order to be treated as such, they have to end
         *       with the @em "//r" characters.
         */
        StartStopAppCmd(is_stop_t tag, const std::vector<std::string>& applications);

        /**
         * @brief Constructor for a daq::rc::RC_COMMAND::RESTARTAPP command object.
         *
         * @param tag Tag parameter
         * @param applications List of applications to be restarted
         *
         * @note Application names can be regular expressions but, in order to be treated as such, they have to end
         *       with the @em "//r" characters.
         */
        StartStopAppCmd(is_restart_t tag, const std::vector<std::string>& applications);

        /**
         * @brief It returns the list of applications to be started, stopped or restarted.
         *
         * @return The list of applications to be started, stopped or restarted
         *
         * @note The returned list may actually contain regular expressions, ending with the @em "//r" characters.
         */
        std::set<std::string> applications() const;

        /**
         * @brief It sets the list of applications to be started, stopped or restarted.
         *
         * @param apps The list of applications to be started, stopped or restarted
         *
         * @note Application names can be regular expressions but, in order to be treated as such, they have to end
         *       with the @em "//r" characters.
         */
        void applications(const std::vector<std::string>& apps);

        /**
         * @brief It returns the current FSM state.
         *
         * @note The FSM state may be used by the segmet's controller in order to decide
         *       whether applications may be safely started, stopped or restarted.
         *
         * @return The current FSM state
         *
         * @throws daq::rc::CommandParameterNotFound The FSM state is not defined
         * @see StartStopAppCmd::destinationState()
         */
        std::string currentState() const;

        /**
         * @brief It sets the current FSM state.
         *
         * @param state The current FSM state
         */
        void currentState(FSM_STATE state);

        /**
         * @brief It returns the FSM state the segment's controller is moving towards.
         *
         * The FSM state returned by this method is equal to the one returned by StartStopAppCmd::currentState()
         * if no FSM transitions are on-going.
         *
         * @return The FSM state the segment's controller is moving towards
         *
         * @throws daq::rc::CommandParameterNotFound The FSM state is not defined
         */
        std::string destinationState() const;

        /**
         * @brief It sets the FSM state the segment's controller is moving towards.
         *
         * @param state The FSM state the segment's controller is moving towards
         */
        void destinationState(FSM_STATE state);

        /**
         * @brief It returns the current or last-executed FSM transition command.
         *
         * @return The current FSM transition command or the last one if no transition is on-going
         *
         * @throws daq::rc::CommandParameterNotFound The transition command is not defined
         */
        std::string transitionCommand() const;

        /**
         * @brief It sets the current or the last-executed FSM transition command.
         *
         * @param cmd The current or the last-executed (if not transition is on-going) FSM transition command
         */
        void transitionCommand(FSM_COMMAND cmd);

        /**
         * @brief It returns @em true if this command object refers to a daq::rc::RC_COMMAND::STARTAPP command.
         *
         * @note This command refers to a daq::rc::RC_COMMAND::STOPAPP command if both StartStopAppCmd::isStart() and
         *       StartStopAppCmd::isRestart() return @em false.
         *
         * @return @em true if this command object refers to a daq::rc::RC_COMMAND::STARTAPP command
         */
        bool isStart() const;

        /**
         * @brief It returns @em true if this command object refers to a daq::rc::RC_COMMAND::RESTARTAPP command.
         *
         * @return @em true if this command object refers to a daq::rc::RC_COMMAND::RESTARTAPP command
         */
        bool isRestart() const;

        /**
          * @brief This command cannot be executed concurrently with other daq::rc::RC_COMMAND::EXIT commands.
         */
        std::set<RC_COMMAND> notConcurrentWith() const override;

        /**
         * @brief It returns a new fresh instance of this class representing the same command and keeping
         *        all its properties and parameters.
         *
         * @return A clone of this object.
         */
        std::unique_ptr<StartStopAppCmd> clone() const;

        std::string toString() const override;

        /**
         * @brief It returns the list of applications to start, stop or restart.
         *
         * @return The list of applications to start, stop or restart
         */
        std::vector<std::string> arguments() const override;

    protected:
        /**
         * @brief It checks whether @em other and this represent the same command.
         *
         * @param other The command object to compare with this one
         *
         * @return @em true if @em other is of type @em daq::rc::StartStopAppCmd, holds the same
         *         list of applications to be started, stopped or restarted and and it refers to the
         *         same action (<em>i.e.</em>, start, stop or restart)
         */
        bool equals(const RunControlBasicCommand& other) const override;

        StartStopAppCmd* doClone() const override;

    private:
        static const std::string APP_TAG;
        static const std::string CURRENT_STATE_TAG;
        static const std::string DESTINATION_STATE_TAG;
        static const std::string COMMAND_TAG;
        static const std::string IS_START_TAG;
        static const std::string IS_RESTART_TAG;
};

/**
 * @brief Command object for the daq::rc::RC_COMMAND::MAKE_TRANSITION command.
 *
 * The segment's controller that receives this command is asked to perform an FSM transition.
 *
 * @see daq::rc::CommandSender::makeTransition(const std::string& recipient, const TransitionCmd& trCommand) const
 * @see daq::rc::CommandSender::makeTransition(const std::string& recipient, FSM_COMMAND cmd) const
 * @see daq::rc::Controllable to have access to actions associated to FSM transitions in leaf applications
 * @see daq::rc::UserRoutines to have access to actions associated to FSM transitions in segment controllers
 */
class TransitionCmd: public RunControlBasicCommand {
    public:
        /**
         * @brief Constructor.
         *
         * @param commandDescription The string representation of the command
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         */
        explicit TransitionCmd(const std::string& commandDescription);

        /**
         * @brief Constructor
         *
         * @param cmd The associated FSM command
         * @param isRestart @em true if the transition is performed as a consequence of a failure (<em>i.e.</em>, a crash)
         *                  of the segment's controller
         *
         * @warning If @em isRestart is set to @em true and the command is sent to a segment's controller, then
         *          the controller will basically skip any action until the proper FSM state is reached. Under
         *          normal usage @em isRestart shall always be set to @em false.
         */
        explicit TransitionCmd(FSM_COMMAND cmd, bool isRestart = false);

        /**
         * @brief Factory-like method creating a TransitionCmd with the proper run-time type.
         *
         * @param commandDescription The string representation of the command
         * @return A TransitionCmd with the proper run-time type
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         *
         * @remark This method, and not the constructor, shall be used any time the type of the transition
         *         command is not known in advance and the proper run-time type is needed
         *
         * @see RunControlCommands::factory(const std::string& stringRepresentation)
         */
        static std::unique_ptr<TransitionCmd> create(const std::string& commandDescription);

        /**
         * @brief It returns the string representation of the daq::rc::FSM_COMMAND associated to this command.
         *
         * @return The string representation of the daq::rc::FSM_COMMAND associated to this command
         *
         * @see daq::rc::FSMCommands::stringToCommand
         */
        std::string fsmCommand() const;

        /**
         * @brief It sets the FSM command associated to this command.
         *
         * @param cmd The FSM command associated to this command
         */
        void fsmCommand(FSM_COMMAND cmd);

        /**
         * @brief It returns the string representation of the daq::rc::FSM_STATE source state of the transition.
         *
         * @return The string representation of the daq::rc::FSM_STATE source state of the transition
         *
         * @throws daq::rc::CommandParameterNotFound The transition's source state is not defined. This method will never throw once
         *                                           it is passed to daq::rc::Controllable or daq::rc::UserRoutines.
         *
         * @see daq::rc::FSMStates::stringToState
         */
        std::string fsmSourceState() const;

        /**
         * @brief It sets the source state of the transition.
         *
         * @param state The source state of the transition
         */
        void fsmSourceState(FSM_STATE state);

        /**
         * @brief It returns the string representation of the daq::rc::FSM_STATE destination state of the transition.
         *
         * @return The string representation of the daq::rc::FSM_STATE destination state of the transition
         *
         * @throws daq::rc::CommandParameterNotFound The transition's destination state is not defined. This method will never throw once
         *                                           it is passed to daq::rc::Controllable or daq::rc::UserRoutines.
         *
         * @see daq::rc::FSMStates::stringToState
         */
        std::string fsmDestinationState() const;

        /**
         * @brief It sets the destination state of the transition.
         *
         * @param state The destination state of the transition
         */
        void fsmDestinationState(FSM_STATE state);

        /**
         * @brief It returns @em true if the transition is performed as a consequence of a failure (<em>i.e.</em>, a crash)
         *        of the segment's controller.
         *
         * @attention This parameter is relevant only for segment's controllers and should be ignored by leaf applications.
         *
         * @return @em true if the transition is performed as a consequence of a failure (<em>i.e.</em>, a crash)
         *         of the segment's controller
         */
        bool isRestart() const;

        /**
         * @brief It sets whether transition is performed as a consequence of a failure (<em>i.e.</em>, a crash)
         *        of the segment's controller
         *
         * @param isControllerRestart @em true if the transition is performed as a consequence of a failure (<em>i.e.</em>, a crash)
         *        of the segment's controller
         */
        void isRestart(bool isControllerRestart);

        /**
         * @brief It returns the list of sub-transitions to be still performed for the current transition.
         *
         * @note Used by segment's controllers in order to keep trace of the the sub-transitions to be dispatched
         *       to children.
         *
         * @attention Not relevant for leaf applications that should not rely on the result of this method in their
         *            actions.
         *
         * @return The list of sub-transitions to be still performed for the current transition
         */
        virtual std::vector<std::string> subTransitions() const;

        /**
         * @brief It sets the list of sub-transitions to be executed for various FSM transitions.
         *
         * @note Used by segment's controllers in order to keep trace of the the sub-transitions to be dispatched
         *       to children.
         *
         * @attention Not relevant for leaf applications that should not rely on the result of this method in their
         *            actions.
         *
         * @param subTrMap List of sub-transitions to be executed for various FSM transitions
         */
        void subTransitions(const std::map<FSM_COMMAND, std::vector<std::string>>& subTrMap);

        /**
         * @brief It returns @em true if any sub-transition has already been executed for the current main transition.
         *
         * @note Used by segment's controllers in order to keep trace of the the sub-transitions to be dispatched
         *       to children.
         *
         * @attention Not relevant for leaf applications that should not rely on the result of this method in their
         *            actions.
         *
         * @return @em true if any sub-transition has already been executed for the current main transition.
         */
        bool subTransitionsDone() const;

        /**
         * @brief It sets whether any sub-transition has already been executed for the current main transition.
         *
         * @param done @em true if any sub-transition has already been executed for the current main transition
         */
        void subTransitionsDone(bool done);

        /**
         * @brief It returns @em true if sub-transitions should be skipped and not executed.
         *
         * @warning Mainly for debugging reason using the @em rc_sender command line uitlity or the @em interactive mode
         *          of the daq::rc::ItemCtrl.
         *
         * @return @em true if sub-transitions should be skipped and not executed
         */
        bool skipSubTransitions() const;

        /**
         * @brief It sets whether sub-transitions should be skipped and not executed.
         *
         * @param skip @em true if sub-transitions should be skipped and not executed
         */
        void skipSubTransitions(bool skip);

        /**
         * @brief It returns a new fresh instance of this class representing the same command and keeping
         *        all its properties and parameters.
         *
         * @return A clone of this object.
         */
        std::unique_ptr<TransitionCmd> clone() const;

        /**
         * @brief This command cannot be executed concurrently with other RC_COMMAND::EXIT, RC_COMMAND::MAKE_TRANSITION, RC_COMMAND::RESTARTAPP,
         *        RC_COMMAND::STARTAPP and RC_COMMAND::STOPAPP commands.
         */
        std::set<RC_COMMAND> notConcurrentWith() const override;

        std::string toString() const override;

        /**
         * @brief It returns the string representation of the FSM command as the only argument.
         *
         * @return The string representation of the FSM command as the only argument
         */
        std::vector<std::string> arguments() const override;

    protected:
        /**
         * @brief It checks whether @em other and this represent the same command.
         *
         * @param other The command object to compare with this one
         *
         * @return @em true if @em other is of type @em daq::rc::TransitionCmd and it refers to the
         *         same FSM transition
         */
        virtual bool equals(const RunControlBasicCommand& other) const override;

        virtual TransitionCmd* doClone() const override;

    private:
        static const std::string FSM_HEADER_TAG;
        static const std::string FSM_COMMAND_TAG;
        static const std::string FSM_SRC_STATE_TAG;
        static const std::string FSM_DST_STATE_TAG;
        static const std::string FSM_SUBTR_DONE_TAG;
        static const std::string FSM_SUBTR_SKIP_TAG;
        static const std::string FSM_IS_RESTART_TAG;

    protected:
        /**
         * @brief Name of the sub-transitions parameter.
         *
         * Provided in order to let derived class properly override TransitionCmd::subTransitions().
         */
        static const std::string FSM_SUBTR_TAG;

};

/**
 * @brief Command object for the daq::rc::FSM_COMMAND::RESYNCH FSM transition.
 *
 * This in-state transition can be performed only in the daq::rc::FSM_STATE::RUNNING state.
 *
 * @see daq::rc::CommandSender::makeTransition(const std::string& recipient, const TransitionCmd& trCommand) const
 * @see daq::rc::CommandSender::makeTransition(const std::string& recipient, FSM_COMMAND cmd) const
 * @see daq::rc::Controllable::resynch(const ResynchCmd& cmd)
 */
class ResynchCmd: public TransitionCmd {
    public:
        /**
         * @brief Constructor.
         *
         * @param commandDescription The string representation of the command
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         */
        explicit ResynchCmd(const std::string& commandDescription);

        /**
         * @brief Constructor.
         *
         * @param ecrCount Number of ECR counts
         * @param extendedL1ID The last valid extended L1 ID
         * @param modules List of modules to be re-synchronized
         */
        explicit ResynchCmd(std::uint32_t ecrCount, std::uint32_t extendedL1ID = 0, const std::vector<std::string>& modules = {});

        /**
         * @brief It returns the number of ECR counts.
         *
         * @return The number of ECR counts
         */
        std::uint32_t ecrCounts() const;

        /**
         * @brief It sets the number of ECR counts.
         *
         * @param ecr The number of ECR counts
         */
        void ecrCounts(const std::uint32_t ecr);

        /**
         * @brief It returns the last valid extended L1 ID
         * <p>
         * @warning A value of zero should not be considered as valid
         *
         * @return The Last valid extended L1 ID
         */
        std::uint32_t extendedL1ID() const;

        /**
         * @brief It sets the last valid extended L1 ID
         *
         * @param extendedL1ID The last valid extended L1 ID
         */
        void extendedL1ID(const std::uint32_t extendedL1ID);

        /**
         * @brief It returns the list of modules that should be re-synchronized.
         *
         * @return The list of modules that should be re-synchronized
         */
        std::vector<std::string> modules() const;

        /**
         * @brief It sets the list of modules that should be re-synchronized.
         *
         * @param mods The list of modules that should be re-synchronized
         */
        void modules(const std::vector<std::string>& mods);

        /**
         * @brief It returns a new fresh instance of this class representing the same command and keeping
         *        all its properties and parameters.
         *
         * @return A clone of this object.
         */
        std::unique_ptr<ResynchCmd> clone() const;

        std::string toString() const override;

        /**
         * @brief It returns (in order) the FSM command, the ECR counts and the list of modules.
         *
         * @return In order, the FSM command, the ECR counts and the list of modules
         */
        std::vector<std::string> arguments() const override;

    protected:
        /**
         * @brief It checks whether @em other and this represent the same command.
         *
         * @param other The command object to compare with this one
         * @return @em true if @em other is of type @em daq::rc::ResynchCmd and holds the same
         *         number of ECR counts and the same list of modules
         */
        bool equals(const RunControlBasicCommand& other) const override;

        ResynchCmd* doClone() const override;

    private:
        static const std::string RESYNCH_ECR_TAG;
        static const std::string RESYNCH_EL1ID_TAG;
        static const std::string RESYNCH_MODULES_TAG;
};

/**
 * @brief Command object for the daq::rc::FSM_COMMAND::USERBROADCAST FSM transition.
 *
 * When received by a segment controller it will dispatch the user-defined command to all of its children.
 *
 * @see daq::rc::CommandSender::makeTransition(const std::string& recipient, const TransitionCmd& trCommand) const
 * @see daq::rc::CommandSender::makeTransition(const std::string& recipient, FSM_COMMAND cmd) const
 * @see daq::rc::Controllable::user(const UserCmd& usrCmd)
 */
class UserBroadcastCmd: public TransitionCmd {
    public:
        /**
         * @brief Constructor.
         *
         * @param commandDescription The string representation of the command
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         */
        explicit UserBroadcastCmd(const std::string& commandDescription);

        /**
         * @brief Constructor.
         *
         * @param userCmd The user-defined command
         */
        explicit UserBroadcastCmd(const UserCmd& userCmd);

        /**
         * @brief It returns the underlining user-defined command.
         *
         * @return The underlining user-defined command
         */
        std::unique_ptr<UserCmd> userCommand() const;

        /**
         * @brief It sets the underlining user-defined command.
         *
         * @param userCmd The the underlining user-defined command
         */
        void userCommand(const UserCmd& userCmd);

        /**
         * @brief It returns a new fresh instance of this class representing the same command and keeping
         *        all its properties and parameters.
         *
         * @return A clone of this object.
         */
        std::unique_ptr<UserBroadcastCmd> clone() const;

        std::string toString() const override;

        /**
         * @brief It returns (in order) the FSM command, the name of the user-defined command and its arguments.
         *
         * @return In order, the FSM command, the name of the user-defined command and its arguments
         */
        std::vector<std::string> arguments() const override;

    protected:
        /**
         * @brief It checks whether @em other and this represent the same command.
         *
         * @param other The command object to compare with this one
         * @return @em true if @em other is of type @em daq::rc::UserBroadcastCmd and holds the same
         *         user-defined command
         */
        bool equals(const RunControlBasicCommand& other) const override;

        UserBroadcastCmd* doClone() const override;

    private:
        static const std::string USR_BRDCST_USR_CMD_TAG;
};

/**
 * @brief Command object for the daq::rc::FSM_COMMAND::SUB_TRANSITION FSM transition.
 *
 * @see daq::rc::CommandSender::makeTransition(const std::string& recipient, const TransitionCmd& trCommand) const
 * @see daq::rc::CommandSender::makeTransition(const std::string& recipient, FSM_COMMAND cmd) const
 * @see daq::rc::Controllable::subTransition(const SubTransitionCmd& usrCmd)
 */
class SubTransitionCmd: public TransitionCmd {
    public:
        /**
         * @brief Constructor.
         *
         * @param commandDescription The string representation of the command
         *
         * @throws daq::rc::BadCommand @em commandDescription cannot be de-serialized to a proper
         *                             command object
         */
        explicit SubTransitionCmd(const std::string& commandDescription);

        /**
         * @brief Constructor.
         *
         * @param subName The name of the sub-transition
         * @param mainTransitionCmd The main transition command
         */
        SubTransitionCmd(const std::string& subName, FSM_COMMAND mainTransitionCmd);

        /**
         * @brief It returns the name of the sub-transition.
         *
         * @return The name of the sub-transition
         */
        std::string subTransition() const;

        /**
         * @brief It sets the name of the sub-transition.
         *
         * @param subTr The name of the sub-transition
         */
        void subTransition(const std::string& subTr);

        /**
         * @brief It returns the name of the main transition.
         *
         * @return The name of the main transition
         */
        std::string mainTransitionCmd() const;

        /**
         * @brief It sets the main transition.
         *
         * @param mainTransitionCmd The main transition
         */
        void mainTransitionCmd(FSM_COMMAND mainTransitionCmd);

        using TransitionCmd::subTransitions;

        std::vector<std::string> subTransitions() const override;

        /**
         * @brief It returns all the sub-transitions for all the available FSM transitions.
         *
         * @remark Used internally by segment's controllers to keep trace of the available sub-transitions.
         *
         * @return All the sub-transitions for all the available FSM transitions
         */
        std::map<FSM_COMMAND, std::vector<std::string>> allSubTransitions();

        /**
         * @brief It sets all the sub-transitions for all the available FSM transitions.
         *
         * @param subTrMap All the sub-transitions for all the available FSM transitions.
         */
        void allSubTransitions(const std::map<FSM_COMMAND, std::vector<std::string>>& subTrMap);

        std::unique_ptr<SubTransitionCmd> clone() const;

        std::string toString() const override;

        /**
         * @brief It returns, in order, the FSM command and the name of the sub-transition.
         *
         * @return In order, the FSM command and the name of the sub-transition
         */
        std::vector<std::string> arguments() const override;

    protected:
        /**
         * @brief It checks whether @em other and this represent the same command.
         *
         * @param other The command object to compare with this one
         * @return @em true if @em other is of type @em daq::rc::SubTransitionCmd and holds the same
         *         sub-transition for the same main transition
         */
        bool equals(const RunControlBasicCommand& other) const override;

        SubTransitionCmd* doClone() const override;

    private:
        static const std::string SUB_TRANSITION_NAME_TAG;
        static const std::string SUB_TRANSITION_MAIN_TR_TAG;
        static const std::string SUB_TRANSITION_ALL_SUBTR_TAG;
};

}}

#endif /* DAQ_RC_RUNCONTROLCOMMANDS_H_ */
