/*
 * FSMCommands.h
 *
 *  Created on: Aug 8, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/FSM/FSMCommands.h
 */

#ifndef DAQ_RC_FSMCOMMANDS_H_
#define DAQ_RC_FSMCOMMANDS_H_

#include <string>
#include <utility>

#include <boost/bimap.hpp>

namespace daq { namespace rc {

/**
 * @brief Enumeration for all the available commands that can be sent to the FSM to perform transitions.
 *
 * @see daq::rc::FSMSCommands::commandToString and daq::rc::FSMCommands::stringToCommand to covert enumeration
 *      items to string and viceversa.
 *
 * @remark String representations of all the FSM commands are provided by the daq::rc::FSMCommands class. Do
 *         no use free strings because error prone and the resulting code will depend on any change in
 *         the string representation of the FSM commands.
 *
 * @note As a general policy, every command that has to be propagated from a controller to its children is
 *       implemented as an FSM transition.
 *
 * @see daq::rc::TransitionCmd The command "structure" to be sent to Run Control applications on order to perform
 *                             FSM transitions.
 */
enum class FSM_COMMAND : unsigned int {
    // Please, keep the right order of commands between INIT_FSM and STOP_FSM
    INIT_FSM = 0,    //!< Command used internally to initialize the FSM
    INITIALIZE,      //!< It asks the FSM to perform the INITIALIZE transition
    CONFIGURE,       //!< It asks the FSM to perform the CONFIGURE transition
    CONFIG,          //!< Multi-step command: it corresponds to send the CONFIGURE command followed by the CONNECT command
    CONNECT,         //!< It asks the FSM to perform the CONNECT transition
    START,           //!< It asks the FSM to perform the START transition
    STOP,            //!< Multi-step command: it corresponds to send commands from STOPROIB to STOPARCHIVING
    STOPROIB,        //!< It asks the FSM to perform the STOPROIB transition
    STOPDC,          //!< It asks the FSM to perform the STOPDC transition
    STOPHLT,         //!< It asks the FSM to perform the STOPHLT transition
    STOPRECORDING,   //!< It asks the FSM to perform the STOPRECORDING transition
    STOPGATHERING,   //!< It asks the FSM to perform the STOPGATHERING transition
    STOPARCHIVING,   //!< It asks the FSM to perform the STOPARCHIVING transition
    DISCONNECT,      //!< It asks the FSM to perform the DISCONNECT transition
    UNCONFIG,        //!< Multi-step command: it corresponds to send the DISCONNECT command followed by the UNCONFIGURE command
    UNCONFIGURE,     //!< It asks the FSM to perform the UNCONFIGURE transition
    SHUTDOWN,        //!< It asks the FSM to perform the SHUTDOWN transition (only available for the Root Controller)
    STOP_FSM,        //!< Command used to terminate the FSM
    USERBROADCAST,   //!< It asks the FSM to execute an user command and then broadcast it to all its children (in-state transition)
    RELOAD_DB,       //!< It asks the FSM to reload the database configuration (in-state transition, only in the daq::rc::FSM_STATE::NONE state)
    RESYNCH,         //!< It asks the FSM to execute a re-synchronization and then broadcast it to all its children (in-state transition, only in the daq::rc::FSM_STATE::RUNNING state)
    NEXT_TRANSITION, //!< It asks the FSM to perform the transition to the next FSM state
    SUB_TRANSITION   //!< It asks the FSM to perform a sub-transition (in-state transition)
};

class FSMCommands {
    private:
        typedef boost::bimap<FSM_COMMAND, std::string> bm_type;
        static const bm_type CMD_MAP;

	public:
        /**
         * @brief Constructor: deleted.
         */
        FSMCommands() = delete;

        /**
         * @brief Destructor: deleted.
         */
        ~FSMCommands() = delete;

        static const std::string INITIALIZE_CMD;        //!< String representation of the daq::rc::FSM_COMMAND::INITIALIZE command
        static const std::string CONFIGURE_CMD;         //!< String representation of the daq::rc::FSM_COMMAND::CONFIGURE command
        static const std::string CONFIG_CMD;            //!< String representation of the daq::rc::FSM_COMMAND::CONFIG command
        static const std::string CONNECT_CMD;           //!< String representation of the daq::rc::FSM_COMMAND::CONNECT command
        static const std::string START_CMD;             //!< String representation of the daq::rc::FSM_COMMAND::START command
        static const std::string STOP_CMD;              //!< String representation of the daq::rc::FSM_COMMAND::STOP command
        static const std::string STOPROIB_CMD;          //!< String representation of the daq::rc::FSM_COMMAND::STOPROIB command
        static const std::string STOPDC_CMD;            //!< String representation of the daq::rc::FSM_COMMAND::STOPDC command
        static const std::string STOPHLT_CMD;           //!< String representation of the daq::rc::FSM_COMMAND::STOPHLT command
        static const std::string STOPRECORDING_CMD;     //!< String representation of the daq::rc::FSM_COMMAND::STOPRECORDING command
        static const std::string STOPGATHERING_CMD;     //!< String representation of the daq::rc::FSM_COMMAND::STOPGATHERING command
        static const std::string STOPARCHIVING_CMD;     //!< String representation of the daq::rc::FSM_COMMAND::STOPARCHIVING command
        static const std::string DISCONNECT_CMD;        //!< String representation of the daq::rc::FSM_COMMAND::DISCONNECT command
        static const std::string UNCONFIG_CMD;          //!< String representation of the daq::rc::FSM_COMMAND::UNCONFIG command
        static const std::string UNCONFIGURE_CMD;       //!< String representation of the daq::rc::FSM_COMMAND::UNCONFIGURE command
        static const std::string SHUTDOWN_CMD;          //!< String representation of the daq::rc::FSM_COMMAND::SHUTDOWN command
        static const std::string USERBROADCAST_CMD;     //!< String representation of the daq::rc::FSM_COMMAND::USERBROADCAST command
        static const std::string RELOAD_DB_CMD;         //!< String representation of the daq::rc::FSM_COMMAND::RELOAD_DB command
        static const std::string RESYNCH_CMD;           //!< String representation of the daq::rc::FSM_COMMAND::RESYNCH command
        static const std::string NEXT_TRANSITION_CMD;   //!< String representation of the daq::rc::FSM_COMMAND::NEXT_TRANSITION command
        static const std::string SUB_TRANSITION_CMD;    //!< String representation of the daq::rc::FSM_COMMAND::SUB_TRANSITION command
        static const std::string INIT_FSM_CMD;          //!< String representation of the daq::rc::FSM_COMMAND::INIT_FSM command
        static const std::string STOP_FSM_CMD;          //!< String representation of the daq::rc::FSM_COMMAND::STOP_FSM command
        static const std::string EOR_CMD;               //!< End of Run: alias for the daq::rc::FSM_COMMAND::STOPRECORDING command
        static const std::string SOR_CMD;               //!< Start of Run: alias for the daq::rc::FSM_COMMAND::START command

        /**
         * @brief Function providing conversion from a daq::rc::FSM_COMMAND item to its string representation.
         *
         * The returned string is equal to one of the static strings defined in this class. For instance,
         * the following relationship will always evaluate to <em>true</em>:
         * @code{.cpp} FSMCommands::commandToString(FSM_COMMAND::START) == FSMCommands::START_CMD @endcode
         *
         * @param cmd The FSM command
         * @return A string representation of the FSM command
         */
        static std::string commandToString(FSM_COMMAND cmd);

        /**
         * @brief Function providing conversion from a string to the corresponding daq::rc::FSM_COMMAND item.
         *
         * Relationships like the following one will always evaluate to <em>true</em>:
         * @code{.cpp} FSMCommands::stringToCommand(FSMCommands::START_CMD) == FSM_COMMAND::START @endcode
         *
         * @param cmd The string representation of an FSM command
         * @return The enumeration item corresponding to @em cmd
         *
         * @throws daq::rc::BadCommand The @em cmd string does not correspond to a valid FSM command
         */
        static FSM_COMMAND stringToCommand(const std::string& cmd);

		/**
		 * @brief Iterator over all the available FSM commands (as listed in the FSM_COMMAND enumeration).
		 */
        typedef bm_type::left_map::const_iterator command_iterator;

        /**
         * @brief Function providing iterators over all the available FSM commands.
         *
         * Here is a simple example on how to iterate over all the FSM commands:
           @code{.cpp}
           const auto& its = FSMCommands::commands();
           for(auto it = its.first; it != its.second; ++it) {
              // it->first gives the FSM_COMMAND item
              // it->second gives the corresponding string representation
           }
           @endcode
         *
         * @return A pair defining the start and the end iterator.
         */
        static std::pair<command_iterator, command_iterator> commands();
};

}}

#endif /* DAQ_RC_FSMCOMMANDS_H_ */
