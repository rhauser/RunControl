/*
 * FSMActive.h
 *
 *  Created on: Aug 15, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/FSM/Details/FSMActive.h
 */

#ifndef DAQ_RC_FSM_ACTIVE_H_
#define DAQ_RC_FSM_ACTIVE_H_

#if !defined(DAQ_RC_FSM_INITIAL_H_)
#error Do not #include this file directly; that is FSM internal.
#endif

#include "RunControl/FSM/FSMStates.h"
#include "RunControl/FSM/Details/FSMBase.h"
#include "RunControl/FSM/Details/FSMInternals.h"

#include <boost/msm/front/state_machine_def.hpp>
#include <boost/msm/front/functor_row.hpp>
#include <boost/msm/front/euml/operator.hpp>
#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/back/tools.hpp>

#include <boost/mpl/vector.hpp>

#include <string>
#include <exception>


namespace daq { namespace rc {

namespace front = boost::msm::front;
namespace euml = boost::msm::front::euml;
namespace back = boost::msm::back;
namespace mpl = boost::mpl;

/**
 * @brief Class representing the sub-FSM contained in the daq::rc::FSM_Initial FSM.
 *
 * @note This FSM is seen by daq::rc::FSM_Initial as a state.
 *
 * @image html FSM_UML.jpg "The FSM: states and main transitions are shown"
 * @image latex FSM_UML.eps "The FSM: states and main transitions are shown" width=7cm
 */
// Since this FSM will be seen by FSM_Initial as a state, it derives from FSMBaseState and not from Polymorphic_Visitable_State
// It is still kept the condition that all the FSM and states must have the same base state
class FSM_Active : public front::state_machine_def<FSM_Active, FSMBaseState>, public FSMBase {
public:
    /**
     * @brief Copy constructor: deleted
     */
    FSM_Active(const FSM_Active&) = delete;

    /**
     * @brief Move constructor: deleted
     */
    FSM_Active(FSM_Active&&) = delete;

    /**
     * @brief Copy assignment operator: deleted
     */
    FSM_Active& operator=(const FSM_Active&) = delete;

    /**
     * @brief Move assignment operator: deleted
     */
    FSM_Active& operator=(FSM_Active&&) = delete;

	// This is the back-end: let's give it full access to this class
	friend class back::state_machine<FSM_Active>;

	// The initial state
	typedef mpl::vector<FSMState<FSM_STATE::CONFIGURED>, READY_ACTIVE> initial_state;

	/**
	 * @brief It returns the name of this FSM as a state
	 *
	 * This FSM is seen by daq::rc::FSM_Initial as a state.
	 *
	 * @return The string corresponding to daq::rc::FSM_STATE::CONFIGURED (this FSM's initial state)
	 */
	std::string name() const override {
		return FSMStates::stateToString(FSM_STATE::CONFIGURED);
	}

	/**
	 * @brief The daq::rc::FSM_STATE item corresponding to this FSM when seen as a state
	 *
	 * This FSM is seen by daq::rc::FSM_Initial as a state.
	 *
	 * @return Always daq::rc::FSM_STATE::CONFIGURED
	 */
	FSM_STATE state() const override {
		return FSM_STATE::CONFIGURED;
	}

	// Transition table
	struct transition_table : mpl::vector<
	//            Start                            Event                                    Next                                Action                   Guard
	//           +--------------------------------+----------------------------------------+-----------------------------------+-----------------------+----------------------+
	front::Row<  FSMState<FSM_STATE::CONFIGURED>  , FSMEvent<FSM_COMMAND::CONNECT>         , FSMState<FSM_STATE::CONNECTED>    , DO_CONNECT            , euml::Not_<HAS_SUBTRANSITIONS>>,
    front::Row<  FSMState<FSM_STATE::CONFIGURED>  , FSMEvent<FSM_COMMAND::CONNECT>         , front::none                       , DO_CONNECT_SUB        , HAS_SUBTRANSITIONS>,
	front::Row<  FSMState<FSM_STATE::CONFIGURED>  , FSMEvent<FSM_COMMAND::UNCONFIGURE>     , PSEUDO_EXIT                       , front::none           , euml::Not_<HAS_SUBTRANSITIONS>>,
    front::Row<  FSMState<FSM_STATE::CONFIGURED>  , FSMEvent<FSM_COMMAND::UNCONFIGURE>     , front::none                       , DO_UNCONFIGURE_SUB    , HAS_SUBTRANSITIONS>,
	front::Row<  FSMState<FSM_STATE::CONFIGURED>  , FSMEvent<FSM_COMMAND::NEXT_TRANSITION> , front::none                       , DO_UNCONFIGURE_SUB    , euml::Not_<GOING_UP>>,
    front::Row<  FSMState<FSM_STATE::CONFIGURED>  , FSMEvent<FSM_COMMAND::NEXT_TRANSITION> , front::none                       , DO_CONNECT_SUB        , GOING_UP>,
	//           +--------------------------------+----------------------------------------+-----------------------------------+-----------------------+----------------------+
	front::Row<  FSMState<FSM_STATE::CONNECTED>   , FSMEvent<FSM_COMMAND::START>           , FSMState<FSM_STATE::RUNNING>      , DO_START              , euml::Not_<HAS_SUBTRANSITIONS>>,
    front::Row<  FSMState<FSM_STATE::CONNECTED>   , FSMEvent<FSM_COMMAND::START>           , front::none                       , DO_START_SUB          , HAS_SUBTRANSITIONS>,
	front::Row<  FSMState<FSM_STATE::CONNECTED>   , FSMEvent<FSM_COMMAND::DISCONNECT>      , FSMState<FSM_STATE::CONFIGURED>   , DO_DISCONNECT         , euml::Not_<HAS_SUBTRANSITIONS>>,
    front::Row<  FSMState<FSM_STATE::CONNECTED>   , FSMEvent<FSM_COMMAND::DISCONNECT>      , front::none                       , DO_DISCONNECT_SUB     , HAS_SUBTRANSITIONS>,
    front::Row<  FSMState<FSM_STATE::CONNECTED>   , FSMEvent<FSM_COMMAND::UNCONFIG>        , front::none					   , DO_UNCONFIG           , front::none>,
    front::Row<  FSMState<FSM_STATE::CONNECTED>   , FSMEvent<FSM_COMMAND::NEXT_TRANSITION> , front::none                       , DO_DISCONNECT_SUB     , euml::Not_<GOING_UP>>,
    front::Row<  FSMState<FSM_STATE::CONNECTED>   , FSMEvent<FSM_COMMAND::NEXT_TRANSITION> , front::none                       , DO_START_SUB          , GOING_UP>,
	//           +--------------------------------+----------------------------------------+-----------------------------------+-----------------------+----------------------+
	front::Row<  FSMState<FSM_STATE::RUNNING>     , FSMEvent<FSM_COMMAND::STOPROIB>        , FSMState<FSM_STATE::ROIBSTOPPED>  , DO_STOPROIB           , euml::Not_<HAS_SUBTRANSITIONS>>,
    front::Row<  FSMState<FSM_STATE::RUNNING>     , FSMEvent<FSM_COMMAND::STOPROIB>        , front::none                       , DO_STOPROIB_SUB       , HAS_SUBTRANSITIONS>,
    front::Row<  FSMState<FSM_STATE::RUNNING>     , FSMEvent<FSM_COMMAND::NEXT_TRANSITION> , front::none                       , DO_STOPROIB_SUB       , front::none>,
	front::Row<  FSMState<FSM_STATE::RUNNING>     , FSMEvent<FSM_COMMAND::RESYNCH>         , front::none            		   , DO_RESYNCH            , front::none>,
	front::Row<  FSMState<FSM_STATE::RUNNING>     , FSMEvent<FSM_COMMAND::STOP>            , front::none            		   , DO_STOP               , front::none>,
	//           +--------------------------------+----------------------------------------+-----------------------------------+-----------------------+----------------------+
	front::Row<  FSMState<FSM_STATE::ROIBSTOPPED> , FSMEvent<FSM_COMMAND::STOPDC>          , FSMState<FSM_STATE::DCSTOPPED>    , DO_STOPDC             , euml::Not_<HAS_SUBTRANSITIONS>>,
    front::Row<  FSMState<FSM_STATE::ROIBSTOPPED> , FSMEvent<FSM_COMMAND::STOPDC>          , front::none                       , DO_STOPDC_SUB         , HAS_SUBTRANSITIONS>,
    front::Row<  FSMState<FSM_STATE::ROIBSTOPPED> , FSMEvent<FSM_COMMAND::NEXT_TRANSITION> , front::none                       , DO_STOPDC_SUB         , front::none>,
    //           +--------------------------------+----------------------------------------+-----------------------------------+-----------------------+----------------------+
	front::Row<  FSMState<FSM_STATE::DCSTOPPED>   , FSMEvent<FSM_COMMAND::STOPHLT>         , FSMState<FSM_STATE::HLTSTOPPED>   , DO_STOPHLT            , euml::Not_<HAS_SUBTRANSITIONS>>,
    front::Row<  FSMState<FSM_STATE::DCSTOPPED>   , FSMEvent<FSM_COMMAND::STOPHLT>         , front::none                       , DO_STOPHLT_SUB        , HAS_SUBTRANSITIONS>,
    front::Row<  FSMState<FSM_STATE::DCSTOPPED>   , FSMEvent<FSM_COMMAND::NEXT_TRANSITION> , front::none                       , DO_STOPHLT_SUB        , front::none>,
    //           +--------------------------------+----------------------------------------+-----------------------------------+-----------------------+----------------------+
	front::Row<  FSMState<FSM_STATE::HLTSTOPPED>  , FSMEvent<FSM_COMMAND::STOPRECORDING>   , FSMState<FSM_STATE::SFOSTOPPED>   , DO_STOPRECORDING      , euml::Not_<HAS_SUBTRANSITIONS>>,
    front::Row<  FSMState<FSM_STATE::HLTSTOPPED>  , FSMEvent<FSM_COMMAND::STOPRECORDING>   , front::none                       , DO_STOPRECORDING_SUB  , HAS_SUBTRANSITIONS>,
	front::Row<  FSMState<FSM_STATE::HLTSTOPPED>  , FSMEvent<FSM_COMMAND::NEXT_TRANSITION> , front::none                       , DO_STOPRECORDING_SUB  , front::none>,
	//           +--------------------------------+----------------------------------------+-----------------------------------+-----------------------+----------------------+
	front::Row<  FSMState<FSM_STATE::SFOSTOPPED>  , FSMEvent<FSM_COMMAND::STOPGATHERING>   , FSMState<FSM_STATE::GTHSTOPPED>   , DO_STOPGATHERING      , euml::Not_<HAS_SUBTRANSITIONS>>,
    front::Row<  FSMState<FSM_STATE::SFOSTOPPED>  , FSMEvent<FSM_COMMAND::STOPGATHERING>   , front::none                       , DO_STOPGATHERING_SUB  , HAS_SUBTRANSITIONS>,
    front::Row<  FSMState<FSM_STATE::SFOSTOPPED>  , FSMEvent<FSM_COMMAND::NEXT_TRANSITION> , front::none                       , DO_STOPGATHERING_SUB  , front::none>,
    //           +--------------------------------+----------------------------------------+-----------------------------------+-----------------------+----------------------+
	front::Row<  FSMState<FSM_STATE::GTHSTOPPED>  , FSMEvent<FSM_COMMAND::STOPARCHIVING>   , FSMState<FSM_STATE::CONNECTED>    , DO_STOPARCHIVING      , euml::Not_<HAS_SUBTRANSITIONS>>,
    front::Row<  FSMState<FSM_STATE::GTHSTOPPED>  , FSMEvent<FSM_COMMAND::STOPARCHIVING>   , front::none                       , DO_STOPARCHIVING_SUB  , HAS_SUBTRANSITIONS>,
    front::Row<  FSMState<FSM_STATE::GTHSTOPPED>  , FSMEvent<FSM_COMMAND::NEXT_TRANSITION> , front::none                       , DO_STOPARCHIVING_SUB  , front::none>,
	//           +--------------------------------+----------------------------------------+-----------------------------------+-----------------------+----------------------+
    front::Row<  READY_ACTIVE                     , FSMEvent<FSM_COMMAND::SUB_TRANSITION>  , front::none                       , DO_SUBTRANSITION      , front::none>
    //           +--------------------------------+----------------------------------------+-----------------------------------+-----------------------+----------------------+
	> {};

	// The "goingUp" flag is different for the "mother" and "child" FSM
	// When we exit from this FSM, we set the "goingUp" flag to true, so
	// that the next transition (invoked by the NEXT TRANSITION event) can
	// move towards the proper state (i.e., towards RUNNING)
	template<class EVT, class FSM>
	void on_exit(const EVT&, FSM&) {
		goingUp(true);
	}

private:
	/**
	 * @brief Constructor
	 */
	FSM_Active() : front::state_machine_def<FSM_Active, FSMBaseState>(), FSMBase() {
	}

    template <typename FSM, typename EVENT>
    void no_transition(const EVENT&, FSM&, int) {
        // Empty to avoid double notifications: the "no_transition" is called anyway in FSM_Initial
    }

    template <typename FSM, typename EVENT>
    void exception_caught(const EVENT& evt, FSM& fsm, std::exception& ex) {
        fsm.notifyTransitionException(*(evt.transitionCommand()), ex);
    }
};

// Typedef for the back-end
typedef back::state_machine<FSM_Active> FSM_ACTIVE;

}}

#endif /* DAQ_RC_FSM_ACTIVE_H_ */
