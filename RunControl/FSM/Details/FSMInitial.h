/*
 * FSMInitial.h
 *
 *  Created on: Aug 15, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/FSM/Details/FSMInitial.h
 */

#ifndef DAQ_RC_FSM_INITIAL_H_
#define DAQ_RC_FSM_INITIAL_H_

// This is to have an FSM with more than 20 transitions (the default max)
#define BOOST_MPL_CFG_NO_PREPROCESSED_HEADERS
#define BOOST_MPL_LIMIT_VECTOR_SIZE 40
#define BOOST_MPL_LIMIT_MAP_SIZE 40

// This is to have more than 10 states (the default max)
#define FUSION_MAX_VECTOR_SIZE 12

#include "RunControl/FSM/FSMStates.h"
#include "RunControl/FSM/Details/FSMBase.h"
#include "RunControl/FSM/Details/FSMInternals.h"
#include "RunControl/FSM/Details/FSMActive.h"

#include <boost/msm/front/state_machine_def.hpp>
#include <boost/msm/front/functor_row.hpp>
#include <boost/msm/front/euml/operator.hpp>
#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/back/queue_container_circular.hpp>
#include <boost/msm/back/tools.hpp>

#include <boost/mpl/vector.hpp>
#include <boost/mpl/if.hpp>
#include <boost/mpl/insert_range.hpp>
#include <boost/mpl/begin_end.hpp>

#include <string>
#include <set>
#include <exception>


namespace daq { namespace rc {

namespace front = boost::msm::front;
namespace euml = boost::msm::front::euml;
namespace back = boost::msm::back;
namespace mpl = boost::mpl;

/**
 * @brief Class representing the main FSM containing the initial state and an orthogonal region
 *
 * The daq::rc::FSM_Active FSM is a sub-FSM of this FSM.
 *
 * @tparam InitialState The FSM initial state (it can only be daq::rc::FSM_STATE::NONE or daq::rc::FSM_STATE::INITIAL)
 *
 * @image html FSM_UML.jpg "The FSM: states and main transitions are shown"
 * @image latex FSM_UML.eps "The FSM: states and main transitions are shown" width=7cm
 */
// Note that this derives from Polymorphic_Visitable_State in order to activate polymorphism
// Indeed it is mandatory for the state machines and the states to have the same base class
template<FSM_STATE InitialState>
class FSM_Initial : public front::state_machine_def<FSM_Initial<InitialState>, Polymorphic_Visitable_State>, public FSMBase {
public:
    /**
     * @brief Copy constructor: deleted
     */
    FSM_Initial(const FSM_Initial&) = delete;

    /**
     * @brief Move constructor: deleted
     */
    FSM_Initial(FSM_Initial&&) = delete;

    /**
     * @brief Copy assignment operator: deleted
     */
    FSM_Initial& operator=(const FSM_Initial&) = delete;

    /**
     * @brief Move assignment operator: deleted
     */
    FSM_Initial& operator=(FSM_Initial&&) = delete;

	// This is the back-end
    // NOTE: the queue is customized in order to have a deferred event queue able to keep only
    // one event (in this case the RELOAD_DB). The size of the queues is set by default to zero
    // and can be modified only at run-time when the fsm is created. This is done in the FSM
    // constructor. Remember to modify that call whenever a change is made here.
	typedef back::state_machine<FSM_Initial<InitialState>, back::queue_container_circular> FSM;

	// We do in such a way that the back-end has full access to this class
    friend class back::state_machine<FSM_Initial<InitialState>, back::queue_container_circular>;

	// The initial state
	typedef mpl::vector<FSMState<InitialState>, READY_INITIAL> initial_state;

	// Activate deferred events
	typedef int activate_deferred_events;

	/**
	 * @brief It returns the name of the initial state
	 *
	 * @return The name of the initial state
	 */
	std::string initialStateName() const {
		return FSMStates::stateToString(InitialState);
	}

	/**
	 * @brief It returns the daq::rc::FSM_STATE item representing the initial state
	 *
	 * @return The daq::rc::FSM_STATE item representing the initial state
	 */
	FSM_STATE initialState() const {
		return InitialState;
	}

	/**
	 * @brief It returns all the daq::rc::FSM_COMMAND items that correspond to a valid
	 *        event for this FSM
	 *
	 * @return All the daq::rc::FSM_COMMAND items that correspond to a valid event for this FSM
	 *
	 * @see daq::rc::FSMEvent
	 */
	const std::set<FSM_COMMAND>& allowedCommands() const {
	    return m_all_commands;
	}


    /**
     * @brief It returns all the daq::rc::FSM_STATE items that correspond to a valid state of this FSM
     *
     * @return All the daq::rc::FSM_STATE items that correspond to a valid state of this FSM
     *
     * @see daq::rc::FSMState
     */
	const std::set<FSM_STATE>& allowedStates() const {
	    return m_all_states;
	}

	// Transitions in common to all the possible initial states
	//
	// USERBROADCAST shall be available for all the states in the whole FSM, that is why it is added to READY_INITIAL
	// From INITIAL, NEXT_TRANSITION needs the guard because it may go to NONE (for the RootController only) or to FSM_ACTIVE
	// Look at the pseudo-exit point: it allows to properly exit from the CONFIGURED state of FSM_ACTIVE and enter INITIAL
	typedef mpl::vector<
	//           Start                             Event                                    Next                           Action                   Guard
	//           +--------------------------------+----------------------------------------+------------------------------+------------------------+----------------------+
	front::Row<  FSMState<FSM_STATE::INITIAL>     , FSMEvent<FSM_COMMAND::CONFIGURE>       , FSM_ACTIVE                   , DO_CONFIGURE           , euml::Not_<HAS_SUBTRANSITIONS>>,
    front::Row<  FSMState<FSM_STATE::INITIAL>     , FSMEvent<FSM_COMMAND::CONFIGURE>       , front::none                  , DO_CONFIGURE_SUB       , HAS_SUBTRANSITIONS>,
	front::Row<  FSMState<FSM_STATE::INITIAL>     , FSMEvent<FSM_COMMAND::NEXT_TRANSITION> , front::none                  , DO_CONFIGURE_SUB       , GOING_UP>,
	front::Row<  FSMState<FSM_STATE::INITIAL>     , FSMEvent<FSM_COMMAND::CONFIG>          , front::none                  , DO_CONFIG              , front::none>,
    front::Row<  FSMState<FSM_STATE::INITIAL>     , FSMEvent<FSM_COMMAND::SUB_TRANSITION>  , front::none                  , DO_SUBTRANSITION       , front::none>,
	//           +--------------------------------+----------------------------------------+------------------------------+------------------------+----------------------+
	front::Row<  FSM_ACTIVE::exit_pt<PSEUDO_EXIT> , FSMEvent<FSM_COMMAND::UNCONFIGURE>     , FSMState<FSM_STATE::INITIAL> , DO_UNCONFIGURE         , front::none>,
	//           +--------------------------------+----------------------------------------+------------------------------+------------------------+----------------------+
    front::Row<  READY_INITIAL                    , FSMEvent<FSM_COMMAND::USERBROADCAST>   , front::none                  , DO_USERBROADCAST       , front::none>
    //           +--------------------------------+----------------------------------------+------------------------------+------------------------+----------------------+
	> common_transitions;

	// Transitions available only if the initial state is NONE (i.e., the RootController)
	//
	// From NONE, NEXT_TRANSITION does not need a guard because from NONE we can only go to INITIAL
	// SHUTDOWN is made available in NONE itself because it my be needed to interrupt the on-going transition (for instance towards INITIAL) and
	// in this case the FSM is still in the NONE state
	// From INITIAL, NEXT_TRANSITION needs the guard (see comment above)
	// FSM_ACTIVE is seen as a state, that's why SHUTDOWN can directly link FSM_ACTIVE and NONE
	// RELOAD_DB is allowed only in NONE and it is deferred in all the other states (see above comment about the queue used for the back-end)
	typedef mpl::vector<
	//           Start                             Event                                    Next                                Action                   Guard
	//           +--------------------------------+----------------------------------------+-------------------------------+------------------------+----------------------+
	front::Row<  FSMState<FSM_STATE::NONE>        , FSMEvent<FSM_COMMAND::INITIALIZE>      , FSMState<FSM_STATE::INITIAL>  , DO_INITIALIZE          , front::none>,
	front::Row<  FSMState<FSM_STATE::NONE>        , FSMEvent<FSM_COMMAND::NEXT_TRANSITION> , FSMState<FSM_STATE::INITIAL>  , DO_INITIALIZE          , front::none>,
    front::Row<  FSMState<FSM_STATE::NONE>        , FSMEvent<FSM_COMMAND::SHUTDOWN>        , front::none                   , DO_SHUTDOWN            , front::none>,
	//           +--------------------------------+----------------------------------------+-------------------------------+------------------------+----------------------+
	front::Row<  FSMState<FSM_STATE::INITIAL>     , FSMEvent<FSM_COMMAND::SHUTDOWN>        , FSMState<FSM_STATE::NONE>     , DO_SHUTDOWN            , front::none>,
	front::Row<  FSMState<FSM_STATE::INITIAL>     , FSMEvent<FSM_COMMAND::NEXT_TRANSITION> , FSMState<FSM_STATE::NONE>     , DO_SHUTDOWN            , euml::Not_<GOING_UP>>,
	//           +--------------------------------+----------------------------------------+-------------------------------+------------------------+----------------------+
	front::Row<  FSM_ACTIVE                       , FSMEvent<FSM_COMMAND::SHUTDOWN>        , FSMState<FSM_STATE::NONE>     , DO_SHUTDOWN            , front::none>,
	//           +--------------------------------+----------------------------------------+-------------------------------+------------------------+----------------------+
    front::Row<  FSMState<FSM_STATE::NONE>        , FSMEvent<FSM_COMMAND::RELOAD_DB>       , front::none                   , DO_RELOAD_DB           , front::none>,
    //           +--------------------------------+----------------------------------------+-------------------------------+------------------------+----------------------+
    front::Row<  FSMState<FSM_STATE::INITIAL>     , FSMEvent<FSM_COMMAND::RELOAD_DB>       , front::none                   , front::Defer           , front::none>,
    //           +--------------------------------+----------------------------------------+-------------------------------+------------------------+----------------------+
    front::Row<  FSM_ACTIVE                       , FSMEvent<FSM_COMMAND::RELOAD_DB>       , front::none                   , front::Defer           , front::none>
    //           +--------------------------------+----------------------------------------+-------------------------------+------------------------+----------------------+
	> none_transitions;


	// If the initial state is NONE, the full transition table includes the transitions from and to the NONE state and the common transitions
	typedef mpl::insert_range<common_transitions,
							  mpl::end<common_transitions>::type,
							  none_transitions
	>::type none_transition_table;


	// If the initial state is INITIAL, the full transition table does not include the transitions from and to the NONE state
	typedef common_transitions initial_transition_table;

	// Here select the right transition table depending on the initial state:
	// NONE -> none_transition_table
	// INITIAL -> initial_transition_table
	// The advantage of this approach is to have in the transition table only the accessible states and transitions given
	// the FSM initial state (otherwise the FSM will have all the states and transitions regardless the initial state, and
	// not allowed or not accessible states should be protected with guards); this leads also to a smaller executable.
	typedef typename mpl::if_c<(InitialState == FSM_STATE::NONE),
								none_transition_table,
								initial_transition_table
	>::type transition_table;

private:
	/**
	 * @brief Constructor
	 */
	FSM_Initial() : front::state_machine_def<FSM_Initial<InitialState>, Polymorphic_Visitable_State>(), FSMBase() {
	    typedef typename back::recursive_get_transition_table<FSM>::type recursive_stt;

	    // Note that the following two calls do not need to be re-done in FSM_ACTIVE

	    // Here we fill the set with all events defined for the FSM
	    typedef typename back::generate_event_set<recursive_stt>::type all_events;
	    boost::mpl::for_each<all_events, boost::msm::wrap<boost::mpl::placeholders::_1>>(FSMEventCollector(m_all_commands));

	    // Here we fill the set with all the states defined in the FSM
	    typedef typename back::generate_state_set<recursive_stt>::type all_states;
	    boost::mpl::for_each<all_states>(FSMStateCollector(m_all_states));
	}

	template <typename FSM, typename EVENT>
	void no_transition(const EVENT& evt, FSM& fsm, int stateID) {
	    // This avoids double-notifications
	    // Notify only if the state is not READY_INITIAL: that is done because the FSM sends a notification
	    // for every orthogonal region (and every sub-machine - indeed this call does nothing in FSM_ACTIVE)
	    if(dynamic_cast<READY_INITIAL*>(fsm.get_state_by_id(stateID)) == nullptr) {
	        fsm.notifyNoTransition(*(evt.transitionCommand()));
	    }
	}

	template <typename FSM, typename EVENT>
    void exception_caught(const EVENT& evt, FSM& fsm, std::exception& ex) {
	    fsm.notifyTransitionException(*(evt.transitionCommand()), ex);
    }

	std::set<FSM_COMMAND> m_all_commands;
	std::set<FSM_STATE> m_all_states;

	// Only very well defined initial states are supported
	static_assert((InitialState == FSM_STATE::NONE) || (InitialState == FSM_STATE::INITIAL), "Bad FSM initial state");
};

}}

#endif /* DAQ_RC_FSM_INITIAL_H_ */
