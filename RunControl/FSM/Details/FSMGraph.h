/*
 * FSMVisitor.h
 *
 *  Created on: Feb 20, 2014
 *      Author: avolio
 */

/**
 * @file RunControl/FSM/Details/FSMGraph.h
 */

#ifndef DAQ_RC_FSMGRAPH_H_
#define DAQ_RC_FSMGRAPH_H_

#include "RunControl/FSM/FSMActions.h"
#include "RunControl/FSM/FSM.h"
#include "RunControl/FSM/FSMStates.h"
#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/RunControlTransitionActions.h"
#include "RunControl/Common/UserRoutines.h"
#include "RunControl/Common/Exceptions.h"

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>

#include <memory>
#include <functional>
#include <tuple>

namespace daq { namespace rc {

/**
 * @brief Class providing an algorithm to calculate the commands/events to be sent to the FSM in order
 *        to make a transition between two different (even not adjacent) states
 *
 * The algorithms builds a graph out of the FSM (where states are nodes and transitions are edges) and calculates
 * the minimum path between two nodes using the Dijkstra algorithm.
 *
 * @tparam FSMInitialState The FSM initial state
 */
template<FSM_STATE FSMInitialState>
class FSMGraph {
    public:
        /**
         * @brief Constructor
         */
        FSMGraph() :
                m_fsm(new FSM<FSMInitialState>(std::make_shared<RunControlTransitionActions>(),
                                               std::make_shared<UserRoutines>()))
        {
            // Register the call-back that will be executed any time a transition is done
            // In the call-back the graph is built step-by-step (source and destination states
            // are vertexes, the transition is the edge)
            m_fsm->registerOnTransitionDoneCallback(std::bind(&FSMGraph::visit, this, std::placeholders::_1));

            // Do a full loop of the FSM, so that all the transitions are executed,
            // the call-back is called and the graph is built
            do {
                m_fsm->makeTransition(std::make_shared<TransitionCmd>(FSM_COMMAND::NEXT_TRANSITION));
            } while(std::get<0>(m_fsm->status()) != FSMInitialState);
        }

        /**
         * @brief It calculates the list commands to be sent to the FSM in order to move from @em src to @em dst
         *
         * @param src The FSM source state
         * @param dst The FSM destination state
         *
         * @return The list of commands to be sent to the FSM in order to move from @em src to @em dst. The list
         *         is empty if the two states are the same or if @em dst cannot be reached
         *
         * @throws daq::rc::FSMPathError Two states (between @em src and @em dst) cannot be linked by any transition
         */
        std::vector<FSM_COMMAND> from(FSM_STATE src, FSM_STATE dst) const {
            ERS_DEBUG(2, "Looking for transitions to go from \"" << FSMStates::stateToString(src) << " to \"" << FSMStates::stateToString(dst) << "\"");

            std::vector<FSM_COMMAND> v;

            // Do nothing if source and destination states are the same
            if(src != dst) {
                const unsigned int sState = static_cast<unsigned int>(src);
                const unsigned int dState = static_cast<unsigned int>(dst);

                // The dijkstra algorithm computes the distance of every node from the source node; it returns
                // the distance between the nodes and each node predecessor (each predecessor is part of the path)
                // Details about the algorithm can be font at http://www.boost.org/doc/libs/1_44_0/libs/graph/doc/dijkstra_shortest_paths.html

                // Number of vertices and vectors keeping trace of node predecessors and distances
                const unsigned int nv = boost::num_vertices(m_fsm_graph);
                std::vector<vertex_descriptor> pred(nv);
                std::vector<int> dist(nv);

                // Define the source vertex and get the weights for each edge in the graph
                vertex_descriptor sourceVertex = boost::vertex(sState, m_fsm_graph);
                boost::property_map<graph_t, boost::edge_weight_t>::type wm = boost::get(boost::edge_weight, m_fsm_graph);

                // Apply the minimum path algorithm
                boost::dijkstra_shortest_paths(m_fsm_graph,
                                               sourceVertex,
                                               weight_map(wm).predecessor_map(&pred[0]).distance_map(&dist[0]));

                // Create a map where at each node corresponds a pair containing its predecessor in the path and the
                // distance between the nodes. Consider that the result of the algorithm is something like that
                // (to go, for instance, from NONE to RUNNING):
                // distance(RUNNING) = 4, parent(RUNNING) = CONNECTED
                // distance(CONNECTED) = 3, parent(CONNECTED) = CONFIGURED
                // distance(CONFIGURED) = 2, parent(CONFIGURED) = INITIAL
                // distance(INITIAL) = 1, parent(INITIAL) = BOOTED
                // distance(NONE) = 0, parent(NONE) = NONE
                std::map<vertex_descriptor, std::pair<vertex_descriptor, int>> pathMap;
                boost::graph_traits<graph_t>::vertex_iterator vi, vend;
                for(boost::tie(vi, vend) = boost::vertices(m_fsm_graph); vi != vend; ++vi) {
                    vertex_descriptor vd = *vi;
                    vertex_descriptor v_parent = pred[*vi];
                    int v_distance = dist[*vi];
                    pathMap[vd] = std::make_pair(v_parent, v_distance);
                }

                // Here we find the path: look in the map for the end state and go back to the source state
                // navigating through the node parents.
                boost::property_map<graph_t, boost::edge_name_t>::type edgeNameMap = boost::get(boost::edge_name, m_fsm_graph);
                boost::property_map<graph_t, boost::vertex_name_t>::type vertexNameMap = boost::get(boost::vertex_name, m_fsm_graph);
                vertex_descriptor destinationVertex = boost::vertex(dState, m_fsm_graph);
                do {
                    // Find parent and distance from the source node: note that if the node
                    // cannot be reached, then the distance is the maximum value of an integer
                    const std::pair<vertex_descriptor, int>& p = pathMap[destinationVertex];
                    int distance = p.second;
                    const vertex_descriptor& tmpVertex = p.first;
                    if(distance != std::numeric_limits<int>::max()) {
                        // The node is reachable: find the edge linking the two nodes
                        // Pay attention to the vertex order!!! We are going back in the path
                        // that means the right edge to look for is tmpVertex -> destinationVertex.
                        const std::pair<edge_descriptor, bool>& edge = boost::edge(tmpVertex, destinationVertex, m_fsm_graph);
                        if(edge.second == true) {
                            // Edge found, get the transition name: remember that the graph
                            // is built in such a way that the edge name is the name of the transition
                            // Note also that we are adding the transitions in the inverse order!!!!
                            const std::string& trName = boost::get(edgeNameMap, edge.first);
                            v.push_back(FSMCommands::stringToCommand(trName));

                            ERS_DEBUG(2, "Going  from \"" << FSMStates::stateToString(src) << " to \"" <<
                                         FSMStates::stateToString(dst) << "\": found transition \"" <<
                                         trName << "\" (" << boost::get(vertexNameMap, tmpVertex) << " -> " <<
                                         boost::get(vertexNameMap, destinationVertex) << ")");

                            // In the next loop the tmpVertex will be the new destination vertex
                            // (i.e., RUNNING -> CONNECTED, CONNECTED -> CONFIGURED, ...)
                            destinationVertex = tmpVertex;
                        } else {
                            // There is no edge defined for two nodes; this should really never happen
                            const std::string& srcTmpName = boost::get(vertexNameMap, tmpVertex);
                            const std::string& dstTmpName = boost::get(vertexNameMap, destinationVertex);
                            const std::string& reason = std::string("no transition is defined between two states in the path: ") + srcTmpName
                                                        + std::string(" and ") + dstTmpName
                                                        + std::string(". This is an application bug!");

                            throw daq::rc::FSMPathError(ERS_HERE,
                                                        FSMStates::stateToString(src),
                                                        FSMStates::stateToString(dst),
                                                        reason);
                        }
                    } else {
                        // The node cannot be reached, exit the loop and clear the vector
                        // Note that we can be here only if the destination state is not reachable.
                        // If the distance between the two states is not infinite, then a path
                        // between them must exist.

                        ers::log(daq::rc::FSMPathError(ERS_HERE,
                                                       FSMStates::stateToString(src),
                                                       FSMStates::stateToString(dst),
                                                       std::string("the two states cannot be reached by any transition")));

                        v.clear();

                        break;
                    }
                } while(destinationVertex != sourceVertex);

                // Revert the vector to have the transitions in the proper order
                std::reverse(v.begin(), v.end());
            }

            // If the vector is empty then it means the src and dst cannot be linked
            // or are the same state
            return v;
        }

    protected:
        /**
         * @brief Method used to "visit" all the FSM states and built the FSM graph
         *
         * @param cmd The current transition
         */
        void visit(const TransitionCmd& cmd) {
            // This is called every time a transition is done

            ERS_DEBUG(3, "Adding transition from " << cmd.fsmSourceState() << " to " <<
                          cmd.fsmDestinationState() << " via " << cmd.fsmCommand() << " to graph");

            const std::string& srcStateName = cmd.fsmSourceState();
            const std::string& dstStateName = cmd.fsmDestinationState();

            // Get the vertex for the source state
            const vertex_descriptor& srcStateVertex = boost::vertex(static_cast<unsigned int>(FSMStates::stringToState(srcStateName)), m_fsm_graph);

            // Add the edge: the transition going from the source to the destination state
            // The name of the edge is the name of the transition command
            boost::add_edge(srcStateVertex,
                            boost::vertex(static_cast<unsigned int>(FSMStates::stringToState(dstStateName)), m_fsm_graph),
                            EdgeName(cmd.fsmCommand(), EdgeWeight(1)),
                            m_fsm_graph);

            // Add the name of the source "vertex"
            boost::property_map<graph_t, boost::vertex_name_t>::type vertexNameMap = boost::get(boost::vertex_name, m_fsm_graph);
            boost::put(vertexNameMap, srcStateVertex, srcStateName);
        }

    private:
        std::unique_ptr<FSMActions> m_fsm;

        // The FSM is described as a graph: this helps, for instance, finding transitions between states (using a minimum path algorithm)
        // Each state represents a node in the graph, and each transition is a graph edge
        // To each graph vertex a 'name' property is associated representing the name of the state
        // At each edge two properties are attached: the name (representing the name of the transition) and
        // the weight (representing the weigh of a transition - all the transitions have the same weight at the moment).
        typedef boost::property<boost::edge_weight_t, int> EdgeWeight;
        typedef boost::property<boost::edge_name_t, std::string, EdgeWeight> EdgeName;
        typedef boost::property<boost::vertex_name_t, std::string> VertexName;
        typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, VertexName, EdgeName> graph_t;
        typedef boost::graph_traits<graph_t>::vertex_descriptor vertex_descriptor;
        typedef boost::graph_traits<graph_t>::edge_descriptor edge_descriptor;

        mutable graph_t m_fsm_graph;
};

}}

#endif /* DAQ_RC_FSMGRAPH_H_ */
