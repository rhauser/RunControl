/*
 * FSMInternals.h
 *
 *  Created on: Aug 14, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/FSM/Details/FSMInternals.h
 */

#ifndef DAQ_RC_FSMINTERNALS_H_
#define DAQ_RC_FSMINTERNALS_H_

#if !defined(DAQ_RC_FSM_INITIAL_H_) && !defined(DAQ_RC_FSM_ACTIVE_H_)
#error Do not #include this file directly; that is FSM internal.
#endif

#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/FSM/FSMStates.h"
#include "RunControl/FSM/Details/FSMBase.h"
#include "RunControl/FSM/Details/Macro.h"
#include "RunControl/Common/RunControlTransitionActions.h"
#include "RunControl/Common/UserRoutines.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/RunControlCommands.h"

#include <ers/Assertion.h>

#include <boost/msm/back/state_machine.hpp>
#include <boost/msm/front/states.hpp>
#include <boost/mpl/bool.hpp>

#include <iostream>
#include <string>
#include <functional>
#include <type_traits>
#include <memory>

namespace daq { namespace rc {

namespace {

    // Here is the link between an FSM command and its corresponding user routine
    std::map<std::string, std::function<void (UserRoutines&, const TransitionCmd&)>> urCall{{FSMCommands::INIT_FSM_CMD,      &UserRoutines::initAction},
                                                                                            {FSMCommands::CONFIGURE_CMD,     &UserRoutines::configureAction},
                                                                                            {FSMCommands::CONNECT_CMD,       &UserRoutines::connectAction},
                                                                                            {FSMCommands::DISCONNECT_CMD,    &UserRoutines::disconnectAction},
                                                                                            {FSMCommands::START_CMD,         &UserRoutines::prepareAction},
                                                                                            {FSMCommands::STOPROIB_CMD,      &UserRoutines::stopROIBAction},
                                                                                            {FSMCommands::STOPDC_CMD,        &UserRoutines::stopDCAction},
                                                                                            {FSMCommands::STOPHLT_CMD,       &UserRoutines::stopHLTAction},
                                                                                            {FSMCommands::STOPRECORDING_CMD, &UserRoutines::stopRecordingAction},
                                                                                            {FSMCommands::STOPGATHERING_CMD, &UserRoutines::stopGatheringAction},
                                                                                            {FSMCommands::STOPARCHIVING_CMD, &UserRoutines::stopArchivingAction},
                                                                                            {FSMCommands::UNCONFIGURE_CMD,   &UserRoutines::unconfigureAction}};

    /**
     * @brief It adds some information to a transition command
     *
     * When the transition command reaches the FSM, usually it contains only the FSM event that has
     * triggered the transition. Here additional transition's parameters are added
     *
     * @param trCmd The command object describing the transition to be performed
     * @param cmd The transition's command
     * @param src The transition's source state
     * @param tgt The transition's destination state
     */
    void augmentTransitionCommand(TransitionCmd& trCmd, FSM_COMMAND cmd, FSM_STATE src, FSM_STATE tgt) {
        trCmd.fsmCommand(cmd);
        trCmd.fsmSourceState(src);
        trCmd.fsmDestinationState(tgt);
    }

    /**
     * @brief It executes the proper daq::rc::UserRoutines method for a given transition
     *
     * @param mainTransition The name of the transition
     * @param trCmd The command object describing the transition being performed
     * @param ur Reference to the daq::rc::UserRoutines instance
     */
    void executeUserRoutines(const std::string& mainTransition, const TransitionCmd& trCmd, UserRoutines& ur) {
        try {
            urCall[mainTransition](ur, trCmd);
            ERS_DEBUG(1, "Executed UserRoutines for main transition " << mainTransition);
        }
        catch(std::exception& ex) {
            std::string msg = std::string(ex.what()) + ". An attempt to recover will be done";
            ers::warning(daq::rc::UserRoutineFailed(ERS_HERE, mainTransition, msg, ex));

            if(ur.clearAction() == true) {
                try {
                    urCall[mainTransition](ur, trCmd);
                }
                catch(std::exception& exx) {
                    throw daq::rc::UserRoutineFailed(ERS_HERE, mainTransition, std::string(ex.what()), exx);
                }
            } else {
                throw daq::rc::UserRoutineFailed(ERS_HERE, mainTransition, std::string(ex.what()), ex);
            }
        }
    }


    /**
     * @brief It executes the proper daq::rc::UserRoutines method for a given sub-transition
     *
     * This is useful when sub-transitions are performed and the user actions have to be executed
     * before the first sub-transition for a given main transition.
     *
     * @param trCmd The command object describing the sub-transition being performed
     * @param ur Reference to the daq::rc::UserRoutines instance
     */
    void executeUserRoutines(const SubTransitionCmd& trCmd, UserRoutines& ur) {
        executeUserRoutines(trCmd.mainTransitionCmd(), trCmd, ur);
    }

    /**
     * @brief It executes the proper daq::rc::UserRoutines method for a given transition
     *
     * @warning Do not use this method when the daq::rc::FSM_COMMAND contained in @em trCmd
     *          does not really correspond to the real command (that may happen for in-state
     *          transitions and for daq::rc::FSM_COMMAND::NEXT_TRANSITION).
     *
     * @param trCmd The command object describing the transition being performed
     * @param ur Reference to the daq::rc::UserRoutines instance
     */
    void executeUserRoutines(const TransitionCmd& trCmd, UserRoutines& ur) {
        executeUserRoutines(trCmd.fsmCommand(), trCmd, ur);
    }
}

//***************************************************************************************
//******************** Definition of (internal) events used by the FSM ******************
//***************************************************************************************
/**
 * @brief Class used to create events for the FSM (triggering transitions) starting from a
 *        daq::rc::FSM_COMMAND
 *
 * @tparam FSMCommand The daq::rc::FSM_COMMAND corresponding to this event
 */
template<FSM_COMMAND FSMCommand> class FSMEvent {
	public:
        /**
         * @brief Constructor
         *
         * A default daq::rc::TransitionCmd object is created using @em FSMCommand
         */
        FSMEvent()
            : FSMEvent(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSMCommand)))
        {
        }

        /**
         * @brief Constructor
         *
         * @param cmd The transition command object containing parameters about the transition to be performed
         * @param executeUserActions If @em true then daq::rc::UserRoutines actions will be executed
         * @param isChained If @em true then it informs the FSM that a chained transition is going to be performed
         */
		FSMEvent(const std::shared_ptr<TransitionCmd>& cmd, bool executeUserActions = true, bool isChained = false)
		    : m_tr_cmd(cmd), m_execute_ua(executeUserActions), m_is_chained(isChained)
		{
#ifndef ERS_NO_DEBUG
		    ERS_ASSERT((FSMCommands::commandToString(FSMCommand) == cmd->fsmCommand()) ||
		               (FSMCommand == FSM_COMMAND::NEXT_TRANSITION));
#endif
		}

		/**
		 * @brief "Conversion" constructor
		 *
		 * @note It allows to convert an event into another one.
		 *       This is needed by the FSM library when pseudo-exit states are used: in this case the events
		 *       to and from the pseudo-exit state must be "convertible". In the current FSM design, a pseudo-exit
		 *       state is used but the two events are actually the same; that means this constructor is not
		 *       really used.
		 *
		 * @param ev The event to be converted
		 */
		template<FSM_COMMAND cmd>
		FSMEvent(const FSMEvent<cmd>& ev)
		    : m_tr_cmd(ev.transitionCommand()), m_execute_ua(ev.executeUserActions()), m_is_chained(ev.isChained())
		{
		}

		/**
		 * @brief Destructor
		 */
		virtual ~FSMEvent() {
		}

		/**
		 * @brief It returns the daq::rc::FSM_COMMAND corresponding to this event
		 *
		 * @return The daq::rc::FSM_COMMAND corresponding to this event
		 */
		FSM_COMMAND fsmCommand() const {
			return FSMCommand;
		}

        /**
         * @brief It returns the string representation of the daq::rc::FSM_COMMAND corresponding to this event
         *
         * @return The string representation of the daq::rc::FSM_COMMAND corresponding to this event
         */
		std::string name() const {
			return FSMCommands::commandToString(FSMCommand);
		}

		/**
		 * @brief It returns the command object containing information about the transition to be executed
		 *
		 * @return The command object containing information about the transition to be executed
		 */
		const std::shared_ptr<TransitionCmd>& transitionCommand() const {
		    return m_tr_cmd;
		}

		/**
		 * @brief It returns @em true if daq::rc::UserRoutines actions will be executed
		 *
		 * @return @em true if daq::rc::UserRoutines actions will be executed
		 */
		bool executeUserActions() const {
		    return m_execute_ua;
		}

		/**
		 * @brief It returns @em true when a chained transition is going to be performed
		 *
		 * @return @em true when a chained transition is going to be performed
		 */
		bool isChained() const {
		    return m_is_chained;
		}

	private:
		// The shared pointer here is needed because the event may be deferred by the FSM
		// and the TransitionCmd must stay valid
		const std::shared_ptr<TransitionCmd> m_tr_cmd;
		const bool m_execute_ua;
		const bool m_is_chained;
};

/**
 * @brief Template specialization for an event corresponding to the daq::rc::FSM_COMMAND::INIT_FSM
 *        command
 *
 * Given the peculiarity of the FSM initial event, an event of this king does not need the additional
 * information needed for events referring to standard transitions
 */
template<>
class FSMEvent<FSM_COMMAND::INIT_FSM> {
    public:
        FSMEvent(bool is_restart) : m_is_restart(is_restart) {
        }

        virtual ~FSMEvent() {
        }

        FSM_COMMAND fsmCommand() const {
            return FSM_COMMAND::INIT_FSM;
        }

        bool isRestart() const {
            return m_is_restart;
        }

        std::string name() const {
            return FSMCommands::commandToString(FSM_COMMAND::INIT_FSM);
        }

    private:
        const bool m_is_restart;
};

/**
 * @brief Class used to collect (at run-time) all the daq::rc::FSM_COMMAND commands
 *        the FSM is able to react upon
 *
 * The FSM allows to visit all the defined events; this mechanism is used
 * to retrieve the command corresponding to an FSM event.
 */
class FSMEventCollector {
    public:
        /**
         * @brief Constructor
         *
         * @param commands The set used to collect all the commands
         */
        FSMEventCollector(std::set<FSM_COMMAND>& commands)
            : m_all_commands(commands)
        {
        }

        /**
         * @brief Destructor
         */
        ~FSMEventCollector() {}

        /**
         * @brief Visitor method where commands are actually collected
         *
         * @tparam Event The type of the FSM event
         */
        template<typename Event>
        void operator()(const boost::msm::wrap<Event>&) {
            Event t;
            m_all_commands.insert(t.fsmCommand());
        }

    private:
        std::set<FSM_COMMAND>& m_all_commands;
};

//***************************************************************************************
//******************** Definition of (internal) states used by the FSM ******************
//***************************************************************************************
/**
 * @brief Class used to make all the FSM states polymorphic
 *
 * This class also makes any state visitable.
 */
class Polymorphic_Visitable_State {
    public:
        typedef boost::msm::back::args<void, FSM_STATE&> accept_sig;

        /**
         * @brief Destructor
         */
        virtual ~Polymorphic_Visitable_State() {}

        /**
         * @brief Method called every time the state is visited
         *
         * Usually only the currently active state is visited.
         * Default implementation is empty.
         *
         * @param state Reference to daq::rc::FSM_STATE variable to be
         *        filled with the current FSM state
         */
        virtual void accept(FSM_STATE& state) {
            (void) state;
        }
};

/**
 * @brief Base class for all the FSM states
 *
 * @note Pseudo-exit states and states belonging to orthogonal regions
 *       are an exception because they are used only internally.
 *
 * @see daq::rc::READY_INITIAL
 * @see daq::rc::READY_ACTIVE
 * @see daq::rc::PSEUDO_EXIT
 */
class FSMBaseState : public Polymorphic_Visitable_State {
	public:
        /**
         * @brief Destructor
         */
		virtual ~FSMBaseState() {;}

		/**
		 * @brief It shall return the name of the state
		 *
		 * @return The name of the state
		 */
		virtual std::string name() const = 0;

		/**
		 * @brief It shall return the daq::rc::FSM_STATE associated to this state
		 *
		 * @return The daq::rc::FSM_STATE associated to this state
		 */
		virtual FSM_STATE state() const = 0;

		/**
		 * @brief It shall return @em true if the state is the end-state of a chained transition
		 *
		 * @return @em true if the state is the end-state of a chained transition
		 */
		virtual bool chainEnd() const { return false; }
};

/**
 * @brief Class representing an FSM state
 */
template<FSM_STATE State>
class FSMState : public boost::msm::front::state<FSMBaseState> {
	public:
        /**
         * @brief Destructor
         */
		virtual ~FSMState() {;}

        /**
         * @brief It returns the name of the state
         *
         * @return The name of the state
         */
		std::string name() const override {
			return FSMStates::stateToString(State);
		}

        /**
         * @brief It returns the daq::rc::FSM_STATE associated to this state
         *
         * @return The daq::rc::FSM_STATE associated to this state
         */
		FSM_STATE state() const override {
			return State;
		}

        /**
         * @brief It returns @em true if the state is the end-state of a chained transition
         *
         * Currently this is true only for daq::rc::FSM_STATE::INITIAL (end state of the
         * daq::rc::FSM_COMMAND::UNCONFIG chained transition) and daq::rc::FSM_STATE::CONNECTED
         * (end state of the daq::rc::FSM_COMMAND::CONFIG and daq::rc::FSM_COMMAND::STOP chained transitions)
         *
         * @return @em true if the state is the end-state of a chained transition
         */
		bool chainEnd() const override {
		    return boost::mpl::bool_<((State == FSM_STATE::INITIAL) || (State == FSM_STATE::CONNECTED))>::value;
		}

		/**
		 * @brief Called any time this state is entered
		 *
		 * @tparam EVT Always of type daq::rc::FSMEvent
		 * @tparam FSM Aleays of type daq::rc::FSMBase
		 * @param evt The transition's event
		 * @param fsm The FSM
		 */
		template<class EVT, class FSM>
		void on_entry(const EVT& evt, FSM& fsm) {
			fsm.notifyTransitionDone(*(evt.transitionCommand()));
		}

		/**
		 * @brief Template partial specialization for events of type daq::rc::FSMEvent<FSM_COMMAND::INIT_FSM>
		 */
		template<class FSM>
		void on_entry(const FSMEvent<FSM_COMMAND::INIT_FSM>& evt, FSM& fsm) {
		    // The INIT_FSM event is not generated by an "external" transition command
		    // but it is the way the FSM is initialized (i.e., it enters the initial state),
		    // that is why the TransitionCmd is artificially created

		    TransitionCmd trCmd(FSM_COMMAND::INIT_FSM);
		    augmentTransitionCommand(trCmd, FSM_COMMAND::INIT_FSM, FSM_STATE::ABSENT, State);
		    trCmd.isRestart(evt.isRestart());

		    fsm.notifyTransitioning(trCmd);

		    executeUserRoutines(trCmd, fsm.userRoutines());
		    fsm.controller().enterFSM(trCmd);

		    fsm.notifyTransitionDone(trCmd);
		}

		/**
		 * @brief It assigns @em st the daq::rc::FSM_STATE item associated to this state
		 */
        void accept(FSM_STATE& st) override {
            st = state();
        }
};

/**
 * @brief Class representing the only state of the orthogonal region of the
 *        daq::rc::FSM_Initial FSM
 *
 * Used only internally: it has no entry/exit actions and is not visited
 */
class READY_INITIAL : public boost::msm::front::state<Polymorphic_Visitable_State> {
};

/**
 * @brief Class representing the only state of the orthogonal region of the
 *        daq::rc::FSM_Active FSM
 *
 * Used only internally: it has no entry/exit actions and is not visited
 */
class READY_ACTIVE : public boost::msm::front::state<Polymorphic_Visitable_State> {
};

/**
 * @brief Class representing the pseudo-exit state used to exit from the daq::rc::FSM_Active
 *        sub-machine.
 *
 * Used only internally: it has no entry/exit actions and is not visited
 */
class PSEUDO_EXIT : public boost::msm::front::exit_pseudo_state<FSMEvent<FSM_COMMAND::UNCONFIGURE>, Polymorphic_Visitable_State> {
};

/**
 * @brief Utility class used to collect, at run-time, all the available states of an FSM
 */
class FSMStateCollector {
    public:
        /**
         * @brief Constructor
         *
         * @param states The set to fill with the available states
         */
        FSMStateCollector(std::set<FSM_STATE>& states) : m_all_states(states) {
        }

        /**
         * @brief It adds the current state to the list
         * @param s The current state
         */
        void operator()(const FSMBaseState& s) {
            m_all_states.insert(s.state());
        }

        /**
         * @brief Empty operator
         *
         * Using SFINAE this will be enabled only if the state is not represented by a derived class of
         * daq::rc::FSMBaseState. This is to not take into account the states belonging to orthogonal
         * regions and the pseudo-exit states (all of them used only internally)
         */
        template<typename State, typename Check = typename std::enable_if<!std::is_base_of<FSMBaseState, State>::value>::type>
        void operator()(const State&) {
        }

    private:
        std::set<FSM_STATE>& m_all_states;
};

//**********************************************************************
//******************** Transition actions for the FSM ******************
//**********************************************************************

// The TRANSITION_ACTION cannot be used because there are no user routines executed
struct DO_INITIALIZE {
	template <typename EVT, typename FSM, typename SourceState, typename TargetState>
	void operator()(const EVT& evt, FSM& fsm, SourceState& src, TargetState& tgt) {
        TransitionCmd& trCmd = *(evt.transitionCommand());
        augmentTransitionCommand(trCmd, FSM_COMMAND::INITIALIZE, src.state(), tgt.state());

        fsm.notifyTransitioning(trCmd);

		fsm.controller().initialize(trCmd);

        fsm.goingUp(true);
	}
};

TRANSITION_ACTION(CONFIGURE, 1, configure)
TRANSITION_ACTION(CONNECT, 1, connect)
TRANSITION_ACTION(START, 1, prepareForRun)
TRANSITION_ACTION(STOPROIB, 0, stopROIB)
TRANSITION_ACTION(STOPDC, 0, stopDC)
TRANSITION_ACTION(STOPHLT, 0, stopHLT)
TRANSITION_ACTION(STOPRECORDING, 0, stopRecording)
TRANSITION_ACTION(STOPGATHERING, 0, stopGathering)
TRANSITION_ACTION(STOPARCHIVING, 0, stopArchiving)
TRANSITION_ACTION(DISCONNECT, 0, disconnect)

// TRANSITION_ACTION cannot be used because of the special treatment for "goingUp"
struct DO_UNCONFIGURE {
	template <typename EVT, typename FSM, typename SourceState, typename TargetState>
	void operator()(const EVT& evt, FSM& fsm, SourceState& src, TargetState& tgt) {
        TransitionCmd& trCmd = *(evt.transitionCommand());
        augmentTransitionCommand(trCmd, FSM_COMMAND::UNCONFIGURE, src.state(), tgt.state());

        fsm.notifyTransitioning(trCmd);

        if((evt.executeUserActions() == true) && (trCmd.subTransitionsDone() == false)) {
            executeUserRoutines(trCmd, fsm.userRoutines());
        }

		fsm.controller().unconfigure(trCmd);

        // The UNCONFIGURE transition causes to enter the initial state for
		// the FSMs whose initial state is INITIAL. In this case we set the
		// "goingUp" to true so that the next transition (using the NEXT_TRANSITION
		// event) will move the FSM in the right direction (i.e., away from the
		// initial state)
        if(tgt.state() == fsm.initialState()) {
            fsm.goingUp(true);
        } else {
            fsm.goingUp(false);
        }
	}
};

// TRANSITION_ACTION cannot be used because of the special treatment for "goingUp"
// and the lack of user routines to be called
struct DO_SHUTDOWN {
	template <typename EVT, typename FSM, typename SourceState, typename TargetState>
	void operator()(const EVT& evt, FSM& fsm, SourceState&, TargetState& tgt) {
	    // The SHUTDOWN transition may be linked to FSM_ACTIVE, then visit the real current state
	    FSM_STATE currState = FSM_STATE::ABSENT;
	    fsm.visit_current_states(boost::ref(currState));

        TransitionCmd& trCmd = *(evt.transitionCommand());
        augmentTransitionCommand(trCmd, FSM_COMMAND::SHUTDOWN, currState, tgt.state());

        fsm.notifyTransitioning(trCmd);

        fsm.controller().shutdown(trCmd);

        // The SHUTDOWN command is allowed in the NONE state as well
        // In this case the FSM state is not changed and the transition
        // done notification has to be called manually
        if(currState == tgt.state()) {
            fsm.notifyTransitionDone(trCmd);
        }

        fsm.goingUp(true);
	}
};

// Cannot use INSTATE_ACTION because DO_SUBTRANSITION is linked to the READY_ACTIVE state
// and uses a specific TransitionCmd (i.e., SubTransitionCmd)
struct DO_SUBTRANSITION {
    template <typename EVT, typename FSM, typename SourceState, typename TargetState>
    void operator()(const EVT& evt, FSM& fsm, SourceState&, TargetState&) {
        // The SUB_TRANSITION is linked to the READY state, in order to make
        // the state machine simpler. But we do not want to expose the READY state
        // to the user. Then visit the current states in order to get the "real" FSM state
        // to pass to the user.
        FSM_STATE currState = FSM_STATE::ABSENT;
        fsm.visit_current_states(boost::ref(currState));

        // Always add the needed information to the original command
        // This helps avoiding problems when the FSM calls call-back function in special cases
        // like exceptions thrown during a transition
        augmentTransitionCommand(*(evt.transitionCommand()), FSM_COMMAND::SUB_TRANSITION, currState, currState);

        // Here we need to create the right command (from the generic transition to the proper one)
        SubTransitionCmd trCmd(evt.transitionCommand()->serialize());
        augmentTransitionCommand(trCmd, FSM_COMMAND::SUB_TRANSITION, currState, currState);

        fsm.notifyTransitioning(trCmd);

        const auto& allSubTrans = trCmd.allSubTransitions();
        const auto& it = allSubTrans.find(FSMCommands::stringToCommand(trCmd.mainTransitionCmd()));

        // If a sub-transition is the result of the execution of any of the _SUB FSM actions, then evt.executeUserActions()
        // will always return false.

        if((evt.executeUserActions() == true) && (it != allSubTrans.end()) && (it->second.empty() == false) && (trCmd.subTransition() == it->second[0])) {
            executeUserRoutines(trCmd, fsm.userRoutines());
        }

        fsm.controller().subTransition(trCmd);

        // Since this is a self-transition, no state is entered or exited, then
        // the notification about the transition been performed has to be done
        // here and not in the "on_enter" state method
        fsm.notifyTransitionDone(trCmd);
    }
};

// Cannot use INSTATE_ACTION because DO_USERBROADCAST is linked to the READY_INITIAL state
// and uses a specific TransitionCmd (i.e., UserBroadcastCmd)
struct DO_USERBROADCAST {
	template <typename EVT, typename FSM, typename SourceState, typename TargetState>
	void operator()(const EVT& evt, FSM& fsm, SourceState&, TargetState&) {
	    // The USERBROADCAST is linked to the READY state, in order to make
	    // the state machine simpler. But we do not want to expose the READY state
	    // to the user. Then visit the current states in order to get the "real" FSM state
	    // to pass to the user.
	    FSM_STATE currState = FSM_STATE::ABSENT;
	    fsm.visit_current_states(boost::ref(currState));

	    // Always add the needed information to the original command
	    // This helps avoiding problems when the FSM calls call-back function in special cases
	    // like exceptions thrown during a transition
	    augmentTransitionCommand(*(evt.transitionCommand()), FSM_COMMAND::USERBROADCAST, currState, currState);

	    // Here we need to create the right command (from the generic transition to the proper one)
	    UserBroadcastCmd trCmd(evt.transitionCommand()->serialize());
        augmentTransitionCommand(trCmd, FSM_COMMAND::USERBROADCAST, currState, currState);

        // Now set the FSM state for the user command
        const std::unique_ptr<UserCmd>& uc = trCmd.userCommand();
        uc->currentFSMState(currState);
        trCmd.userCommand(*uc);

        fsm.notifyTransitioning(trCmd);

		fsm.controller().userBroadcast(trCmd);

		// Since this is a self-transition, no state is entered or exited, then
		// the notification about the transition been performed has to be done
		// here and not in the "on_enter" state method
		fsm.notifyTransitionDone(trCmd);
	}
};

// Cannot use INSTATE_ACTION because DO_RESYNCH uses a specific TransitionCmd (i.e., ResynchCmd)
struct DO_RESYNCH {
    template <typename EVT, typename FSM, typename SourceState, typename TargetState>
    void operator()(const EVT& evt, FSM& fsm, SourceState& src, TargetState& tgt) {
        // Always add the needed information to the original command
        // This helps avoiding problems when the FSM calls call-back function in special cases
        // like exceptions thrown during a transition
        augmentTransitionCommand(*(evt.transitionCommand()), FSM_COMMAND::RESYNCH, src.state(), tgt.state());

        // Here we need to create the right command (from the generic transition to the proper one)
        ResynchCmd resCmd(evt.transitionCommand()->serialize());
        augmentTransitionCommand(resCmd, FSM_COMMAND::RESYNCH, src.state(), tgt.state());

        fsm.notifyTransitioning(resCmd);

        fsm.controller().resynch(resCmd);

        // Since this is a self-transition, no state is entered or exited, then
        // the notification about the transition been performed has to be done
        // here and not in the "on_enter" state method
        fsm.notifyTransitionDone(resCmd);
    }
};

INSTATE_ACTION(RELOAD_DB, reloadDB)

//**********************************************************************
//************* SUBTRANSITIONS AND CHAINED TRANSITIONS******************
//**********************************************************************

SUBTRANSITION_ACTION(CONFIGURE, INITIAL, CONFIGURED)
SUBTRANSITION_ACTION(CONNECT, CONFIGURED, CONNECTED)
SUBTRANSITION_ACTION(START, CONNECTED, RUNNING)
SUBTRANSITION_ACTION(DISCONNECT, CONNECTED, CONFIGURED)
SUBTRANSITION_ACTION(UNCONFIGURE, CONFIGURED, INITIAL)
SUBTRANSITION_ACTION(STOPROIB, RUNNING, ROIBSTOPPED)
SUBTRANSITION_ACTION(STOPDC, ROIBSTOPPED, DCSTOPPED)
SUBTRANSITION_ACTION(STOPHLT, DCSTOPPED, HLTSTOPPED)
SUBTRANSITION_ACTION(STOPRECORDING, HLTSTOPPED, SFOSTOPPED)
SUBTRANSITION_ACTION(STOPGATHERING, SFOSTOPPED, GTHSTOPPED)
SUBTRANSITION_ACTION(STOPARCHIVING, GTHSTOPPED, CONNECTED)

CHAINED_TRANSITION_ACTION(DO_STOP, STOPROIB)
CHAINED_TRANSITION_ACTION(DO_CONFIG, CONFIGURE)
CHAINED_TRANSITION_ACTION(DO_UNCONFIG, DISCONNECT)

//**********************************************************************
//******************** Guards actions for the FSM **********************
//**********************************************************************

struct GOING_UP {
	template <typename EVT, typename FSM, typename SourceState, typename TargetState>
	bool operator()(const EVT&, FSM& fsm, SourceState&, TargetState&) {
		return fsm.goingUp();
	}
};

struct HAS_SUBTRANSITIONS {
    template <typename EVT, typename FSM, typename SourceState, typename TargetState>
    bool operator()(const EVT& evt, FSM&, SourceState&, TargetState&) {
        const auto& trCmd = evt.transitionCommand();
        return (trCmd->subTransitions().empty() == false);
    }
};

}}

#endif /* DAQ_RC_FSMINTERNALS_H_ */
