/*
 * Macro.h
 *
 *  Created on: May 14, 2013
 *      Author: avolio
 */

/**
 * @file RunControl/FSM/Details/Macro.h
 */

#ifndef DAQ_RC_MACRO_H_
#define DAQ_RC_MACRO_H_

#if !defined(DAQ_RC_FSMINTERNALS_H_)
#error Do not #include this file directly; that is FSM internal.
#endif

#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/control/if.hpp>
#include <boost/preprocessor/cat.hpp>

namespace daq { namespace rc {

#define CHAINED_TRANSITION_ACTION(name, NEXT_FSM_EVENT) \
struct name { \
    template <typename EVT, typename FSM, typename SourceState, typename TargetState> \
    void operator()(const EVT& evt, FSM& fsm, SourceState&, TargetState&) { \
        const std::shared_ptr<TransitionCmd>& trCmd = evt.transitionCommand(); \
        /* This is important because it allows the FSM guard to check if sub-transitions are present */ \
        trCmd->fsmCommand(FSM_COMMAND::NEXT_FSM_EVENT); \
        /* Flag the event as a chained one */ \
        \
        ERS_DEBUG(1, "Sending event to the FSM with transition command:\n" << trCmd->serialize()); \
        ERS_DEBUG(0, "Scheduling event \"" << FSMCommands::commandToString(FSM_COMMAND::NEXT_FSM_EVENT)); \
        \
        fsm.process_event(FSMEvent<FSM_COMMAND::NEXT_FSM_EVENT>(trCmd, true, true)); \
    } \
}; \

#define SUBTRANSITION_ACTION(FSM_EVENT, FSM_SOURCE, FSM_DESTINATION) \
struct BOOST_PP_CAT(DO_, BOOST_PP_CAT(FSM_EVENT, _SUB)) { \
    template <typename EVT, typename FSM, typename SourceState, typename TargetState> \
    void operator()(const EVT& evt, FSM& fsm, SourceState&, TargetState&) { \
        const std::shared_ptr<TransitionCmd>& trCmd = evt.transitionCommand(); \
        const bool subsDoneBefore = trCmd->subTransitionsDone(); \
        trCmd->subTransitionsDone(false); \
        augmentTransitionCommand(*trCmd, FSM_COMMAND::FSM_EVENT, FSM_STATE::FSM_SOURCE, FSM_STATE::FSM_DESTINATION); \
        \
        /* First execute the UserRoutines for FSM_EVENT */ \
        if((evt.executeUserActions() == true) && (subsDoneBefore == false)) { \
            executeUserRoutines(*trCmd, fsm.userRoutines()); \
        } \
        /* Send events to the FSM for each sub-transition to be executed */ \
        const auto& subTrans = trCmd->subTransitions(); \
        for(const auto& tr : subTrans) { \
            /* Generate the proper events for a sub-transition */ \
            /* Remember that the sub-transition is an in-state transition */ \
            /* A copy of the TransitionCmd is done so that each sub-transition has its own*/ \
            std::shared_ptr<SubTransitionCmd> stc(new SubTransitionCmd(tr, FSM_COMMAND::FSM_EVENT)); \
            stc->isRestart(trCmd->isRestart()); \
            \
            ERS_DEBUG(1, "Sending event to the FSM with transition command:\n" << stc->serialize()); \
            ERS_DEBUG(0, "Scheduling sub-transition \"" << tr << "\" for the main transition \"" << trCmd->fsmCommand() << "\""); \
            \
            \
            fsm.process_event(FSMEvent<FSM_COMMAND::SUB_TRANSITION>(stc, false, false)); \
            \
            /* Remember that here "process_event" just queues the event to the FSM processing queue */ \
        } \
        \
        /* Remove the sub-transitions for this main transition from the list */ \
        /* This allows the guard in the FSM to execute the "normal" transition */ \
        trCmd->subTransitions({{FSM_COMMAND::FSM_EVENT, std::vector<std::string>()}}); \
        trCmd->subTransitionsDone(subTrans.empty() == false); \
        \
        /* For the next "main" transition the UserRoutines should not be executed*/ \
        ERS_DEBUG(1, "Sending event to the FSM with transition command:\n" << trCmd->serialize()); \
        ERS_DEBUG(0, "Scheduling event " << FSMCommands::commandToString(FSM_COMMAND::FSM_EVENT)); \
        fsm.process_event(FSMEvent<FSM_COMMAND::FSM_EVENT>(trCmd, false, evt.isChained())); \
    } \
}; \

#define TRANSITION_ACTION(FSM_EVENT, UP_TRANSITION, controllableAction) \
struct BOOST_PP_CAT(DO_, FSM_EVENT) { \
    template <typename EVT, typename FSM, typename SourceState, typename TargetState> \
    void operator()(const EVT& evt, FSM& fsm, SourceState& src, TargetState& tgt) { \
        TransitionCmd& trCmd = *(evt.transitionCommand()); \
        augmentTransitionCommand(trCmd, FSM_COMMAND::FSM_EVENT, src.state(), tgt.state()); \
        \
        fsm.notifyTransitioning(trCmd); \
        \
        /* Do not execute the user routines if sub-transitions have been executed before */ \
        /* Indeed user routines are executed before the first sub-transition for a given */ \
        /* transition is performed                                                       */ \
        if((evt.executeUserActions() == true) && (trCmd.subTransitionsDone() == false)) { \
            executeUserRoutines(trCmd, fsm.userRoutines()); \
        } \
        \
        ERS_DEBUG(1, "Calling the controllable with transition command:\n" << trCmd.serialize()); \
        \
        fsm.controller().controllableAction(trCmd); \
        \
        fsm.goingUp(BOOST_PP_IF(UP_TRANSITION, true, false));\
        \
        if((evt.isChained() == true) && (tgt.chainEnd() == false)) { \
            std::shared_ptr<TransitionCmd> newTrCmd(new TransitionCmd(trCmd)); \
            newTrCmd->subTransitionsDone(false); \
            \
            ERS_DEBUG(1, "Sending event with transition command:\n" << newTrCmd->serialize()); \
            ERS_DEBUG(0, "Scheduling event \"NEXT_TRANSITION\""); \
            \
            fsm.process_event(FSMEvent<FSM_COMMAND::NEXT_TRANSITION>(newTrCmd, true, true)); \
        } \
    } \
}; \

#define INSTATE_ACTION(FSM_EVENT, controllableAction) \
struct BOOST_PP_CAT(DO_, FSM_EVENT) { \
    template <typename EVT, typename FSM, typename SourceState, typename TargetState> \
    void operator()(const EVT& evt, FSM& fsm, SourceState& src, TargetState& tgt) { \
        TransitionCmd& trCmd = *(evt.transitionCommand()); \
        augmentTransitionCommand(trCmd, FSM_COMMAND::FSM_EVENT, src.state(), tgt.state()); \
        \
        fsm.notifyTransitioning(trCmd); \
        \
        ERS_DEBUG(1, "Calling the controllable with transition command:\n" << trCmd.serialize()); \
        \
        fsm.controller().controllableAction(trCmd); \
        \
        /* Since this is a self-transition, no state is entered or exited, then */ \
        /* the notification about the transition been performed has to be done */ \
        /* here and not in the "on_enter" state method */ \
        fsm.notifyTransitionDone(trCmd); \
    } \
}; \

}}


#endif /* DAQ_RC_MACRO_H_ */
