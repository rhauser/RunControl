/*
 * FSMStates.h
 *
 *  Created on: Aug 8, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/FSM/FSMStates.h
 */

#ifndef DAQ_RC_FSMSTATES_H_
#define DAQ_RC_FSMSTATES_H_

#include <string>
#include <utility>

#include <boost/bimap.hpp>

namespace daq { namespace rc {

/**
 * @brief  Enumeration for all the available FSM states.
 *
 * @see daq::rc::FSMStates::stateToString and daq::rc::FSMStates::stringToState to covert enumeration
 *      items to string and viceversa.
 *
 * @remark String representations of all the FSM states are provided by the daq::rc::FSMStates class. Do
 *         no use free strings because error prone and the resulting code will depend on any change in
 *         the string representation of the FSM states.
 */
enum class FSM_STATE : unsigned int {
    // Please, keep the right order of this enumeration
    ABSENT = 0,   //!< The ABSENT state (used as default state)
    NONE,         //!< The NONE state (available only for the Root Controller - that's its initial state)
    INITIAL,      //!< The INITIAL state (the FSM's initial state for child controllers and leaf applications)
    CONFIGURED,   //!< The CONFIGURED state
    CONNECTED,    //!< The CONNECTED state
    GTHSTOPPED,   //!< The GTHSTOPPED state
    SFOSTOPPED,   //!< The SFOSTOPPED state
    HLTSTOPPED,   //!< The HLTSTOPPED state
    DCSTOPPED,    //!< The DCSTOPPED state
    ROIBSTOPPED,  //!< The ROIBSTOPPED state
    RUNNING       //!< The RUNNING state
};

/**
 * @brief Utility class providing string representations for all the FSM states and conversion functions
 *        from daq::rc::FSM_STATE items to strings and viceversa.
 */
class FSMStates {
    private:
        typedef boost::bimap<FSM_STATE, std::string> bm_type;
        static bm_type STATE_MAP;

	public:
        /**
         * @brief Default constructor: deleted.
         */
        FSMStates() = delete;

        /**
         * @brief Destructor: deleted.
         */
        ~FSMStates() = delete;

        static const std::string ABSENT_STATE;      //!< String representation of the daq::rc::FSM_STATE::ABSENT state
        static const std::string NONE_STATE;        //!< String representation of the daq::rc::FSM_STATE::NONE state
        static const std::string INITIAL_STATE;     //!< String representation of the daq::rc::FSM_STATE::INITIAL state
        static const std::string CONFIGURED_STATE;  //!< String representation of the daq::rc::FSM_STATE::CONFIGURED state
        static const std::string CONNECTED_STATE;   //!< String representation of the daq::rc::FSM_STATE::CONNECTED state
        static const std::string RUNNING_STATE;     //!< String representation of the daq::rc::FSM_STATE::RUNNING state
        static const std::string ROIBSTOPPED_STATE; //!< String representation of the daq::rc::FSM_STATE::ROIBSTOPPED state
        static const std::string DCSTOPPED_STATE;   //!< String representation of the daq::rc::FSM_STATE::DCSTOPPED state
        static const std::string HLTSTOPPED_STATE;  //!< String representation of the daq::rc::FSM_STATE::HLTSTOPPED state
        static const std::string SFOSTOPPED_STATE;  //!< String representation of the daq::rc::FSM_STATE::SFOSTOPPED state
        static const std::string GTHSTOPPED_STATE;  //!< String representation of the daq::rc::FSM_STATE::GTHSTOPPED state

		/**
		 * @brief Function providing conversion from a daq::rc::FSM_STATE item to its string representation.
		 *
		 * The returned string is equal to one of the static strings defined in this class. For instance,
		 * the following relationship will always evaluate to <em>true</em>:
		 * @code{.cpp} FSMStates::stateToString(FSM_STATE::RUNNING) == FSMStates::RUNNING_STATE @endcode
		 *
		 * @param state The FSM state
		 * @return A string representation of the FSM state
		 */
        static std::string stateToString(FSM_STATE state);

		/**
		 * @brief Function providing conversion from a string to the corresponding daq::rc::FSM_STATE item.
		 *
		 * Relationships like the following one will always evaluate to <em>true</em>:
		 * @code{.cpp} FSMStates::stringToState(FSMStates::RUNNING_STATE) == FSM_STATE::RUNNING @endcode
		 *
		 * @param state The string representation of an FSM state
		 * @return The enumeration item corresponding to @em state
		 *
		 * @throws daq::rc::InvalidState The @em state string does not correspond to a valid FSM state
		 */
        static FSM_STATE stringToState(const std::string& state);

		/**
		 * @brief Iterator over all the available FSM states (as listed in the FSM_STATE enumeration).
		 */
        typedef bm_type::left_map::const_iterator state_iterator;

		/**
		 * @brief Function providing iterators over all the available FSM states.
		 *
		 * Here is a simple example on how to iterate over all the FSM states:
		   @code{.cpp}
		   const auto& its = FSMStates::states();
		   for(auto it = its.first; it != its.second; ++it) {
		      // it->first gives the FSM_STATE item
		      // it->second gives the corresponding string representation
		   }
		   @endcode
		 *
		 * @return A pair defining the start and the end iterator.
		 */
        static std::pair<state_iterator, state_iterator> states();
};

}}

#endif /* DAQ_RC_FSMSTATES_H_ */
