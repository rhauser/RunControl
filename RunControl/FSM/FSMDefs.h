/*
 * FSMDefs.h
 *
 *  Created on: Dec 13, 2012
 *      Author: avolio
 */

/**
 * @file RunControl/FSM/FSMDefs.h
 */

#ifndef DAQ_RC_FSMDEFS_H_
#define DAQ_RC_FSMDEFS_H_

#include <boost/signals2/signal.hpp>

namespace std {
    class exception;
}

namespace daq { namespace rc {

class TransitionCmd;
enum class FSM_STATE : unsigned int;

/**
 * @brief Definitions for the FSM
 */
class FSMDefs {
    public:
        /**
         * @brief Constructor: deleted
         */
        FSMDefs() = delete;

        /**
         * @brief Destructor: deleted
         */
        ~FSMDefs() = delete;

        typedef boost::signals2::signal<void (const TransitionCmd&)> OnTransitioning;

        /**
         * @brief Typedef for a call-back function with signature <b>void (const TransitionCmd&)</b> where
         *        the argument is the command identifying the current FSM transition
         */
        typedef OnTransitioning::slot_type OnTransitioningSlotType;

        typedef boost::signals2::signal<void (const TransitionCmd&)> OnTransitionDone;

        /**
         * @brief Typedef for a call-back function with signature <b>void (const TransitionCmd&)</b> where
         *        the argument is the command identifying the current FSM transition
         */
        typedef OnTransitionDone::slot_type OnTransitionDoneSlotType;

        typedef boost::signals2::signal<void (bool)> OnFSMBusy;

        /**
         * @brief Typedef for a call-back function with signature <b>void (bool)</b> where
         *        the argument represents the FSM's busy state
         */

        typedef OnFSMBusy::slot_type OnFSMBusyType;

        typedef boost::signals2::signal<void (const TransitionCmd&)> OnNoTransition;

        /**
         * @brief Typedef for a call-back function with signature <b>void (const TransitionCmd&)</b> where
         *        the argument is the command identifying the current FSM transition
         */
        typedef OnNoTransition::slot_type OnNoTransitionSlotType;

        typedef boost::signals2::signal<void (const TransitionCmd&, std::exception&)> OnTransitionException;

        /**
         * @brief Typedef for a call-back function with signature <b>void (const TransitionCmd&, std::exception&)</b> where
         *        the arguments are the command identifying the current FSM transition and the exception that cause the
         *        transition to be stopped
         */
        typedef OnTransitionException::slot_type OnTransitionExceptionSlotType;
};

}}

#endif /* DAQ_RC_FSMDEFS_H_ */
