/*
 * ControllerFactory.cxx
 *
 *  Created on: Oct 29, 2012
 *      Author: avolio
 */

#include "RunControl/Common/CommandReceiverFactory.h"
#include "RunControl/Common/CORBAReceiver.h"
#include "RunControl/Common/CmdLineReceiver.h"

namespace daq { namespace rc {

std::shared_ptr<CommandReceiver> CommandReceiverFactory::create(RECV_TYPE type, CommandedApplication& ca) {
    std::shared_ptr<CommandReceiver> toReturn;

    switch(type) {
	    case RECV_TYPE::CMDLN:
	    {
	        toReturn = CmdLineReceiver::create(ca);
	        break;
	    }
	    case RECV_TYPE::CORBA:
	    default:
	    {
	        toReturn = CORBAReceiver::create(ca);
	        break;
	    }
	}

    return toReturn;
}

}}
