/*
 * RunControlBasicCommand.cxx
 *
 *  Created on: Oct 3, 2012
 *      Author: avolio
 */

#include "RunControl/Common/RunControlBasicCommand.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/Clocks.h"

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <sys/types.h>
#include <unistd.h>

#include <locale>

namespace daq { namespace rc {

std::mutex RunControlBasicCommand::m_rand_mutex;
std::mt19937 RunControlBasicCommand::m_rand_gen;
std::once_flag RunControlBasicCommand::m_init_flag;

const std::string RunControlBasicCommand::TOP_HEADER = "_rc_command_";
const std::string RunControlBasicCommand::CMD_NAME_TAG = "_rc_command_name_";
const std::string RunControlBasicCommand::CMD_ID_TAG = "_rc_command_id_";
const std::string RunControlBasicCommand::VALUE_TAG = "_parameter_value_";
const std::string RunControlBasicCommand::REMOTE_REFERENCE_TAG = "_remote_reference_";
const std::string RunControlBasicCommand::NOTIFY_SUPPORTED_TAG = "_notify_supported_";

RunControlBasicCommand::RunControlBasicCommand(RC_COMMAND command) {
	parameter<std::string>(RunControlBasicCommand::CMD_NAME_TAG, RunControlCommands::commandToString(command));
	parameter<uid_t>(RunControlBasicCommand::CMD_ID_TAG, RunControlBasicCommand::generateUID());
	parameter<bool>(RunControlBasicCommand::NOTIFY_SUPPORTED_TAG, true);
}

RunControlBasicCommand::RunControlBasicCommand(const std::string& commandDescription) {
	try {
	    std::istringstream cmd(commandDescription);
	   	boost::property_tree::xml_parser::read_xml(cmd, m_ptree, boost::property_tree::xml_parser::trim_whitespace);

	    // Check that the command name is defined: if it is not, then the command is bad
	    try {
	        name();
	    }
	    catch(daq::rc::Exception& ex) {
	        throw daq::rc::BadCommand(ERS_HERE, "unknown", "command name is mandatory but it has not been defined", ex);
	    }

	    // Check that the command id is defined: if it is not, then the command is bad
	    try {
	        uid();
	    }
	    catch(daq::rc::Exception& ex) {
	        throw daq::rc::BadCommand(ERS_HERE, name(), "command identifier is mandatory but it has not been defined", ex);
	    }

	}
	catch(boost::property_tree::xml_parser_error& ex) {
	    const std::string& errMsg = std::string("command parser error (") + ex.what() + ")";
	    throw daq::rc::BadCommand(ERS_HERE, "unknown", errMsg, ex);
	}
}

RunControlBasicCommand& RunControlBasicCommand::operator=(RunControlBasicCommand other) {
    swap(other);
	return *this;
}

bool RunControlBasicCommand::operator==(const RunControlBasicCommand& other) const {
	return equals(other);
}

RunControlBasicCommand::~RunControlBasicCommand() noexcept {
}

void RunControlBasicCommand::notifySupported(bool supported) {
    parameter<bool>(RunControlBasicCommand::NOTIFY_SUPPORTED_TAG, supported);
}

bool RunControlBasicCommand::notifySupported() const {
    try {
        return parameter<bool>(RunControlBasicCommand::NOTIFY_SUPPORTED_TAG);
    }
    catch(daq::rc::Exception& ex) {
        return true;
    }
}

std::string RunControlBasicCommand::serialize() const {
	std::ostringstream os;

	try {
	    boost::property_tree::xml_parser::write_xml(os, m_ptree, boost::property_tree::xml_writer_settings<std::string>('\t', 1));
	}
	catch(boost::property_tree::xml_parser_error& ex) {
	    const std::string& errMsg = std::string("command parser error (") + ex.what() + ")";
	    throw daq::rc::BadCommand(ERS_HERE, name(), errMsg, ex);
	}

	return os.str();
}

void RunControlBasicCommand::command(const std::string& cmdIdentifier, const RunControlBasicCommand& cmd) {
    checkParameter(cmdIdentifier, false);

    m_ptree.put_child(RunControlBasicCommand::TOP_HEADER + "." + cmdIdentifier, cmd.m_ptree);
}

std::string RunControlBasicCommand::commandRepresentation(const std::string& cmdIdentifier) const {
    try {
    	std::ostringstream os;

        boost::optional<const boost::property_tree::ptree&> child = m_ptree.get_child_optional(RunControlBasicCommand::TOP_HEADER + "." + cmdIdentifier);
        if(child) {
            boost::property_tree::xml_parser::write_xml(os, child.get(), boost::property_tree::xml_writer_settings<std::string>('\t', 1));
        } else {
            throw daq::rc::CommandParameterNotFound(ERS_HERE, cmdIdentifier, name());
        }

        return os.str();
    }
    catch(boost::property_tree::xml_parser_error& ex) {
        throw daq::rc::CommandParameterInvalid(ERS_HERE, cmdIdentifier, name(), ex.what(), true, ex);
    }
}

std::string RunControlBasicCommand::name() const {
	return parameter<std::string>(RunControlBasicCommand::CMD_NAME_TAG);
}

RunControlBasicCommand::uid_t RunControlBasicCommand::uid() const {
    return parameter<uid_t>(RunControlBasicCommand::CMD_ID_TAG);
}

std::string RunControlBasicCommand::senderReference() const {
    try {
        return parameter<std::string>(RunControlBasicCommand::REMOTE_REFERENCE_TAG);
    }
    catch(daq::rc::CommandParameterNotFound& ex) {
        return "";
    }
}

void RunControlBasicCommand::senderReference(const std::string& reference) {
    parameter<std::string>(RunControlBasicCommand::REMOTE_REFERENCE_TAG, reference);
}

std::string RunControlBasicCommand::toString() const {
    return name();
}

std::vector<std::string> RunControlBasicCommand::arguments() const {
    return {};
}

std::set<RC_COMMAND> RunControlBasicCommand::notConcurrentWith() const {
    return std::set<RC_COMMAND>();
}

std::unique_ptr<RunControlBasicCommand> RunControlBasicCommand::clone() const {
    return std::unique_ptr<RunControlBasicCommand>(doClone());
}

RunControlBasicCommand* RunControlBasicCommand::doClone() const {
    return new RunControlBasicCommand(*this);
}

bool RunControlBasicCommand::equals(const RunControlBasicCommand& other) const {
    return (name() == other.name());
}

void RunControlBasicCommand::swap(RunControlBasicCommand& other) {
	::swap(*this, other);
}

void RunControlBasicCommand::checkParameter(const std::string& parName, bool isGet) const {
    std::locale loc;
    for(char c : parName) {
        if(std::isspace(c, loc) == true) {
            throw daq::rc::CommandParameterInvalid(ERS_HERE, parName, name(), "parameter names cannot contain white spaces", isGet);
        }
    }
}

RunControlBasicCommand::uid_t RunControlBasicCommand::generateUID() {
    std::call_once(RunControlBasicCommand::m_init_flag,
                   [](){ RunControlBasicCommand::m_rand_gen.seed(clock_t::now().time_since_epoch().count() + ::getpid()); });

    std::lock_guard<std::mutex> lk(RunControlBasicCommand::m_rand_mutex);
    return boost::uuids::to_string(boost::uuids::basic_random_generator<std::mt19937>(RunControlBasicCommand::m_rand_gen)());
}

}}

void swap(daq::rc::RunControlBasicCommand& lhs, daq::rc::RunControlBasicCommand& rhs) {
    lhs.m_ptree.swap(rhs.m_ptree);
}
