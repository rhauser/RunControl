/*
 * CmdLineReceiver.cxx
 *
 *  Created on: Sep 4, 2013
 *      Author: avolio
 */

#include "RunControl/Common/Algorithms.h"
#include "RunControl/Common/CmdLineReceiver.h"
#include "RunControl/Common/CommandedApplication.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/RunControlBasicCommand.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/FSM/FSMCommands.h"

#include <system/Host.h>
#include <system/User.h>
#include <ers/ers.h>
#include <ers/Issue.h>

#include <boost/algorithm/string/trim_all.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

#include <iostream>
#include <deque>
#include <unistd.h>

namespace daq { namespace rc {

namespace {
    const std::string exit_command = "quit";
}

struct CmdLineReceiverDeleter {
    void operator()(CmdLineReceiver* ctrl) const {
        delete ctrl;
    }
};

std::shared_ptr<CmdLineReceiver> CmdLineReceiver::create(CommandedApplication& ca) {
    return std::shared_ptr<CmdLineReceiver>(new CmdLineReceiver(ca), CmdLineReceiverDeleter());
}

CmdLineReceiver::CmdLineReceiver(CommandedApplication& ca)
    : CommandReceiver(), m_user_name(System::User().name_safe()), m_host_name(System::LocalHost().full_name()),
      m_sender_id("STDIN"), m_commanded_application(ca)
{
}

CmdLineReceiver::~CmdLineReceiver() noexcept {
}

void CmdLineReceiver::init() {
}

void CmdLineReceiver::start() {
    CommandedApplication::SenderContext sc(m_user_name, m_host_name, m_sender_id);
    std::string input;

    while((std::cin.eof() == false) && (input != exit_command)) {
        const std::string& status = m_commanded_application.status();
        daq::rc::ApplicationStatusCmd appStatus(status);

        std::cout << "\n[" << m_commanded_application.id() << "] Current state is " << appStatus.fsmState() <<
                     "\nNext transition command [" << FSMCommands::NEXT_TRANSITION_CMD << "]: ";
        std::getline(std::cin, input);
        std::cout << std::endl;

        boost::algorithm::trim_all(input);
        if((input.empty() == true) && (std::cin.eof() == false)) {
            input = FSMCommands::NEXT_TRANSITION_CMD;
        }

        try {
            if(input == exit_command) {
                break;
            } else {
                std::deque<std::string> tokens;
                boost::algorithm::split(tokens, input, boost::algorithm::is_space());

                bool isTransitionCmd = true;
                try {
                    // This will throw if the command is not a transition one
                    FSMCommands::stringToCommand(tokens[0]);
                }
                catch(daq::rc::BadCommand&) {
                    isTransitionCmd = false;
                }

                if(isTransitionCmd == true) {
                    std::unique_ptr<RunControlBasicCommand> cmd = Algorithms::buildCommand(RC_COMMAND::MAKE_TRANSITION, tokens);

                    // The "buildCommand" function promises to return the proper runtime type
                    TransitionCmd* trCmd = static_cast<TransitionCmd*>(cmd.get());
                    trCmd->skipSubTransitions(true);

                    m_commanded_application.makeTransition(sc, trCmd->serialize());
                } else {
                    // The command was not a transition
                    // If the command is not valid yet, then let the code throw
                    const RC_COMMAND rcCmd = RunControlCommands::stringToCommand(tokens[0]);
                    tokens.pop_front();

                    m_commanded_application.executeCommand(sc, Algorithms::buildCommand(rcCmd, tokens)->serialize());
                }
             }
        }
        catch(ers::Issue& ex) {
            ers::error(ex);
        }
    }
}

void CmdLineReceiver::shutdown() {
    // Be careful to what you call here! This may be called in a signal handler and not everything is safe!!!
    // https://www.securecoding.cert.org/confluence/pages/viewpage.action?pageId=1420

    // The "write" system call can be safely called in signal handlers
    ::write(0, exit_command.c_str(), exit_command.size() + 1);
}

void CmdLineReceiver::deactivate() {
    shutdown();
}

}}


