/*
 * AMBridge.cxx
 *
 *  Created on: Jul 12, 2013
 *      Author: avolio
 */

#include "RunControl/Common/AMBridge.h"

#include <AccessManager/xacml/impl/RunControlResource.h>
#include <AccessManager/client/RequestorInfo.h>
#include <AccessManager/client/ServerInterrogator.h>
#include <AccessManager/util/ErsIssues.h>

#include <system/User.h>
#include <ers/ers.h>

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/case_conv.hpp>

#include <thread>
#include <chrono>

namespace daq { namespace rc {

AMBridge::AMBridge(const std::string& partName)
    : m_partition_name(partName), m_process_owner(System::User().name_safe()), m_server_int(new daq::am::ServerInterrogator())
{
}

AMBridge::~AMBridge() {
}

bool AMBridge::canExecuteCommand(const std::string& user, const std::string& cmdName) {
    static const unsigned short MAX_RETRIES = 3;

    bool allow = true;

    if(m_process_owner != user) {
        const daq::am::RequestorInfo amReqInfo(user);
        const daq::am::RunControlResource amResource(boost::algorithm::to_upper_copy(cmdName), m_partition_name);

        bool done = false;
        unsigned short retry = 1;
        do {
            try {
                allow = m_server_int->isAuthorizationGranted(amResource, amReqInfo);
                done = true;
            }
            catch(daq::am::Exception& ex) {
                allow = false;

                if(retry == MAX_RETRIES) {
                    ers::warning(ex);
                } else {
                    ers::log(ex);
                }

                // Just wait for a short time before sending the next request
                std::this_thread::sleep_for(std::chrono::milliseconds(500));
            }
        } while((done == false) && (retry++ < MAX_RETRIES));
    }

    return allow;
}

}}

