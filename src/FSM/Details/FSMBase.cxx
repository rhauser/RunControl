/*
 * FSMBase.cxx
 *
 *  Created on: Sep 7, 2012
 *      Author: avolio
 */

#include "RunControl/FSM/Details/FSMBase.h"

#include <exception>

namespace daq { namespace rc {

FSMBase::FSMBase() :
    m_ctrl(), m_user(), m_going_up(true), m_transitioning_sig(), m_transition_done_sig(), m_no_transition_sig(), m_transition_ex_sig() {
}

FSMBase::~FSMBase() {
}

RunControlTransitionActions& FSMBase::controller() const {
	return (*m_ctrl.get());
}

void FSMBase::controller(const std::shared_ptr<RunControlTransitionActions>& ctrl) {
	m_ctrl = ctrl;
}

void FSMBase::userRoutines(const std::shared_ptr<UserRoutines>& userFunctions) {
	m_user = userFunctions;
}

UserRoutines& FSMBase::userRoutines() const {
	return (*m_user.get());
}

bool FSMBase::goingUp() const {
	return m_going_up;
}

void FSMBase::goingUp(bool up) {
	m_going_up = up;
}

void FSMBase::notifyTransitionDone(const TransitionCmd& trCmd) {
    m_transition_done_sig(trCmd);
}

void FSMBase::notifyTransitioning(const TransitionCmd& trCmd) {
    m_transitioning_sig(trCmd);
}

void FSMBase::registerTransitioningCallback(const FSMDefs::OnTransitioningSlotType& cb) {
    m_transitioning_sig.connect(cb);
}

void FSMBase::registerTransitionDoneCallback(const FSMDefs::OnTransitionDoneSlotType& cb) {
    m_transition_done_sig.connect(cb);
}

void FSMBase::notifyNoTransition(const TransitionCmd& trCmd) {
    m_no_transition_sig(trCmd);
}

void FSMBase::registerNoTransitionCallback(const FSMDefs::OnNoTransitionSlotType& cb) {
    m_no_transition_sig.connect(cb);
}

void FSMBase::notifyTransitionException(const TransitionCmd& trCmd, std::exception& ex) {
    m_transition_ex_sig(trCmd, ex);
}

void FSMBase::registerTransitionExceptionCallback(const FSMDefs::OnTransitionExceptionSlotType& cb) {
    m_transition_ex_sig.connect(cb);
}

}}
