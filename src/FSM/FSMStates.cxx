/*
 * FSMStates.cxx
 *
 *  Created on: Aug 24, 2012
 *      Author: avolio
 */

#include "RunControl/FSM/FSMStates.h"
#include "RunControl/Common/Exceptions.h"

#include <ers/LocalContext.h>

#include <boost/assign/list_of.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/case_conv.hpp>

#include <type_traits>

namespace daq { namespace rc {

const std::string FSMStates::ABSENT_STATE("ABSENT");
const std::string FSMStates::NONE_STATE("NONE");
const std::string FSMStates::INITIAL_STATE("INITIAL");
const std::string FSMStates::CONFIGURED_STATE("CONFIGURED");
const std::string FSMStates::CONNECTED_STATE("CONNECTED");
const std::string FSMStates::RUNNING_STATE("RUNNING");
const std::string FSMStates::ROIBSTOPPED_STATE("ROIBSTOPPED");
const std::string FSMStates::DCSTOPPED_STATE("DCSTOPPED");
const std::string FSMStates::HLTSTOPPED_STATE("HLTSTOPPED");
const std::string FSMStates::SFOSTOPPED_STATE("SFOSTOPPED");
const std::string FSMStates::GTHSTOPPED_STATE("GTHSTOPPED");

FSMStates::bm_type FSMStates::STATE_MAP = boost::assign::list_of<FSMStates::bm_type::relation>(FSM_STATE::ABSENT, FSMStates::ABSENT_STATE)
                                                                                              (FSM_STATE::NONE, FSMStates::NONE_STATE)
																							  (FSM_STATE::INITIAL, FSMStates::INITIAL_STATE)
																							  (FSM_STATE::CONFIGURED, FSMStates::CONFIGURED_STATE)
																							  (FSM_STATE::CONNECTED, FSMStates::CONNECTED_STATE)
																							  (FSM_STATE::RUNNING, FSMStates::RUNNING_STATE)
																							  (FSM_STATE::ROIBSTOPPED, FSMStates::ROIBSTOPPED_STATE)
																							  (FSM_STATE::DCSTOPPED, FSMStates::DCSTOPPED_STATE)
																							  (FSM_STATE::HLTSTOPPED, FSMStates::HLTSTOPPED_STATE)
																							  (FSM_STATE::SFOSTOPPED, FSMStates::SFOSTOPPED_STATE)
																							  (FSM_STATE::GTHSTOPPED, FSMStates::GTHSTOPPED_STATE);

std::string FSMStates::stateToString(FSM_STATE state) {
	const FSMStates::bm_type::left_map::const_iterator& it = FSMStates::STATE_MAP.left.find(state);
	return it->second;
}

FSM_STATE FSMStates::stringToState(const std::string& state) {
	const FSMStates::bm_type::right_map::const_iterator& it = FSMStates::STATE_MAP.right.find(boost::algorithm::to_upper_copy(state));
	if(it != FSMStates::STATE_MAP.right.end()) {
		return it->second;
	} else {
		throw daq::rc::InvalidState(ERS_HERE, state);
	}
}

std::pair<FSMStates::state_iterator, FSMStates::state_iterator> FSMStates::states() {
    return std::make_pair(FSMStates::STATE_MAP.left.begin(), FSMStates::STATE_MAP.left.end());
}

}}
