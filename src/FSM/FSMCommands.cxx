/*
 * FSMCommands.cxx
 *
 *  Created on: Aug 24, 2012
 *      Author: avolio
 */

#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/Common/Exceptions.h"

#include <ers/LocalContext.h>

#include <boost/assign/list_of.hpp>
#include <boost/algorithm/string.hpp>

namespace daq { namespace rc {

// Are you updating this? If yes, then take 10 seconds of your time and answer to the
// following question: am I changing the definition of EOR and SOR? If the answer is
// "yes", than update EOR_CMD and SOR_CMD
const std::string FSMCommands::INITIALIZE_CMD("INITIALIZE");
const std::string FSMCommands::CONFIGURE_CMD("CONFIGURE");
const std::string FSMCommands::CONFIG_CMD("CONFIG");
const std::string FSMCommands::CONNECT_CMD("CONNECT");
const std::string FSMCommands::START_CMD("START");
const std::string FSMCommands::STOP_CMD("STOP");
const std::string FSMCommands::STOPROIB_CMD("STOPROIB");
const std::string FSMCommands::STOPDC_CMD("STOPDC");
const std::string FSMCommands::STOPHLT_CMD("STOPHLT");
const std::string FSMCommands::STOPRECORDING_CMD("STOPRECORDING");
const std::string FSMCommands::STOPGATHERING_CMD("STOPGATHERING");
const std::string FSMCommands::STOPARCHIVING_CMD("STOPARCHIVING");
const std::string FSMCommands::DISCONNECT_CMD("DISCONNECT");
const std::string FSMCommands::UNCONFIG_CMD("UNCONFIG");
const std::string FSMCommands::UNCONFIGURE_CMD("UNCONFIGURE");
const std::string FSMCommands::SHUTDOWN_CMD("SHUTDOWN");
const std::string FSMCommands::USERBROADCAST_CMD("USERBROADCAST");
const std::string FSMCommands::RELOAD_DB_CMD("RELOAD_DB");
const std::string FSMCommands::RESYNCH_CMD("RESYNCH");
const std::string FSMCommands::NEXT_TRANSITION_CMD("NEXT_TRANSITION");
const std::string FSMCommands::SUB_TRANSITION_CMD("SUB_TRANSITION");
const std::string FSMCommands::INIT_FSM_CMD("INIT_FSM");
const std::string FSMCommands::STOP_FSM_CMD("STOP_FSM");
const std::string FSMCommands::EOR_CMD(FSMCommands::STOPARCHIVING_CMD);
const std::string FSMCommands::SOR_CMD(FSMCommands::START_CMD);

const FSMCommands::bm_type FSMCommands::CMD_MAP = boost::assign::list_of<FSMCommands::bm_type::relation>(FSM_COMMAND::INITIALIZE, FSMCommands::INITIALIZE_CMD)
													   													(FSM_COMMAND::CONFIGURE, FSMCommands::CONFIGURE_CMD)
													   													(FSM_COMMAND::CONFIG, FSMCommands::CONFIG_CMD)
													   													(FSM_COMMAND::CONNECT, FSMCommands::CONNECT_CMD)
													   													(FSM_COMMAND::START, FSMCommands::START_CMD)
													   													(FSM_COMMAND::STOP, FSMCommands::STOP_CMD)
													   													(FSM_COMMAND::STOPROIB, FSMCommands::STOPROIB_CMD)
													   													(FSM_COMMAND::STOPDC, FSMCommands::STOPDC_CMD)
													   													(FSM_COMMAND::STOPHLT, FSMCommands::STOPHLT_CMD)
													   													(FSM_COMMAND::STOPRECORDING, FSMCommands::STOPRECORDING_CMD)
													   													(FSM_COMMAND::STOPGATHERING, FSMCommands::STOPGATHERING_CMD)
													   													(FSM_COMMAND::STOPARCHIVING, FSMCommands::STOPARCHIVING_CMD)
													   													(FSM_COMMAND::DISCONNECT, FSMCommands::DISCONNECT_CMD)
													   													(FSM_COMMAND::UNCONFIG, FSMCommands::UNCONFIG_CMD)
													   													(FSM_COMMAND::UNCONFIGURE, FSMCommands::UNCONFIGURE_CMD)
													   													(FSM_COMMAND::SHUTDOWN, FSMCommands::SHUTDOWN_CMD)
													   													(FSM_COMMAND::USERBROADCAST, FSMCommands::USERBROADCAST_CMD)
													   													(FSM_COMMAND::RELOAD_DB, FSMCommands::RELOAD_DB_CMD)
													   													(FSM_COMMAND::RESYNCH, FSMCommands::RESYNCH_CMD)
													   													(FSM_COMMAND::NEXT_TRANSITION, FSMCommands::NEXT_TRANSITION_CMD)
                                                                                                        (FSM_COMMAND::SUB_TRANSITION, FSMCommands::SUB_TRANSITION_CMD)
                                                                                                        (FSM_COMMAND::INIT_FSM, FSMCommands::INIT_FSM_CMD)
													   													(FSM_COMMAND::STOP_FSM, FSMCommands::STOP_FSM_CMD);

std::string FSMCommands::commandToString(FSM_COMMAND cmd) {
	const FSMCommands::bm_type::left_map::const_iterator& it = FSMCommands::CMD_MAP.left.find(cmd);
	return it->second;
}

FSM_COMMAND FSMCommands::stringToCommand(const std::string& cmd) {
	const FSMCommands::bm_type::right_map::const_iterator& it = FSMCommands::CMD_MAP.right.find(boost::algorithm::to_upper_copy(cmd));
	if(it != FSMCommands::CMD_MAP.right.end()) {
		return it->second;
	} else {
	    const std::string& errMsg = std::string("the command \"") + cmd + "\" is not an FSM command" ;
		throw daq::rc::BadCommand(ERS_HERE, cmd, errMsg);
	}
}

std::pair<FSMCommands::command_iterator, FSMCommands::command_iterator> FSMCommands::commands() {
    return std::make_pair(FSMCommands::CMD_MAP.left.begin(), FSMCommands::CMD_MAP.left.end());
}

}}
