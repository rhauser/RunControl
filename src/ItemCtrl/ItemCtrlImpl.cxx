/*
 * ItemCtrlImpl.cxx
 *
 *  Created on: Jun 10, 2013
 *      Author: avolio
 */

#include "RunControl/ItemCtrl/ItemCtrlImpl.h"
#include "RunControl/ItemCtrl/FSMReactions.h"
#include "RunControl/ItemCtrl/ItemCtrlStatus.h"
#include "RunControl/Common/CommandReceiverFactory.h"
#include "RunControl/Common/CommandReceiver.h"
#include "RunControl/Common/CommandSender.h"
#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/RunControlBasicCommand.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/ThreadPool.h"
#include "RunControl/Common/ScheduledThreadPool.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/AMBridge.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/Constants.h"
#include "RunControl/Common/CommandNotifierAdapter.h"
#include "RunControl/FSM/FSM.h"
#include "RunControl/FSM/FSMCommands.h"

#include <ers/ers.h>
#include <ipc/core.h>
#include <ipc/exceptions.h>
#include <ipc/partition.h>
#include <ers/Assertion.h>
#include <ers/Issue.h>
#include <ers/IssueCatcherHandler.h>
#include <ers/LocalContext.h>
#include <ers/Severity.h>
#include <pmg/pmg_initSync.h>
#include <rc/RCStateInfoNamed.h>

#include <boost/bind/bind.hpp>
#include <boost/bind/placeholders.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include <boost/scope_exit.hpp>

#include <functional>
#include <exception>
#include <array>
#include <set>
#include <cstdlib>
#include <limits>
#include <algorithm>
#include <mutex>
#include <chrono>
#include <map>
#include <utility>


namespace daq { namespace rc {

const unsigned int ItemCtrlImpl::DEFAULT_PROBE_INT = 10;
const unsigned int ItemCtrlImpl::DEFAULT_FULL_STATS_INT = 60;
std::once_flag ItemCtrlImpl::m_subtr_init_flag;

class ExecuteCmdTask : public FutureTask<void> {
    public:
        // throws daq::rc::BadCommand
        ExecuteCmdTask(ItemCtrlImpl& itemCtrl, const std::shared_ptr<RunControlBasicCommand>& cmd)
            : FutureTask<void>(), m_item_ctrl(itemCtrl), m_cmd(cmd), m_cmd_name(cmd->name()),
              m_cmd_enum(RunControlCommands::stringToCommand(m_cmd_name))
        {
            if(ExecuteCmdTask::VALID_COMMANDS.find(m_cmd_enum) == ExecuteCmdTask::VALID_COMMANDS.end()) {
                throw daq::rc::BadCommand(ERS_HERE, m_cmd_name, m_cmd_name + " is not a valid command for this application");
            }
        }

        void run() override {
            {
                std::lock_guard<std::shared_mutex> lk(m_item_ctrl.m_status_mutex);
                m_item_ctrl.m_status->setLastCmd(std::shared_ptr<RunControlBasicCommand>(m_cmd->clone()));
                m_item_ctrl.m_status->setExecutingCmd(true);
            }

            m_item_ctrl.publishInformation();

            try {
                switch(m_cmd_enum) {
                    case RC_COMMAND::ENABLE:
                    {
                        const std::shared_ptr<const ChangeStatusCmd>& enableCmd = std::static_pointer_cast<const ChangeStatusCmd>(m_cmd);
                        m_item_ctrl.m_dispatcher->dispatch(m_item_ctrl.m_controllables,
                                                           std::bind(&Controllable::enable, std::placeholders::_1, enableCmd->components()),
                                                           true);
                        break;
                    }
                    case RC_COMMAND::DISABLE:
                    {
                        const std::shared_ptr<const ChangeStatusCmd>& disableCmd = std::static_pointer_cast<const ChangeStatusCmd>(m_cmd);
                        m_item_ctrl.m_dispatcher->dispatch(m_item_ctrl.m_controllables,
                                                           std::bind(&Controllable::disable, std::placeholders::_1, disableCmd->components()),
                                                           true);
                        break;
                    }
                    case RC_COMMAND::PUBLISH:
                    {
                        m_item_ctrl.m_dispatcher->dispatch(m_item_ctrl.m_controllables,
                                                           std::bind(&Controllable::publish, std::placeholders::_1),
                                                           true);
                        break;
                    }
                    case RC_COMMAND::PUBLISHSTATS:
                    {
                        m_item_ctrl.m_dispatcher->dispatch(m_item_ctrl.m_controllables,
                                                           std::bind(&Controllable::publishFullStats, std::placeholders::_1),
                                                           true);

                        break;
                    }
                    case RC_COMMAND::USER:
                    {
                        const std::shared_ptr<UserCmd>& userCmd = std::static_pointer_cast<UserCmd>(m_cmd);

                        // It is needed to add the current state to the user command
                        // If not done the user command will fail when asked about the current state!!!
                        // The same is done in the FSM when the user command is executed as a result
                        // of an user broadcast command
                        const FSM_STATE currState = std::get<0>(m_item_ctrl.m_fsm->status());
                        userCmd->currentFSMState(currState);

                        m_item_ctrl.m_dispatcher->dispatch(m_item_ctrl.m_controllables,
                                                           std::bind(&Controllable::user, std::placeholders::_1, std::cref(*userCmd)),
                                                           true);
                        break;
                    }
                    case RC_COMMAND::CHANGE_PROBE_INTERVAL:
                    {
                        const std::shared_ptr<const ChangePublishIntervalCmd>& cmd = std::static_pointer_cast<const ChangePublishIntervalCmd>(m_cmd);

                        const unsigned int newDelay = cmd->publishInterval();
                        if(newDelay != 0) {
                            m_item_ctrl.m_timers_tp->rescheduleExecution({{RunControlCommands::PUBLISH_CMD, {{0, newDelay}}}});
                        } else {
                            m_item_ctrl.m_timers_tp->rescheduleExecution({{RunControlCommands::PUBLISH_CMD, {{std::numeric_limits<unsigned int>::max(),
                                                                                                              std::numeric_limits<unsigned int>::max()}}}});
                        }

                        break;
                    }
                    case RC_COMMAND::CHANGE_FULLSTATS_INTERVAL:
                    {
                        const std::shared_ptr<const ChangeFullStatIntervalCmd>& cmd = std::static_pointer_cast<const ChangeFullStatIntervalCmd>(m_cmd);

                        const unsigned int newDelay = cmd->fullStatInterval();
                        if(newDelay != 0) {
                            m_item_ctrl.m_timers_tp->rescheduleExecution({{RunControlCommands::PUBLISHSTATS_CMD, {{0, newDelay}}}});
                        } else {
                            m_item_ctrl.m_timers_tp->rescheduleExecution({{RunControlCommands::PUBLISHSTATS_CMD, {{std::numeric_limits<unsigned int>::max(),
                                                                                                                   std::numeric_limits<unsigned int>::max()}}}});
                        }

                        break;
                    }
                    default:
                    {
                        // This is a bug
                        ERS_ASSERT_MSG(false, "we should never be here");
                        break;
                    }
                }
            }
            catch(std::exception& ex) {
                ers::fatal(daq::rc::ExecutionFailed(ERS_HERE, m_cmd_name, "", ex));
            }
        }

        void done() noexcept override {
            {
                std::lock_guard<std::shared_mutex> lk(m_item_ctrl.m_status_mutex);
                m_item_ctrl.m_status->setExecutingCmd(false);
            }

            m_item_ctrl.publishInformation();

            m_item_ctrl.notifyCommandSender(m_cmd);
        }

    private:
        static const std::set<RC_COMMAND> VALID_COMMANDS;
        const ItemCtrlImpl& m_item_ctrl;
        const std::shared_ptr<RunControlBasicCommand> m_cmd;
        const std::string m_cmd_name;
        const RC_COMMAND m_cmd_enum;
};

const std::set<RC_COMMAND> ExecuteCmdTask::VALID_COMMANDS = { RC_COMMAND::ENABLE, RC_COMMAND::DISABLE, RC_COMMAND::PUBLISH,
                                                              RC_COMMAND::PUBLISHSTATS, RC_COMMAND::USER, RC_COMMAND::CHANGE_PROBE_INTERVAL,
                                                              RC_COMMAND::CHANGE_FULLSTATS_INTERVAL };

class TransitionTask : public FutureTask<void> {
    public:
        TransitionTask(ItemCtrlImpl& itemCtrl, const std::shared_ptr<TransitionCmd>& cmd)
            : FutureTask<void>(), m_item_ctrl(itemCtrl), m_tr_cmd(cmd)
        {
        }

        void run() override {
            // All the commands are queued: the FSM can never be busy
            try {
                m_item_ctrl.m_fsm->makeTransition(m_tr_cmd);
            }
            catch(ers::Issue& ex) {
                ers::warning(ex);
            }
        }

    protected:
        void done() noexcept override {
            m_item_ctrl.notifyCommandSender(m_tr_cmd);
        }

    private:
        const ItemCtrlImpl& m_item_ctrl;
        const std::shared_ptr<TransitionCmd> m_tr_cmd;
};

ItemCtrlImpl::ItemCtrlImpl(const std::string& name,
                           const std::string& parent,
                           const std::string& segment,
                           const std::string& partition,
                           const ControllableDispatcher::ControllableList& ctrlList,
                           bool isInteractive,
                           const std::shared_ptr<ControllableDispatcher>& dispatcher)
try : CommandedApplication(), m_exiting_sem(0), m_name(name), m_parent(parent), m_segment(segment), m_partition(partition),
      m_am_bridge(new AMBridge(partition)), m_is_interactive(isInteractive), m_probe_interval(ItemCtrlImpl::probeInterval()),
      m_full_stat_interval(ItemCtrlImpl::fullStatsInterval()), m_last_is_update(0), m_cmd_sender(new CommandSender(partition, name)),
      m_status(new ItemCtrlStatus()), m_controllables(ctrlList), m_dispatcher(dispatcher),
      m_fsm(new FSM<FSM_STATE::INITIAL>(std::make_shared<FSMReactions>(ctrlList, dispatcher))), m_notifier_tp(),
      m_publisher_tp(new ThreadPool(1)), m_timers_tp(new ScheduledThreadPool(2)), m_cmd_tp(new ThreadPool(1))
{
    m_timers_tp->pauseExecution(true);
}
catch(daq::rc::ThreadPoolError& ex) {
    throw daq::rc::ItemCtrlInitializationFailed(ERS_HERE, "some threads could not be created", ex);
}

ItemCtrlImpl::~ItemCtrlImpl() noexcept {
}

void ItemCtrlImpl::init() {
    try {
        IPCCore::init({});
    }
    catch(daq::ipc::AlreadyInitialized& ex) {
        ers::log(ex);
    }
    catch(daq::ipc::CannotInitialize& ex) {
        throw daq::rc::ItemCtrlInitializationFailed(ERS_HERE, "cannot initialize the IPC sub-system", ex);
    }

    // Install the ers issue catcher
    m_ers_handler.reset(ers::set_issue_catcher(boost::bind(&ItemCtrlImpl::issueCatcher, this, boost::placeholders::_1)));

    // Register call-backs to the FSM
    m_fsm->registerOnTransitioningCallback(std::bind(&ItemCtrlImpl::fsmTransitioning, this, std::placeholders::_1));
    m_fsm->registerOnTransitionDoneCallback(std::bind(&ItemCtrlImpl::fsmTransitionDone, this, std::placeholders::_1));
    m_fsm->registerOnNoTransitionCallback(std::bind(&ItemCtrlImpl::fsmNoTransition, this, std::placeholders::_1));
    m_fsm->registerOnTransitionExceptionCallback(std::bind(&ItemCtrlImpl::fsmTransitionException, this, std::placeholders::_1, std::placeholders::_2));

    if(m_is_interactive == false) {
        m_cmd_rec = CommandReceiverFactory::create(RECV_TYPE::CORBA, *this);
    } else {
        m_cmd_rec = CommandReceiverFactory::create(RECV_TYPE::CMDLN, *this);
    }

    // Set the timers
    m_timers_tp->submit(std::bind(&ItemCtrlImpl::periodicProbe, this),
                        RunControlCommands::PUBLISH_CMD,
                        (m_probe_interval != 0) ? 0 : std::numeric_limits<unsigned int>::max(),
                        (m_probe_interval != 0) ? m_probe_interval : std::numeric_limits<unsigned int>::max());

    m_timers_tp->submit(std::bind(&ItemCtrlImpl::periodicFullStats, this),
                        RunControlCommands::PUBLISHSTATS_CMD,
                        (m_full_stat_interval != 0) ? 0 : std::numeric_limits<unsigned int>::max(),
                        (m_full_stat_interval != 0) ? m_full_stat_interval : std::numeric_limits<unsigned int>::max());

    // Publish current status
    publishInformation();

    // This publishes in IPC (when the CORBA receiver is used)
    try {
        m_cmd_rec->init();
    }
    catch(daq::rc::OnlineServicesFailure& ex) {
        throw daq::rc::ItemCtrlInitializationFailed(ERS_HERE, ex.message(), ex);
    }
}

void ItemCtrlImpl::run() {
    // The PMG synch
    if(m_is_interactive == false) {
        pmg_initSync();
    }

    // For a leaf application this does nothing: it just generates
    // the initial event and the FSM is now in its initial state
    m_fsm->start(false);

    // Reach the parent state
    auto statusToReach = [this]() -> std::string
    {
        const auto& parent = m_cmd_sender->status(m_parent);
        const auto& trCmd = parent->currentTransitionCommand();
        if(trCmd.get() != nullptr) {
            return trCmd->fsmDestinationState();
        } else {
            return parent->fsmState();
        }
    };

    if(m_is_interactive == false) {
        try {
            // When we leave this scope, inform that everything has been done
            // This is needed in particular for the proper completion of a restart
            BOOST_SCOPE_EXIT(this_)
            {
                {
                    std::lock_guard<std::shared_mutex> lk(this_->m_status_mutex);
                    this_->m_status->setFullyInitialized(true);
                }

                this_->publishInformation();
            }
            BOOST_SCOPE_EXIT_END

            FSM_STATE currState = std::get<0>(m_fsm->status());
            FSM_STATE destState = FSMStates::stringToState(statusToReach());
            while(currState != destState) {
                const auto& trans = m_fsm->transitions(currState, destState);
                if(trans.empty() == false) {
                    for(const FSM_COMMAND cmd : trans) {
                        std::shared_ptr<TransitionCmd> trCmd(new TransitionCmd(cmd));
                        // We are restarting, let's do the sub-transitions as well
                        trCmd->subTransitions(subTransitions());

                        std::shared_ptr<TransitionTask> trTask(new TransitionTask(*this, trCmd));
                        m_cmd_tp->execute<void>(trTask);
                        while(true) {
                            try {
                                trTask->get(std::chrono::milliseconds(1));
                                break;
                            }
                            catch(daq::rc::TimeoutException&) {
                                if(m_exiting_sem.get_value() != 0) {
                                    throw daq::rc::TransitionInterrupted(ERS_HERE, trCmd->fsmCommand());
                                }
                            }
                        }

                        FSM_STATE oldState = currState;
                        currState = std::get<0>(m_fsm->status());
                        if(oldState == currState) {
                            throw daq::rc::TransitionInterrupted(ERS_HERE, trCmd->fsmCommand());
                        }
                    }

                    destState = FSMStates::stringToState(statusToReach());
                } else {
                    throw daq::rc::FSMPathError(ERS_HERE,
                                                FSMStates::stateToString(currState),
                                                FSMStates::stateToString(destState),
                                                "no path returned by the FSM");
                }
            }

            ERS_DEBUG(1, "My state " << FSMStates::stateToString(currState) << " - Parent state " << FSMStates::stateToString(destState));

            // Now that we have reached the right state, check that the parent was not executing some in-state transition
            // If it was, then this application has to execute it as well, otherwise the parent will wait forever
            const auto& parentStatus = m_cmd_sender->status(m_parent);
            const std::shared_ptr<TransitionCmd> trCmd(parentStatus->currentTransitionCommand());
            if((trCmd.get() != nullptr) && (trCmd->fsmDestinationState() == trCmd->fsmSourceState())) {
                if(trCmd->fsmCommand() != rc::FSMCommands::SUB_TRANSITION_CMD) {
                    m_cmd_tp->execute<void>(std::shared_ptr<TransitionTask>(new TransitionTask(*this, trCmd)));
                } else {
                    // If the parent is in the middle of a sub-transition, then we need to be sure that - in case
                    // of multiple sub-transitions - all the sub-transitions are properly executed up to the current one

                    // Cast to the proper command
                    const std::shared_ptr<const SubTransitionCmd>& sub = std::static_pointer_cast<const SubTransitionCmd>(trCmd);

                    // Look for the main transition and the current sub-transition
                    const FSM_COMMAND mainTr = FSMCommands::stringToCommand(sub->mainTransitionCmd());
                    const std::string& subTrName = sub->subTransition();

                    // Get all the possible sub-transitions for the current main transition
                    const auto& search = subTransitions().find(mainTr);
                    if(search != subTransitions().end()) {
                        const std::vector<std::string>& allSubTr = search->second;

                        // Execute all the sub-transitions up to the current one (sub-transitions are ordered)
                        const auto& it = std::find(allSubTr.begin(), allSubTr.end(), subTrName);
                        if(it != allSubTr.end()) {
                            for(auto itt = allSubTr.begin(); itt != (it + 1); ++itt) {
                                const auto& c = std::make_shared<SubTransitionCmd>(*itt, mainTr);
                                m_cmd_tp->execute<void>(std::shared_ptr<TransitionTask>(new TransitionTask(*this, c)));
                            }
                        } else {
                            // The sub-transition is not in the list of allowed sub-transitions
                            throw daq::rc::NotAllowed(ERS_HERE,
                                                      rc::FSMCommands::SUB_TRANSITION_CMD + subTrName,
                                                      "sub-transition not found for the main transition " + sub->mainTransitionCmd());
                        }
                    } else {
                        // This should not happen: no sub-transitions have been found for the current transition
                        // but the parent is doing a sub-transition!!!
                        throw daq::rc::NotAllowed(ERS_HERE,
                                                  rc::FSMCommands::SUB_TRANSITION_CMD + subTrName,
                                                  "no sub-transitions found for the main transition " + sub->mainTransitionCmd());
                    }
                }
            }
        }
        catch(daq::rc::Exception& ex) {
            ex.wrap_message("", ". The parent controller's FSM state cannot be properly reached and any pending command not executed");
            ers::error(ex);
        }
    }

    // This blocks: it will return when 'shutdown' is called
    m_cmd_rec->start();

    // We are exiting, let's do some clean-up
    cleanUp();
}

void ItemCtrlImpl::makeTransition(const CommandedApplication::SenderContext& sc,
                                  const std::string& commandDescription)
{
    std::shared_ptr<TransitionCmd> trCmd(TransitionCmd::create(commandDescription));
    const std::string& fsmCmdName = trCmd->fsmCommand();

    {
        std::shared_lock<std::shared_mutex> lk(m_status_mutex);
        if(m_status->isError() == true) {
            throw daq::rc::InErrorState(ERS_HERE, fsmCmdName);
        }
    }

    // Ask the AM
    if(m_am_bridge->canExecuteCommand(sc.getUserName(), fsmCmdName) == false) {
        throw daq::rc::NotAllowed(ERS_HERE, fsmCmdName, "the AccessManager denied its execution");
    }

    // If the sub-transitions have not been done (i.e., the applications has
    // received a transition command not from the parent), then do them
    if((fsmCmdName != FSMCommands::SUB_TRANSITION_CMD) &&
       (trCmd->subTransitionsDone() == false) &&
       (trCmd->skipSubTransitions() == false))
    {
        trCmd->subTransitions(subTransitions());
    }

    try {
        const std::shared_ptr<TransitionTask> tt(new TransitionTask(*this, trCmd));
        m_cmd_tp->execute<void>(tt);

        // When running in interactive way we want to wait for the transition to be completed
        if(m_is_interactive == true) {
            try {
                tt->get();
            }
            catch(ers::Issue& ex) {
                ers::fatal(ex);
            }
        }
    }
    catch(daq::rc::ThreadPoolPaused& ex) {
        // It means we are exiting
        ers::log(ex);
    }
}

void ItemCtrlImpl::executeCommand(const CommandedApplication::SenderContext& sc,
                                  const std::string& commandDescription)
{
    const std::shared_ptr<RunControlBasicCommand> recvCmd(RunControlCommands::factory(commandDescription));

    // Ask the AM
    const std::string& cmdName = recvCmd->name();
    if(m_am_bridge->canExecuteCommand(sc.getUserName(), cmdName) == false) {
        throw daq::rc::NotAllowed(ERS_HERE, cmdName, "the AccessManager denied its execution");
    }

    try {
        const std::shared_ptr<ExecuteCmdTask> ct(new ExecuteCmdTask(*this, recvCmd));
        m_cmd_tp->execute<void>(ct);

        // When running in interactive way we want the command to be executed before returning
        if(m_is_interactive == true) {
            try {
                ct->get();
            }
            catch(ers::Issue& ex) {
                ers::error(ex);
            }
        }
    }
    catch(daq::rc::ThreadPoolPaused& ex) {
        // We are exiting
        ers::log(ex);
    }
}

std::string ItemCtrlImpl::status() {
    const std::shared_ptr<ItemCtrlStatus> currentStatus(new ItemCtrlStatus());

    {
        std::shared_lock<std::shared_mutex> lk(m_status_mutex);
        *currentStatus = *m_status;
    }

    return ItemCtrlImpl::parentUpdateCmd(m_name, *currentStatus)->serialize();
}

std::string ItemCtrlImpl::childStatus(const std::string& childName) {
    throw daq::rc::UnknownChild(ERS_HERE, childName);
}

void ItemCtrlImpl::updateChild(const std::string&) {
    // Nothing to do, not supported command
}

void ItemCtrlImpl::setError(const CommandedApplication::SenderContext&,
                            const CommandedApplication::ErrorItems&)
{
    // Nothing to do, not supported command
}

void ItemCtrlImpl::shutdown() {
    // Be careful to what you call here! This may be called in a signal handler and not everything is safe!!!
    // https://www.securecoding.cert.org/confluence/pages/viewpage.action?pageId=1420
    m_exiting_sem.post();
    m_cmd_rec->shutdown();
}

std::string ItemCtrlImpl::id() {
    return m_name;
}

std::string ItemCtrlImpl::partition() {
    return m_partition;
}

const std::string& ItemCtrlImpl::getParent() const {
    return m_parent;
}

const std::string& ItemCtrlImpl::getSegment() const {
    return m_segment;
}

void ItemCtrlImpl::fsmTransitioning(const TransitionCmd& trCmd) {
    // This is always called in the thread where the transition is being executed

    // This is the transition start time
    m_transition_start = clock_t::now();

    {
        std::lock_guard<std::shared_mutex> lk(m_status_mutex);
        m_status->setTransitioning(true);
        m_status->setCurrentTransitionCmd(std::shared_ptr<TransitionCmd>(trCmd.clone()));
    }

    publishInformation();
}

void ItemCtrlImpl::fsmTransitionDone(const TransitionCmd& trCmd) {
    // This is always called in the thread where the transition is being executed
    const FSM_COMMAND fsmCmd = FSMCommands::stringToCommand(trCmd.fsmCommand());
    switch(fsmCmd) {
        case FSM_COMMAND::CONFIGURE:
        {
            // At CONFIGURE reset timers to their default values
            std::map<std::string, std::array<unsigned int, 2>> resch;

            if(m_probe_interval == 0) {
                resch.insert(std::make_pair(RunControlCommands::PUBLISH_CMD, std::array<unsigned int, 2>{{std::numeric_limits<unsigned int>::max(),
                                                                                                          std::numeric_limits<unsigned int>::max()}}));
            } else {
                resch.insert(std::make_pair(RunControlCommands::PUBLISH_CMD, std::array<unsigned int, 2>{{0, m_probe_interval}}));
            }

            if(m_full_stat_interval == 0) {
                resch.insert(std::make_pair(RunControlCommands::PUBLISHSTATS_CMD, std::array<unsigned int, 2>{{std::numeric_limits<unsigned int>::max(),
                                                                                                               std::numeric_limits<unsigned int>::max()}}));
            } else {
                resch.insert(std::make_pair(RunControlCommands::PUBLISHSTATS_CMD, std::array<unsigned int, 2>{{0, m_full_stat_interval}}));
            }

            m_timers_tp->rescheduleExecution(resch);

            break;
        }
        case FSM_COMMAND::CONNECT:
        {
            // At CONNECT restart the timers
            m_timers_tp->resumeExecution();

            break;
        }
        case FSM_COMMAND::DISCONNECT:
        {
            // At DISCONNECT timers are stopped
            ERS_LOG("Pausing the execution of periodic alarms");
            m_timers_tp->pauseExecution(true);
            ERS_LOG("Periodic alarms have been suspended");

            break;
        }
        default:
            break;
    }

    // This is the time the transition is done
    auto stopTime = clock_t::now();

    {
        std::lock_guard<std::shared_mutex> lk(m_status_mutex);
        m_status->setTransitioning(false);
        m_status->setCurrentState(FSMStates::stringToState(trCmd.fsmDestinationState()));
        m_status->setLastTransitionCmd(m_status->getCurrentTransitionCmd());
        m_status->setCurrentTransitionCmd(nullptr);
        m_status->setLastTransitionTime(chrono::duration_cast<chrono::milliseconds>(stopTime - m_transition_start).count());
    }

    publishInformation();
}

void ItemCtrlImpl::fsmNoTransition(const TransitionCmd& trCmd) {
    // This is always called in the thread where the transition is being executed
    ers::warning(daq::rc::EventNotProcessed(ERS_HERE, trCmd.fsmCommand()));
}

void ItemCtrlImpl::fsmTransitionException(const TransitionCmd& trCmd, std::exception& ex) {
    // This is always called in the thread where the transition is being executed

    ers::fatal(daq::rc::TransitionFailed(ERS_HERE, trCmd.fsmCommand(), "", ex));

    // The transition has not been completed
    {
        std::lock_guard<std::shared_mutex> lk(m_status_mutex);
        m_status->setTransitioning(false);
        m_status->setLastTransitionCmd(m_status->getCurrentTransitionCmd());
        m_status->setCurrentTransitionCmd(nullptr);
    }

    publishInformation();
}

void ItemCtrlImpl::issueCatcher(const ers::Issue& issue) {
    if(issue.severity() == ers::Warning) {
        ers::warning(issue);
    } else if(issue.severity() == ers::Error) {
        ers::error(issue);
    } else if(issue.severity() == ers::Fatal) {
        ers::fatal(issue);

        {
            std::lock_guard<std::shared_mutex> lk(m_status_mutex);
            auto&& currentErrors = m_status->getErrorReasons();
            currentErrors.push_back(std::string(issue.get_class_name()) + ": " + issue.message());
            m_status->setErrorReasons(currentErrors);
        }

        publishInformation();
    }
}

bool ItemCtrlImpl::periodicProbe() {
    // This will throw if the thread is interrupted (i.e., the TP is stopped)
    // Let the exception go, the TP manages this
    boost::this_thread::interruption_point();

    BOOST_SCOPE_EXIT(this_)
    {
        {
            std::lock_guard<std::shared_mutex> lk(this_->m_status_mutex);
            this_->m_status->setPublishing(false);
        }

        this_->publishInformation();
    }
    BOOST_SCOPE_EXIT_END

    {
        std::lock_guard<std::shared_mutex> lk(m_status_mutex);
        m_status->setPublishing(true);
    }

    publishInformation();

    try {
        // Disable interruption for user code (the TP uses the
        // boost mechanism to pause/stop the execution of tasks)
        boost::this_thread::disable_interruption di;

        m_dispatcher->dispatch(m_controllables,
                               std::bind(&Controllable::publish, std::placeholders::_1),
                               true);
    }
    catch(std::exception& ex) {
        ers::fatal(daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::PUBLISH_CMD, "", ex));
    }

    return true;
}

bool ItemCtrlImpl::periodicFullStats() {
    // This will throw if the thread is interrupted (i.e., the TP is stopped)
    // Let the exception go, the TP manages this
    boost::this_thread::interruption_point();

    BOOST_SCOPE_EXIT(this_)
    {
        {
            std::lock_guard<std::shared_mutex> lk(this_->m_status_mutex);
            this_->m_status->setPublishingStats(false);
        }

        this_->publishInformation();
    }
    BOOST_SCOPE_EXIT_END

    {
        std::lock_guard<std::shared_mutex> lk(m_status_mutex);
        m_status->setPublishingStats(true);
    }

    publishInformation();

    try {
        // Disable interruption for user code (the TP uses the
        // boost mechanism to pause/stop the execution of tasks)
        boost::this_thread::disable_interruption di;

        m_dispatcher->dispatch(m_controllables,
                               std::bind(&Controllable::publishFullStats, std::placeholders::_1),
                               true);
    }
    catch(std::exception& ex) {
        ers::fatal(daq::rc::ExecutionFailed(ERS_HERE, RunControlCommands::PUBLISH_CMD, "", ex));
    }

    return true;
}

void ItemCtrlImpl::cleanUp() {
    // Remove the issue catcher
    m_ers_handler.reset(nullptr);

    // Drain the thread-pools
    m_publisher_tp->pause();
    m_publisher_tp->flush();

    m_cmd_tp->pause();
    m_cmd_tp->flush();

    m_timers_tp->pauseExecution(false);

    // Call user code: "onExit" is declared "noexcept"
    m_dispatcher->dispatch(m_controllables,
                           std::bind(&Controllable::onExit, std::placeholders::_1, std::get<0>(m_fsm->status())),
                           true);

    // Remove the IS information
    if(m_is_interactive == false) {
        try {
            RCStateInfoNamed rcInfo(m_partition, Constants::RC_IS_SERVER_NAME + "." + m_name);
            rcInfo.remove();
        }
        catch(ers::Issue& ex) {
            ers::warning(ex);
        }
    }
}

void ItemCtrlImpl::publishInformation_corba() const {
    const std::shared_ptr<ItemCtrlStatus> currentStatus(new ItemCtrlStatus());

    {
        std::shared_lock<std::shared_mutex> lk(m_status_mutex);
        *currentStatus = *m_status;
    }

    try {
        m_publisher_tp->submit<void>([currentStatus, this]()
                                     {
                                        // Update the parent
                                        try {
                                            m_cmd_sender->updateChild(m_parent,
                                                                      *ItemCtrlImpl::parentUpdateCmd(m_name, *currentStatus));
                                        }
                                        catch(ers::Issue& ex) {
                                            ers::error(daq::rc::ParentUpdateFailure(ERS_HERE, m_parent, ex));
                                        }

                                        // Update IS: in this case publish always the fresh information
                                        // Be aware that the following check works only because we use a single
                                        // thread to update to IS (otherwise more synchronization is needed)
                                        const auto this_time = currentStatus->getInfoTimeTag();
                                        if(this_time >= m_last_is_update) {
                                            RCStateInfoNamed rcInfo(m_partition, Constants::RC_IS_SERVER_NAME + "." + m_name);

                                            rcInfo.state = FSMStates::stateToString(currentStatus->getCurrentState());
                                            rcInfo.busy = currentStatus->isBusy();
                                            rcInfo.transitionTime = currentStatus->getLastTransitionTime();
                                            rcInfo.fault = currentStatus->isError();
                                            rcInfo.transitioning = currentStatus->isTransitioning();
                                            rcInfo.errorReasons = currentStatus->getErrorReasons();

                                            const auto& currTr = currentStatus->getCurrentTransitionCmd();
                                            if(currTr.get() != nullptr) {
                                                rcInfo.currentTransitionName = currTr->fsmCommand();

                                                const auto& args = currTr->arguments();
                                                if(((args.size() != 1) || (args[0] != rcInfo.currentTransitionName))) {
                                                    rcInfo.currentTransitionArgs = args;
                                                }

                                                rcInfo.currentTransitionFull = currTr->serialize();
                                            }

                                            const auto& lastTr = currentStatus->getLastTransitionCmd();
                                            if(lastTr.get() != nullptr) {
                                                rcInfo.lastTransitionName = lastTr->fsmCommand();

                                                const auto& args = lastTr->arguments();
                                                if(((args.size() != 1) || (args[0] != rcInfo.lastTransitionName))) {
                                                    rcInfo.lastTransitionArgs = args;
                                                }

                                                rcInfo.lastTransitionFull = lastTr->serialize();
                                            }

                                            const auto& lastCmd = currentStatus->getLastCmd();
                                            if(lastCmd.get() != nullptr) {
                                                rcInfo.lastCmdName = lastCmd->name();
                                                rcInfo.lastCmdArgs = lastCmd->arguments();
                                                rcInfo.lastCmdFull = lastCmd->serialize();
                                            }

                                            rcInfo.fullyInitialized = currentStatus->isFullyInitialized();

                                            try {
                                                rcInfo.checkin();
                                                m_last_is_update = this_time;
                                            }
                                            catch(ers::Issue& ex) {
                                                ers::error(ex);
                                            }
                                        }
                                     }
        );
    }
    catch(daq::rc::ThreadPoolPaused& ex) {
        // This means that we are exiting
        ers::info(ex);
    }
}

void ItemCtrlImpl::publishInformation_cmdln() const {
    // Just do nothing
}

void ItemCtrlImpl::publishInformation() const {
    // This way the impact on performance is almost null and thread safety is assured by the c++ standard
    static std::function<void ()> pubF = ((m_is_interactive == true) ? std::bind(&ItemCtrlImpl::publishInformation_cmdln, this) :
                                                                       std::bind(&ItemCtrlImpl::publishInformation_corba, this));

    pubF();
}

unsigned int ItemCtrlImpl::probeInterval() {
    const char* pi = std::getenv("TDAQ_PROBE_INTERVAL");
    if(pi != nullptr) {
        try {
            return boost::lexical_cast<unsigned int>(std::string(pi));
        }
        catch(boost::bad_lexical_cast& ex) {
            ers::warning(daq::rc::Exception(ERS_HERE, "Bad value for the publishing period: default value will be used", ex));
        }
    }

    return ItemCtrlImpl::DEFAULT_PROBE_INT;
}

unsigned int ItemCtrlImpl::fullStatsInterval() {
    const char* si = std::getenv("TDAQ_PUBLISHSTATISTICS_INTERVAL");
    if(si != nullptr) {
        try {
            return boost::lexical_cast<unsigned int>(std::string(si));
        }
        catch(boost::bad_lexical_cast& ex) {
            ers::warning(daq::rc::Exception(ERS_HERE, "Bad value for the full-statistics publishing period: default value will be used", ex));
        }
    }

    return ItemCtrlImpl::DEFAULT_FULL_STATS_INT;
}

const Algorithms::SubtransitionMap& ItemCtrlImpl::subTransitions() const {
    std::call_once(ItemCtrlImpl::m_subtr_init_flag,
                   [this]() -> void
                   {
                      OnlineServices& os = OnlineServices::instance();

                      // First sub-transitions from parents, then others
                      m_subtr_map =  Algorithms::subTransitionsFromParents(os.getConfiguration(),
                                                                           os.getPartition(),
                                                                           os.getSegment(),
                                                                           os.segmentName());
                      Algorithms::subTransitions(os.getSegment(), m_subtr_map);
                   }
    );

    return m_subtr_map;
}

void ItemCtrlImpl::notifyCommandSender(const std::shared_ptr<RunControlBasicCommand>& cmd) const {
    const std::string& senderReference = cmd->senderReference();
    if(senderReference.empty() == false) {
        try {
            std::call_once(m_notifier_tp_flag, [this]() { m_notifier_tp.reset(new ThreadPool(1)); } );

            m_notifier_tp->submit<void>([senderReference, cmd]()
                                        {
                                            try {
                                                CommandNotifierAdapter::notifyCommandSender(senderReference, cmd);
                                            }
                                            catch(daq::rc::CannotNotifyCommandSender& ex) {
                                                ers::info(ex);
                                            }
                                        }
            );
        }
        catch(daq::rc::ThreadPoolError& ex) {
            ers::info(daq::rc::CannotNotifyCommandSender(ERS_HERE,
                                                         senderReference,
                                                         cmd->toString(),
                                                         "failed to start the notification thread",
                                                         ex));
        }
    }
}

std::unique_ptr<ApplicationStatusCmd> ItemCtrlImpl::parentUpdateCmd(const std::string& appName,
                                                                    const ItemCtrlStatus& status)
{
    std::unique_ptr<ApplicationStatusCmd> statusCmd(new ApplicationStatusCmd(appName,
                                                                             status.getCurrentState(),
                                                                             status.isTransitioning(),
                                                                             status.isBusy(),
                                                                             status.isFullyInitialized(),
                                                                             status.getInfoTimeTag(),
                                                                             status.getInfoWallTime()));

    const auto& es = status.getErrorReasons();
    statusCmd->errorReasons(es);
    if(es.empty() == false) {
        statusCmd->badChildren({appName});
    }

    const auto& lastTr = status.getLastTransitionCmd();
    if(lastTr.get() != nullptr) {
        statusCmd->lastTransitionCommand(*lastTr);
    }

    const auto& currTr = status.getCurrentTransitionCmd();
    if(currTr.get() != nullptr) {
        statusCmd->currentTransitionCommand(*currTr);
    }

    statusCmd->lastTransitionDuration(status.getLastTransitionTime());

    const auto& lastCmd = status.getLastCmd();
    if(lastCmd.get() != nullptr) {
        statusCmd->lastCommand(*lastCmd);
    }

    return statusCmd;
}

}}

