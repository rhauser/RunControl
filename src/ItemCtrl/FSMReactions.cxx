/*
 * FSMReactions.cxx
 *
 *  Created on: Jun 11, 2013
 *      Author: avolio
 */

#include "RunControl/ItemCtrl/FSMReactions.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/Controllable.h"


namespace daq { namespace rc {

FSMReactions::FSMReactions(const ControllableDispatcher::ControllableList& ctrlList,
                           const std::shared_ptr<ControllableDispatcher>& dispatcher)
    : RunControlTransitionActions(), m_ctrl_list(ctrlList), m_ctrl_dispatcher(dispatcher)
{
}

void FSMReactions::configure(const TransitionCmd& cmd) {
    m_ctrl_dispatcher->dispatch(m_ctrl_list, [&cmd](const std::shared_ptr<Controllable>& c) { c->configure(cmd); }, true);
}

void FSMReactions::connect(const TransitionCmd& cmd) {
    m_ctrl_dispatcher->dispatch(m_ctrl_list, [&cmd](const std::shared_ptr<Controllable>& c) { c->connect(cmd); }, true);
}

void FSMReactions::prepareForRun(const TransitionCmd& cmd) {
    m_ctrl_dispatcher->dispatch(m_ctrl_list, [&cmd](const std::shared_ptr<Controllable>& c) { c->prepareForRun(cmd); }, true);
}

void FSMReactions::stopROIB(const TransitionCmd& cmd) {
    m_ctrl_dispatcher->dispatch(m_ctrl_list, [&cmd](const std::shared_ptr<Controllable>& c) { c->stopROIB(cmd); }, false);
}

void FSMReactions::stopDC(const TransitionCmd& cmd) {
    m_ctrl_dispatcher->dispatch(m_ctrl_list, [&cmd](const std::shared_ptr<Controllable>& c) { c->stopDC(cmd); }, false);
}

void FSMReactions::stopHLT(const TransitionCmd& cmd) {
    m_ctrl_dispatcher->dispatch(m_ctrl_list, [&cmd](const std::shared_ptr<Controllable>& c) { c->stopHLT(cmd); }, false);
}

void FSMReactions::stopRecording(const TransitionCmd& cmd) {
    m_ctrl_dispatcher->dispatch(m_ctrl_list, [&cmd](const std::shared_ptr<Controllable>& c) { c->stopRecording(cmd); }, false);
}

void FSMReactions::stopGathering(const TransitionCmd& cmd) {
    m_ctrl_dispatcher->dispatch(m_ctrl_list, [&cmd](const std::shared_ptr<Controllable>& c) { c->stopGathering(cmd); }, false);
}

void FSMReactions::stopArchiving(const TransitionCmd& cmd) {
    m_ctrl_dispatcher->dispatch(m_ctrl_list, [&cmd](const std::shared_ptr<Controllable>& c) { c->stopArchiving(cmd); }, false);
}

void FSMReactions::disconnect(const TransitionCmd& cmd) {
    m_ctrl_dispatcher->dispatch(m_ctrl_list, [&cmd](const std::shared_ptr<Controllable>& c) { c->disconnect(cmd); }, false);
}

void FSMReactions::unconfigure(const TransitionCmd& cmd) {
    m_ctrl_dispatcher->dispatch(m_ctrl_list, [&cmd](const std::shared_ptr<Controllable>& c) { c->unconfigure(cmd); }, false);
}

void FSMReactions::subTransition(const SubTransitionCmd& cmd) {
    m_ctrl_dispatcher->dispatch(m_ctrl_list, [&cmd](const std::shared_ptr<Controllable>& c) { c->subTransition(cmd); }, true);
}

void FSMReactions::resynch(const ResynchCmd& cmd) {
    m_ctrl_dispatcher->dispatch(m_ctrl_list, [&cmd](const std::shared_ptr<Controllable>& c) { c->resynch(cmd); }, false);
}

void FSMReactions::userBroadcast(const UserBroadcastCmd& cmd) {
    m_ctrl_dispatcher->dispatch(m_ctrl_list, [&cmd](const std::shared_ptr<Controllable>& c) { c->user(*(cmd.userCommand())); }, false);
}

}}
