/*
 * ControllableDispatcher.cpp
 *
 *  Created on: Jul 14, 2011
 *      Author: avolio
 */

#include "RunControl/ItemCtrl/ControllableDispatcher.h"

#include <tbb/parallel_for_each.h>

#include <algorithm>
#include <vector>


namespace daq { namespace rc {

DefaultDispatcher::DefaultDispatcher() :
	ControllableDispatcher()
{
}

DefaultDispatcher::~DefaultDispatcher() {
}

void DefaultDispatcher::dispatch(const ControllableList& ctrlList,
                                 const ControllableCall& f,
                                 bool)
{
	std::for_each(ctrlList.begin(), ctrlList.end(), f);
}

InverseDispatcher::InverseDispatcher() :
	ControllableDispatcher()
{
}

InverseDispatcher::~InverseDispatcher() {
}

void InverseDispatcher::dispatch(const ControllableList& ctrlList,
                                 const ControllableCall& f,
                                 bool isUpTransition)
{
	if(isUpTransition == true) {
		std::for_each(ctrlList.begin(), ctrlList.end(), f);
	} else {
		std::for_each(ctrlList.rbegin(), ctrlList.rend(), f);
	}
}

ParallelDispatcher::ParallelDispatcher() :
	ControllableDispatcher()
{
}

ParallelDispatcher::~ParallelDispatcher() {
}

void ParallelDispatcher::dispatch(const ControllableList& ctrlList,
                                  const ControllableCall& f,
                                  bool)
{
	// 1 - We need to copy the container because the tbb function wants non-const iterators
	// 2 - Since a copy is needed we fill a vector because the tbb documentation says the algorithm scales better with random access iterators
	std::vector<std::shared_ptr<Controllable>> l(ctrlList.begin(), ctrlList.end());
	tbb::parallel_for_each(l.begin(), l.end(), f);
}

}}
