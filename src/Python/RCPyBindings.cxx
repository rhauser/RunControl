/*
 * RCPyBindings.cxx

 *  Created on: August, 2013
 *      Author: Igor Pelevanyuk (gavelock@gmail.com)
 */

// Include this first in order to avoid compilation
// warning on slc6 about "_XOPEN_SOURCE redefined"
#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include "RunControl/Common/CommandSender.h"
#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/FSM/FSMStates.h"
#include "RunControl/FSM/FSMCommands.h"

#include <ipc/partition.h>

#include <boost/foreach.hpp>

#include <iostream>

#include <Python.h>


using namespace boost::python; 
//------------------------------------------------------------------------------
//          From Python list->vector converter
struct iterable_converter
{
   /// @note Registers converter from a python interable type to the
   ///       provided type.
   template <typename Container>
   iterable_converter& from_python()
   {
       boost::python::converter::registry::push_back(
       &iterable_converter::convertible,
       &iterable_converter::construct<Container>,
       boost::python::type_id<Container>());
       return *this;
   }

   // ------------------------------------------
   /// @brief Check if PyObject is iterable.
   static void* convertible(PyObject* object)  {
       return PyList_Check(object) ? object : NULL;
   }

   // ------------------------------------------
   template <typename Container>
   static void construct(PyObject* object,
                       boost::python::converter::rvalue_from_python_stage1_data* data)
   {
       namespace python = boost::python;
       // Object is a borrowed reference, so create a handle indicting it is
       // borrowed for proper reference counting.
       python::handle<> handle(python::borrowed(object));

       typedef python::converter::rvalue_from_python_storage<Container>
                                                                storage_type;
       void* storage = reinterpret_cast<storage_type*>(data)->storage.bytes;

       typedef python::stl_input_iterator<typename Container::value_type>
                                                                    iterator;

       data->convertible = new (storage) Container(
       iterator(python::object(handle)), // begin
       iterator());                      // end
   }
};
//---------------------------------------------------------------------------------
//          From Python dict->map converter
template<typename S, typename T> std::map<S, T> pydict2cppmap(PyObject* obj)
{    
	std::map<S, T> cppmap;
	S key;
	T val;
	boost::python::dict pydict(boost::python::borrowed(obj));
	boost::python::list keylst = pydict.keys();
	int keylstlen = extract<int>(keylst.attr("__len__")());

	for(int i=0; i<keylstlen; i++)
	{
		key = extract<S>(keylst[i]);
		// WARNING; This fails if a default-constructed object of T
		// cannot be assigned to by arbitrary objects of T.
		// See specialization for blitz Array.
		val = extract<T>(pydict[keylst[i]]);
		cppmap.insert(std::make_pair(key, val));
	}
	return cppmap;
}

template<typename S, typename T>
struct cppmap_from_python_dict
{
 	cppmap_from_python_dict()
 	{
   		boost::python::converter::registry::push_back(
                         &convertible,
                         &construct,
                         boost::python::type_id<std::map<S, T> >());
	}
 
   static void* convertible(PyObject* obj_ptr)
   	{
       	if (!PyDict_Check(obj_ptr)) return 0;
           	return obj_ptr;
   	}
 
 	static void construct(
           PyObject* obj_ptr,
           boost::python::converter::rvalue_from_python_stage1_data* data)
 	{
   		void* storage = (
            (boost::python::converter::rvalue_from_python_storage<std::map<S, T> >*)
            data)->storage.bytes;
   		new (storage) std::map<S, T>(pydict2cppmap<S, T>(obj_ptr));
   		data->convertible = storage;
 	}
};
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------
template<typename T>
struct cppset_to_python_list
{
    static PyObject* convert(const std::set<T>& clst)
    {
        T val;
		boost::python::list lst;
		typename std::set<T>::iterator pos;
		for(pos = clst.begin(); pos != clst.end(); pos++)
		{
		    val = *pos;
		    lst.append(val);
		}
		return boost::python::incref(boost::python::object(lst).ptr());
    }
};
//------------------------------------------------------------------------------
template<typename T>
struct custom_vector_to_list{
    static PyObject* convert(const std::vector<T>& v) {
        list ret;
        BOOST_FOREACH(const T& e, v) ret.append(e);
        return incref(ret.ptr());
    }
};

/*******************************************************************************
                    Wrapper for Controllable
*******************************************************************************/
class ControllableWrapper : public daq::rc::Controllable,
                            public wrapper<daq::rc::Controllable>
{
public:
    //CONFIGURE, CONNECT, PREPAREFORRUN
    void configure(const daq::rc::TransitionCmd& tCmd)
    {
        if (override f = this->get_override("configure"))
            f(tCmd);
        else daq::rc::Controllable::configure(tCmd);
    }
    void connect(const daq::rc::TransitionCmd& tCmd)
    {
        if (override f = this->get_override("connect"))
            f(tCmd);
        else daq::rc::Controllable::connect(tCmd);
    }
    void prepareForRun(const daq::rc::TransitionCmd& tCmd)
    {
        if (override f = this->get_override("prepareForRun"))
            f(tCmd);
        else daq::rc::Controllable::prepareForRun(tCmd);
    }

    //STOPS
    void stopROIB(const daq::rc::TransitionCmd& tCmd)
    {
        if (override f = this->get_override("stopROIB"))
            f(tCmd);
        else daq::rc::Controllable::stopROIB(tCmd);
    }
    void stopDC(const daq::rc::TransitionCmd& tCmd)
    {
        if (override f = this->get_override("stopDC"))
            f(tCmd);
        else daq::rc::Controllable::stopDC(tCmd);
    }
    void stopHLT(const daq::rc::TransitionCmd& tCmd)
    {
        if (override f = this->get_override("stopHLT"))
            f(tCmd);
        else daq::rc::Controllable::stopHLT(tCmd);
    }
    void stopRecording(const daq::rc::TransitionCmd& tCmd)
    {
        if (override f = this->get_override("stopRecording"))
            f(tCmd);
        else daq::rc::Controllable::stopRecording(tCmd);
    }
    void stopGathering(const daq::rc::TransitionCmd& tCmd)
    {
        if (override f = this->get_override("stopGathering"))
            f(tCmd);
        else daq::rc::Controllable::stopGathering(tCmd);
    }
    void stopArchiving(const daq::rc::TransitionCmd& tCmd)
    {
        if (override f = this->get_override("stopArchiving"))
            f(tCmd);
        else daq::rc::Controllable::stopArchiving(tCmd);
    }

    // disconnect, unconfigure, subTransition, resynch
    void disconnect(const daq::rc::TransitionCmd& tCmd)
    {
        if (override f = this->get_override("disconnect"))
            f(tCmd);
        else daq::rc::Controllable::disconnect(tCmd);
    }
    void unconfigure(const daq::rc::TransitionCmd& tCmd)
    {
        if (override f = this->get_override("unconfigure"))
            f(tCmd);
        else daq::rc::Controllable::unconfigure(tCmd);
    }
    void subTransition(const daq::rc::SubTransitionCmd& stCmd)
    {
        if (override f = this->get_override("subTransition"))
            f(stCmd);
        else daq::rc::Controllable::subTransition(stCmd);
    }
    void resynch(const daq::rc::ResynchCmd& rCmd)
    {
        if (override f = this->get_override("resynch"))
            f(rCmd);
        else daq::rc::Controllable::resynch(rCmd);
    }

    //publish, publishFullStats, user, enable, disable, onExit
    void publish()
    {
        if (override f = this->get_override("publish"))
            f();
        else daq::rc::Controllable::publish();
    }
    void publishFullStats()
    {
        if (override f = this->get_override("publishFullStats"))
            f();
        else daq::rc::Controllable::publishFullStats();
    }
    void user(const daq::rc::UserCmd& uCmd)
    {
        if (override f = this->get_override("user"))
            f(uCmd);
        else daq::rc::Controllable::user(uCmd);
    }
    void enable(const std::vector<std::string>& strVec)
    {
        if (override f = this->get_override("enable"))
            f(strVec);
        else daq::rc::Controllable::enable(strVec);
    }
    void disable(const std::vector<std::string>& strVec)
    {
        if (override f = this->get_override("disable"))
            f(strVec);
        else daq::rc::Controllable::disable(strVec);
    }
    void onExit(daq::rc::FSM_STATE fsm_state) noexcept
    {
        if (override f = this->get_override("onExit"))
            f(fsm_state);
        else daq::rc::Controllable::onExit(fsm_state);
    }

    void default_configure(const daq::rc::TransitionCmd& tCmd)      {
        return this->daq::rc::Controllable::configure(tCmd); }
    void default_connect(const daq::rc::TransitionCmd& tCmd)        {
        return this->daq::rc::Controllable::connect(tCmd); }
    void default_prapareForRun(const daq::rc::TransitionCmd& tCmd)  {
        return this->daq::rc::Controllable::prepareForRun(tCmd); }

    void default_stopROIB(const daq::rc::TransitionCmd& tCmd)       {
        return this->daq::rc::Controllable::stopROIB(tCmd); }
    void default_stopDC(const daq::rc::TransitionCmd& tCmd)         {
        return this->daq::rc::Controllable::stopDC(tCmd); }
    void default_stopHLT(const daq::rc::TransitionCmd& tCmd)        {
        return this->daq::rc::Controllable::stopHLT(tCmd); }
    void default_stopRecording(const daq::rc::TransitionCmd& tCmd)  {
        return this->daq::rc::Controllable::stopRecording(tCmd); }
    void default_stopGathering(const daq::rc::TransitionCmd& tCmd)  {
        return this->daq::rc::Controllable::stopGathering(tCmd); }
    void default_stopArchiving(const daq::rc::TransitionCmd& tCmd)  {
        return this->daq::rc::Controllable::stopArchiving(tCmd); }

    void default_disconnect(const daq::rc::TransitionCmd& tCmd)         {
        return this->daq::rc::Controllable::disconnect(tCmd); }
    void default_unconfigure(const daq::rc::TransitionCmd& tCmd)        {
        return this->daq::rc::Controllable::unconfigure(tCmd); }
    void default_subTransition(const daq::rc::SubTransitionCmd& stCmd)  {
        return this->daq::rc::Controllable::subTransition(stCmd); }
    void default_resynch(const daq::rc::ResynchCmd& rCmd)               {
        return this->daq::rc::Controllable::resynch(rCmd); }

    void default_publish()                                          {
        return this->daq::rc::Controllable::publish(); }
    void default_publishFullStats()                                 {
        return this->daq::rc::Controllable::publishFullStats(); }
    void default_user(const daq::rc::UserCmd& uCmd)                 {
        return this->daq::rc::Controllable::user(uCmd); }
    void default_disable(const std::vector<std::string>& strVec)    {
        return this->daq::rc::Controllable::disable(strVec); }
    void default_enable(const std::vector<std::string>& strVec)     {
        return this->daq::rc::Controllable::enable(strVec); }
    void default_onExit(daq::rc::FSM_STATE fsm_state)               {
        return this->daq::rc::Controllable::onExit(fsm_state); }
};


BOOST_PYTHON_MODULE(librc_PyModules)
{
		to_python_converter<std::vector<std::string >, custom_vector_to_list<std::string> >();
		to_python_converter<std::set<std::string>, cppset_to_python_list<std::string> >();
		iterable_converter().from_python<std::vector<std::string> >()
   ;
   		cppmap_from_python_dict<daq::rc::FSM_COMMAND, std::vector<std::string> >();

        enum_<daq::rc::FSM_STATE>("FSM_STATE")
        .value(daq::rc::FSMStates::ABSENT_STATE.c_str(), daq::rc::FSM_STATE::ABSENT)
        .value(daq::rc::FSMStates::NONE_STATE.c_str(), daq::rc::FSM_STATE::NONE)
        .value(daq::rc::FSMStates::INITIAL_STATE.c_str(), daq::rc::FSM_STATE::INITIAL)
        .value(daq::rc::FSMStates::CONFIGURED_STATE.c_str(), daq::rc::FSM_STATE::CONFIGURED)
        .value(daq::rc::FSMStates::CONNECTED_STATE.c_str(), daq::rc::FSM_STATE::CONNECTED)
        .value(daq::rc::FSMStates::GTHSTOPPED_STATE.c_str(), daq::rc::FSM_STATE::GTHSTOPPED)
        .value(daq::rc::FSMStates::SFOSTOPPED_STATE.c_str(), daq::rc::FSM_STATE::SFOSTOPPED)
        .value(daq::rc::FSMStates::HLTSTOPPED_STATE.c_str(), daq::rc::FSM_STATE::HLTSTOPPED)
        .value(daq::rc::FSMStates::DCSTOPPED_STATE.c_str(), daq::rc::FSM_STATE::DCSTOPPED)
        .value(daq::rc::FSMStates::ROIBSTOPPED_STATE.c_str(), daq::rc::FSM_STATE::ROIBSTOPPED)
        .value(daq::rc::FSMStates::RUNNING_STATE.c_str(), daq::rc::FSM_STATE::RUNNING)
    ;
        enum_<daq::rc::FSM_COMMAND>("FSM_COMMAND")
        .value(daq::rc::FSMCommands::INIT_FSM_CMD.c_str(), daq::rc::FSM_COMMAND::INIT_FSM)
        .value(daq::rc::FSMCommands::INITIALIZE_CMD.c_str(), daq::rc::FSM_COMMAND::INITIALIZE)
        .value(daq::rc::FSMCommands::CONFIGURE_CMD.c_str(), daq::rc::FSM_COMMAND::CONFIGURE)
        .value(daq::rc::FSMCommands::CONFIG_CMD.c_str(), daq::rc::FSM_COMMAND::CONFIG)
        .value(daq::rc::FSMCommands::CONNECT_CMD.c_str(), daq::rc::FSM_COMMAND::CONNECT)
        .value(daq::rc::FSMCommands::START_CMD.c_str(), daq::rc::FSM_COMMAND::START)
        .value(daq::rc::FSMCommands::STOP_CMD.c_str(), daq::rc::FSM_COMMAND::STOP)
        .value(daq::rc::FSMCommands::STOPROIB_CMD.c_str(), daq::rc::FSM_COMMAND::STOPROIB)
        .value(daq::rc::FSMCommands::STOPDC_CMD.c_str(), daq::rc::FSM_COMMAND::STOPDC)
        .value(daq::rc::FSMCommands::STOPHLT_CMD.c_str(), daq::rc::FSM_COMMAND::STOPHLT)
        .value(daq::rc::FSMCommands::STOPRECORDING_CMD.c_str(), daq::rc::FSM_COMMAND::STOPRECORDING)
        .value(daq::rc::FSMCommands::STOPGATHERING_CMD.c_str(), daq::rc::FSM_COMMAND::STOPGATHERING)
        .value(daq::rc::FSMCommands::STOPARCHIVING_CMD.c_str(), daq::rc::FSM_COMMAND::STOPARCHIVING)
        .value(daq::rc::FSMCommands::DISCONNECT_CMD.c_str(), daq::rc::FSM_COMMAND::DISCONNECT)
        .value(daq::rc::FSMCommands::UNCONFIG_CMD.c_str(), daq::rc::FSM_COMMAND::UNCONFIG)
        .value(daq::rc::FSMCommands::UNCONFIGURE_CMD.c_str(), daq::rc::FSM_COMMAND::UNCONFIGURE)
        .value(daq::rc::FSMCommands::SHUTDOWN_CMD.c_str(), daq::rc::FSM_COMMAND::SHUTDOWN)
        .value(daq::rc::FSMCommands::STOP_FSM_CMD.c_str(), daq::rc::FSM_COMMAND::STOP_FSM)
        .value(daq::rc::FSMCommands::USERBROADCAST_CMD.c_str(), daq::rc::FSM_COMMAND::USERBROADCAST)
        .value(daq::rc::FSMCommands::RELOAD_DB_CMD.c_str(), daq::rc::FSM_COMMAND::RELOAD_DB)
        .value(daq::rc::FSMCommands::RESYNCH_CMD.c_str(), daq::rc::FSM_COMMAND::RESYNCH)
        .value(daq::rc::FSMCommands::NEXT_TRANSITION_CMD.c_str(), daq::rc::FSM_COMMAND::NEXT_TRANSITION)
        .value(daq::rc::FSMCommands::SUB_TRANSITION_CMD.c_str(), daq::rc::FSM_COMMAND::SUB_TRANSITION)
    ;
        enum_<daq::rc::RC_COMMAND>("RC_COMMAND")
        .value(daq::rc::RunControlCommands::MAKE_TRANSITION_CMD.c_str(), daq::rc::RC_COMMAND::MAKE_TRANSITION)
        .value(daq::rc::RunControlCommands::ENABLE_CMD.c_str(), daq::rc::RC_COMMAND::ENABLE)
        .value(daq::rc::RunControlCommands::DISABLE_CMD.c_str(), daq::rc::RC_COMMAND::DISABLE)
        .value(daq::rc::RunControlCommands::IGNORE_ERROR_CMD.c_str(), daq::rc::RC_COMMAND::IGNORE_ERROR)
        .value(daq::rc::RunControlCommands::STOPAPP_CMD.c_str(), daq::rc::RC_COMMAND::STOPAPP)
        .value(daq::rc::RunControlCommands::STARTAPP_CMD.c_str(), daq::rc::RC_COMMAND::STARTAPP)
        .value(daq::rc::RunControlCommands::RESTARTAPP_CMD.c_str(), daq::rc::RC_COMMAND::RESTARTAPP)
        .value(daq::rc::RunControlCommands::PUBLISH_CMD.c_str(), daq::rc::RC_COMMAND::PUBLISH)
        .value(daq::rc::RunControlCommands::PUBLISHSTATS_CMD.c_str(), daq::rc::RC_COMMAND::PUBLISHSTATS)
        .value(daq::rc::RunControlCommands::EXIT_CMD.c_str(), daq::rc::RC_COMMAND::EXIT)
        .value(daq::rc::RunControlCommands::USER_CMD.c_str(), daq::rc::RC_COMMAND::USER)
        .value(daq::rc::RunControlCommands::DYN_RESTART_CMD.c_str(), daq::rc::RC_COMMAND::DYN_RESTART)
        .value(daq::rc::RunControlCommands::CHANGE_PROBE_INTERVAL_CMD.c_str(), daq::rc::RC_COMMAND::CHANGE_PROBE_INTERVAL)
        .value(daq::rc::RunControlCommands::CHANGE_FULLSTATS_INTERVAL_CMD.c_str(), daq::rc::RC_COMMAND::CHANGE_FULLSTATS_INTERVAL)
        .value(daq::rc::RunControlCommands::UPDATE_CHILD_STATUS_CMD.c_str(), daq::rc::RC_COMMAND::UPDATE_CHILD_STATUS)
        .value(daq::rc::RunControlCommands::TESTAPP_CMD.c_str(), daq::rc::RC_COMMAND::TESTAPP)
   ;
        class_<daq::rc::FSMStates, boost::noncopyable>("FSMStates", no_init)
        .def("stringToState", &daq::rc::FSMStates::stringToState)
        .staticmethod("stringToState")
    ;
        class_<daq::rc::FSMCommands, boost::noncopyable >("FSMCommands", no_init)
        .def("stringToCommand", &daq::rc::FSMCommands::stringToCommand)
        .staticmethod("stringToCommand")
    ;
        class_<daq::rc::RunControlCommands, boost::noncopyable >("RunControlCommands", no_init)
        .def("stringToCommand", &daq::rc::RunControlCommands::stringToCommand)
        .staticmethod("stringToCommand")
    ;

        class_<daq::rc::RunControlBasicCommand, bases<> >("RunControlBasicCommand", no_init)
        .def("toString", &daq::rc::RunControlBasicCommand::toString)
        .def("name", &daq::rc::RunControlBasicCommand::name)
        .def("serialize", &daq::rc::RunControlBasicCommand::serialize)
        .def("uid", &daq::rc::RunControlBasicCommand::uid)
        .def("arguments", &daq::rc::RunControlBasicCommand::arguments)
    ;

        class_<daq::rc::TransitionCmd, bases<daq::rc::RunControlBasicCommand>, boost::shared_ptr<daq::rc::TransitionCmd>  > ("TransitionCmd", init<const std::string&>())
        .def(init<daq::rc::FSM_COMMAND, optional<bool>>())
        .def("fsmCommand", (std::string (daq::rc::TransitionCmd::*)() const)&daq::rc::TransitionCmd::fsmCommand)
        .def("fsmSourceState", (std::string (daq::rc::TransitionCmd::*)() const)&daq::rc::TransitionCmd::fsmSourceState)
        .def("fsmDestinationState", (std::string (daq::rc::TransitionCmd::*)() const)&daq::rc::TransitionCmd::fsmDestinationState)
        .def("isRestart", (bool (daq::rc::TransitionCmd::*)() const)&daq::rc::TransitionCmd::isRestart)
        .def("subTransitionsDone", (bool (daq::rc::TransitionCmd::*)() const)&daq::rc::TransitionCmd::subTransitionsDone)
        .def("skipSubTransitions", (bool (daq::rc::TransitionCmd::*)() const)&daq::rc::TransitionCmd::skipSubTransitions)
        .def("subTransitions", (std::vector<std::string> (daq::rc::TransitionCmd::*)() const)&daq::rc::TransitionCmd::subTransitions)
    ;
        class_<daq::rc::SubTransitionCmd, bases<daq::rc::TransitionCmd>, boost::shared_ptr<daq::rc::SubTransitionCmd> > ("SubTransitionCmd", init<const std::string&>())
        .def(init< const std::string&, daq::rc::FSM_COMMAND >())
        .def("subTransition", (std::string (daq::rc::SubTransitionCmd::*)() const)&daq::rc::SubTransitionCmd::subTransition)
        .def("mainTransitionCmd", (std::string (daq::rc::SubTransitionCmd::*)() const)&daq::rc::SubTransitionCmd::mainTransitionCmd)
    ;

        class_<daq::rc::ResynchCmd, bases<daq::rc::TransitionCmd>, boost::shared_ptr<daq::rc::ResynchCmd> > ("ResynchCmd", init<const std::string&>())
        .def(init< std::uint32_t, std::uint32_t, const std::vector<std::string>& >())
        .def("ecrCounts", (std::uint32_t (daq::rc::ResynchCmd::*)() const)&daq::rc::ResynchCmd::ecrCounts)
        .def("extendedL1ID", (std::uint32_t (daq::rc::ResynchCmd::*)() const)&daq::rc::ResynchCmd::extendedL1ID)
        .def("modules", (std::vector<std::string> (daq::rc::ResynchCmd::*)() const)&daq::rc::ResynchCmd::modules)
    ; 

        class_<daq::rc::UserBroadcastCmd, bases<daq::rc::TransitionCmd>, boost::shared_ptr<daq::rc::UserBroadcastCmd> > ("UserBroadcastCmd", init<const std::string&>())
        .def(init< const daq::rc::UserCmd& >())
    ;

        class_<daq::rc::TestAppCmd, bases<daq::rc::RunControlBasicCommand>, boost::shared_ptr<daq::rc::TestAppCmd> > ("TestAppCmd", init<const std::vector<std::string>&>())
        .def("level", (void (daq::rc::TestAppCmd::*)(int))&daq::rc::TestAppCmd::level)
        .def("level", (int (daq::rc::TestAppCmd::*)()const)&daq::rc::TestAppCmd::level)
        .def("scope", (void (daq::rc::TestAppCmd::*)(const std::string&))&daq::rc::TestAppCmd::scope)
        .def("scope", (std::string (daq::rc::TestAppCmd::*)()const)&daq::rc::TestAppCmd::scope)
    ;

        class_<daq::rc::UserCmd, bases<daq::rc::RunControlBasicCommand>, boost::shared_ptr<daq::rc::UserCmd> > ("UserCmd", init<const std::string&>())
        .def(init<const std::string&, const std::vector<std::string>&>())
        .def("currentFSMState", (std::string (daq::rc::UserCmd::*)() const)&daq::rc::UserCmd::currentFSMState)
        .def("commandName", (std::string (daq::rc::UserCmd::*)() const)&daq::rc::UserCmd::commandName)
        .def("commandParameters", (std::vector<std::string> (daq::rc::UserCmd::*)() const)&daq::rc::UserCmd::commandParameters)
    ;
    	class_<daq::rc::ChangeStatusCmd, bases<daq::rc::RunControlBasicCommand>, boost::shared_ptr<daq::rc::ChangeStatusCmd> > ("ChangeStatusCmd", init<const std::string&>())
        .def(init< bool, std::string& >())
        .def(init< bool, const std::vector<std::string>& >())
        .def("isEnable", &daq::rc::ChangeStatusCmd::isEnable)
        .def("components", (std::vector<std::string> (daq::rc::ChangeStatusCmd::*)() const)&daq::rc::ChangeStatusCmd::components)
    ;
    	class_<daq::rc::StartStopAppCmd::is_start_t>("is_start_t");
    	class_<daq::rc::StartStopAppCmd::is_stop_t>("is_stop_t");
    	class_<daq::rc::StartStopAppCmd::is_restart_t>("is_restart_t");
    	class_<daq::rc::StartStopAppCmd, bases<daq::rc::RunControlBasicCommand>, boost::shared_ptr<daq::rc::StartStopAppCmd> > ("StartStopAppCmd", init<const std::string&>())
    	.def(init< daq::rc::StartStopAppCmd::is_start_t, const std::vector<std::string>& >())
        .def(init< daq::rc::StartStopAppCmd::is_stop_t, const std::vector<std::string>& >())
        .def(init< daq::rc::StartStopAppCmd::is_restart_t, const std::vector<std::string>& >())
    	.def("applications", (std::set<std::string> (daq::rc::StartStopAppCmd::*)() const)&daq::rc::StartStopAppCmd::applications)
    	.def("currentState", (std::string (daq::rc::StartStopAppCmd::*)() const)&daq::rc::StartStopAppCmd::currentState)
        .def("destinationState", (std::string (daq::rc::StartStopAppCmd::*)() const)&daq::rc::StartStopAppCmd::destinationState)
        .def("transitionCommand", (std::string (daq::rc::StartStopAppCmd::*)() const)&daq::rc::StartStopAppCmd::transitionCommand)
        .def("isRestart", &daq::rc::StartStopAppCmd::isRestart)
        .def("isStart", &daq::rc::StartStopAppCmd::isStart)
    ;

    	class_<daq::rc::CommandSender::ErrorElement, boost::shared_ptr<daq::rc::CommandSender::ErrorElement> > ("ErrorElement", init<const std::string&, const std::string&, const std::string&>())
    	.def("applicationName", &daq::rc::CommandSender::ErrorElement::applicationName, return_value_policy<copy_const_reference>())
    	.def("errorDescription", &daq::rc::CommandSender::ErrorElement::errorDescription, return_value_policy<copy_const_reference>())
    	.def("errorType", &daq::rc::CommandSender::ErrorElement::errorType, return_value_policy<copy_const_reference>())
    ;
    	class_<daq::rc::CommandSender, boost::shared_ptr<daq::rc::CommandSender> > ("CommandSender", init<const std::string&, const std::string&>())
    	.def(init< const IPCPartition&, const std::string&>())
    	.def("makeTransition", (void (daq::rc::CommandSender::*)(const std::string&, const daq::rc::TransitionCmd&) const)&daq::rc::CommandSender::makeTransition)
        .def("makeTransition", (void (daq::rc::CommandSender::*)(const std::string&, const daq::rc::FSM_COMMAND) const)&daq::rc::CommandSender::makeTransition)
        .def("userCommand", (void (daq::rc::CommandSender::*)(const std::string&, const daq::rc::UserCmd&) const)&daq::rc::CommandSender::userCommand)
    	.def("userCommand", (void (daq::rc::CommandSender::*)(const std::string&, const std::string&, const std::vector<std::string>&) const)&daq::rc::CommandSender::userCommand)
    	.def("ignoreError", &daq::rc::CommandSender::ignoreError )
    	.def("changeStatus", (void (daq::rc::CommandSender::*)(const std::string&, const daq::rc::ChangeStatusCmd&) const)&daq::rc::CommandSender::changeStatus)
    	.def("changeStatus", (void (daq::rc::CommandSender::*)(const std::string&, bool, const std::vector<std::string>&) const)&daq::rc::CommandSender::changeStatus)
    	.def("startStopApplications", &daq::rc::CommandSender::startStopApplications)
    	.def("startApplications", &daq::rc::CommandSender::startApplications)
    	.def("stopApplications", &daq::rc::CommandSender::stopApplications)
    	.def("restartApplications", &daq::rc::CommandSender::restartApplications)
    	.def("publish", &daq::rc::CommandSender::publish)
    	.def("changePublishInterval", &daq::rc::CommandSender::changePublishInterval)
    	.def("publishStats", &daq::rc::CommandSender::publishStats)
    	.def("changeFullStatsInterval", &daq::rc::CommandSender::changeFullStatsInterval)
    	.def("debugLevel", (int (daq::rc::CommandSender::*)(const std::string&) const)&daq::rc::CommandSender::debugLevel)
        .def("debugLevel", (void (daq::rc::CommandSender::*)(const std::string&, int) const)&daq::rc::CommandSender::debugLevel)
        .def("testApp", (void (daq::rc::CommandSender::*)(const std::string&, const daq::rc::TestAppCmd&) const)&daq::rc::CommandSender::testApp)
        .def("testApp", (void (daq::rc::CommandSender::*)(const std::string&, const std::vector<std::string>&) const)&daq::rc::CommandSender::testApp)
        .def("exit", &daq::rc::CommandSender::exit)
        .def("sendCommand", (bool (daq::rc::CommandSender::*)(const std::string&, daq::rc::RunControlBasicCommand&, unsigned long) const)&daq::rc::CommandSender::sendCommand)
    ;

        class_<ControllableWrapper, bases<>, boost::shared_ptr<ControllableWrapper>, boost::noncopyable >("Controllable")
        .def("configure", &daq::rc::Controllable::configure, &ControllableWrapper::default_configure)
        .def("connect", &daq::rc::Controllable::connect, &ControllableWrapper::default_connect)
        .def("prepareForRun", &daq::rc::Controllable::prepareForRun, &ControllableWrapper::default_prapareForRun)
        .def("stopROIB", &daq::rc::Controllable::stopROIB, &ControllableWrapper::default_stopROIB)
        .def("stopDC", &daq::rc::Controllable::stopDC, &ControllableWrapper::default_stopDC)
        .def("stopHLT", &daq::rc::Controllable::stopHLT, &ControllableWrapper::default_stopHLT)
        .def("stopRecording", &daq::rc::Controllable::stopRecording, &ControllableWrapper::default_stopRecording)
        .def("stopGathering", &daq::rc::Controllable::stopGathering, &ControllableWrapper::default_stopGathering)
        .def("stopArchiving", &daq::rc::Controllable::stopArchiving, &ControllableWrapper::default_stopArchiving)
        .def("disconnect", &daq::rc::Controllable::disconnect, &ControllableWrapper::default_disconnect)
        .def("unconfigure", &daq::rc::Controllable::unconfigure, &ControllableWrapper::default_unconfigure)
        .def("subTransition", &daq::rc::Controllable::subTransition, &ControllableWrapper::default_subTransition)
        .def("resynch", &daq::rc::Controllable::resynch, &ControllableWrapper::default_resynch)
        .def("publish", &daq::rc::Controllable::publish, &ControllableWrapper::default_publish)
        .def("publishFullStats", &daq::rc::Controllable::publishFullStats, &ControllableWrapper::default_publishFullStats)
        .def("user", &daq::rc::Controllable::user, &ControllableWrapper::default_user)
        .def("disable", &daq::rc::Controllable::disable, &ControllableWrapper::default_disable)
        .def("enable", &daq::rc::Controllable::enable, &ControllableWrapper::default_enable)
        .def("onExit", &daq::rc::Controllable::onExit, &ControllableWrapper::default_onExit)
    ;
}
