/*
 * RunControllerFSMOperations.cxx
 *
 *  Created on: Jan 9, 2013
 *      Author: avolio
 */

#include "RunControl/Controller/RunControllerFSMOperations.h"
#include "RunControl/Controller/DVSController.h"
#include "RunControl/Controller/ApplicationController.h"
#include "RunControl/Controller/ApplicationList.h"
#include "RunControl/Controller/Application.h"
#include "RunControl/Controller/ApplicationUpdateNotifier.h"
#include "RunControl/Controller/ApplicationSupervisorInfo.h"
#include "RunControl/Controller/ApplicationSelfInfo.h"
#include "RunControl/Controller/InformationPublisher.h"
#include "RunControl/FSM/FSMStates.h"
#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/ThreadPool.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/RunControlBasicCommand.h"

#include <ers/Assertion.h>
#include <ers/Issue.h>
#include <ers/LocalContext.h>
#include <ers/ers.h>

#include <boost/scope_exit.hpp>
#include <tbb/concurrent_unordered_map.h>

#include <chrono>
#include <thread>
#include <string>
#include <utility>
#include <algorithm>
#include <random>
#include <future>
#include <iterator>
#include <ostream>
#include <set>
#include <vector>
#include <memory>


namespace daq { namespace rc {

RunControllerFSMOperations::RunControllerFSMOperations(const std::shared_ptr<ApplicationController>& appCtrl,
                                                       const std::shared_ptr<DVSController>& dvsCtrl,
                                                       const std::shared_ptr<ApplicationList>& appList,
                                                       const std::shared_ptr<InformationPublisher>& infoPublisher)
    :   RunControlTransitionActions(), m_is_interrupted(false), m_app_ctrl(appCtrl), m_dvs_ctrl(dvsCtrl), m_app_list(appList),
        m_info_publisher(infoPublisher), m_cmd_sender(OnlineServices::instance().getIPCPartition(), OnlineServices::instance().applicationName()),
        m_tp(new ThreadPool(5))
{
}

RunControllerFSMOperations::~RunControllerFSMOperations() noexcept
{
}

void RunControllerFSMOperations::enterFSM(const TransitionCmd& cmd) {
    performTransition(cmd, false, false);
}

void RunControllerFSMOperations::initialize(const TransitionCmd& cmd) {
    performTransition(cmd, false, false);
}

void RunControllerFSMOperations::configure(const TransitionCmd& cmd) {
    performTransition(cmd);
}

void RunControllerFSMOperations::connect(const TransitionCmd& cmd) {
    performTransition(cmd);
}

void RunControllerFSMOperations::prepareForRun(const TransitionCmd& cmd) {
    performTransition(cmd);
}

void RunControllerFSMOperations::stopROIB(const TransitionCmd& cmd) {
    performTransition(cmd);
}

void RunControllerFSMOperations::stopDC(const TransitionCmd& cmd) {
    performTransition(cmd);
}

void RunControllerFSMOperations::stopHLT(const TransitionCmd& cmd) {
    performTransition(cmd);
}

void RunControllerFSMOperations::stopRecording(const TransitionCmd& cmd) {
    performTransition(cmd);
}

void RunControllerFSMOperations::stopGathering(const TransitionCmd& cmd) {
    performTransition(cmd);
}

void RunControllerFSMOperations::stopArchiving(const TransitionCmd& cmd) {
    performTransition(cmd);
}

void RunControllerFSMOperations::disconnect(const TransitionCmd& cmd) {
    performTransition(cmd);
}

void RunControllerFSMOperations::unconfigure(const TransitionCmd& cmd) {
    performTransition(cmd);
}

void RunControllerFSMOperations::subTransition(const SubTransitionCmd& cmd) {
    performTransition(cmd);
}

void RunControllerFSMOperations::shutdown(const TransitionCmd& cmd) {
    performTransition(cmd, false, false);
}

void RunControllerFSMOperations::userBroadcast(const UserBroadcastCmd& cmd) {
    // Self-transition: source and destination states are the same
    FSM_STATE currState = FSMStates::stringToState(cmd.fsmSourceState());
    switch(currState) {
        case FSM_STATE::NONE:
        {
            ERS_LOG("The UserBroadcast command is not propagated to any child because the current FSM state is NONE");
            break;
        }
        default:
        {
            performTransition(cmd);
            break;
        }
    }
}

void RunControllerFSMOperations::resynch(const ResynchCmd& cmd) {
    performTransition(cmd);
}

void RunControllerFSMOperations::reloadDB(const TransitionCmd& cmd) {
    // Reload supported only in NONE
    ERS_ASSERT(cmd.fsmSourceState() == FSMStates::NONE_STATE);

    try {
        // Here basically we reset the converters
        OnlineServices::instance().reloadConfiguration(false);

        // Build the new application list
        ApplicationList newAppList;
        newAppList.load(OnlineServices::instance().getConfiguration(), OnlineServices::instance().getPartition(), OnlineServices::instance().segmentName());

        ERS_DEBUG(0, "The new application list has been created");

        // Get all the applications in the current application list
        const auto& oldApps = m_app_list->getAllApplications();

        // Divide the applications into four categories:
        // Applications removed: they are not part any more of the application list
        // Applications added: these are new applications
        // Applications kept: applications still present after the database reload
        // Applications to be restarted: applications whose running host has been changed and it is not even part of the back-up hosts
        Application::ApplicationSet removedApps;
        Application::ApplicationSet addedApps;
        Application::ApplicationSet keptApps;
        Application::ApplicationSet restartApps;

        auto appChanged = [](const std::shared_ptr<Application>& oldApp_, const std::shared_ptr<Application>& newApp_) -> bool
                            {
                                return (oldApp_->getEnvironment() != newApp_->getEnvironment() ||
                                        oldApp_->getParameters() != newApp_->getParameters() ||
                                        oldApp_->getRestartParameters() != newApp_->getRestartParameters());
                            };

        auto hostChanged = [](const std::shared_ptr<Application>& oldApp_, const std::shared_ptr<Application>& newApp_) -> bool
                             {
                                const std::string& currentRunningHost = oldApp_->getApplicationSVInfo().getRunningHost();

                                std::set<std::string> newHosts = newApp_->getBackupHosts();
                                newHosts.insert(newApp_->getHost());

                                return (newHosts.find(currentRunningHost) == newHosts.end());
                             };

        // Loop over the current list in order to identify applications been removed or kept
        for(const auto& oldApp_ : oldApps) {
            const auto& newApp_ = newAppList.getApplication(oldApp_->getName());
            if(newApp_.get() != nullptr) {
                keptApps.insert(oldApp_);

                // Update the dynamic part of the info: starting from now the Applications
                // in the new list share the dynamic information with the Applications in the
                // old list: in such a way no information is lost when the lists are swapped
                // and there is no additional need of synchronization
                newApp_->update(*oldApp_);

                // Restarting the Online segment applications is incredibly dangerous...
                if((newApp_->type() == ApplicationType::ONLINE_SEGMENT_INFRASTRUCTURE) && (appChanged(oldApp_, newApp_) == true)) {
                    const std::string& msg = std::string("this is an online segment infrastructure application whose description has been modified ") +
                                                         "with the database reload; the partition needs to be restarted if the changes have to be applied";
                    ers::warning(daq::rc::ApplicationGenericIssue(ERS_HERE, newApp_->getName(), msg));
                } else if((hostChanged(oldApp_, newApp_) == true) && (oldApp_->getStartAt() == FSMCommands::INIT_FSM_CMD)) {
                    const std::string& msg = std::string("this is an application whose running host has been modified ") +
                                                         "with the database reload and is not part anymore of the back-up host set. It will be restarted";
                    ers::warning(daq::rc::ApplicationGenericIssue(ERS_HERE, newApp_->getName(), msg));

                    restartApps.insert(oldApp_);
                }
             } else {
                removedApps.insert(oldApp_);
            }
        }

#ifndef ERS_NO_DEBUG
        auto printApps = [](const Application::ApplicationSet& apps) -> std::string
                           {
                                std::string s;
                                for(const auto& app_ : apps) {
                                    s += app_->getName() + " ";
                                }
                                return s;
                           };
#endif

        ERS_DEBUG(1, "Removed applications:\n" << printApps(removedApps));
        ERS_DEBUG(1, "Kept applications:\n" << printApps(keptApps));
        ERS_DEBUG(1, "Applications to be restarted:\n" << printApps(restartApps));

        // Now determine the new applications
        const auto& newApps = newAppList.getAllApplications();
        std::set_difference(newApps.begin(), newApps.end(),
                            keptApps.begin(), keptApps.end(),
                            std::inserter(addedApps, addedApps.begin()),
                            addedApps.key_comp());

        ERS_DEBUG(1, "New applications:\n" << printApps(addedApps));

        // 1. Stop applications: they are the ones been removed
        {
            if(removedApps.empty() == false) {
                std::vector<std::string> av;
                for(const auto& app_ : removedApps) {
                    av.push_back(app_->getName());
                }

                StartStopAppCmd stopCmd(StartStopAppCmd::is_stop_app_cmd, av);
                stopCmd.currentState(FSMStates::stringToState(cmd.fsmSourceState()));
                stopCmd.destinationState(FSMStates::stringToState(cmd.fsmDestinationState()));
                stopCmd.transitionCommand(FSMCommands::stringToCommand(cmd.fsmCommand()));

                const auto& reallyStoppedApps = m_app_ctrl->stopApps(stopCmd);

                // Remove the information of removed applications from IS
                for(const auto& app_ : reallyStoppedApps) {
                    m_info_publisher->removeProcessInfo(app_->getName());
                }

                if(removedApps != reallyStoppedApps) {
                    // TODO: this is probably too much
                    throw daq::rc::DBReloadFailure(ERS_HERE, "some no more existing applications could not be properly stopped");
                }

                ERS_DEBUG(0, "Removed applications have been stopped");
            }
        }

        // 2. Applications to be started: they are the one that have been added
        //
        // NOTE: swap the application list before, otherwise new applications will be unknown!!!
        m_app_list->swap(newAppList);

        // Reload the DVS core, so that new applications can be properly tested
        m_dvs_ctrl->reload(addedApps, removedApps);

        {
            std::vector<std::string> av;
            for(const auto& app_ : addedApps) {
                // This works only because the reload is done in NONE (and then only by the Root)
                // If this assumption is no more valid, then the applications should be started
                // taking into account their types (i.e., first the OS, then infrastructure, etc)
                // and the state in which the reload is done (in NONE the only transition done is INIT_FSM).
                if(app_->getStartAt() == FSMCommands::INIT_FSM_CMD) {
                    av.push_back(app_->getName());
                }
            }

            if(av.empty() == false) {
                StartStopAppCmd startCmd(StartStopAppCmd::is_start_app_cmd, av);
                startCmd.currentState(FSMStates::stringToState(cmd.fsmSourceState()));
                startCmd.destinationState(FSMStates::stringToState(cmd.fsmDestinationState()));
                startCmd.transitionCommand(FSMCommands::stringToCommand(cmd.fsmCommand()));

                m_app_ctrl->startApps(startCmd);

                ERS_DEBUG(0, "New added applications have been started");
            }
        }

        // 3. Applications to be restarted
        {
            std::vector<std::string> av;
            for(const auto& app_ : restartApps) {
                av.push_back(app_->getName());
            }

            if(av.empty() == false) {
                StartStopAppCmd restartCmd(StartStopAppCmd::is_restart_app_cmd, av);
                restartCmd.currentState(FSMStates::stringToState(cmd.fsmSourceState()));
                restartCmd.destinationState(FSMStates::stringToState(cmd.fsmDestinationState()));
                restartCmd.transitionCommand(FSMCommands::stringToCommand(cmd.fsmCommand()));

                m_app_ctrl->restartApps(restartCmd);

                ERS_DEBUG(0, "Applications have been restarted");
            }
        }
    }
    catch(daq::rc::DBReloadFailure&) {
        throw;
    }
    catch(ers::Issue& ex) {
        throw daq::rc::DBReloadFailure(ERS_HERE, ex.message(), ex);
    }
}

void RunControllerFSMOperations::performTransition(const TransitionCmd& trCmd, bool toApplications, bool toControllers) {
    // Make a shared pointer out of it
    // The transition command must be passed few times as a copy and the pointer avoids slicing problems
    std::shared_ptr<const TransitionCmd> cmd(trCmd.clone());
    ERS_ASSERT(trCmd == *cmd);

    if(cmd->isRestart() == false) {
        std::vector<std::future<void>> tasks;
        BOOST_SCOPE_EXIT(&tasks)
        {
            for(auto& t : tasks) {
                try {
                    t.get();
                }
                catch(std::exception&) {
                }
            }
        }
        BOOST_SCOPE_EXIT_END

        try {
            // Keep track of the previous transition command
            typedef tbb::concurrent_unordered_map<std::string, std::shared_ptr<const TransitionCmd>> TransitionMap;
            std::shared_ptr<TransitionMap> lastTrCmdMap(std::make_shared<TransitionMap>());

            // Applications that receive the transition command
            struct AppTransitionStatus {
                std::shared_ptr<Application> m_app;
                std::atomic_bool m_propogate_transition;
                std::atomic_bool m_transition_done;
            };

            typedef tbb::concurrent_unordered_map<std::string, std::shared_ptr<AppTransitionStatus>> ApplicationStatusMap;
            std::shared_ptr<ApplicationStatusMap> apps(std::make_shared<ApplicationStatusMap>());

            if(toControllers == true) {
                const auto& cc = m_app_list->getApplications(ApplicationType::CHILD_CONTROLLER);
                for(const auto& app : cc) {
                    apps->emplace(app->getName(), std::make_shared<AppTransitionStatus>(app, true, false));
                }
            }

            if(toApplications == true) {
                const auto& rc = m_app_list->getApplications(ApplicationType::RUN_CONTROL);
                for(const auto& app : rc) {
                    apps->emplace(app->getName(), std::make_shared<AppTransitionStatus>(app, true, false));
                }
            }

            // Applications that are started during the transition
            struct StartedAppStatus {
                std::shared_ptr<Application> m_app;
                std::atomic_bool m_start_completed;
            };

            typedef tbb::concurrent_unordered_map<std::string, std::shared_ptr<StartedAppStatus>> StartedAppsMap;
            std::shared_ptr<StartedAppsMap> appsToStart(std::make_shared<StartedAppsMap>());

            for(const auto& app : m_app_list->getApplications(cmd->fsmCommand(), true)) {
                const std::string& appName = app->getName();
                appsToStart->emplace(appName, std::make_shared<StartedAppStatus>(app, false));

                // Do not propagate the transition to the just started applications
                // (they reach the proper state by themselves)
                const auto& it = apps->find(appName);
                if(it != apps->end()) {
                    it->second->m_propogate_transition = false;
                }
            }

            // This lambda checks whether an application has been properly started
            auto applicationStarted = [cmd](const std::string& appName_,
                                            const ApplicationSVInfo& appSVInfo,
                                            const ApplicationSelfInfo& appSelfInfo,
                                            ApplicationType appType) -> bool
            {
                bool isAppOK = false;

                ERS_DEBUG(3, "Application " << appName_ << " is in the FSM state " << FSMStates::stateToString(appSelfInfo.getFSMState()) <<
                              " with membership " << appSVInfo.isMembership() << " and its transitioning state is " << appSelfInfo.isTransitioning());

                if(appSVInfo.isMembership() == false) {
                    // If the app is out of membership, then just forget about it
                    isAppOK = true;
                } else {
                    switch(appType) {
                        case ApplicationType::CHILD_CONTROLLER:
                        case ApplicationType::RUN_CONTROL: {
                            // RC apps and controllers are requested to be in the right state, not transitioning,
                            // not in error and with their test status equal to PASSED
                            if((FSMStates::stateToString(appSelfInfo.getFSMState()) == cmd->fsmDestinationState()) &&
                                (appSelfInfo.isTransitioning() == false) &&
                                (appSelfInfo.isInternalError() == false) &&
                                (appSVInfo.isRestarting() == false))
                            {
                                isAppOK = true;
                            }

                            break;
                        }
                        default: {
                            isAppOK = true;
                            break;
                        }
                    }
                }

                if(isAppOK == true) {
                    ERS_DEBUG(2, "Start procedure for application " << appName_ << " is done");
                }

                return isAppOK;
            };

            // This lambda propagates the transition command
            auto dispatch = [&lastTrCmdMap, &tasks, &cmd, this](const std::shared_ptr<Application>& app_) -> bool
                            {
                                interrupted(*cmd);

                                // Decide whether to propagate or not the command to the application
                                // The command must not be propagated if:
                                // - the application is out of membership
                                // - the application has already executed or is being executed the same command (for instance
                                //   a controller dying in the middle of a transition does not know the applications its previous
                                //   instance sent a command to).
                                const bool membership = app_->getApplicationSVInfo().isMembership();

                                const ApplicationSelfInfo& appInfo = app_->getApplicationSelfInfo();
                                const auto& appCurrTrCmd = appInfo.getCurrentTransitionCommand();
                                const auto& cmdToCmpr = (appCurrTrCmd.get() != nullptr) ? appCurrTrCmd : appInfo.getLastTransitionCommand();
                                const bool alreadyDone = (cmdToCmpr.get() == nullptr) ? false : ((*cmdToCmpr == *cmd) && (cmdToCmpr->uid() == cmd->uid()));

                                const bool propagate = ((membership == true) && (alreadyDone == false));

                                if(propagate == true) {
                                    // Take note of the last transition done before sending the new transition command
                                    lastTrCmdMap->emplace(app_->getName(), appInfo.getLastTransitionCommand());

                                    auto&& fut = m_tp->submit<void>([this, app_, cmd]()
                                                                    {
                                                                        short nRetries = 2;

                                                                        // The membership is checked again in order to avoid some command to
                                                                        // be re-sent even if the application has been put out of membership
                                                                        while((app_->getApplicationSVInfo().isMembership() == true) && (nRetries-- > 0)) {
                                                                            try {
                                                                                 interrupted(*cmd);

                                                                                 m_cmd_sender.makeTransition(app_->getName(), *cmd);

                                                                                 ERS_DEBUG(1, "Transition command " << cmd->fsmCommand() <<
                                                                                              " sent to application " << app_->getName());

                                                                                 break;
                                                                            }
                                                                            catch(daq::rc::InterruptedException&) {
                                                                                // Just stop doing it...
                                                                                break;
                                                                            }
                                                                            catch(ers::Issue& ex) {
                                                                                daq::rc::TransitionPropagationFailure issue(ERS_HERE,
                                                                                                                            cmd->fsmCommand(),
                                                                                                                            app_->getName(),
                                                                                                                            ex);

                                                                                if(nRetries == 0) {
                                                                                    ers::error(issue);
                                                                                } else {
                                                                                    issue.wrap_message("", ". Another attempt will be done.");
                                                                                    ers::warning(issue);

                                                                                    std::this_thread::sleep_for(std::chrono::milliseconds(50));
                                                                                }
                                                                            }
                                                                        }
                                                                    });

                                    tasks.push_back(std::move(fut));
                                } else {
                                    if(membership == false) {
                                        ERS_DEBUG(1, "Transition command " << cmd->fsmCommand() << " not sent to application " << app_->getName() << " because out of membership");
                                    } else if(alreadyDone) {
                                        ERS_LOG("Transition command " << cmd->fsmCommand() << " not sent to application " <<
                                                app_->getName() << " because already executed or being executed");
                                    }
                                }

                                return propagate;
                            };

            // This lambda checks whether the transition is completed
            auto done = [lastTrCmdMap, cmd](const std::string& appName_,
                                            const ApplicationSVInfo& appSVInfo,
                                            const ApplicationSelfInfo& appSelfInfo) -> bool
                        {

                            ERS_DEBUG(3, "Application " << appName_ << " with membership " << appSVInfo.isMembership() <<
                                         " is in the FSM state " << FSMStates::stateToString(appSelfInfo.getFSMState()) <<
                                         " and its transitioning state is " << appSelfInfo.isTransitioning());

                            // For a transition between two different states, the transition is considered as done if the application:
                            // - is OUT of membership
                            // OR
                            // - has reached the destination state;
                            // - is no more transitioning;
                            // - is not in error;
                            // - is not in the restarting phase;
                            //
                            // For a self transition (i.e., a transition that does not change the FSM state), the transition is considered
                            // as done if the application:
                            // - is OUT of membership
                            // OR
                            // - is no more transitioning or is transitioning and the current application transition is not the same as this transition;
                            // - is not in error;
                            // - is not in the restarting phase;
                            // - the application last executed transition command is not null (it can be null for the first received command)
                            //   and equal to this transition command;
                            // - the application previous last executed command:
                            //      - null (this is the very first transition command received by the application);
                            //      - different (semantically) from the current last received command or with a different id (this removes the possibility
                            //        to have a false positive if the same command is issued two times sequentially).

                            const FSM_STATE trSrc = FSMStates::stringToState(cmd->fsmSourceState());
                            const FSM_STATE trDst = FSMStates::stringToState(cmd->fsmDestinationState());

                            auto transitionCompleted = [&appSVInfo, &appSelfInfo, trDst]() -> bool
                                                       {
                                                           return ((appSelfInfo.isTransitioning() == false) &&
                                                                   (appSelfInfo.getFSMState() == trDst) &&
                                                                   (appSVInfo.isRestarting() == false) &&
                                                                   (appSelfInfo.isInternalError() == false)
                                                                  );
                                                       };

                            auto inStateTransitionCompleted = [&lastTrCmdMap, &appSVInfo, &appSelfInfo, &cmd, &appName_, trDst]() -> bool
                                                              {
                                                                  const TransitionCmd* oldLastTrCmd = nullptr;

                                                                  const auto& it = lastTrCmdMap->find(appName_);
                                                                  if(it != lastTrCmdMap->end()) {
                                                                      oldLastTrCmd = it->second.get();
                                                                      const TransitionCmd* const newLastTrCmd = appSelfInfo.getLastTransitionCommand().get();
                                                                      const TransitionCmd* const currentTrCmd = appSelfInfo.getCurrentTransitionCommand().get();

                                                                      // At a first glance it may look that the check on the UID may be sufficient
                                                                      // But in that case we are more exposed to clashes in the UID: two commands may actually be
                                                                      // semantically different but with the same UID (although the probability to have
                                                                      // two identical UIDs during the life of the applications is really null)

                                                                      bool result = ((appSVInfo.isRestarting() == false) &&
                                                                                     (appSelfInfo.isInternalError() == false) &&
                                                                                     (appSelfInfo.getFSMState() == trDst) &&
                                                                                     (newLastTrCmd != nullptr) &&
                                                                                     (*newLastTrCmd == *cmd) &&
                                                                                     // See https://its.cern.ch/jira/browse/ATDAQCCRC-50 for this condition
                                                                                     ((appSelfInfo.isTransitioning() == false) || ((currentTrCmd != nullptr) && (!(*currentTrCmd == *cmd) || (currentTrCmd->uid() != cmd->uid())))) &&
                                                                                     ((oldLastTrCmd == nullptr) ? true : ((!(*oldLastTrCmd == *newLastTrCmd)) || (oldLastTrCmd->uid() != newLastTrCmd->uid())))
                                                                      );

#ifndef ERS_NO_DEBUG
                                                                      std::ostringstream s;
                                                                      s << appName_ << "-" << " Restarting: " << appSVInfo.isRestarting()
                                                                                           << " Internal Error: " << appSelfInfo.isInternalError()
                                                                                           << " FSM State: " << FSMStates::stateToString(appSelfInfo.getFSMState())
                                                                                           << " newLastTrCmd: " << (newLastTrCmd == nullptr ? "null" : newLastTrCmd->toString())
                                                                                           << " Transitioning: " << appSelfInfo.isTransitioning()
                                                                                           << " currentTrCmd: " << (currentTrCmd == nullptr ? "null" : currentTrCmd->toString())
                                                                                           << " oldLastTrCmd: " << (oldLastTrCmd == nullptr ? "null" : oldLastTrCmd->toString())
                                                                                           << " cmd: " << cmd->toString()
                                                                                           << " Result: " << result
                                                                                           << " Cond 1: " << (appSVInfo.isRestarting() == false)
                                                                                           << " Cond 2: " << (appSelfInfo.isInternalError() == false)
                                                                                           << " Cond 3: " << (appSelfInfo.getFSMState() == trDst)
                                                                                           << " Cond 4: " << (newLastTrCmd != nullptr)
                                                                                           << " Cond 5: " << (*newLastTrCmd == *cmd)
                                                                                           << " Cond 6: " << ((appSelfInfo.isTransitioning() == false) || ((currentTrCmd != nullptr) && (!(*currentTrCmd == *cmd) || (currentTrCmd->uid() != cmd->uid()))))
                                                                                           << " Cond 7: " << ((oldLastTrCmd == nullptr) ? true : ((!(*oldLastTrCmd == *newLastTrCmd)) || (oldLastTrCmd->uid() != newLastTrCmd->uid())));
                                                                      ERS_DEBUG(1, s.str());
#endif


                                                                      return result;
                                                                  } else {
                                                                      // This just means that a call-back has been received before the transition command has been received
                                                                      ERS_DEBUG(1, appName_ + " oldLastTrCmd not found");

                                                                      return false;
                                                                  }

                                                              };

                            const bool transitionDone = (appSVInfo.isMembership() == false) ||
                                                        ((trDst != trSrc) ? transitionCompleted() : inStateTransitionCompleted());

                            if(transitionDone == true) {
                                ERS_DEBUG(2, "Transition done for application " << appName_);
                            }

                            return transitionDone;
                        };

            ///
            /// 1 --> Start applications
            ///
            interrupted(*cmd);

            ERS_DEBUG(0, "Starting applications for transition " << cmd->fsmCommand());

            // Register call-back to determine when all the applications are properly started
            auto token_start = ApplicationUpdateNotifier::scopedToken(ApplicationUpdateNotifier::instance()
                                                                      .registerCallback([applicationStarted, appsToStart](const std::string& appName_,
                                                                                                                          const ApplicationSVInfo& sv_,
                                                                                                                          const ApplicationSelfInfo& self_)
            {
                const auto& it = appsToStart->find(appName_);
                if((it != appsToStart->end()) &&
                   (it->second->m_start_completed == false) &&
                   (applicationStarted(appName_, sv_, self_, it->second->m_app->type()) == true))
                {
                    it->second->m_start_completed = true;
                }
            }));

            // Remember that this returns only when all the applications to be started
            // have been actually started (or put out of membership)
            m_app_ctrl->startApps(*cmd);

            ERS_DEBUG(1, "Applications for transition " << cmd->fsmCommand() << " are started");

            ///
            /// 2 --> Transition command propagation
            ///
            interrupted(*cmd);

            ERS_DEBUG(0, "Now propagating the transition command " << cmd->fsmCommand());

            // Register call-back to react to changes in the application status and state
            auto token_transition = ApplicationUpdateNotifier::scopedToken(ApplicationUpdateNotifier::instance()
                                                                           .registerCallback([done, apps](const std::string& appName_,
                                                                                                          const ApplicationSVInfo& sv_,
                                                                                                          const ApplicationSelfInfo& self_)
            {
                const auto& it = apps->find(appName_);
                if((it != apps->end()) && (it->second->m_transition_done == false) && (done(appName_, sv_, self_) == true)) {
                    it->second->m_transition_done = true;
                }
            }));

            // In this loop the transition command is sent to all the concerned applications
            for(auto it = apps->begin(); it != apps->end(); ++it) {
                if((it->second->m_propogate_transition == false) || (dispatch(it->second->m_app) == false)) {
                    it->second->m_transition_done = true;
                }
            }

            ///
            /// 3 --> Waiting for the started application to reach the right state
            ///
            ERS_DEBUG(1, "Waiting for started application to reach the right state...");

            bool startDone = false;
            do {
                startDone = true;
                for(auto it = appsToStart->begin(); it != appsToStart->end(); ++it) {
                    startDone = (it->second->m_start_completed ||
                                 (applicationStarted(it->second->m_app->getName(),
                                                     it->second->m_app->getApplicationSVInfo(),
                                                     it->second->m_app->getApplicationSelfInfo(),
                                                     it->second->m_app->type()) &&
                                  ((it->second->m_start_completed = true) == true))) && startDone;
                }

                std::this_thread::sleep_for(std::chrono::milliseconds(1));

                interrupted(*cmd);
            } while(startDone == false);

            ApplicationUpdateNotifier::instance().removeCallback(token_start);

            ///
            /// 4 --> Waiting for the applications that received the transition command to reach the right state
            ///
            ERS_DEBUG(1, "Waiting for applications that received the transition command to reach the right state...");

            bool allDone = false;
            do {
                allDone = true;
                for(auto it = apps->begin(); it != apps->end(); ++it) {
                    allDone = (it->second->m_transition_done ||
                               (done(it->second->m_app->getName(),
                                     it->second->m_app->getApplicationSVInfo(),
                                     it->second->m_app->getApplicationSelfInfo()) &&
                                ((it->second->m_transition_done = true) == true))) && allDone;
                }

                std::this_thread::sleep_for(std::chrono::milliseconds(1));

                interrupted(*cmd);
            } while(allDone == false);

            ApplicationUpdateNotifier::instance().removeCallback(token_transition);

            ERS_DEBUG(0, "The transition " << cmd->fsmCommand() << " has been propagated and all the relevant applications have reached the right state");

            ///
            /// 5 --> Stop applications
            ///
            interrupted(*cmd);
            ERS_DEBUG(0, "Stopping applications for transition " << cmd->fsmCommand());

            m_app_ctrl->stopApps(*cmd);

            ERS_DEBUG(1, "Applications for transition " << cmd->fsmCommand() << " are stopped");
        }
        catch(daq::rc::InterruptedException& ex) {
            throw daq::rc::TransitionInterrupted(ERS_HERE, cmd->fsmCommand(), ex);
        }
    } else {
        ERS_LOG("No actions taken for transition \"" << cmd->fsmCommand() << "\" because the controller is restarting");
    }
}

void RunControllerFSMOperations::interrupt(bool shouldInterrupt) {
    m_is_interrupted = shouldInterrupt;
}

void RunControllerFSMOperations::interrupted(const TransitionCmd& cmd) const {
    if(m_is_interrupted == true) {
        throw daq::rc::TransitionInterrupted(ERS_HERE, cmd.fsmCommand());
    }
}

}}
