/*
 * Application.cxx
 *
 *  Created on: Oct 31, 2012
 *      Author: avolio
 */

#include "RunControl/Controller/Application.h"
#include "RunControl/Controller/ApplicationUpdateNotifier.h"
#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/FSM/FSMStates.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/RunControlBasicCommand.h"
#include "RunControl/Common/RunControlCommands.h"

#include <config/Configuration.h>
#include <config/DalObject.h>
#include <config/Errors.h>
#include <config/map.h>
#include <dal/BaseApplication.h>
#include <dal/CustomLifetimeApplicationBase.h>
#include <dal/Computer.h>
#include <dal/ComputerProgram.h>
#include <dal/Partition.h>
#include <dal/Segment.h>
#include <dal/util.h>
#include <ers/Assertion.h>
#include <ers/Issue.h>
#include <ers/LocalContext.h>
#include <ers/ers.h>

#include <boost/functional/hash.hpp>

#include <utility>
#include <mutex>
#include <ostream>


namespace daq { namespace rc {

const std::string Application::START_AT_USERDEFINED = "UserDefined";

Application::Application(Configuration& db,
                         const daq::core::Partition& p,
                         const daq::core::BaseApplication& appConf,
                         bool isParentRoot)
        :   m_spontaneous_exit_allowed(false), m_partition(p.UID()), m_name(appConf.UID()), m_host(appConf.get_host()->UID()),
            m_segment(appConf.get_segment()->UID()), m_dyn_info(new DynamicInfo())
{
    m_dyn_info->m_app_sv_info.reset(new ApplicationSVInfo());
    m_dyn_info->m_app_self_info.reset(new ApplicationSelfInfo());

	try {
	    appConf.get_info(m_env,
	                     m_bin_path,
	                     m_parameters,
	                     m_restart_parameters);

		m_logging = appConf.get_Logging();
		if(m_logging == true) {
		    m_log_path = p.get_log_directory();
		}

		m_input_device = appConf.get_InputDevice();
		m_init_timeout = appConf.get_InitTimeout();
		m_exit_timeout = appConf.get_ExitTimeout();

		m_working_dir = appConf.get_StartIn();
		if(m_working_dir.empty() == true) {
			m_working_dir = p.get_WorkingDirectory();
		}

		// Calculate the application's lifetime

	    // General rule for Boot_Shutdown:
	    // - if the parent of this application is Root, then it is started at INITIALIZE and terminate at SHUTDOWN
	    // - if the parent is not Root, then it is started when the FSM is initialized and terminated when the FSM is stopped
		// Only exceptions are the OS infrastructure applications: their parent is Root but they are started/stopped at
		// INIT_FSM_CMD/STOP_FSM_CMD (this is done in the OSInfrastructureApplication class).

		// ATTENTION: if a "stop at" is added, then most likely some changes have to be applied in few other places
		// - ApplicationController::stopApps(const TransitionCmd& cmd)
		//   the method needs to be adapted when the transition command is SHUTDOWN. That is to be sure that applications are
		//   properly stopped when the SHUTDOWN command is sent (and it may happen in whatever FSM state).
		// - ApplicationAlgorithms::canBeStarted
		//   adapt the algorithm in order to decide whether an application can be started in a given FSM state

		const daq::core::CustomLifetimeApplicationBase* cltApp = db.cast<daq::core::CustomLifetimeApplicationBase>(&appConf);
		if(cltApp != nullptr) {
		    const std::string& lifeTime = cltApp->get_Lifetime();
		    if(lifeTime == daq::core::CustomLifetimeApplicationBase::Lifetime::Boot_Shutdown) {
		        m_start_at = (isParentRoot == true) ? FSMCommands::INITIALIZE_CMD : FSMCommands::INIT_FSM_CMD;
		        m_stop_at = (isParentRoot == true) ? FSMCommands::SHUTDOWN_CMD : FSMCommands::STOP_FSM_CMD;
		    } else if(lifeTime == daq::core::CustomLifetimeApplicationBase::Lifetime::SOR_EOR) {
		        m_start_at = FSMCommands::SOR_CMD;
		        m_stop_at = FSMCommands::EOR_CMD;
		    } else if(lifeTime == daq::core::CustomLifetimeApplicationBase::Lifetime::EOR_SOR) {
		        m_start_at = FSMCommands::EOR_CMD;
		        m_stop_at = FSMCommands::SOR_CMD;
		    } else if(lifeTime == daq::core::CustomLifetimeApplicationBase::Lifetime::UserDefined_Shutdown) {
		        m_start_at = Application::START_AT_USERDEFINED;
		        m_stop_at = (isParentRoot == true) ? FSMCommands::SHUTDOWN_CMD : FSMCommands::STOP_FSM_CMD;
		    } else if(lifeTime == daq::core::CustomLifetimeApplicationBase::Lifetime::Configure_Unconfigure) {
		        m_start_at = FSMCommands::CONFIGURE_CMD;
		        m_stop_at = FSMCommands::UNCONFIGURE_CMD;
		    } else {
                // Default to Boot_Shutdown
		        ers::warning(daq::rc::ApplicationGenericIssue(ERS_HERE, m_name, "an unknown lifetime value has been found; defaulting to BOOT_SHUTDOWN"));

		        m_start_at = (isParentRoot == true) ? FSMCommands::INITIALIZE_CMD : FSMCommands::INIT_FSM_CMD;
		        m_stop_at = (isParentRoot == true) ? FSMCommands::SHUTDOWN_CMD : FSMCommands::STOP_FSM_CMD;
		    }

		    m_spontaneous_exit_allowed = cltApp->get_AllowSpontaneousExit();
		} else {
		    // Run Control and Infrastructure applications come here
		    m_start_at = (isParentRoot == true) ? FSMCommands::INITIALIZE_CMD : FSMCommands::INIT_FSM_CMD;
		    m_stop_at = (isParentRoot == true) ? FSMCommands::SHUTDOWN_CMD : FSMCommands::STOP_FSM_CMD;
		}

		m_restartable_running = appConf.get_RestartableDuringRun();

		const daq::core::ComputerProgram* cp = appConf.get_Program();
		if(cp->get_Needs().empty() == false) {
			m_sw_object = cp->UID();
		}

		// Set back-up hosts
		for(const auto c : appConf.get_backup_hosts()) {
		    if(c->get_State() == true) {
		        m_backup_hosts.insert(c->UID());
		    }
		}

		if((m_backup_hosts.empty() == true) && (appConf.get_backup_hosts().empty() == false)) {
		    ers::warning(daq::rc::BackupHostsDisabled(ERS_HERE, m_name));
		}

		// Set the running host to the one defined in the db
		m_dyn_info->m_app_sv_info->setRunningHost(m_host);
	}
	catch(daq::config::Exception& ex) {
	     const std::string& msg = std::string("Cannot retrieve the information about application \"") +  m_name + "\" from the database";
	     const std::string& reason = ex.message();
	     throw daq::rc::ConfigurationIssue(ERS_HERE, msg + ": " + reason, reason, ex);
	}
	catch(daq::core::AlgorithmError& ex) {
        const std::string& msg = std::string("Cannot retrieve the information about application \"") +  m_name + "\" from the database";
        const std::string& reason = ex.message();
        throw daq::rc::ConfigurationIssue(ERS_HERE, msg + ": " + reason, reason, ex);
	}
}

Application::~Application() noexcept {
}

void Application::unfoldStartDependencies(const daq::core::BaseApplication& appConf, const std::vector<const daq::core::BaseApplication*>& allApps) {
    const auto& initDeps = appConf.get_initialization_depends_from(allApps);
    for(const auto ad : initDeps) {
        m_init_dep.insert(ad->UID());
    }
}

void Application::unfoldShutdownDependencies(const daq::core::BaseApplication& appConf, const std::vector<const daq::core::BaseApplication*>& allApps) {
    const auto& shutDeps = appConf.get_shutdown_depends_from(allApps);
    for(const auto ad : shutDeps) {
        m_shutdown_dep.insert(ad->UID());
    }
}

ApplicationType Application::type() const noexcept {
	return ApplicationType::STANDARD;
}

unsigned int Application::getExitTimeout() const {
	return m_exit_timeout;
}

std::string Application::getHost() const {
	return m_host;
}

const std::set<std::string>& Application::getInitDep() const {
	return m_init_dep;
}

unsigned int Application::getInitTimeout() const {
	return m_init_timeout;
}

std::string Application::getInputDevice() const {
	return m_input_device;
}

std::string Application::getLogPath() const {
	return m_log_path;
}

bool Application::isLogging() const {
	return m_logging;
}

std::string Application::getPartition() const {
    return m_partition;
}

std::string Application::getName() const {
	return m_name;
}

std::string Application::getParameters() const {
	return m_parameters;
}

std::string Application::getRestartParameters() const {
	return m_restart_parameters;
}

bool Application::isRestartableRunning() const {
	return m_restartable_running;
}

std::string Application::getSegment() const {
	return m_segment;
}

const std::set<std::string>& Application::getShutdownDep() const {
	return m_shutdown_dep;
}

std::string Application::getStartAt() const {
	return m_start_at;
}

std::string Application::getStopAt() const {
	return m_stop_at;
}

std::string Application::getSwObject() const {
	return m_sw_object;
}

std::string Application::getWorkingDir() const {
	return m_working_dir;
}

const std::vector<std::string>& Application::getBinPath() const {
	return m_bin_path;
}

const std::map<std::string, std::string>& Application::getEnvironment() const {
	return m_env;
}

const std::set<std::string>& Application::getBackupHosts() const {
	return m_backup_hosts;
}

bool Application::isSpontaneousExitAllowed() const {
    return m_spontaneous_exit_allowed;
}

void Application::addStartParameters(const std::string& params) {
	m_parameters += " " + params;
}

void Application::addRestartParameters(const std::string& params) {
	m_restart_parameters += " " + params;
}

void Application::addEnvironment(const std::string& name, const std::string& value) {
	m_env.insert({name, value});
}

void Application::setExitTimeout(unsigned int timeout) {
	m_exit_timeout = timeout;
}

void Application::setStartAt(const std::string& startAt) {
	m_start_at = startAt;
}

void Application::setStopAt(const std::string& stopAt) {
	m_stop_at = stopAt;
}

ApplicationSVInfo Application::getApplicationSVInfo() const {
	std::shared_lock<std::shared_mutex> lk(m_dyn_info->m_app_sv_mutex);
	return *m_dyn_info->m_app_sv_info;
}

std::pair<ApplicationSVInfo, bool> Application::setSupervisorAttribute(const std::function<bool ()>& checkAndSet)
{
    std::unique_lock<std::shared_mutex> lk(m_dyn_info->m_app_sv_mutex);

    const bool isDifferent = checkAndSet();
    ApplicationSVInfo svInfo = *m_dyn_info->m_app_sv_info;

    lk.unlock();

    ApplicationUpdateNotifier::instance().notify(m_name, svInfo, getApplicationSelfInfo());
    return std::make_pair(std::move(svInfo), isDifferent);
}

std::pair<ApplicationSVInfo, bool> Application::setMembership(bool membership) {
    return setSupervisorAttribute([this, &membership]() {
        const bool old_status = m_dyn_info->m_app_sv_info->isMembership();
        m_dyn_info->m_app_sv_info->setMembership(membership);
        return old_status != membership;
    });
}

std::pair<ApplicationSVInfo, bool> Application::setRestarting(bool restarting) {
    return setSupervisorAttribute([this, &restarting]() {
        const bool old_status = m_dyn_info->m_app_sv_info->isRestarting();
        m_dyn_info->m_app_sv_info->setRestarting(restarting);
        return old_status != restarting;
    });
}

std::pair<ApplicationSVInfo, bool> Application::setRunningHost(const std::string& runningHost) {
    return setSupervisorAttribute([this, &runningHost]() {
        const std::string& old_status = m_dyn_info->m_app_sv_info->getRunningHost();
        m_dyn_info->m_app_sv_info->setRunningHost(runningHost);
        return old_status != runningHost;
    });
}

std::pair<ApplicationSVInfo, bool> Application::setStatus(APPLICATION_STATUS status) {
    return setSupervisorAttribute([this, &status]() {
        const APPLICATION_STATUS old_status = m_dyn_info->m_app_sv_info->getStatus();
        m_dyn_info->m_app_sv_info->setStatus(status);
        return old_status != status;
    });
}

std::pair<ApplicationSVInfo, bool> Application::setNotResponding(bool notResponding) {
    return setSupervisorAttribute([this, &notResponding]() {
        const bool old_status = m_dyn_info->m_app_sv_info->isNotResponding();
        m_dyn_info->m_app_sv_info->setNotResponding(notResponding);
        return old_status != notResponding;
    });
}

std::pair<ApplicationSVInfo, bool> Application::setForceLookup(bool force) {
    return setSupervisorAttribute([this, &force]() {
        const bool old_status = m_dyn_info->m_app_sv_info->isForceLookup();
        m_dyn_info->m_app_sv_info->setForceLookup(force);
        return old_status != force;
    });
}

std::pair<ApplicationSVInfo, bool> Application::setTestStatus(daq::tmgr::TestResult status) {
    return setSupervisorAttribute([this, &status]() {
        const daq::tmgr::TestResult old_status = m_dyn_info->m_app_sv_info->getTestStatus();
        m_dyn_info->m_app_sv_info->setTestStatus(status);
        return old_status != status;
    });
}

std::pair<ApplicationSVInfo, bool> Application::setHandle(const std::string& pmgHandle) {
    return setSupervisorAttribute([this, &pmgHandle]() {
        const std::string& old_status = m_dyn_info->m_app_sv_info->getHandle();
        m_dyn_info->m_app_sv_info->setHandle(pmgHandle);
        return old_status != pmgHandle;
    });
}

std::pair<ApplicationSVInfo, bool> Application::setLastID(unsigned int id) {
    return setSupervisorAttribute([this, &id]() {
        const unsigned int old_status = m_dyn_info->m_app_sv_info->getLastID();
        m_dyn_info->m_app_sv_info->setLastID(id);
        return old_status != id;
    });
}

std::pair<ApplicationSVInfo, bool> Application::setExitCode(int exCode) {
    return setSupervisorAttribute([this, &exCode]() {
        const int old_status = m_dyn_info->m_app_sv_info->getExitCode();
        m_dyn_info->m_app_sv_info->setExitCode(exCode);
        return old_status != exCode;
    });
}

std::pair<ApplicationSVInfo, bool> Application::setExitSignal(int signal) {
    return setSupervisorAttribute([this, &signal]() {
        const int old_status = m_dyn_info->m_app_sv_info->getExitSignal();
        m_dyn_info->m_app_sv_info->setExitSignal(signal);
        return old_status != signal;
    });
}

void Application::update(const std::shared_ptr<ApplicationSVInfo>& newInfo, bool checkTime, unsigned int timeWindow) {
    {
        std::lock_guard<std::shared_mutex> lk(m_dyn_info->m_app_sv_mutex);

        infotime_t newTime;
        infotime_t oldTime;
        if((checkTime == false) ||
           ((newTime = newInfo->getInfoTimeTag()) >= (oldTime = m_dyn_info->m_app_sv_info->getInfoTimeTag())) ||
           chrono::duration_cast<chrono::milliseconds>(duration_t(oldTime - newTime)) > chrono::milliseconds(timeWindow))
        {
            (m_dyn_info->m_app_sv_info) = newInfo;
        } else {
            ERS_LOG("Out of order update detected for application " << getName() << " (Old info time is " << oldTime
                    << "; new info time is " << newTime << ")");
        }
    }

    ApplicationUpdateNotifier::instance().notify(m_name, *newInfo, getApplicationSelfInfo());
}

ApplicationSelfInfo Application::getApplicationSelfInfo() const {
    std::shared_lock<std::shared_mutex> lk(m_dyn_info->m_app_self_mutex);
    return *m_dyn_info->m_app_self_info;
}


std::pair<ApplicationSelfInfo, bool> Application::setSelfAttribute(const std::function<bool ()>& checkAndSet)
{
    std::unique_lock<std::shared_mutex> lk(m_dyn_info->m_app_self_mutex);

    const bool isDifferent = checkAndSet();
    ApplicationSelfInfo selfInfo = *m_dyn_info->m_app_self_info;

    lk.unlock();

    ApplicationUpdateNotifier::instance().notify(m_name, getApplicationSVInfo(), selfInfo);
    return std::make_pair(std::move(selfInfo), isDifferent);
}


std::pair<ApplicationSelfInfo, bool> Application::setFSMState(FSM_STATE state) {
    return setSelfAttribute([this, &state]() {
        const FSM_STATE old_state = m_dyn_info->m_app_self_info->getFSMState();
        m_dyn_info->m_app_self_info->setFSMState(state);
        return old_state != state;
    });
}

std::pair<ApplicationSelfInfo, bool> Application::setTransitioning(bool isTransitioning) {
    return setSelfAttribute([this, &isTransitioning]() {
        const bool old_state = m_dyn_info->m_app_self_info->isTransitioning();
        m_dyn_info->m_app_self_info->setTransitioning(isTransitioning);
        return old_state != isTransitioning;
    });
}

std::pair<ApplicationSelfInfo, bool> Application::setInternalError(const std::vector<std::string>& errorReasons,
                                                                   const std::vector<std::string>& badChildren)
{
    return setSelfAttribute([this, &errorReasons, &badChildren]() {
        const auto& old_error_reasons = m_dyn_info->m_app_self_info->getErrorReasons();
        const auto& old_bad_ch = m_dyn_info->m_app_self_info->getBadChildren();
        const bool isDifferent = (old_bad_ch != badChildren) || (old_error_reasons != errorReasons);
        m_dyn_info->m_app_self_info->setInternalError(errorReasons, badChildren);
        return isDifferent;
    });
}

std::pair<ApplicationSelfInfo, bool> Application::setLastTransitionCommand(const std::shared_ptr<const TransitionCmd>& cmd) {
    return setSelfAttribute([this, &cmd]() {
        const std::shared_ptr<const TransitionCmd> old_state = m_dyn_info->m_app_self_info->getLastTransitionCommand();
        const bool isDifferent = ((cmd.get() != nullptr && old_state.get() != nullptr) ? !(*old_state == *cmd) : ((cmd.get() == nullptr && old_state.get() == nullptr) ? false : true));
        m_dyn_info->m_app_self_info->setLastTransitionCommand(cmd);
        return isDifferent;
    });
}

std::pair<ApplicationSelfInfo, bool> Application::setCurrentTransitionCommand(const std::shared_ptr<const TransitionCmd>& cmd) {
    return setSelfAttribute([this, &cmd]() {
        const std::shared_ptr<const TransitionCmd> old_state = m_dyn_info->m_app_self_info->getCurrentTransitionCommand();
        const bool isDifferent = ((cmd.get() != nullptr && old_state.get() != nullptr) ? !(*old_state == *cmd) : ((cmd.get() == nullptr && old_state.get() == nullptr) ? false : true));
        m_dyn_info->m_app_self_info->setCurrentTransitionCommand(cmd);
        return isDifferent;
    });
}

std::pair<ApplicationSelfInfo, bool> Application::setLastTransitionTime(infotime_t time) {
    return setSelfAttribute([this, &time]() {
        const bool old_state = m_dyn_info->m_app_self_info->getLastTransitionTime();
        m_dyn_info->m_app_self_info->setLastTransitionTime(time);
        return old_state != time;
    });

}

std::pair<ApplicationSelfInfo, bool> Application::setLastCommand(const std::shared_ptr<const RunControlBasicCommand>& cmd) {
    return setSelfAttribute([this, &cmd]() {
        const std::shared_ptr<const RunControlBasicCommand> old_state = m_dyn_info->m_app_self_info->getLastCommand();
        const bool isDifferent = ((cmd.get() != nullptr && old_state.get() != nullptr) ? !(*old_state == *cmd) : ((cmd.get() == nullptr && old_state.get() == nullptr) ? false : true));
        m_dyn_info->m_app_self_info->setLastCommand(cmd);
        return isDifferent;
    });
}

std::pair<ApplicationSelfInfo, bool> Application::setBusy(bool isBusy) {
    return setSelfAttribute([this, &isBusy]() {
        const bool old_state = m_dyn_info->m_app_self_info->isBusy();
        m_dyn_info->m_app_self_info->setBusy(isBusy);
        return old_state != isBusy;
    });

}

std::pair<ApplicationSelfInfo, bool> Application::setFullyInitialized(bool done) {
    return setSelfAttribute([this, &done]() {
        const bool old_state = m_dyn_info->m_app_self_info->isFullyInitialized();
        m_dyn_info->m_app_self_info->setFullyInitialized(done);
        return old_state != done;
    });
}

void Application::update(const std::shared_ptr<ApplicationSelfInfo>& newInfo, bool checkTime, unsigned int timeWindow) {
    {
        std::lock_guard<std::shared_mutex> lk(m_dyn_info->m_app_self_mutex);

        infotime_t newTime;
        infotime_t oldTime;
        if((checkTime == false) ||
           ((newTime = newInfo->getInfoTimeTag()) >= (oldTime = m_dyn_info->m_app_self_info->getInfoTimeTag())) ||
           chrono::duration_cast<chrono::milliseconds>(duration_t(oldTime - newTime)) > chrono::milliseconds(timeWindow))
        {
            m_dyn_info->m_app_self_info = newInfo;
        } else {
            ERS_LOG("Out of order update detected for application " << getName() << " (Old info time is " << oldTime
                    << "; new info time is " << newTime << ")");
        }
    }

    ApplicationUpdateNotifier::instance().notify(m_name, getApplicationSVInfo(), *newInfo);
}

void Application::update(const Application& app) {
    ERS_ASSERT(getName() == app.getName());

    {
        std::shared_ptr<DynamicInfo> temp;

        std::unique_lock<std::shared_mutex> lk_1(m_dyn_info->m_app_self_mutex, std::defer_lock);
        std::unique_lock<std::shared_mutex> lk_2(m_dyn_info->m_app_sv_mutex, std::defer_lock);

        std::shared_lock<std::shared_mutex> lk_3(app.m_dyn_info->m_app_self_mutex, std::defer_lock);
        std::shared_lock<std::shared_mutex> lk_4(app.m_dyn_info->m_app_sv_mutex, std::defer_lock);

        std::lock(lk_1, lk_2, lk_3, lk_4);

        // The store of the shared pointer to the temp is needed to avoid the object being deleted
        // after the last assignment is done (i.e., when lk_1 and lk_2 are unlocked the old object
        // m_dyn_info was pointing to when the locks were created may already be destroyed)
        temp = m_dyn_info;
        m_dyn_info = app.m_dyn_info;
    }

    ApplicationUpdateNotifier::instance().notify(m_name, app.getApplicationSVInfo(), app.getApplicationSelfInfo());
}

std::size_t ApplicationType_hasher::operator()(const ApplicationType& at) const {
    return boost::hash<unsigned int>()(static_cast<unsigned int>(at));
}

}}
