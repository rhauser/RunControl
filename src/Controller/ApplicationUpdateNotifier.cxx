/*
 * ApplicationUpdateNotifier.cxx
 *
 *  Created on: May 24, 2024
 *      Author: avolio
 */

#include <RunControl/Controller/ApplicationUpdateNotifier.h>
#include "RunControl/Controller/ApplicationSupervisorInfo.h"
#include "RunControl/Controller/ApplicationSelfInfo.h"


namespace daq { namespace rc {

class AUNDeleter {
    public:
        void operator()(ApplicationUpdateNotifier* onl_svc) const {
            delete onl_svc;
        }
};

std::once_flag ApplicationUpdateNotifier::m_init_flag;
std::unique_ptr<ApplicationUpdateNotifier, AUNDeleter> ApplicationUpdateNotifier::m_self(nullptr, AUNDeleter());

ApplicationUpdateNotifier::ApplicationUpdateNotifier() {
}

ApplicationUpdateNotifier::~ApplicationUpdateNotifier() {
}

ApplicationUpdateNotifier& ApplicationUpdateNotifier::instance() {
    std::call_once(ApplicationUpdateNotifier::m_init_flag,
                   []() { ApplicationUpdateNotifier::m_self.reset(new ApplicationUpdateNotifier()); });

    return *(ApplicationUpdateNotifier::m_self.get());
}

ApplicationUpdateNotifier::CallbackToken ApplicationUpdateNotifier::registerCallback(const Callback& cb) {
    return m_app_update_signal.connect(cb);
}

void ApplicationUpdateNotifier::removeCallback(const CallbackToken& token) {
    token.disconnect();
}

void ApplicationUpdateNotifier::removeCallback(ScopedCallbackToken& token) {
    token.release().disconnect();
}

ApplicationUpdateNotifier::ScopedCallbackToken ApplicationUpdateNotifier::scopedToken(const CallbackToken& cbt) {
    return ScopedCallbackToken(cbt);
}

void ApplicationUpdateNotifier::notify(const std::string& appName,
                                       const ApplicationSVInfo& svUpdate,
                                       const ApplicationSelfInfo& selfUpdate)
{
    m_app_update_signal(appName, svUpdate, selfUpdate);
}

}}


