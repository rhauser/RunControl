/*
 * RunController.cxx
 *
 *  Created on: Dec 5, 2012
 *      Author: avolio
 */

#include "RunControl/Controller/RunController.h"
#include "RunControl/Controller/Application.h"
#include "RunControl/Controller/ApplicationSelfInfo.h"
#include "RunControl/Controller/ApplicationSupervisorInfo.h"
#include "RunControl/Controller/ApplicationList.h"
#include "RunControl/Controller/ApplicationAlgorithms.h"
#include "RunControl/Controller/RunControllerFSMOperations.h"
#include "RunControl/Controller/InformationPublisher.h"
#include "RunControl/Controller/DVSController.h"
#include "RunControl/Controller/ControllerApplication.h"
#include "RunControl/Controller/RunControlApplication.h"
#include "RunControl/FSM/FSMStates.h"
#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/Common/CommandReceiver.h"
#include "RunControl/Common/CommandReceiverFactory.h"
#include "RunControl/Common/UserRoutines.h"
#include "RunControl/Common/CommandSender.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/ThreadPool.h"
#include "RunControl/Common/ScheduledThreadPool.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/AMBridge.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/Constants.h"
#include "RunControl/Common/RunControlBasicCommand.h"

#include <dal/Segment.h>
#include <dal/Partition.h>
#include <rc/DAQApplicationInfoNamed.h>
#include <pmg/pmg_initSync.h>
#include <TestManager/Test.h>
#include <ers/Assertion.h>
#include <ers/Issue.h>
#include <ers/LocalContext.h>
#include <ers/ers.h>
#include <ipc/partition.h>
#include <is/exceptions.h>
#include <tmgr/tmresult.h>

#include <boost/thread/thread.hpp>
#include <boost/scope_exit.hpp>

#include <future>
#include <functional>
#include <thread>
#include <chrono>
#include <set>
#include <array>
#include <map>
#include <algorithm>
#include <limits>
#include <mutex>
#include <ostream>
#include <tuple>
#include <type_traits>
#include <typeinfo>
#include <vector>

class Configuration;
namespace daq { namespace dvs {
    struct Result;
}}

namespace ph = std::placeholders;

namespace daq { namespace rc {

class StartOperationsTask : public FutureTask<void> {
    public:
        StartOperationsTask(RunController& rc) : FutureTask<void>(), m_rc(rc), m_restart_after_a_crash(false) {}

        ~StartOperationsTask() {}

    protected:
        void run() override {
            try {
                // Get the status of all the children (PMGs are asked as well) and of the parent
                std::string chFSMState;
                try {
                    // This throws daq::rc::InterruptedException: it means we are asked to exit
                    // Let the exception go and be caught at by the enclosing try-catch block
                    chFSMState = m_rc.get().getChildrenStatus();
                }
                catch(daq::rc::BadChildren& ex) {
                    ers::error(ex);
                }

                m_restart_after_a_crash = ((chFSMState != FSMStates::ABSENT_STATE) && (chFSMState != FSMStates::INITIAL_STATE));

                // Even if the start procedure may include additional transitions in order to reach the children or the parent
                // FSM status, let's acquire the resource only once (so that the action appears atomic-like)
                const auto& transitionGrant = m_rc.get().m_cmd_ctrl->grantResource(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::INIT_FSM)));
                if(transitionGrant.first == false) {
                    // We should never be here
                    ers::fatal(daq::rc::Busy(ERS_HERE, FSMCommands::INIT_FSM_CMD));
                } else {
                    try {
                        // Use the children status in order to identify a restarting-after-crash
                        m_rc.get().m_fsm_ctrl->start(m_restart_after_a_crash);

                        interrupted();

                        // If we are restarting, then we need to perform the right sub-transitions as well
                        // Be aware of the order: first sub-transitions from parents and then the others
                        Algorithms::SubtransitionMap sts = m_rc.get().m_subtr_tree;
                        const auto& selfST = m_rc.get().thisControllerApplication()->subTransitions();
                        for(const auto& pair : selfST) {
                            for(const std::string& st : pair.second) {
                                sts[pair.first].push_back(st);
                            }
                        }

                        // If the state of children could not be identified, then do nothing
                        if(chFSMState.empty() == false) {
                            // Reach the children state if different than ABSENT
                            if(chFSMState != FSMStates::ABSENT_STATE) {
                                m_rc.get().m_fsm_ctrl->goToState(FSMStates::stringToState(chFSMState), (chFSMState != FSMStates::INITIAL_STATE), sts);
                            }

                            interrupted();

                            // Now try to reach the parent's state
                            auto stateToReach = [this]() -> std::string
                            {
                                std::string state;

                                const auto& parentStatus = m_rc.get().getParentStatus();
                                if(parentStatus.get() != nullptr) {
                                    const auto& currTr = parentStatus->currentTransitionCommand();
                                    if(currTr.get() != nullptr) {
                                        state = currTr->fsmDestinationState();
                                    } else {
                                        state = parentStatus->fsmState();
                                    }
                                }

                                return state;
                            };

                            // Stop if the parent's state cannot be determined or when its status is reached
                            std::string parentFSMState = stateToReach();
                            while((parentFSMState.empty() == false) &&
                                  (parentFSMState != FSMStates::stateToString(std::get<0>(m_rc.get().m_fsm_ctrl->status()))))
                            {
                                m_rc.get().m_fsm_ctrl->goToState(FSMStates::stringToState(parentFSMState), false, sts);
                                parentFSMState = stateToReach();
                            }

                            interrupted();

                            // Now that the parent's state has been reached, see if some in-state transition was being
                            // executed and eventually perform it, otherwise the parent my be waiting for ever
                            const auto& parentStatus = m_rc.get().getParentStatus();
                            if(parentStatus.get() != nullptr) {
                                auto&& currTr = parentStatus->currentTransitionCommand();
                                if((currTr.get() != nullptr) && (currTr->fsmDestinationState() == currTr->fsmSourceState())) {
                                    m_rc.get().m_fsm_ctrl->makeTransition(std::shared_ptr<TransitionCmd>(std::move(currTr)));
                                }
                            }
                         }
                    }
                    catch(daq::rc::TransitionInterrupted& ex) {
                        ers::warning(ex);
                    }
                    catch(daq::rc::InvalidState& ex) {
                        ers::error(ex);
                    }
                    catch(daq::rc::FSMPathError& ex) {
                        ers::error(ex);
                    }
                    catch(ers::Issue& ex) {
                        ers::fatal(ex);
                    }
                }
            }
            catch(daq::rc::InterruptedException& ex) {
                ers::warning(ex);
            }
        }

        void done() noexcept override {
            // This is particularly important in order to let the restart procedure complete
            const auto& mys = m_rc.get().thisControllerApplication();
            const ApplicationSelfInfo& myInfo = (mys->setFullyInitialized(true)).first;

            if(m_restart_after_a_crash == true) {
                m_rc.get().bulkUpdateToES(true);
            }

            m_rc.get().notifyStatusChange(myInfo, mys->getActionTimeout(), mys->getExitTimeout());
        }

    private:
        std::reference_wrapper<RunController> m_rc;
        bool m_restart_after_a_crash;
};

class TransitionTask : public FutureTask<void> {
    public:
        // throws daq::rc::BadCommand
        TransitionTask(RunController& rc, const std::shared_ptr<TransitionCmd>& trCmd, bool isRescheduled = false)
            : FutureTask<void>(), m_rc(rc), m_tr_cmd(trCmd), m_is_rescheduled(isRescheduled)
        {
            // Check the validity of the command before starting any action (i.e., some commands
            // may not be accepted by the FSM even if in principle they represent a valid FSM event - the
            // RootController's FSM, for instance, will accept the SHUTDOWN command, while a leaf controller's FSM will not)
            const std::string& fsmCmd = m_tr_cmd->fsmCommand();
            if(rc.m_fsm_ctrl->isCommandValid(FSMCommands::stringToCommand(fsmCmd)) == false) {
                const std::string& msg = std::string("the command \"") + fsmCmd + "\" does not correspond to a valid event accepted by the FSM";
                throw daq::rc::BadCommand(ERS_HERE, RunControlCommands::MAKE_TRANSITION_CMD, msg);
            }
        }

        ~TransitionTask() {}

    protected:
        void run() override {
            const std::string& cmdName = m_tr_cmd->fsmCommand();

            try {
                // Remember that this throws daq::rc::InterruptedException
                interrupted();

                // Check if the transition is allowed given the current commands in execution
                const auto& tr_res = m_rc.get().m_cmd_ctrl->grantResource(m_tr_cmd);
                if(tr_res.first == false) {
                    // Sleep for a while (not too much...) before submitting again the task
                    std::this_thread::sleep_for(std::chrono::milliseconds(100));
                    interrupted();

                    std::shared_ptr<TransitionTask> newTask(new TransitionTask(m_rc.get(), m_tr_cmd, true));
                    m_rc.get().m_tbk->addTask(newTask);
                    m_rc.get().m_transitions_tp->execute<void>(newTask); // This may throw if the TP is paused

                    if(m_is_rescheduled == false) {
                        const std::string& msg = std::string("The \"") + cmdName +
                                "\" transition cannot be executed now because the controller is busy." +
                                " The transition request has been queued and will be executed at a proper time";
                        ers::warning(daq::rc::Busy(ERS_HERE, msg, cmdName));
                    }
                } else {
                    if(cmdName == FSMCommands::SHUTDOWN_CMD) {
                        // No risks to badly interrupt even if the FSM does not accept the SHUTDOWN command
                        // This kind of check is done in the constructor
                        m_rc.get().m_cmd_ctrl->interruptCurrentTasks();
                    }

                    // Only one transition command at a given moment in time, but always accept SHUTDOWN
                    // It is important that the size of the TP used for transitions is at least 2: both will never
                    // be used concurrently to perform transitions, but this allows to have the SHUTDOWN come along
                    // the transition in execution

                    // The loop done in this way allows to straight execute SHUTDOWN if the FSM is not busy
                    // because there is no need to interrupt the current transition

                    // At this point "makeTransition" may throw only if the transition command
                    // is valid but cannot be processed given the current FSM state (EventNotProcessed).
                    // The BadCommand and TransitionNotAllowed exceptions should never happen because
                    // the command is checked in the constructor
                    while(m_rc.get().m_fsm_ctrl->makeTransition(m_tr_cmd) == false) {
                        if(cmdName == FSMCommands::SHUTDOWN_CMD) {
                            // At SHUTDOWN we want to interrupt the current transition
                            m_rc.get().m_app_ctrl->interrupt(true);
                            m_rc.get().m_fsm_ctrl->interruptTransition(true);

                            BOOST_SCOPE_EXIT(this_)
                            {
                                // Remove the "interrupted" state at the next loop
                                this_->m_rc.get().m_app_ctrl->interrupt(false);
                                this_->m_rc.get().m_fsm_ctrl->interruptTransition(false);
                            }
                            BOOST_SCOPE_EXIT_END

                            // Now we wait for the FSM not being busy anymore
                            // If it is not busy, then the FSM will be available for the SHUTDOWN because:
                            // 1 - At most SHUTDOWN + only one transition can be on the fly
                            // 2 - If we are here the other transition is already in execution
                            while(m_rc.get().m_fsm_ctrl->isBusy() == true) {
                                interrupted();
                                std::this_thread::sleep_for(std::chrono::milliseconds(1));
                            }
                        } else {
                            // This means that the FSM is already executing the SHUTDOWN
                            // Fine to not process the on-the-fly command
                            ers::warning(daq::rc::FSMBusy(ERS_HERE, cmdName));
                            break;
                        }
                    }
                }
            }
            catch(daq::rc::ThreadPoolPaused& ex) {
                const std::string& msg = std::string("The transition \"") + cmdName + "\" cannot be executed because the controller is shutting down";
                ers::warning(daq::rc::Busy(ERS_HERE, msg, cmdName, ex));
            }
            catch(daq::rc::InterruptedException& ex) {
                const std::string& msg = std::string("The transition \"") + cmdName + "\" cannot be executed because the corresponding task has been interrupted";
                ers::warning(daq::rc::Busy(ERS_HERE, msg, cmdName, ex));
            }
            catch(ers::Issue& ex) {
                ers::warning(ex);
            }
        }

    private:
        const std::reference_wrapper<RunController> m_rc;
        const std::shared_ptr<TransitionCmd> m_tr_cmd;
        const bool m_is_rescheduled;
};

class ChildStatusTask : public FutureTask<void> {
    public:
        ChildStatusTask(const std::shared_ptr<Application>& child, RunController& rc)
            : FutureTask<void>(), m_child(child), m_rc(rc)
        {
        }

    protected:
        // throws daq::rc::InterruptedException
        void run() override {
            const std::string& appName = m_child->getName();

            // Get the application membership from IS
            bool isIN = true;
            try {
                ERS_DEBUG(3, "Checking IS info for application \"" << appName << "\"");

                DAQApplicationInfoNamed info(OnlineServices::instance().getIPCPartition(),
                                             Constants::RC_IS_SERVER_NAME + "." + Constants::RC_SV_INFO + "." + appName);
                info.checkout();
                isIN = info.membership;
                m_child->setMembership(isIN);

                // This is the only way to retrieve the exit code and the exit
                // signal in case the application is no more up (i.e., ABSENT)
                m_child->setExitCode(info.exitCode);
                m_child->setExitSignal(info.exitSignal);
            }
            catch(daq::is::InfoNotFound&) {
                // The information is not there, nothing bad (i.e., this is normal for a clean start)
            }
            catch(daq::is::Exception& ex) {
                ers::log(ex);
            }

            interrupted();

            try {
                // The 'tryToConnect' will properly update the SV information and publish
                // the fresh information (including notification to the ES)
                const bool alreadyUp = (m_rc.get().m_app_ctrl->tryToConnect(m_child)).first;
                if(alreadyUp == true) {
                    const ApplicationType appType = m_child->type();
                    if((appType == ApplicationType::RUN_CONTROL) || (appType == ApplicationType::CHILD_CONTROLLER)) {
                        try {
                            std::unique_ptr<ApplicationStatusCmd>&& appStatus = m_rc.get().m_cmd_sender->status(appName);

                            // Get the application FSM state:
                            const std::string& appFSMState = appStatus->fsmState();
                            ERS_LOG("Application \"" << appName << "\" is already running, in the FSM state \"" << appFSMState << "\" and with membership " << isIN);

                            // Update the application state structure
                            // This call will also update the ES
                            m_rc.get().updateChild(std::shared_ptr<ApplicationStatusCmd>(std::move(appStatus)));
                        }
                        catch(daq::rc::IPCLookup&) {
                            // The application may not be running anymore
                        }
                        catch(daq::rc::Exception& ex) {
                            // TODO: what to do in this case? The application cannot be reached
                            // Set as not responding?
                            ers::error(ex);
                        }
                    } else {
                        ERS_LOG("Application \"" << appName << "\" is already running and with membership " << isIN);
                    }
                }
            }
            catch(daq::rc::ApplicationReconnectionFailure& ex) {
                // Not able to determine whether an application is already running
                ers::error(ex);
            }
        }

    private:
        const std::shared_ptr<Application> m_child;
        const std::reference_wrapper<RunController> m_rc;
};

class TransitionChecker {
    public:
        TransitionChecker()
            : m_end_time(), m_enabled(false), m_already_elapsed(false)
        {
        }

        void activate(const chrono::time_point<clock_t>& endTime, const std::string& transition) {
            std::lock_guard<std::mutex> lk(m_mutex);

            m_transition = transition;
            m_end_time = endTime;
            m_already_elapsed = false;
            m_enabled = true;
        }

        void deactivate() {
            std::lock_guard<std::mutex> lk(m_mutex);

            m_enabled = false;
        }

        bool check() {
            std::lock_guard<std::mutex> lk(m_mutex);

            if(m_enabled == true) {
                if((m_already_elapsed == false) && (clock_t::now() > m_end_time)) {
                    // Disable boost thread interruption: see ADHI-4495.
                    // In the actual implementation, it is anyway impossible that the thread
                    // checker is interrupted just before the timeout warning is going to be sent.
                    // Indeed "deactivate()" is called at the end of any transition ("fsmTransitionDone(...)"),
                    // while the thread pool is paused (hence the interrupt mechanism is used) only when
                    // the FSM is not busy anymore ("fsmBusy(...)").
                    {
                        boost::this_thread::disable_interruption di;
                        ers::warning(daq::rc::TransitionTimeout(ERS_HERE, m_transition));
                    }

                    m_already_elapsed = true;
                }
            }

            return true;
        }

    private:
        std::mutex m_mutex;
        chrono::time_point<clock_t> m_end_time;
        std::string m_transition;
        bool m_enabled;
        bool m_already_elapsed;
};

RunController::RunController(const std::string& name,
                             const std::string& parent,
                             const std::string& partition,
                             const std::string& segment,
                             std::shared_ptr<UserRoutines> userActions,
                             bool isInteractive)
try :   CommandedApplication(),
        m_app_name(name), m_parent(parent), m_segment(segment), m_partition(partition), m_is_interactive(isInteractive), m_is_exiting(false),
        m_myself(RunController::createThisControllerApplication()), m_am_bridge(new AMBridge(partition)), m_tbk(new TaskBookkeeper()),
        m_info_publisher(new InformationPublisher(std::bind(&RunController::thisControllerApplicationStatus, this))),
        m_cmd_sender(new CommandSender(partition, name)), m_app_list(new ApplicationList()), m_dvs_ctrl(new DVSController(m_app_list, m_info_publisher)),
        m_app_ctrl(ApplicationController::create(m_app_list, m_dvs_ctrl, m_info_publisher)), m_user_actions(userActions),
        m_parentsender_tp(new ThreadPool(1)), m_transition_checker(new TransitionChecker()),
        m_fsm_ctrl(new FSMController(std::make_shared<RunControllerFSMOperations>(m_app_ctrl, m_dvs_ctrl, m_app_list, m_info_publisher), userActions)),
        m_scheduled_tp(new ScheduledThreadPool(2)),
        m_cmd_ctrl(new CommandController(m_app_ctrl, m_app_list, m_dvs_ctrl, m_info_publisher, m_user_actions, *m_scheduled_tp)),
        m_transition_timeout_timer(new ScheduledThreadPool(1)), m_transitions_tp(new ThreadPool(2))
{
    ERS_DEBUG(3, "Creating a controller with name \"" << name << "\", whose parent is \"" << parent <<
                 "\" and controlling the segment \"" << segment << "\" in the context of partition \"" << partition << "\"");

    // Write default values to IS (just to not confuse the IS readers...)
    m_info_publisher->publish(m_app_name, m_myself->getActionTimeout(), m_myself->getExitTimeout(), ApplicationSelfInfo(), InformationPublisher::IS_SERVER);

    // Periodic tasks have to be executed at the right moment (i.e., in the FSM)
    // Here any execution is paused
    m_transition_timeout_timer->pauseExecution(true);
    m_scheduled_tp->pauseExecution(true);

    m_fsm_ctrl_tokens.push_back(m_fsm_ctrl->registerOnFSMBusyCallback(std::bind(&RunController::fsmBusy, this, ph::_1)));
    m_fsm_ctrl_tokens.push_back(m_fsm_ctrl->registerOnTransitioningCallback(std::bind(&RunController::fsmTransitioning, this, ph::_1)));
    m_fsm_ctrl_tokens.push_back(m_fsm_ctrl->registerOnTransitionDoneCallback(std::bind(&RunController::fsmTransitionDone, this, ph::_1)));
    m_fsm_ctrl_tokens.push_back(m_fsm_ctrl->registerOnNoTransitionCallback(std::bind(&RunController::fsmNoTransition, this, ph::_1)));
    m_fsm_ctrl_tokens.push_back(m_fsm_ctrl->registerOnTransitionExceptionCallback(std::bind(&RunController::fsmTransitionException, this, ph::_1, ph::_2)));

    m_app_ctrl_token = m_app_ctrl->registerOnApplicationExitCallback(std::bind(&RunController::applicationExited, this, ph::_1, ph::_2));

    m_cmd_ctrl_token = m_cmd_ctrl->registerOnBusyCallback(std::bind(&RunController::busyWithCommands, this, ph::_1));
}
catch(daq::rc::PMGSystemError& ex) { // From ApplicationController
    throw daq::rc::ControllerInitializationFailed(ERS_HERE, ex.message(), ex);
}
catch(daq::rc::ConfigurationIssue& ex) { // From RunController::createThisControllerApplication()
    throw daq::rc::ControllerInitializationFailed(ERS_HERE, ex.message(), ex);
}
catch(daq::rc::ThreadPoolError& ex) {
    throw daq::rc::ControllerInitializationFailed(ERS_HERE, "failed creating one of the thread pools", ex);
}

RunController::~RunController() noexcept {
    // Should we pause/flash the thread pools?
    // No, all the things are done in "onExit"
}

void RunController::init() {
    try {
        OnlineServices& os = OnlineServices::instance();
        Configuration& db = os.getConfiguration();
        const daq::core::Partition& p = os.getPartition();
        m_app_list->load(db, p, m_segment);

        m_dvs_ctrl->load(db, os.getIPCPartition().name(), os.segmentName());

        m_subtr_tree = Algorithms::subTransitionsFromParents(db, p, os.getSegment(), os.segmentName());
        ERS_DEBUG(1, "Sub-transitions from parent segments (if any):");

#ifndef ERS_NO_DEBUG
        for(const auto& pair : m_subtr_tree) {
            ERS_DEBUG(1, "Main transition: " << FSMCommands::commandToString(pair.first));
            for(const auto& st : pair.second) {
                ERS_DEBUG(1, "\t" << st);
            }
        }
#endif

        if(m_is_interactive == false) {
            m_cmd_recv = CommandReceiverFactory::create(RECV_TYPE::CORBA, *this);
        } else {
            throw daq::rc::ControllerInitializationFailed(ERS_HERE, "interactive mode not supported");
        }

        onInit();

        // Add periodic tasks to the TP
        // Here we are just submitting the actions, then the TP
        // will be paused and/or resumed at the right moment during
        // the FSM transitions
        const auto& mys = thisControllerApplication();
        m_scheduled_tp->submit(std::bind(&RunController::periodicPublish, this),
                               RunControlCommands::PUBLISH_CMD,
                               0,
                               mys->getProbeInterval());

        m_scheduled_tp->submit(std::bind(&RunController::periodicUserPublish, this),
                               RunControlCommands::PUBLISH_CMD,
                               0,
                               mys->getProbeInterval());

        m_scheduled_tp->submit(std::bind(&RunController::periodicUserPublishStats, this),
                               RunControlCommands::PUBLISHSTATS_CMD,
                               0,
                               mys->getFullStatInterval());

        m_transition_timeout_timer->submit(std::bind(&TransitionChecker::check, m_transition_checker.get()),
                                           "TRANSITION_TIMEOUT_CHECKER",
                                           0,
                                           1);

        // Here we publish in IPC
        m_cmd_recv->init(); // this throws daq::rc::OnlineServicesFailure
    }
    catch(daq::rc::OnlineServicesFailure& ex) {
        const std::string& errMsg = std::string("failed accessing some online services (") + ex.message() + ")";
        throw daq::rc::ControllerInitializationFailed(ERS_HERE, errMsg, ex);
    }
    catch(ers::Issue& ex) {
        // TODO: this is to handle failures during the load of the configuration by DVS
        throw daq::rc::ControllerInitializationFailed(ERS_HERE, ex.message(), ex);
    }
}

void RunController::onInit() {
    m_fsm_ctrl->init<FSM_STATE::INITIAL>();
}

void RunController::run() {
    // The PMG sync
    pmg_initSync();

    try {
        std::shared_ptr<StartOperationsTask> startTask(new StartOperationsTask(*this));
        m_tbk->addTask(startTask);
        m_transitions_tp->execute<void>(startTask);
    }
    catch(daq::rc::ThreadPoolPaused& ex) {
        // This should never happen: the thread pool is paused only in 'onExit()'
        ers::error(ex);
    }

    // This blocks
    m_cmd_recv->start();

    // Start the exit procedure
    m_is_exiting = true;

    onExit();

    // Deactivate the (CORBA) receiver
    // After this call the controller will not be published in IPC any more
    // and will not receive any additional external command.
    // This cannot be done in the constructor because could create issues to
    // any derived class: the derived class could still receive external commands
    // after its destructor has been executed (destructors are chained).
    m_cmd_recv->deactivate();
}

void RunController::shutdown() {
    // This will allow 'run' to continue its execution
    // It is called by the signal handler installed by "Controller"
    // and by this class when the EXIT command is received

    // Be careful to what you call here! This may be called in a signal handler and not everything is safe!!!
    // https://www.securecoding.cert.org/confluence/pages/viewpage.action?pageId=1420
    m_cmd_recv->shutdown();
}

void RunController::onExit() {
    // This method cannot be called twice, but let's acquire the resources for this as well
    // so that the execution of not-compatible commands is not allowed
    {
        auto&& exit_res = m_cmd_ctrl->grantResource(std::shared_ptr<ExitCmd>(new ExitCmd()));

        // Interrupt the ApplicationController: from now it will refuse to execute anything but 'exiting()'
        m_app_ctrl->interrupt(true);

        // Do not accept transition commands any more and interrupt the current transition
        m_transitions_tp->pause();
        m_transitions_tp->flush();
        m_fsm_ctrl->interruptTransition(true);

        // Interrupt tasks currently executed by the command controller
        // Additional task not compatible with EXIT will then be rejected
        m_cmd_ctrl->interruptCurrentTasks();

        // Now that all the main components are halted, interrupt all the on-fly tasks
        m_tbk->interruptTasks();

        // Wait at most 1 s before starting to kill processes
        // This is just to have some possibility to kill processes once the FSM is idle
        // It is not strictly needed but it could help
        auto end = clock_t::now() + chrono::milliseconds(1000);
        while((m_fsm_ctrl->isBusy() == true) && (clock_t::now() <= end)) {
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }

        if(m_fsm_ctrl->isBusy() == true) {
            ERS_LOG("FSM not interrupted within the specified timeout, going on...");
        }

        // Stop the alarms but do not wait for them: it could be
        // time consuming and, anyway, the threads are joined when the
        // TP is destroyed
        m_scheduled_tp->pauseExecution(false);
        m_transition_timeout_timer->pauseExecution(false);

        // Kill all the processes: the "exiting()" will also take care
        // of removing the information from the IS server
        m_app_ctrl->exiting();

        // Not really needed but stops the compiler issuing a warning about
        // a not used variable
        exit_res.second.reset();
    }
    // The scope is closed here so that the resource for exit is released
    // and the IS information about the busy removal is updated
    // Otherwise that update would happen after the removal of the IS info

    // Remove info from the IS server
    // If not done some IS data structure may survive causing some issues
    // (like the persistence of the 'out' membership of some applications).
    m_info_publisher->removeRCInfo(m_app_name);

    // Remove registered call-backs
    m_app_ctrl->removeOnApplicationExitCallback(m_app_ctrl_token);

    for(const auto& t : m_fsm_ctrl_tokens) {
        // Look at the 'removeCallback' doc for synchronization issues
        m_fsm_ctrl->removeCallback(t);
    }

    // Look at the 'removeCallback' doc for synchronization issues
    m_cmd_ctrl->removeCallback(m_cmd_ctrl_token);
}

void RunController::makeTransition(const CommandedApplication::SenderContext& sc,
                                   const std::string& commandDescription)
{
    // Remember that a mal-formed command will throw
    // It is very important that the command has the proper runtime type
    // That's why the 'create' method is used here
    // The type is important because the command will be forwarded to the FSM that will call
    // the proper actions with the right command (i.e., the USERBROADCAST has its own command)

    ERS_LOG("Received a transition command from \"" << sc.getApplicationId() << "\"" << " running on host \""
            << sc.getHostName() << "\" as user \"" << sc.getUserName() << "\":\n" << commandDescription);

    const std::shared_ptr<TransitionCmd> trCmd(TransitionCmd::create(commandDescription));

    // Check with the AM
    const std::string& cmdName = trCmd->fsmCommand();
    if(m_am_bridge->canExecuteCommand(sc.getUserName(), cmdName) == false) {
        throw daq::rc::NotAllowed(ERS_HERE, cmdName, "the AccessManager denied its execution");
    }

    makeTransition(trCmd);
}

void RunController::makeTransition(const std::shared_ptr<TransitionCmd>& trCmd) {
    const std::string& fsmCmd = trCmd->fsmCommand();

    // Do not do the transition if in error (and the transition is not SHUTDOWN)
    if((thisControllerApplication()->getApplicationSelfInfo().isInternalError() == true) &&
       (fsmCmd != FSMCommands::SHUTDOWN_CMD))
    {
        throw daq::rc::InErrorState(ERS_HERE, fsmCmd);
    }

    // Add to the transition command information about the sub-transitions
    // Please, note that the order means!!!
    Algorithms::SubtransitionMap stm = m_subtr_tree;
    const Algorithms::SubtransitionMap& thisSubTr = thisControllerApplication()->subTransitions();
    for(const auto& pair : thisSubTr) {
        for(const auto& st : pair.second) {
            stm[pair.first].push_back(st);
        }
    }

    if(fsmCmd == FSMCommands::SUB_TRANSITION_CMD) {
        SubTransitionCmd* stc = static_cast<SubTransitionCmd*>(trCmd.get());
        const std::string& subTrName = stc->subTransition();
        FSM_COMMAND mainTrCmd = FSMCommands::stringToCommand(stc->mainTransitionCmd());

        const auto& mySelf = thisControllerApplication();
        const auto& it_1 = mySelf->subTransitions().find(mainTrCmd);
        const auto& it_2 = m_subtr_tree.find(mainTrCmd);

        if(((it_1 == mySelf->subTransitions().end()) || (std::find(it_1->second.begin(), it_1->second.end(), subTrName) == it_1->second.end())) &&
           ((it_2 == m_subtr_tree.end()) || (std::find(it_2->second.begin(), it_2->second.end(), subTrName) == it_2->second.end())))
        {
            throw daq::rc::BadCommand(ERS_HERE, subTrName, std::string("\"") + subTrName +
                                                           "\" is not a valid sub-transition for the main transition \"" +
                                                           stc->mainTransitionCmd() + "\"");
        }

        // This allows the FSM to compare the current sub-transition with the full list
        stc->allSubTransitions(stm);
    } else if(trCmd->skipSubTransitions() == false) {
        // Do not add sub-transitions to sub-transitions!
        if((trCmd->subTransitionsDone() == true) || (m_subtr_tree.empty() == true)) {
            // Sub-transitions from parents have already been done (or are not there),
            // add just the sub-transitions present in the segment
            trCmd->subTransitions(thisSubTr);
        } else {
            // Sub-transitions from parents have not been done (the controller received
            // a direct command, not through the RC tree). For consistency add the sub-transitions
            // that would have been executed if the command was propagated through the RC tree
            trCmd->subTransitions(stm);
        }
    }

    try {
        // Note that the TransitionTask constructor may throw
        std::shared_ptr<TransitionTask> trTask(new TransitionTask(*this, trCmd));
        m_tbk->addTask(trTask);
        m_transitions_tp->execute<void>(trTask);
    }
    catch(daq::rc::ThreadPoolPaused& ex) {
        const std::string& trName = trCmd->fsmCommand();
        const std::string& msg = std::string("The transition \"") + trName + "\" cannot be executed because the controller is shutting down";
        throw daq::rc::Busy(ERS_HERE, msg, trName, ex);
    }
}

void RunController::fsmBusy(bool isBusy) {
    // This is never concurrent with 'fsmTransitioning' and 'fsmTransitionDone' (always executed in the FSM thread)
    ERS_DEBUG(0, "FSM busy state is " << isBusy);

    if(isBusy == true) {
        m_transition_timeout_timer->resumeExecution();
    } else {
        m_transition_timeout_timer->pauseExecution(true);
    }

    const auto& mys = thisControllerApplication();
    const ApplicationSelfInfo& myInfo = (mys->setTransitioning(isBusy)).first;

    ERS_ASSERT(myInfo.isBusy() == true);

    // Update the ES about the status of all the run control and child controller applications
    // This is needed by the ES in order to properly evaluate issues like "child inconsistent"
    if(isBusy == false) {
        bulkUpdateToES();
    }

    notifyStatusChange(myInfo, mys->getActionTimeout(), mys->getExitTimeout());
}

void RunController::bulkUpdateToES(bool includeSV) {
    const auto& apps = m_app_list->getAllApplications();

    if(includeSV == true) {
        std::map<std::string, ApplicationSVInfo> statusInfo;
        for(const auto& app : apps) {
            statusInfo.insert(std::make_pair(app->getName(), app->getApplicationSVInfo()));
        }

        m_info_publisher->publish(statusInfo);
    }

    {
        std::map<std::string, ApplicationSelfInfo> stateInfo;
        for(const auto& app : apps) {
            const ApplicationType appType = app->type();
            if((appType == ApplicationType::RUN_CONTROL) || (appType == ApplicationType::CHILD_CONTROLLER)) {
                stateInfo.insert(std::make_pair(app->getName(), app->getApplicationSelfInfo()));
            }
        }

        m_info_publisher->publish(stateInfo);
    }
}

void RunController::fsmTransitioning(const TransitionCmd& trCmd) {
    // This is never concurrent with 'fsmBusy' and 'fsmTransitionDone' (always executed in the FSM thread)
    m_tr_start = clock_t::now();

    ERS_LOG("Transitioning from " << trCmd.fsmSourceState() << " to " << trCmd.fsmDestinationState() << " (command \"" << trCmd.toString() << "\")");

    const auto& mys = thisControllerApplication();

    m_transition_checker->activate(clock_t::now() + chrono::seconds(mys->getActionTimeout()),
                                   trCmd.fsmCommand());

    // Here the TransitionCmd is cloned because we want to keep all the original information (included the command UID)
    const ApplicationSelfInfo& myInfo = (mys->setCurrentTransitionCommand(std::shared_ptr<TransitionCmd>(trCmd.clone()))).first;
    notifyStatusChange(myInfo, mys->getActionTimeout(), mys->getExitTimeout());
}

void RunController::fsmTransitionDone(const TransitionCmd& trCmd) {
    // This is never concurrent with 'fsmTransitioning' and 'fsmBusy' (always executed in the FSM thread)
    const std::shared_ptr<ControllerApplication> mySelf(thisControllerApplication());

    // Here we can perform some actions that are related to the FSM but not to be propagated to children
    const FSM_COMMAND fsmCmd = FSMCommands::stringToCommand(trCmd.fsmCommand());
    switch(fsmCmd) {
        case FSM_COMMAND::CONFIGURE:
        {
            // At CONFIGURE restore the default timer values
            std::map<std::string, std::array<unsigned int, 2>> resch;

            const unsigned int probInt = mySelf->getProbeInterval();
            if(probInt != 0) {
                resch.insert(std::make_pair(RunControlCommands::PUBLISH_CMD, std::array<unsigned int, 2>{{0, probInt}}));
            } else {
                resch.insert(std::make_pair(RunControlCommands::PUBLISH_CMD, std::array<unsigned int, 2>{{std::numeric_limits<unsigned int>::max(),
                                                                                                          std::numeric_limits<unsigned int>::max()}}));
            }

            const unsigned int fullStatInt = mySelf->getFullStatInterval();
            if(fullStatInt != 0) {
                resch.insert(std::make_pair(RunControlCommands::PUBLISHSTATS_CMD, std::array<unsigned int, 2>{{0, fullStatInt}}));
            } else {
                resch.insert(std::make_pair(RunControlCommands::PUBLISHSTATS_CMD, std::array<unsigned int, 2>{{std::numeric_limits<unsigned int>::max(),
                                                                                                               std::numeric_limits<unsigned int>::max()}}));
            }

            m_scheduled_tp->rescheduleExecution(resch);

            break;
        }
        case FSM_COMMAND::CONNECT:
        {
            // At CONNECT restart the timers
            m_scheduled_tp->resumeExecution();

            break;
        }
        case FSM_COMMAND::DISCONNECT:
        case FSM_COMMAND::SHUTDOWN:
        {
            // At DISCONNECT and SHUTDOWN the timers are stopped
            ERS_LOG("Pausing the execution of periodic alarms");
            m_scheduled_tp->pauseExecution(true);
            ERS_LOG("Periodic alarms have been suspended");

            break;
        }
        default:
            break;
    }

    if(trCmd.isRestart() == false) {
        const std::string& srcState = trCmd.fsmSourceState();
        const std::string& dstState = trCmd.fsmDestinationState();

        const auto& allApps = m_app_list->getAllApplications();
        for(const auto& app_ : allApps) {
            if((ApplicationAlgorithms::canRunInState(*app_, dstState) == false)
               && ((fsmCmd == FSM_COMMAND::SHUTDOWN) || (ApplicationAlgorithms::canRunInState(*app_, srcState) == true)))
            {
                // Reset the application status to ABSENT if the application was in FAILED or EXITED
                // This make ES's life easier because the FAILED and EXITED states may trigger some rule
                m_app_ctrl->resetAppStatus(app_);

                // Reset the application's membership
                m_cmd_ctrl->executeCommand(std::shared_ptr<ChangeStatusCmd>(new ChangeStatusCmd(true, app_->getName())));
            }
        }
    }

    // This is the time the transition is done
    const auto stop = clock_t::now();

    m_transition_checker->deactivate();

    const std::string& newState = trCmd.fsmDestinationState();
    std::shared_ptr<const TransitionCmd>&& currTrCmd = (mySelf->setFSMState(FSMStates::stringToState(newState))).first.getCurrentTransitionCommand();
    mySelf->setLastTransitionCommand(currTrCmd); // No need to clone the command (it is retrieved from ApplicationSelfInfo - already cloned)
    mySelf->setLastTransitionTime(chrono::duration_cast<chrono::milliseconds>(stop - m_tr_start).count());
    const ApplicationSelfInfo& myInfo = (mySelf->setCurrentTransitionCommand(std::shared_ptr<TransitionCmd>(nullptr))).first;

    notifyStatusChange(myInfo, mySelf->getActionTimeout(), mySelf->getExitTimeout());

    ERS_LOG("Reached state " << newState);
}

void RunController::fsmNoTransition(const TransitionCmd& trCmd) {
    // This is always executed in the FSM thread
    ers::warning(daq::rc::EventNotProcessed(ERS_HERE, trCmd.fsmCommand()));
}

void RunController::fsmTransitionException(const TransitionCmd& trCmd, std::exception& ex) {
    // This is always executed in the FSM thread
    try {
        const daq::rc::TransitionInterrupted& intEx = dynamic_cast<const daq::rc::TransitionInterrupted&>(ex);
        ers::warning(intEx);
    }
    catch(std::bad_cast&) {
        try {
            const daq::rc::UserRoutineFailed& urEx = dynamic_cast<const daq::rc::UserRoutineFailed&>(ex);
            ers::fatal(urEx);
        }
        catch(std::bad_cast&) {
            ers::fatal(daq::rc::TransitionFailed(ERS_HERE, trCmd.fsmCommand(), "", ex));
        }
    }

    // The transition has been interrupted because of an exception, take note
    // 'fsmTransitionDone' is not called because the transition has not been completed
    fsmTransitionDoneAfterException(trCmd);
}

void RunController::fsmTransitionDoneAfterException(const TransitionCmd& trCmd) {
    m_transition_checker->deactivate();

    const auto& mySelf = thisControllerApplication();
    mySelf->setLastTransitionCommand(std::shared_ptr<TransitionCmd>(trCmd.clone()));
    const ApplicationSelfInfo& myInfo = (mySelf->setCurrentTransitionCommand(std::shared_ptr<TransitionCmd>(nullptr))).first;

    notifyStatusChange(myInfo, mySelf->getActionTimeout(), mySelf->getExitTimeout());
}

void RunController::executeCommand(const CommandedApplication::SenderContext& sc,
                                   const std::string& commandDescription)
{
    ERS_LOG("Received a command from \"" << sc.getApplicationId() << "\"" << " running on host \""
            << sc.getHostName() << "\" as user \"" << sc.getUserName() << "\":\n" << commandDescription);

    // The factory method gives the right run-time type of the command
    const std::shared_ptr<RunControlBasicCommand> recvCmd(RunControlCommands::factory(commandDescription));
    const std::string& cmdName = recvCmd->name();

    // Ask the AM
    if(m_am_bridge->canExecuteCommand(sc.getUserName(), cmdName) == false) {
        throw daq::rc::NotAllowed(ERS_HERE, cmdName, "the AccessManager denied its execution");
    }

    const RC_COMMAND cmd = RunControlCommands::stringToCommand(cmdName);

    // One of the aim of PUBLISH is to refresh this application info
    // So, let's not modify it!!!
    const auto& mys = thisControllerApplication();
    const ApplicationSelfInfo& myInfo = (cmd == RC_COMMAND::PUBLISH)
                                         ? mys->getApplicationSelfInfo()
                                         : (mys->setLastCommand(std::shared_ptr<RunControlBasicCommand>(recvCmd->clone()))).first;

    notifyStatusChange(myInfo, mys->getActionTimeout(), mys->getExitTimeout());

    try {
        switch(cmd) {
            case RC_COMMAND::TESTAPP:
            {
                m_cmd_ctrl->executeCommand(std::static_pointer_cast<const TestAppCmd>(recvCmd));
                break;
            }
            case RC_COMMAND::ENABLE:
            case RC_COMMAND::DISABLE:
            {
                m_cmd_ctrl->executeCommand(std::static_pointer_cast<const ChangeStatusCmd>(recvCmd));
                break;
            }
            case RC_COMMAND::STARTAPP:
            case RC_COMMAND::STOPAPP:
            case RC_COMMAND::RESTARTAPP:
            {
                const auto& fsmStatus = m_fsm_ctrl->status();

                std::shared_ptr<StartStopAppCmd>&& startStopCmd = std::static_pointer_cast<StartStopAppCmd>(recvCmd);
                startStopCmd->currentState(std::get<0>(fsmStatus));
                startStopCmd->destinationState(std::get<1>(fsmStatus));
                startStopCmd->transitionCommand(std::get<2>(fsmStatus));

                m_cmd_ctrl->executeCommand(startStopCmd);

                break;
            }
            case RC_COMMAND::CHANGE_PROBE_INTERVAL:
            {
                m_cmd_ctrl->executeCommand(std::static_pointer_cast<const ChangePublishIntervalCmd>(recvCmd));
                break;
            }
            case RC_COMMAND::CHANGE_FULLSTATS_INTERVAL:
            {
                m_cmd_ctrl->executeCommand(std::static_pointer_cast<const ChangeFullStatIntervalCmd>(recvCmd));
                break;
            }
            case RC_COMMAND::PUBLISH:
            {
                m_cmd_ctrl->executeCommand(std::static_pointer_cast<const PublishCmd>(recvCmd));
                break;
            }
            case RC_COMMAND::PUBLISHSTATS:
            {
                m_cmd_ctrl->executeCommand(std::static_pointer_cast<const PublishStatsCmd>(recvCmd));
                break;
            }
            case RC_COMMAND::IGNORE_ERROR:
            case RC_COMMAND::DYN_RESTART:
            {
                // Not doing anything: the ES will detect that the command has been received and
                // it will take any needed action in order to execute the command
                break;
            }
            case RC_COMMAND::EXIT:
            {
                // Resources for EXIT are allocated in 'onExit' given that those method
                // is the one executed both when the exit procedure is triggered by the
                // signal handler or by this command
                shutdown();
                break;
            }
            default:
            {
                throw daq::rc::BadCommand(ERS_HERE, cmdName, "is not a valid command for this controller");
            }
        }
    }
    catch(daq::rc::NotAllowed& ex) {
        throw daq::rc::Busy(ERS_HERE, cmdName, ex);
    }
}

void RunController::busyWithCommands(bool isBusy) {
    ERS_DEBUG(0, "Change in the controller busy state: busy state is now " << isBusy);

    const auto& mys = thisControllerApplication();

    const ApplicationSelfInfo& myInfo = (mys->setBusy(isBusy)).first;

    notifyStatusChange(myInfo, mys->getActionTimeout(), mys->getExitTimeout());
}

std::string RunController::status() {
    return buildApplicationStatusCmd(thisControllerApplication()->getApplicationSelfInfo())->serialize();
}

std::string RunController::childStatus(const std::string& childName) {
    const std::shared_ptr<Application>& app = m_app_list->getApplication(childName);
    if(app.get() != nullptr) {
        return buildApplicationStatusCmd(app->getApplicationSelfInfo())->serialize();
    } else {
        throw daq::rc::UnknownChild(ERS_HERE, childName);
    }
}

void RunController::updateChild(const std::shared_ptr<const ApplicationStatusCmd>& childStatus) {
    ERS_DEBUG(3, "Received update for child application:\n" << childStatus->serialize());

    // The "child update" is not executed in a dedicated thread
    // This means that almost all the times it is executed in the CORBA threads
    // Performance measurements have shown that in this way the performances are much better (2x)
    // Consistency is kept by the 'Application::update' method which is synchronized and
    // can deal with out-of-order updates
    const std::string& childName = childStatus->applicationName();
    const std::shared_ptr<Application>& app = m_app_list->getApplication(childName);
    if(app.get() != nullptr) {
        std::shared_ptr<ApplicationSelfInfo> appInfo(
                new ApplicationSelfInfo(FSMStates::stringToState(childStatus->fsmState()),
                                        childStatus->isTransitioning(),
                                        childStatus->errorReasons(),
                                        childStatus->badChildren(),
                                        childStatus->lastTransitionCommand(),
                                        childStatus->currentTransitionCommand(),
                                        childStatus->lastTransitionDuration(),
                                        childStatus->lastCommand(),
                                        childStatus->isBusy(),
                                        childStatus->isFullyInitialized(),
                                        childStatus->infoTimeTag(),
                                        childStatus->infoWallTime())
        );

        // Inform the ES; do not even update IS for this kind of info
        const std::shared_ptr<RunControlApplication> rcApp = std::static_pointer_cast<RunControlApplication>(app);
        m_info_publisher->publish(rcApp->getName(), rcApp->getActionTimeout(), rcApp->getExitTimeout(), *appInfo,
                                  InformationPublisher::EXPERT_SYSTEM);

        app->update(appInfo, true);
   } else {
        ers::warning(daq::rc::UnknownChild(ERS_HERE, childName));
    }
}

void RunController::updateChild(const std::string& childDescription) {
    std::shared_ptr<ApplicationStatusCmd> chUpdt;

    try {
        chUpdt.reset(new ApplicationStatusCmd(childDescription));
        updateChild(chUpdt);
    }
    catch(daq::rc::BadCommand& ex) {
        daq::rc::ChildUpdateFailure issue(ERS_HERE, "unknown", ex);
        issue.wrap_message("", ". The received update notification is corrupted");

        ers::error(issue);
    }
    catch(daq::rc::Exception& ex) {
        ers::error(daq::rc::ChildUpdateFailure(ERS_HERE, chUpdt->applicationName(), ex));
    }
}

void RunController::setError(const CommandedApplication::SenderContext& sc,
                             const CommandedApplication::ErrorItems& errorItems)
{
    ERS_LOG("Asked to set the error state from \"" << sc.getApplicationId() << "\" running on host \"" <<
            sc.getHostName() << "\" as user \"" << sc.getUserName() << "\"");

    if(m_am_bridge->canExecuteCommand(sc.getUserName(), "SET_ERROR") == false) {
        throw daq::rc::NotAllowed(ERS_HERE, "SET_ERROR", "the AccessManager denied its execution");
    }

    std::string fullError;
    std::vector<std::string> errs;
    std::vector<std::string> badCh;

    for(const auto& ei : errorItems) {
        // ApplicationName: error description (error type)
        const std::string& msg = ei[0] + ": " + ei[1] + " (" + ei[2] + ")";
        errs.push_back(msg);
        badCh.push_back(ei[0]);
        fullError += msg + " ";
    }

    const auto& mys = thisControllerApplication();
    const auto& update = mys->setInternalError(errs, badCh);
    if(update.second == true) {
        notifyStatusChange(update.first, mys->getActionTimeout(), mys->getExitTimeout());
    }

    ERS_LOG("Error is \"" + fullError + "\"");
}

void RunController::applicationExited(const std::shared_ptr<Application>& app, bool globalExit) {
    app->update(std::shared_ptr<ApplicationSelfInfo>(new ApplicationSelfInfo()), false);
    if(globalExit == false) {
        m_dvs_ctrl->resetTestStatus(app);
    }
}

bool RunController::periodicPublish() {
    // NOTE: this map is enough because this method is never called concurrently
    // If that changes, then additional synchronization is needed
    static std::map<std::string, std::future<daq::dvs::Result>> results;

    try {
        const auto& res = m_cmd_ctrl->grantResource(std::shared_ptr<PublishCmd>(new PublishCmd()));
        if(res.first == true) {
            const auto& mys = thisControllerApplication();
            const ApplicationSelfInfo& myInfo = mys->getApplicationSelfInfo();
            notifyStatusChange(myInfo, mys->getActionTimeout(), mys->getExitTimeout());

            // Decide whether tests can be done or not
            const auto& apps = m_app_list->getAllApplications();
            for(const auto& app_ : apps) {
                boost::this_thread::interruption_point();

                // Test applications that are IN and that are UP or should be UP
                // Test is done only if the previous session of periodic testing is completed
                const ApplicationSVInfo& appInfo = app_->getApplicationSVInfo();
                if((appInfo.isMembership() == true) && (appInfo.getStatus() == APPLICATION_STATUS::UP)) {
                    bool testIt = false;

                    const std::string& appName = app_->getName();
                    const auto& it = results.find(appName);
                    if(it == results.end()) {
                        // Test finished
                        testIt = true;
                    } else {
                        // Test not finished yet: check for its completion
                        std::future_status st = it->second.wait_for(std::chrono::milliseconds(1));
                        if(st == std::future_status::ready) {
                            results.erase(it);
                            testIt = true;
                        }
                    }

                    if(testIt == true) {
                        const daq::tmgr::TestResult appTestStatus = appInfo.getTestStatus();
                        auto&& fut = m_dvs_ctrl->testApplication(app_,
                                                                 daq::tm::Test::Scope::Functional,
                                                                 0,
                                                                 (appTestStatus == daq::tmgr::TmFail) ? true : false);
                        results.insert(std::make_pair(appName, std::move(fut)));
                    } else {
                        ERS_DEBUG(0, "Periodic tests not started for application \"" + appName + "\" because still being tested");
                    }
                }
            }
        } else {
            ERS_DEBUG(0, "Periodic publish not allowed: maybe we are exiting...");
        }
    }
    catch(boost::thread_interrupted&) {
        // Let this go: the TP will proper handle this
        // This is the mechanism used in order to pause/stop
        // the execution of periodic tasks
        ERS_DEBUG(0, RunControlCommands::PUBLISH_CMD << " has been interrupted");

        // Stop all the pending tests
        for(const auto& rems : results) {
            m_dvs_ctrl->stopTest(rems.first);
        }

        results.clear();

        throw;
    }
    catch(ers::Issue& ex) {
        ers::error(ex);
    }

    return true;
}

bool RunController::periodicUserPublish() {
    // This will throw if the thread is interrupted (i.e., the TP is stopped)
    // Let the exception go, the TP manages this
    boost::this_thread::interruption_point();

    const auto& res = m_cmd_ctrl->grantResource(std::shared_ptr<PublishCmd>(new PublishCmd()));
    if(res.first == true) {
        try {
            // Disable interruption for user code (the TP uses the
            // boost mechanism to pause/stop the execution of tasks)
            boost::this_thread::disable_interruption di;
            m_user_actions->publishAction();
        }
        catch(std::exception& ex) {
            ers::fatal(daq::rc::UserRoutineFailed(ERS_HERE, RunControlCommands::PUBLISH_CMD, ex.what()));
        }
    } else {
        ERS_DEBUG(0, "Periodic (user) publish not allowed: maybe we are exiting...");
    }

    return true;
}

bool RunController::periodicUserPublishStats() {
    // This will throw if the thread is interrupted (i.e., the TP is stopped)
    // Let the exception go, the TP manages this
    boost::this_thread::interruption_point();

    const auto& res = m_cmd_ctrl->grantResource(std::shared_ptr<PublishStatsCmd>(new PublishStatsCmd()));
    if(res.first == true) {
        try {
            // Disable interruption for user code (the TP uses the
            // boost mechanism to pause/stop the execution of tasks)
            boost::this_thread::disable_interruption di;
            m_user_actions->publishStatisticsAction();
        }
        catch(std::exception& ex) {
            ers::fatal(daq::rc::UserRoutineFailed(ERS_HERE, RunControlCommands::PUBLISHSTATS_CMD, ex.what()));
        }
    } else {
        ERS_DEBUG(0, "Periodic (user) publish-stats not allowed: maybe we are exiting...");
    }

    return true;
}

const std::string& RunController::getParent() const {
    return m_parent;
}

const std::string& RunController::getSegment() const {
    return m_segment;
}

std::string RunController::id() {
    return m_app_name;
}

std::string RunController::partition() {
    return m_partition;
}

std::shared_ptr<ControllerApplication> RunController::thisControllerApplication() const {
    std::shared_lock<std::shared_mutex> lk(m_myapp_mutex);
    return m_myself;
}

std::pair<bool, ApplicationSelfInfo> RunController::thisControllerApplicationStatus() const {
    std::shared_lock<std::shared_mutex> lk(m_myapp_mutex);
    return std::make_pair(m_is_exiting.load(), m_myself->getApplicationSelfInfo());
}

std::shared_ptr<ControllerApplication> RunController::thisControllerApplication(ControllerApplication* newApp) {
    std::lock_guard<std::shared_mutex> lk(m_myapp_mutex);
    newApp->update(*m_myself);
    m_myself.reset(newApp);

    return m_myself;
}

ApplicationController& RunController::applicationController() const {
    return *m_app_ctrl;
}

InformationPublisher& RunController::infoPublisher() const {
    return *m_info_publisher;
}

UserRoutines& RunController::userRoutines() const {
    return *m_user_actions;
}

FSMController& RunController::fsmController() const {
    return *m_fsm_ctrl;
}

DVSController& RunController::dvsController() const {
    return *m_dvs_ctrl;
}

const ApplicationList& RunController::applicationList() const {
    return *m_app_list;
}

void RunController::notifyStatusChange(const ApplicationSelfInfo& myInfo, int actionTimeout, int exitTimeout) {
    m_info_publisher->publish(m_app_name, actionTimeout, exitTimeout, myInfo, InformationPublisher::IS_SERVER);

    m_parentsender_tp->submit<void>([myInfo, this]() -> void
                                    {
                                         try {
                                            std::unique_ptr<ApplicationStatusCmd> asc = buildApplicationStatusCmd(myInfo);

                                            ERS_DEBUG(3, "Updating parent " << m_parent << " with info\n" << asc->serialize());

                                            m_cmd_sender->updateChild(m_parent, *asc);
                                        }
                                        catch(daq::rc::Exception& ex) {
                                            ers::error(daq::rc::ParentUpdateFailure(ERS_HERE, m_parent, ex));
                                        }
                                    }
    );
}

std::unique_ptr<const ApplicationStatusCmd> RunController::getParentStatus() const {
    std::unique_ptr<const ApplicationStatusCmd> parentStatus;

    try {
        parentStatus.reset((m_cmd_sender->status(m_parent)).release());
    }
    catch(daq::rc::Exception& ex) {
        ers::error(ex);
    }

    return parentStatus;
}

std::string RunController::getChildrenStatus() {
    // Try to connect to any application that is already running
    const auto& allApps = m_app_list->getAllApplications();

    // This thread pool is used only here, no need to keep it alive
    ThreadPool child_tp(5);

    std::vector<std::shared_ptr<FutureTask<void>>> results;
    for(const auto& app_ : allApps) {
        std::shared_ptr<FutureTask<void>> task(new ChildStatusTask(app_, *this));
        m_tbk->addTask(task);
        child_tp.execute<void>(task);
        results.push_back(std::move(task));
    }

    for(const auto& taskRes : results) {
        // This throws daq::rc::InterruptedException: let the exception go
        taskRes->get();
    }

    // Keep trace separately of the state of RC applications that are IN or OUT
    std::set<std::string> appsINState;
    std::set<std::string> appsOUTState;
    for(const auto& app_ : allApps) {
        const ApplicationType appType = app_->type();
        if((appType == ApplicationType::RUN_CONTROL) || (appType == ApplicationType::CHILD_CONTROLLER)) {
            const std::string& appState = FSMStates::stateToString(app_->getApplicationSelfInfo().getFSMState());
            // Do not take into account the applications that are ABSENT
            // It means that if the sets are empty then all the applications are ABSENT;
            // if the set has seize one, then all the applications are in that state.
            if(appState != FSMStates::ABSENT_STATE) {
                if(app_->getApplicationSVInfo().isMembership() == true) {
                    appsINState.insert(appState);
                } else {
                    appsOUTState.insert(appState);
                }
            }
        }
    }

    // In the following lines we want to achieve the following goal:
    // 1 - If the IN applications are in different states, then no assumption can be done and the not
    //     uniform state shall be reported;
    // 2 - If the IN applications are all in the same state, then this state is considered as the
    //     uniform state that is reported back to the caller;
    // 3 - If the IN applications are in ABSENT or INITIAL and the OUT applications are in ABSENT
    //     or INITIAL, then the membership is reset for all of them
    //
    // This will cover the case of the restart of a controller after a crash (i.e., the controller will
    // reach the state, if consistent, of the children) and the case in which the children are found only
    // in ABSENT (i.e., not running) or INITIAL (in this case the controller will reconnect to them and
    // continue its job). The reset of the membership is added to cover the case in which the controller
    // is killed before completing its job (i.e., the exit timeout elapses) and some application is left
    // out of membership.

    // This shall be filled with the consistent status (if any) of children
    std::string fsmState;

    const auto numOfINStates = appsINState.size();
    if(numOfINStates <= 1) {
        fsmState = ((numOfINStates == 1) ? *(appsINState.begin()) : FSMStates::ABSENT_STATE);

        if(((fsmState == FSMStates::ABSENT_STATE) || (fsmState == FSMStates::INITIAL_STATE)) &&
           ((appsOUTState.empty() == true) || ((appsOUTState.size() == 1) &&
            ((*appsOUTState.begin() == FSMStates::ABSENT_STATE) || (*appsOUTState.begin() == FSMStates::INITIAL_STATE)))))
        {
            // In this case all the applications that are OUT or IN are either in ABSENT or INITIAL (uniformly):
            // reset the membership of the applications that are OUT (they are going to be properly started later)
            for(const auto& app_ : allApps) {
                const auto& result = app_->setMembership(true);
                if(result.second == true) {
                    m_info_publisher->publish(app_->getName(), result.first);
                }
            }
        }
    } else {
        // Applications are not all in the same state: white flag!
        throw daq::rc::BadChildren(ERS_HERE, "Children are not in a consistent state: impossible to determine the FSM state to reach");
    }

    return fsmState;
}

std::unique_ptr<ApplicationStatusCmd> RunController::buildApplicationStatusCmd(const ApplicationSelfInfo& myInfo) const {
    std::unique_ptr<ApplicationStatusCmd> updateCmd(new ApplicationStatusCmd(m_app_name,
                                                                             myInfo.getFSMState(),
                                                                             myInfo.isTransitioning(),
                                                                             myInfo.isBusy(),
                                                                             myInfo.isFullyInitialized(),
                                                                             myInfo.getInfoTimeTag(),
                                                                             myInfo.getInfoWallTime()));
    updateCmd->errorReasons(myInfo.getErrorReasons());
    updateCmd->badChildren(myInfo.getBadChildren());

    const std::shared_ptr<const TransitionCmd>& ltc = myInfo.getLastTransitionCommand();
    if(ltc.get() != nullptr) {
        updateCmd->lastTransitionCommand(*ltc);
    }

    const std::shared_ptr<const TransitionCmd>& ctc = myInfo.getCurrentTransitionCommand();
    if(ctc.get() != nullptr) {
        updateCmd->currentTransitionCommand(*ctc);
    }

    const std::shared_ptr<const RunControlBasicCommand>& lc = myInfo.getLastCommand();
    if(lc.get() != nullptr) {
        updateCmd->lastCommand(*lc);
    }

    updateCmd->lastTransitionDuration(myInfo.getLastTransitionTime());

    return updateCmd;
}

std::unique_ptr<ControllerApplication> RunController::createThisControllerApplication() {
    try {
        OnlineServices& os = OnlineServices::instance();
        const daq::core::Partition& p = os.getPartition();
        Configuration& db = os.getConfiguration();

        const daq::core::Segment* segConf = p.get_segment(os.segmentName());

        return std::unique_ptr<ControllerApplication>(new ControllerApplication(db, p, *(segConf->get_controller())));
    }
    catch(daq::rc::ConfigurationIssue& ex) {
        throw;
    }
    catch(ers::Issue& ex) {
        throw daq::rc::ConfigurationIssue(ERS_HERE, ex.message(), ex);
    }
}

}}
