/*
 * ApplicationAlgorithms.cxx
 *
 *  Created on: Dec 4, 2012
 *      Author: avolio
 */

#include "RunControl/Controller/ApplicationAlgorithms.h"
#include "RunControl/Controller/ApplicationSupervisorInfo.h"
#include "RunControl/FSM/FSMStates.h"
#include "RunControl/FSM/FSMCommands.h"
#include "RunControl/Common/Exceptions.h"

#include <ProcessManager/Singleton.h>
#include <ProcessManager/Process.h>
#include <ProcessManager/Handle.h>
#include <ProcessManager/Exceptions.h>

#include <ers/ers.h>
#include <ers/Issue.h>
#include <ers/LocalContext.h>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/graph_selectors.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/named_function_params.hpp>
#include <boost/graph/properties.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/pending/property.hpp>
#include <boost/tuple/tuple.hpp>

#include <future>
#include <utility>
#include <algorithm>
#include <set>
#include <chrono>
#include <functional>
#include <iterator>
#include <list>
#include <system_error>
#include <thread>
#include <type_traits>


namespace daq { namespace rc {

// Simple structure used by the algorithm checking for cycles in the dependencies
struct cycle_detector : public boost::dfs_visitor<> {
    public:
        cycle_detector(bool& has_cycle) : m_has_cycle(has_cycle) {
        }

        template<typename Edge, typename Graph>
        void back_edge(Edge, Graph&) {
            m_has_cycle = true;
        }

    protected:
        bool& m_has_cycle;
};

void ApplicationAlgorithms::killLeftOver(const std::vector<std::shared_ptr<daq::pmg::Handle>>& foundHandles) {
    std::map<std::string, std::future<void>> killResults;

    for(const auto& pmgHandle : foundHandles) {
        try {
            auto result = std::async(std::launch::deferred,
                                     [pmgHandle]()
                                     {
                                        std::shared_ptr<daq::pmg::Process> p = getPmgProcess(*(pmgHandle.get()));
                                        if((p.get() != nullptr) && (p->exited() == false)) {
                                            try {
                                                p->kill();
                                                ERS_DEBUG(1, "Killed left-over process with handle " << pmgHandle->toString());
                                            }
                                            catch(daq::pmg::Process_Already_Exited& ex) {
                                                // The PMG gives this even if the process is REQUESTED
                                                if(p->exited() == false) {
                                                    throw daq::rc::ApplicationStopFailure(ERS_HERE, pmgHandle->applicationName(), "the application is still being started", ex);
                                                }
                                            }
                                            catch(daq::pmg::Exception& ex) {
                                                throw daq::rc::ApplicationStopFailure(ERS_HERE, pmgHandle->applicationName(), ex.message(), ex);
                                            }
                                        }
                                     }
            );

            killResults.insert(std::make_pair(pmgHandle->server(), std::move(result)));
        }
        catch(std::future_error& ex) {
            std::string errMsg = std::string("cannot start the task to kill the instance running on host ") + pmgHandle->server() + ": " + ex.what();
            throw daq::rc::LeftoverKillingFailure(ERS_HERE, pmgHandle->applicationName(), errMsg, ex);
        }
    }

    // Now loop over all the task results
    std::string failingKills;
    for(auto& kr : killResults) {
        try {
            kr.second.get();
        }
        catch(ers::Issue& ex) {
            ers::error(ex);
            failingKills += kr.first + " ";
        }
    }

    if(failingKills.empty() == false) {
        std::string errMsg = std::string("some issues occurred contacting the PMG on the following hosts: ") + failingKills;
        throw daq::rc::LeftoverKillingFailure(ERS_HERE, foundHandles[0]->applicationName(), errMsg);
    }
}

std::shared_ptr<daq::pmg::Process> ApplicationAlgorithms::getPmgProcess(const daq::pmg::Handle& handle) {
    std::shared_ptr<daq::pmg::Process> p;

    int nRetries = 3;
    while(nRetries-- > 0) {
        try {
            daq::pmg::Singleton* pmgSing = daq::pmg::Singleton::instance();
            p = pmgSing->get_process(handle);

            break;
        }
        catch(daq::pmg::No_PMG_Server& ex) {
            throw daq::rc::PMGError(ERS_HERE, handle.server(), "it looks like the PMG is not available", false, ex);
        }
        catch(daq::pmg::Exception& ex) {
            // Failed_Get_Process, Bad_PMG
            if(nRetries == 0) {
                throw daq::rc::PMGError(ERS_HERE, handle.server(), ex.message(), true, ex);
            }
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(300));
    }

    return p;
}

std::unique_ptr<daq::pmg::Handle>
ApplicationAlgorithms::getPmgHandle(const std::string& appName, const std::string& hostName, const std::string& partName)
{
    std::unique_ptr<daq::pmg::Handle> h;

    int nRetries = 3;
    while(nRetries-- > 0) {
        try {
            daq::pmg::Singleton* pmgSing = daq::pmg::Singleton::instance();
            h = pmgSing->lookup(hostName, appName, partName);

            break;
        }
        catch(daq::pmg::No_PMG_Server& ex) {
            // The PMG is not running, no need to continue
            throw daq::rc::PMGError(ERS_HERE, hostName, "it looks like the PMG is not available", false, ex);
        }
        catch(daq::pmg::Exception& ex) {
            if(nRetries == 0) {
                throw daq::rc::PMGError(ERS_HERE, hostName, ex.message(), true, ex);
            }
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(300));
    }

    return h;
}

ApplicationAlgorithms::OrderedApplicationMap ApplicationAlgorithms::resolveDependencies(const Application::ApplicationSet& appList,
                                                                                        bool isStart)
{
    typedef boost::property<boost::vertex_name_t, std::string> VertexName;
    typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS, VertexName> Graph;
    typedef boost::graph_traits<Graph>::vertex_descriptor vertex_descriptor;

    // This is the graph object
    Graph g;
    boost::property_map<Graph, boost::vertex_name_t>::type vertexNameMap = boost::get(boost::vertex_name, g);

    // To every application associate the Application pointer and the vertex in the graph
    // Here a vertex is created for each application and added to the graph
    std::map<std::string, std::pair<std::shared_ptr<Application>, vertex_descriptor>> nameMap;
    for(const std::shared_ptr<Application>& app_ : appList) {
        const std::string& appName = app_->getName();
        const vertex_descriptor& vd = boost::add_vertex(g);
        nameMap.insert(std::make_pair(appName, std::make_pair(app_, vd)));
        boost::put(vertexNameMap, vd, appName);
    }

    // Now loop over the list of applications and add the edges connecting the vertexes to the graph
    // (i.e., add the dependency relationships)
    std::function<std::set<std::string> (Application*)> getDependencies = ((isStart == true) ? &Application::getInitDep : &Application::getShutdownDep);
    for(const auto& p : nameMap) {
        const std::set<std::string>& deps = getDependencies((p.second).first.get());
        for(const std::string& name : deps) {
            const auto& it = nameMap.find(name);
            if(it != nameMap.end()) {
                boost::add_edge((p.second).second, (it->second).second, g);
                ERS_DEBUG(3, "Added dependency: " << p.second.first->getName() << " -> " << it->second.first->getName());
            }
        }
    }

    // Detect possible cycles in the dependency graph
    bool has_cycle = false;
    cycle_detector vis(has_cycle);
    boost::depth_first_search(g, boost::visitor(vis));

    if(has_cycle == true) {
        std::string apps;
        for(const std::shared_ptr<Application>& app_ : appList) {
            apps += app_->getName() + " ";
        }

        throw daq::rc::DependencyCycle(ERS_HERE, isStart, apps);
    }

    // This map collects the applications that can be started in parallel
    ApplicationAlgorithms::OrderedApplicationMap orderedApplications;

    // Now run the algorithm to solve the dependencies
    // Taken from one of the boost::graph examples
    typedef std::list<vertex_descriptor> MakeOrder;
    MakeOrder::iterator i;
    MakeOrder make_order;

    boost::topological_sort(g, std::front_inserter(make_order));
    std::vector<int> time(nameMap.size(), 0);
    for(i = make_order.begin(); i != make_order.end(); ++i) {
        // Walk through the in_edges an calculate the maximum time.
        if(boost::in_degree(*i, g) > 0) {
            Graph::in_edge_iterator j, j_end;
            int maxdist = 0;
            // Through the order from topological sort, we are sure that every
            // time we are using here is already initialized.
            for(boost::tie(j, j_end) = boost::in_edges(*i, g); j != j_end; ++j) {
                maxdist = (std::max)(time[boost::source(*j, g)], maxdist);
            }
            time[*i] = maxdist + 1;
        }

        // Applications with the same 'time' value can be started in parallel
        // Applications with highest 'time' values should be started first
        const std::string& vertexName = boost::get(vertexNameMap, *i);
        orderedApplications[time[*i]].push_back(nameMap[vertexName].first);
    }

    return orderedApplications;
}

bool ApplicationAlgorithms::shouldRunInState(const Application& app, const std::string& state) {
    return ((app.isSpontaneousExitAllowed() == false) &&
            (ApplicationAlgorithms::canRunInState(app, state) == true));
}

bool ApplicationAlgorithms::canRunInState(const Application& app, const std::string& state) {
    return ApplicationAlgorithms::canBeStarted(app, state, state, false);
}

bool ApplicationAlgorithms::canBeStarted(const Application& app,
                                         const std::string& currentState,
                                         const std::string& destinationState,
                                         bool isRestart) {
    const bool restartInRunning = ((isRestart == true) &&
                                   (app.isRestartableRunning() == false) &&
                                   (currentState == FSMStates::stateToString(FSM_STATE::RUNNING)));
    if(restartInRunning == true) {
        return false;
    }

    bool allow = false;
    bool allowedCombination = false;

    const unsigned int stateRef = static_cast<unsigned int>(FSMStates::stringToState((currentState == destinationState) ? currentState : destinationState));
    const std::string& startAt = app.getStartAt();
    const std::string& stopAt = app.getStopAt();
    if((startAt == FSMCommands::SOR_CMD) && (stopAt == FSMCommands::EOR_CMD)) {
        // Lifetime SOR_EOR
        allow = ((stateRef > static_cast<unsigned int>(FSM_STATE::CONNECTED)) &&
                (stateRef <= static_cast<unsigned int>(FSM_STATE::RUNNING)));
        allowedCombination = true;
    } else if((startAt == FSMCommands::EOR_CMD) && (stopAt == FSMCommands::SOR_CMD)) {
        // Lifetime EOR_SOR
        allow = (stateRef <= static_cast<unsigned int>(FSM_STATE::CONNECTED));
        allowedCombination = true;
    } else if((startAt == FSMCommands::INIT_FSM_CMD) && (stopAt == FSMCommands::STOP_FSM_CMD)) {
        // Lifetime BOOT_SHUTDOWN
        allow = true;
        allowedCombination = true;
    } else if((startAt == FSMCommands::INITIALIZE_CMD) && (stopAt == FSMCommands::SHUTDOWN_CMD)) {
        // Lifetime BOOT_SHUTDOWN
        allow = (stateRef >= static_cast<unsigned int>(FSM_STATE::INITIAL));
        allowedCombination = true;
    } else if((startAt == FSMCommands::CONFIGURE_CMD) && (stopAt == FSMCommands::UNCONFIGURE_CMD)) {
        // Lifetime Configure_Unconfigure
        allow = (stateRef >= static_cast<unsigned int>(FSM_STATE::CONFIGURED));
        allowedCombination = true;
    } else if(startAt == Application::START_AT_USERDEFINED) {
        // Lifetime USERDEFINED_SHUTDOWN
        if(stopAt == FSMCommands::STOP_FSM_CMD) {
            allow = true;
            allowedCombination = true;
        } else if(stopAt == FSMCommands::SHUTDOWN_CMD) {
            allow = (stateRef >= static_cast<unsigned int>(FSM_STATE::INITIAL));
            allowedCombination = true;
        }
   }

    if(allowedCombination == false) {
        const std::string& msg = std::string("bad startAt/stopAt combination detected (") + startAt + "/" + stopAt + ")";
        ers::warning(daq::rc::ApplicationGenericIssue(ERS_HERE, app.getName(), msg));
    }

    return allow;
}

bool ApplicationAlgorithms::isStartCompleted(const Application& app, bool checkMembership, bool isTransitioning) {
    // The application is considered as restarted if it is UP, EXITED or FAILED
    // The check on EXITED has a meaning because this state can be reached only if the application was UP before
    const ApplicationSVInfo& appInfo = app.getApplicationSVInfo();
    const APPLICATION_STATUS appStatus = appInfo.getStatus();

    bool startCompleted = (((checkMembership == true) && (appInfo.isMembership() == false)) || (appStatus == APPLICATION_STATUS::UP)
            || (appStatus == APPLICATION_STATUS::EXITED) || ((isTransitioning == false) && (appStatus == APPLICATION_STATUS::FAILED)));

    if(startCompleted == true) {
        ERS_DEBUG(4, "Application " << app.getName() << " is started");
    }

    return startCompleted;
}

bool ApplicationAlgorithms::isStopCompleted(const Application& app, bool checkMembership) {
    const ApplicationSVInfo& appSVInfo = app.getApplicationSVInfo();
    const APPLICATION_STATUS appStatus = appSVInfo.getStatus();
    const bool membership = appSVInfo.isMembership();

    const bool result = (((checkMembership == true) && (membership == false)) || (appStatus == APPLICATION_STATUS::EXITED)
            || (appStatus == APPLICATION_STATUS::FAILED) || (appStatus == APPLICATION_STATUS::ABSENT));

    if(result == true) {
        ERS_DEBUG(4, "Application " << app.getName() << " is stopped");
    }

    return result;
}

}}

