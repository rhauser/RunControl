/*
 * CommandController.cxx
 *
 *  Created on: Feb 19, 2013
 *      Author: avolio
 */

#include "RunControl/Controller/CommandController.h"
#include "RunControl/Controller/DVSController.h"
#include "RunControl/Controller/Application.h"
#include "RunControl/Controller/ApplicationController.h"
#include "RunControl/Controller/ApplicationList.h"
#include "RunControl/Controller/InformationPublisher.h"
#include "RunControl/Controller/ApplicationSelfInfo.h"
#include "RunControl/Controller/ApplicationSupervisorInfo.h"
#include "RunControl/Common/UserRoutines.h"
#include "RunControl/Common/ThreadPool.h"
#include "RunControl/Common/ScheduledThreadPool.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/CommandNotifierAdapter.h"

#include <TestManager/Test.h>
#include <ers/Issue.h>
#include <ers/LocalContext.h>
#include <ers/ers.h>

#include <vector>
#include <set>
#include <thread>
#include <functional>
#include <algorithm>
#include <limits>
#include <mutex>
#include <chrono>
#include <iterator>
#include <ostream>


namespace daq { namespace rc {

// WARNING: when deriving from this class, in case the "done()" method is overridden,
// remember to ALWAYS (really ALWAYS) call the "done()" method from this base class
// from the derived class as the last action
class CommandTask : public FutureTask<void> {
    public:
        // throws daq::rc::NotAllowed
        CommandTask(const std::shared_ptr<const RunControlBasicCommand>& cmd, CommandController& cmdCtrl)
            : FutureTask<void>(), m_cmd(cmd), m_cmd_ctrl(cmdCtrl)
        {
            m_cmd_ctrl.get().allocateCommandResource(m_cmd); // this throws
        }

        virtual ~CommandTask() {}

    protected:
        void done() noexcept override {
            // Release the resource
            m_cmd_ctrl.get().releaseCommandResource(m_cmd);

            // Notify the command sender if requested
            m_cmd_ctrl.get().notifyCommandSender(m_cmd);
        }

        const RunControlBasicCommand& command() const {
            return *m_cmd;
        }

        const ApplicationList& applicationList() const {
            return *(m_cmd_ctrl.get().m_app_list);
        }

        InformationPublisher& infoPublisher() const {
            return *(m_cmd_ctrl.get().m_info_publisher);
        }

        ApplicationController& applicationController() const {
            return *(m_cmd_ctrl.get().m_app_ctrl);
        }

        DVSController& dvsController() const {
            return *(m_cmd_ctrl.get().m_dvs_ctrl);
        }

        ScheduledThreadPool& scheduledActions() const {
            return m_cmd_ctrl.get().m_scheduled_actions.get();
        }

        UserRoutines& userActions() const {
            return *(m_cmd_ctrl.get().m_user_actions);
        }

    private:
        const std::shared_ptr<const RunControlBasicCommand> m_cmd;
        const std::reference_wrapper<CommandController> m_cmd_ctrl;
};

class StartAppTask : public CommandTask {
    public:
        // throws daq::rc::NotAllowed
        StartAppTask(const std::shared_ptr<const StartStopAppCmd>& cmd, CommandController& cmdCtrl)
            : CommandTask(cmd, cmdCtrl)
        {
        }

    protected:
        void run() override {
            const StartStopAppCmd& cmd = static_cast<const StartStopAppCmd&>(command());

            try {
                interrupted();

                // This throws
                auto&& startedApps = applicationController().startApps(cmd);
                while(startedApps.empty() == false) {
                    for(auto it = startedApps.begin(); it != startedApps.end(); ) {
                        const ApplicationType appType = (*it)->type();
                        const ApplicationSelfInfo& appSelfInfo = (*it)->getApplicationSelfInfo();
                        const ApplicationSVInfo& appSVInfo = (*it)->getApplicationSVInfo();

                        // The application restart is considered done if:
                        // 1 - The application is not a run controlled application (including child controllers);
                        // 2 - The application is no more UP (i.e., signaled, killed or exited);
                        // 3 - The application is out of membership;
                        // 4 - The application is in error state;
                        // 5 - The application acknowledges its full initialization.
                        const bool appStartIsDone = (((appType != ApplicationType::RUN_CONTROL) && (appType != ApplicationType::CHILD_CONTROLLER)) ||
                                                     (appSVInfo.getStatus() != APPLICATION_STATUS::UP) ||
                                                     (appSVInfo.isMembership() == false) ||
                                                     (appSelfInfo.isInternalError() == true) ||
                                                     (appSelfInfo.isFullyInitialized() == true));

                        if(appStartIsDone == true) {
                            startedApps.erase(it++);
                        } else {
                            ++it;
                        }
                    }

                    interrupted();

                    std::this_thread::sleep_for(std::chrono::milliseconds(1));
                }
            }
            catch(daq::rc::InterruptedException& ex) {
                ers::warning(daq::rc::ExecutionFailed(ERS_HERE, cmd.name(), "the execution has been interrupted (probably the controller is shutting down)", ex));
            }
            catch(daq::rc::Exception& ex) {
                ers::error(daq::rc::ExecutionFailed(ERS_HERE, cmd.name(), "", ex));
            }
        }
};

class KillAppTask : public CommandTask {
    public:
        // throws daq::rc::NotAllowed
        KillAppTask(const std::shared_ptr<const StartStopAppCmd>& cmd, CommandController& cmdCtrl)
            : CommandTask(cmd, cmdCtrl)
        {
        }

    protected:
        void run() override {
            const StartStopAppCmd& cmd = static_cast<const StartStopAppCmd&>(command());

            try {
                applicationController().stopApps(cmd);
            }
            catch(daq::rc::InterruptedException& ex) {
                ers::error(daq::rc::ExecutionFailed(ERS_HERE, cmd.name(), "the execution has been interrupted", ex));
            }
            catch(daq::rc::Exception& ex) {
                ers::error(daq::rc::ExecutionFailed(ERS_HERE, cmd.name(), "", ex));
            }
        }
};

class RestartAppTask : public CommandTask {
    public:
        // throws daq::rc::NotAllowed
        RestartAppTask(const std::shared_ptr<const StartStopAppCmd>& cmd, CommandController& cmdCtrl)
            : CommandTask(cmd, cmdCtrl), m_apps_to_restart()
        {
        }

        ~RestartAppTask() {
            for(const auto& app_ : m_apps_to_restart) {
                const auto& rstPair = app_->setRestarting(false);
                if(rstPair.second == true) {
                    infoPublisher().publish(app_->getName(), rstPair.first);
                }
            }
        }

    protected:
        void run() override {
            const StartStopAppCmd& cmd = static_cast<const StartStopAppCmd&>(command());

            try {
                // Set the restarting flag for the applications
                std::vector<std::string> appsToRestart_Names;
                for(auto&& entry : applicationList().getApplications(cmd.applications())) {
                    if(entry.second.get() != nullptr) {
                        const auto& rstPair = entry.second->setRestarting(true);
                        if(rstPair.second == true) {
                            infoPublisher().publish(entry.first, rstPair.first);

                            appsToRestart_Names.push_back(entry.first);
                            m_apps_to_restart.insert(std::move(entry.second));
                        } else {
                            const std::string& msg = std::string("the application \"") + entry.first +
                                                     "\" cannot be restarted because it is already being restarted. Kill it instead if needed!";
                            ers::warning(daq::rc::ApplicationGenericIssue(ERS_HERE, entry.first, msg));
                        }
                    } else {
                        const std::string& msg = std::string("the application \"") + entry.first + "\" cannot be restarted because it does not exist";
                        ers::warning(daq::rc::ApplicationGenericIssue(ERS_HERE, entry.first, msg));
                    }
                }

                interrupted();

                // Clone and change the list of applications
                // Cloning is better because it avoids synchronization issues
                std::unique_ptr<StartStopAppCmd> cmd_new = cmd.clone();
                cmd_new->applications(appsToRestart_Names);

                // This throws
                const auto& reallyStartedApps = applicationController().restartApps(*cmd_new);

                // The restart is finished for applications that could not be restarted
                Application::ApplicationSet notStartedApps;
                std::set_difference(m_apps_to_restart.begin(), m_apps_to_restart.end(),
                                    reallyStartedApps.begin(), reallyStartedApps.end(),
                                    std::inserter(notStartedApps, notStartedApps.end()),
                                    notStartedApps.key_comp());

                for(const auto& app_ : notStartedApps) {
                    const auto& rstPair = app_->setRestarting(false);
                    if(rstPair.second == true) {
                        infoPublisher().publish(app_->getName(), rstPair.first);
                    }

                    // Remove from the list of restarted applications: it failed
                    m_apps_to_restart.erase(app_);
                }

                // Now wait for applications to reach the right state
                while(m_apps_to_restart.empty() == false) {
                    for(auto it = m_apps_to_restart.begin(); it != m_apps_to_restart.end(); ) {
                        const ApplicationType appType = (*it)->type();
                        const ApplicationSelfInfo& appSelfInfo = (*it)->getApplicationSelfInfo();
                        const ApplicationSVInfo& appSVInfo = (*it)->getApplicationSVInfo();

                        // At this point in the code the restart is considered done if:
                        // 1 - The application is not a run controlled application (including child controllers);
                        // 2 - The application is no more UP (i.e., signaled, killed or exited);
                        // 3 - The application is out of membership;
                        // 4 - The application is in an error state;
                        // 5 - The application acknowledges its full initialization.
                        const bool appRestartIsDone = (((appType != ApplicationType::RUN_CONTROL) && (appType != ApplicationType::CHILD_CONTROLLER)) ||
                                                        (appSVInfo.getStatus() != APPLICATION_STATUS::UP) ||
                                                        (appSVInfo.isMembership() == false) ||
                                                        (appSelfInfo.isInternalError() == true) ||
                                                        (appSelfInfo.isFullyInitialized() == true));
                        if(appRestartIsDone == true) {
                            const auto& rstPair = (*it)->setRestarting(false);
                            if(rstPair.second == true) {
                                // Here use the application info from the pair in order to be sure to pick-up
                                // any possible concurrent modification since the beginning of the loop
                                infoPublisher().publish((*it)->getName(), rstPair.first);
                            }

                            m_apps_to_restart.erase(it++);
                        } else {
                            ++it;
                        }
                    }

                    interrupted();

                    std::this_thread::sleep_for(std::chrono::milliseconds(1));
                }
            }
            catch(daq::rc::InterruptedException& ex) {
                ers::warning(daq::rc::ExecutionFailed(ERS_HERE, cmd.name(), "the execution has been interrupted (probably the controller is shutting down)", ex));
            }
            catch(daq::rc::Exception& ex) {
                ers::error(daq::rc::ExecutionFailed(ERS_HERE, cmd.name(), "", ex));
            }
        }

    private:
        Application::ApplicationSet m_apps_to_restart;
};

class TestAppTask : public CommandTask {
    public:
        // throws daq::rc::NotAllowed
        TestAppTask(const std::shared_ptr<const TestAppCmd>& cmd, CommandController& cmdCtrl)
            : CommandTask(cmd, cmdCtrl)
        {
        }

    protected:
        void run() override {
            const TestAppCmd& cmd = static_cast<const TestAppCmd&>(command());

            const auto& apps = cmd.applications();
            const auto& expandedApps = applicationList().getApplications(std::set<std::string>({apps.begin(), apps.end()}));
            for(const auto& entry : expandedApps) {
                if(entry.second.get() != nullptr) {
                    dvsController().testApplication(entry.second, cmd.scope(), cmd.level(), true);
                } else {
                    ers::warning(daq::rc::ApplicationGenericIssue(ERS_HERE,
                                                                  entry.first,
                                                                  "received a command to test it, but it is not in the list of child applications"));
                }
            }
        }
};

class PublishTask : public CommandTask {
    public:
        // throws daq::rc::NotAllowed
        PublishTask(const std::shared_ptr<const PublishCmd>& cmd, CommandController& cmdCtrl)
            : CommandTask(cmd, cmdCtrl)
        {
        }

    protected:
        void run() override {
            const PublishCmd& cmd = static_cast<const PublishCmd&>(command());

            try {
                // Here we call only the user action because the controller always
                // publishes its information when a command is received

                // NOTE: if this is ever changed, then look at RunController::periodicPublish
                // and check that it keeps to be consistent even in case of concurrent calls
                userActions().publishAction();
            }
            catch(std::exception& ex) {
                ers::error(daq::rc::ExecutionFailed(ERS_HERE, cmd.name(), "", ex));
            }
        }
};

class ChangePublishTask : public CommandTask {
    public:
        // throws daq::rc::NotAllowed
        ChangePublishTask(const std::shared_ptr<const ChangePublishIntervalCmd>& cmd, CommandController& cmdCtrl)
            : CommandTask(cmd, cmdCtrl)
        {
        }

    protected:
        void run() override {
            const ChangePublishIntervalCmd& cmd = static_cast<const ChangePublishIntervalCmd&>(command());

            const unsigned int newInterval = cmd.publishInterval();
            if(newInterval != 0) {
                scheduledActions().rescheduleExecution({{RunControlCommands::PUBLISH_CMD, {{0, newInterval}}}});
            } else {
                scheduledActions().rescheduleExecution({{RunControlCommands::PUBLISH_CMD, {{std::numeric_limits<unsigned int>::max(),
                                                                                            std::numeric_limits<unsigned int>::max()}}}});
            }
        }
};

class PublishStatsTask : public CommandTask {
    public:
        // throws daq::rc::NotAllowed
        PublishStatsTask(const std::shared_ptr<const PublishStatsCmd>& cmd, CommandController& cmdCtrl)
            : CommandTask(cmd, cmdCtrl)
        {
        }

    protected:
        void run() override {
            const PublishStatsCmd& cmd = static_cast<const PublishStatsCmd&>(command());

            try {
                userActions().publishStatisticsAction();
            }
            catch(std::exception& ex) {
                ers::error(daq::rc::ExecutionFailed(ERS_HERE, cmd.name(), "", ex));
            }
        }
};

class ChangeFullStatsTask : public CommandTask {
    public:
        // throws daq::rc::NotAllowed
        ChangeFullStatsTask(const std::shared_ptr<const ChangeFullStatIntervalCmd>& cmd, CommandController& cmdCtrl)
            : CommandTask(cmd, cmdCtrl)
        {
        }

    protected:
        void run() override {
            const ChangeFullStatIntervalCmd& cmd = static_cast<const ChangeFullStatIntervalCmd&>(command());

            const unsigned int newInterval = cmd.fullStatInterval();
            if(newInterval != 0) {
                scheduledActions().rescheduleExecution({{RunControlCommands::PUBLISHSTATS_CMD, {{0, newInterval}}}});
            } else {
                scheduledActions().rescheduleExecution({{RunControlCommands::PUBLISHSTATS_CMD, {{std::numeric_limits<unsigned int>::max(),
                                                                                                 std::numeric_limits<unsigned int>::max()}}}});
            }
        }
};

CommandController::CommandController(const std::shared_ptr<ApplicationController>& appCtrl,
                                     const std::shared_ptr<const ApplicationList>& appList,
                                     const std::shared_ptr<DVSController>& dvsCtrl,
                                     const std::shared_ptr<InformationPublisher>& infoPublisher,
                                     const std::shared_ptr<UserRoutines>& userActions,
                                     ScheduledThreadPool& scheduledActions)
    : m_tbk(new TaskBookkeeper()), m_commands(), m_app_list(appList), m_dvs_ctrl(dvsCtrl), m_app_ctrl(appCtrl), m_user_actions(userActions),
      m_info_publisher(infoPublisher), m_notifier_tp(), m_scheduled_actions(std::ref(scheduledActions)),
      m_command_executor_tp(new ThreadPool(15))
{
    // The "m_notifier_tp" thread pool is started only when needed (see notifyCommandSender)
}

CommandController::~CommandController() {
}

std::pair<bool, CommandController::CommandResource> CommandController::grantResource(const std::shared_ptr<RunControlBasicCommand>& cmd) {
    try {
        allocateCommandResource(cmd);

        // Check whether the command sender has to be notified about the execution of the command
        // If the command is a transition command, then we need to remove the sender reference
        // from the command attributes. This is a simple way in order to avoid that child applications
        // receiving the transition command try to notify the sender as well.
        // Of course this "stripping" is not done for normal commands because they are not propagated
        // through out the Run Control tree.
        const std::string& senderReference = cmd->senderReference();
        if((senderReference.empty() == false) && (cmd->name() == RunControlCommands::MAKE_TRANSITION_CMD)) {
            cmd->senderReference("");
        }

        return std::make_pair(true, CommandController::CommandResource(nullptr, [this, cmd, senderReference](void*)
                                                                                {
                                                                                    releaseCommandResource(cmd);
                                                                                    notifyCommandSender(senderReference, cmd);
                                                                                }
                                                                       )
                             );
    }
    catch(daq::rc::NotAllowed&) {
        return std::make_pair(false, CommandController::CommandResource(nullptr, [](void*) {}));
    }
}

void CommandController::executeCommand(const std::shared_ptr<const ChangeStatusCmd>& cmd) {
    const std::vector<std::string>& apps = cmd->components();
    const auto& expandedApps = m_app_list->getApplications(std::set<std::string>{apps.begin(), apps.end()});
    for(const auto& entry : expandedApps) {
        if(entry.second.get() != nullptr) {
            const auto& update = entry.second->setMembership(cmd->isEnable());
            if(update.second == true) {
                m_info_publisher->publish(entry.second->getName(), update.first);

                if((cmd->isEnable() == true) && (update.first.getStatus() == APPLICATION_STATUS::UP)) {
                    m_dvs_ctrl->testApplication(entry.second, daq::tm::Test::Scope::Functional, 0, true);
                }
            }
        } else {
            ers::warning(daq::rc::ExecutionFailed(ERS_HERE, cmd->name(), std::string("application \"") + entry.first + "\" is not known"));
        }
    }

    // This is the only synchronous command, that's why we notify the sender here
    notifyCommandSender(cmd);
}

void CommandController::executeCommand(const std::shared_ptr<const StartStopAppCmd>& cmd) {
    // This synchronizes with 'interrupt': in such a way we are sure that when the TP is
    // paused and cleared and the tasks are interrupted, there is no task that in the while is going
    // to be submitted to the TP (and will escape from the interruption)
    // The read-lock should be acquired any time a task is created and/or submitted to the TP
    std::shared_lock<std::shared_mutex> lk(m_sub_mutex);

    std::shared_ptr<FutureTask<void>> task;
    try {
        if(cmd->isRestart() == true) {
            task.reset(new RestartAppTask(cmd, *this));
        } else if(cmd->isStart() == true) {
            task.reset(new StartAppTask(cmd, *this));
        } else {
            task.reset(new KillAppTask(cmd, *this));
        }

        m_command_executor_tp->execute<void>(task);
        m_tbk->addTask(task);
    }
    catch(daq::rc::ThreadPoolPaused& ex) {
        throw daq::rc::NotAllowed(ERS_HERE, cmd->name(), "the command execution is suspended (probably the controller is shutting down)", ex);
    }
}

void CommandController::executeCommand(const std::shared_ptr<const TestAppCmd>& cmd) {
    std::shared_lock<std::shared_mutex> lk(m_sub_mutex);

    std::shared_ptr<FutureTask<void>> task(new TestAppTask(cmd, *this));

    try {
        m_command_executor_tp->execute<void>(task);
        m_tbk->addTask(task);
    }
    catch(daq::rc::ThreadPoolPaused& ex) {
        throw daq::rc::NotAllowed(ERS_HERE, cmd->name(), "the command execution is suspended (probably the controller is shutting down)", ex);
    }
}

void CommandController::executeCommand(const std::shared_ptr<const PublishCmd>& cmd) {
    std::shared_lock<std::shared_mutex> lk(m_sub_mutex);

    std::shared_ptr<FutureTask<void>> task(new PublishTask(cmd, *this));

    try {
        m_command_executor_tp->execute<void>(task);
        m_tbk->addTask(task);
    }
    catch(daq::rc::ThreadPoolPaused& ex) {
        throw daq::rc::NotAllowed(ERS_HERE, cmd->name(), "the command execution is suspended (probably the controller is shutting down)", ex);
    }
}

void CommandController::executeCommand(const std::shared_ptr<const ChangePublishIntervalCmd>& cmd) {
    std::shared_lock<std::shared_mutex> lk(m_sub_mutex);

    std::shared_ptr<FutureTask<void>> task(new ChangePublishTask(cmd, *this));

    try {
        m_command_executor_tp->execute<void>(task);
        m_tbk->addTask(task);
    }
    catch(daq::rc::ThreadPoolPaused& ex) {
        throw daq::rc::NotAllowed(ERS_HERE, cmd->name(), "the command execution is suspended (probably the controller is shutting down)", ex);
    }
}

void CommandController::executeCommand(const std::shared_ptr<const PublishStatsCmd>& cmd) {
    std::shared_lock<std::shared_mutex> lk(m_sub_mutex);

    std::shared_ptr<FutureTask<void>> task(new PublishStatsTask(cmd, *this));

    try {
        m_command_executor_tp->execute<void>(task);
        m_tbk->addTask(task);
    }
    catch(daq::rc::ThreadPoolPaused& ex) {
        throw daq::rc::NotAllowed(ERS_HERE, cmd->name(), "the command execution is suspended (probably the controller is shutting down)", ex);
    }
}

void CommandController::executeCommand(const std::shared_ptr<const ChangeFullStatIntervalCmd>& cmd) {
    std::shared_lock<std::shared_mutex> lk(m_sub_mutex);

    std::shared_ptr<FutureTask<void>> task(new ChangeFullStatsTask(cmd, *this));

    try {
        m_command_executor_tp->execute<void>(task);
        m_tbk->addTask(task);
    }
    catch(daq::rc::ThreadPoolPaused& ex) {
        throw daq::rc::NotAllowed(ERS_HERE, cmd->name(), "the command execution is suspended (probably the controller is shutting down)", ex);
    }
}

void CommandController::allocateCommandResource(const std::shared_ptr<const RunControlBasicCommand>& cmd) {
    const std::set<RC_COMMAND>& badCmds = cmd->notConcurrentWith();
    const std::string& cmdName = cmd->name();

    ERS_DEBUG(3, "Asked to allocate resources for command \"" << cmdName << "\"");

    std::lock_guard<std::mutex> lk(m_res_mutex);

    bool canBeExecuted = true;
    for(RC_COMMAND rc_cmd : badCmds) {
        const auto& it = m_commands.find(rc_cmd);
        if(it != m_commands.end()) {
            canBeExecuted = false;
            break;
        }
    }

    if(canBeExecuted == true) {
        auto& cmds = m_commands[RunControlCommands::stringToCommand(cmdName)];

        if(cmds.insert(cmd).second == false) {
            throw daq::rc::NotAllowed(ERS_HERE, cmdName, "another command with the same UID is still in execution");
        }

        ERS_DEBUG(1, "Resources allocated for command \"" << cmdName << "\"");

        // Inform about the busy only if a single resource for commands is present
        if((m_commands.size() == 1) && (cmds.size() == 1)) {
            ERS_DEBUG(2, "Informing about the busy being set");

            m_busy_sig(true);
        }
    } else {
        std::string commands;
        for(const auto& cmds : m_commands) {
            commands += " " + RunControlCommands::commandToString(cmds.first);
        }

        throw daq::rc::NotAllowed(ERS_HERE, cmdName, std::string("it is not compatible with other commands already in execution (" + commands + ")"));
    }
}

void CommandController::releaseCommandResource(const std::shared_ptr<const RunControlBasicCommand>& cmd) {
    const RC_COMMAND rcCmd = RunControlCommands::stringToCommand(cmd->name());

    ERS_DEBUG(3, "Asked to de-allocate resources for command \"" << cmd->name() << "\"");

    std::lock_guard<std::mutex> lk(m_res_mutex);

    auto& cmds = m_commands[rcCmd];
    const unsigned int num = cmds.erase(cmd);
    if(num == 0) {
        ERS_LOG("Not able to de-allocate resources for command \"" << cmd->name() << "\". That is normal if the resource was not granted before");
    } else {
        ERS_DEBUG(1, "Resources de-allocated for command \"" << cmd->name() << "\"");
    }

    if(cmds.empty() == true) {
        m_commands.erase(rcCmd);
    }

    if(m_commands.empty() == true) {
        ERS_DEBUG(2, "Informing about the busy being removed");

        m_busy_sig(false);
    }
}

void CommandController::notifyCommandSender(const std::shared_ptr<const RunControlBasicCommand>& cmd) {
    notifyCommandSender(cmd->senderReference(), cmd);
}

void CommandController::notifyCommandSender(const std::string& senderReference, const std::shared_ptr<const RunControlBasicCommand>& cmd) {
    if(senderReference.empty() == false) {
        try {
            std::call_once(m_notifier_tp_flag, [this]() { m_notifier_tp.reset(new ThreadPool(1)); } );

            m_notifier_tp->submit<void>([senderReference, cmd]()
                                        {
                                            try {
                                                CommandNotifierAdapter::notifyCommandSender(senderReference, cmd);
                                            }
                                            catch(daq::rc::CannotNotifyCommandSender& ex) {
                                                ers::info(ex);
                                            }
                                        }
            );
        }
        catch(daq::rc::ThreadPoolError& ex) {
            ers::info(daq::rc::CannotNotifyCommandSender(ERS_HERE,
                                                         senderReference,
                                                         cmd->toString(),
                                                         "failed to start the notification thread",
                                                         ex));
        }
    }
}

CommandController::CallbackToken CommandController::registerOnBusyCallback(const OnBusySlotType& cb) {
    return m_busy_sig.connect(cb);
}

void CommandController::removeCallback(const CallbackToken& t) {
    t.disconnect();
}

void CommandController::interruptCurrentTasks() {
    std::lock_guard<std::shared_mutex> lk(m_sub_mutex);

    m_command_executor_tp->pause();
    m_command_executor_tp->flush();

    m_tbk->interruptTasks();

    m_command_executor_tp->resume();
}

}}
