/*
 * RootController.cxx
 *
 *  Created on: Dec 10, 2012
 *      Author: avolio
 */

#include "RunControl/Controller/Root/RootController.h"
#include "RunControl/Controller/Root/RootRoutines.h"
#include "RunControl/Controller/Root/ActiveTime.h"
#include "RunControl/Controller/Root/RunParameters.h"
#include "RunControl/Controller/FSMController.h"
#include "RunControl/Controller/DVSController.h"
#include "RunControl/Controller/ControllerApplication.h"
#include "RunControl/Controller/ApplicationSupervisorInfo.h"
#include "RunControl/Controller/ApplicationController.h"
#include "RunControl/Controller/ApplicationList.h"
#include "RunControl/Controller/ApplicationAlgorithms.h"
#include "RunControl/Controller/InformationPublisher.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/UserRoutines.h"
#include "RunControl/Common/RunControlTransitionActions.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/Constants.h"
#include "RunControl/FSM/FSMStates.h"
#include "RunControl/FSM/FSMCommands.h"

#include <rc/TriggerStateNamed.h>
#include <rc/rc.hh>

#include <ers/StreamManager.h>
#include <ipc/partition.h>
#include <is/infoT.h>
#include <is/exceptions.h>
#include <config/SubscriptionCriteria.h>
#include <config/Change.h>
#include <TriggerCommander/TriggerCommander.h>
#include <dal/OnlineSegment.h>
#include <dal/Segment.h>
#include <dal/Partition.h>
#include <dal/MasterTrigger.h>
#include <dal/RunControlApplicationBase.h>
#include <dal/util.h>
#include <ExpertSystemInterface/Exceptions.h>
#include <ResourceManager/exceptions.h>

#include <functional>
#include <algorithm>
#include <chrono>
#include <mutex>
#include <cstdlib>

namespace daq { namespace rc {

const std::string RootController::TRG_IS_SERVER = Constants::RC_IS_SERVER_NAME + ".TriggerState";
const std::string RootController::ERS_STREAM_SUBSCRIPTION = "mts";
const std::string RootController::MAX_EV_MSG_TYPE = "rc::MAX_EVT_DONE";
const std::string RootController::START_TIME_INFO_IS_SERVER = Constants::RC_IS_SERVER_NAME + ".LastStartTime";
const std::string RootController::CONFIG_VERSION_ENV_NAME = "TDAQ_DB_VERSION";

// throws daq::rc::ControllerInitializationFailed
RootController::RootController(const std::string& name,
                               const std::string& partition,
                               const std::string& segment,
                               const std::shared_ptr<UserRoutines>& ur) :
        RunController(name, name, partition, segment, ur, false), m_config_cbk(0), m_rm_client(), m_mt_name(findMasterTrigger()),
        m_is_dict(::IPCPartition(partition))
{
    // This is the constructor for the RootController, be sure that this is really the root!
    bool isRoot = false;
    try {
        OnlineServices& os = OnlineServices::instance();

        const daq::core::OnlineSegment* onlSeg = os.getPartition().get_OnlineInfrastructure();
        if(onlSeg == nullptr) {
            throw daq::rc::ControllerInitializationFailed(ERS_HERE, "cannot find the Online segment in the database");
        } else if(onlSeg->UID() == os.segmentName()) {
            isRoot = true;
        }
    }
    catch(daq::rc::OnlineServicesFailure& ex) {
        const std::string& errMsg = std::string("failed accessing some online services (") + ex.message() + ")";
        throw daq::rc::ControllerInitializationFailed(ERS_HERE, errMsg, ex);
    }

    if(isRoot == false) {
        const std::string& msg = std::string("attempt to create the RootController but the specified segment \"") + segment
                + "\" is not the Online segment";
        throw daq::rc::ControllerInitializationFailed(ERS_HERE, msg);
    }

    // Check that there is no other RootController already up
    const IPCPartition& p = OnlineServices::instance().getIPCPartition();
    try {
        if(p.isObjectValid<::rc::commander>(name)) {
            const std::string& msg = std::string("An instance of \"") + name + "\" is already running";
            throw daq::rc::ControllerInitializationFailed(ERS_HERE, msg);
        }
    }
    catch(daq::ipc::Exception& ex) {
        throw daq::rc::ControllerInitializationFailed(ERS_HERE, "the partition IPC server cannot be contacted", ex);
    }
}

RootController::~RootController() noexcept {
}

void RootController::createAndRun(const std::string& name,
                                  const std::string& partition,
                                  const std::string& segment)
{
    if(name.empty() || partition.empty() || segment.empty()) {
        throw daq::rc::ControllerInitializationFailed(ERS_HERE,
                                                      "application partition, and segment names cannot be empty");
    }

    try {
        OnlineServices::initialize(partition, segment);
    }
    catch(daq::rc::OnlineServicesFailure& ex) {
        const std::string& errMsg = std::string("failed accessing some online services (") + ex.message() + ")";
        throw daq::rc::ControllerInitializationFailed(ERS_HERE, errMsg, ex);
    }

    // The RootController has no parent; it needs to update its status by itself
    auto updateStatus = [] (RootController& rc, APPLICATION_STATUS status) {
        const auto& mySelf = rc.thisControllerApplication();
        mySelf->setStatus(status);
        rc.infoPublisher().publish(rc.id(), mySelf->getApplicationSVInfo());
    };

    try {
        const std::shared_ptr<ActiveTime> activeTime(new ActiveTime()); // This throws
        const std::shared_ptr<RunParameters> runParams(new RunParameters(activeTime));
        const std::shared_ptr<RootRoutines> rootRoutines(new RootRoutines(runParams, activeTime));

        RootController rc(name, partition, segment, rootRoutines);
        updateStatus(rc, APPLICATION_STATUS::ABSENT);

        rc.init();
        updateStatus(rc, APPLICATION_STATUS::UP);

        rc.run();
    }
    catch(daq::rc::ThreadPoolError& ex) {
        throw daq::rc::ControllerInitializationFailed(ERS_HERE,
                                                      "low-level error, some threads could not be started",
                                                      ex);
    }
}

void RootController::onInit() {
    RunController::fsmController().init<FSM_STATE::NONE>();

    RootRoutines& rr = static_cast<RootRoutines&>(userRoutines());
    rr.getActiveTime().init();

    try {
        m_config_cbk = OnlineServices::instance().getConfiguration().subscribe(::ConfigurationSubscriptionCriteria(),
                                                                               &RootController::configCallback,
                                                                               this);
        ERS_DEBUG(0, "Subscribed to database changes");
    }
    catch(daq::config::Exception& ex) {
        throw daq::rc::ConfigurationIssue(ERS_HERE, "cannot subscribe to configuration modifications", ex);
    }

    // Update the current config version
    try {
        const char* const configVersion = std::getenv(RootController::CONFIG_VERSION_ENV_NAME.c_str());
        if(configVersion != nullptr) {
            daq::core::set_config_version(partition(), std::string(configVersion), false);
        }
    }
    catch(daq::config::Exception& ex) {
        throw daq::rc::ConfigurationIssue(ERS_HERE, "cannot set the current configuration version", ex);
    }
}

void RootController::fsmTransitioning(const TransitionCmd& trCmd) {
    ers::info(daq::rc::OngoingTransition(ERS_HERE, std::string("\"") + trCmd.fsmCommand() + "\""));

    RunController::fsmTransitioning(trCmd);

    RootRoutines& rr = static_cast<RootRoutines&>(userRoutines());

    // This is important to let RunParams know if the run has been stopped normally
    rr.getRunParams().setErrorState(thisControllerApplication()->getApplicationSelfInfo().isInternalError());

    const FSM_COMMAND fsmCmd = FSMCommands::stringToCommand(trCmd.fsmCommand());
    switch(fsmCmd) {
        case FSM_COMMAND::RELOAD_DB:
        {
            try {
                infoPublisher().dbReloading();
            }
            catch(daq::es::ESInterface::CannotNotify& ex) {
                throw daq::rc::DBReloadFailure(ERS_HERE,
                                               "failed to notify the Expert System",
                                               ex);
            }

            try {
                m_rm_client.rmConfigurationUpdate(OnlineServices::instance().getIPCPartition().name());
            }
            catch(daq::rmgr::Exception& ex) {
                ers::error(ex);
            }

            setMasterTrigger(findMasterTrigger());
            rr.getRunParams().reloadDetectorMask();

            break;
        }
        case FSM_COMMAND::STOPROIB:
        {
            if(trCmd.isRestart() == false) {
                const std::string& mt = getMasterTrigger();
                if(mt.empty() == false) {
                    try {
                        daq::trigger::TriggerCommander trgCmd(OnlineServices::instance().getIPCPartition(), mt);
                        trgCmd.hold();
                    }
                    catch(ers::Issue& ex) {
                        ers::error(ex);
                    }
                } else {
                    ers::warning(daq::rc::MasterTriggerNotDefined(ERS_HERE));
                }
            }

            break;
        }
        case FSM_COMMAND::SHUTDOWN:
        {
            const FSM_STATE srcState = FSMStates::stringToState(trCmd.fsmSourceState());
            cleanUp(srcState);

            break;
        }
        default:
            break;
    }
}

void RootController::fsmTransitionDone(const TransitionCmd& trCmd) {
    const FSM_COMMAND fsmCmd = FSMCommands::stringToCommand(trCmd.fsmCommand());
    switch(fsmCmd) {
        case FSM_COMMAND::SHUTDOWN:
        {
            // Clean the error at SHUTDOWN only if the error is caused by some child
            // application and not by the controller itself (internal error)
            const auto& mySelf = thisControllerApplication();
            const auto& myStatus = mySelf->getApplicationSelfInfo();
            const auto& bc = myStatus.getBadChildren();
            if((myStatus.isInternalError() == true) &&
               (std::find(bc.begin(), bc.end(), mySelf->getName()) == bc.end()))
            {
                mySelf->setInternalError({}, {});

                infoPublisher().shutdownExecuted();

                const auto& allApps = applicationList().getAllApplications();
                if(allApps.empty() == false) {
                    std::map<std::string, ApplicationSVInfo> statusMap;

                    for(const auto& app : allApps) {
                        statusMap.insert(std::make_pair(app->getName(), app->getApplicationSVInfo()));
                    }

                    // Note that the update about the application's state is done in RunController::fsmBusy
                    infoPublisher().publish(statusMap);
                }
            }

            break;
        }
        case FSM_COMMAND::RELOAD_DB:
        {
            try {
                // This synchronizes with the ES
                infoPublisher().dbReloaded();

                // Inform the ES about the status of all the applications
                const auto& allApps = applicationList().getAllApplications();
                if(allApps.empty() == false) {
                    std::map<std::string, ApplicationSVInfo> statusMap;

                    for(const auto& app : allApps) {
                        ApplicationSVInfo appInfo = app->getApplicationSVInfo();

                        // Test applications that are IN and that are UP or should be UP
                        if((appInfo.isMembership() == true) &&
                           ((appInfo.getStatus() == APPLICATION_STATUS::UP) ||
                            (ApplicationAlgorithms::shouldRunInState(*app, FSMStates::stateToString(std::get<0>(fsmController().status()))) == true)))
                        {
                            // After a reload better to reset any test cached value
                            dvsController().testApplication(app, daq::tm::Test::Scope::Functional, 1, true);
                        } else {
                            dvsController().resetTestStatus(app);
                        }

                        statusMap.insert(std::make_pair(app->getName(), std::move(appInfo)));
                    }

                    // Note that the update about the application's state is done in RunController::fsmBusy
                    infoPublisher().publish(statusMap);
                }

                // Update the controller's own information
                const auto& mySelf = thisControllerApplication(RunController::createThisControllerApplication().release());

                // Update the IS information for myself (the RootController has no parent)
                infoPublisher().publish(id(), mySelf->getApplicationSVInfo());
            }
            catch(daq::es::ESInterface::CannotNotify& ex) {
                throw daq::rc::DBReloadFailure(ERS_HERE, "failed to notify the Expert System", ex);
            }
            catch(ers::Issue& ex) {
                throw daq::rc::DBReloadFailure(ERS_HERE, ex.message(), ex);
            }

            break;
        }
        case FSM_COMMAND::INIT_FSM:
        {
            try {
                ers::StreamManager::instance().add_receiver(RootController::ERS_STREAM_SUBSCRIPTION,
                                                            std::string("msg=") + RootController::MAX_EV_MSG_TYPE,
                                                            this);
            }
            catch(ers::InvalidFormat& ex) {
                ers::error(ex);
            }

            RootRoutines& rr = static_cast<RootRoutines&>(userRoutines());

            // This will write to the IS server, that's why it has to be done after
            // the main infrastructure has been started (i.e., the NONE state has been reached)
            rr.getRunParams().init();

            break;
        }
        case FSM_COMMAND::START:
        {
            if(trCmd.isRestart() == false) {
                const std::string& mt = getMasterTrigger();
                if(mt.empty() == false) {
                    try {
                        daq::trigger::TriggerCommander trgCmd(OnlineServices::instance().getIPCPartition(), mt);

                        RootRoutines& rr = static_cast<RootRoutines&>(userRoutines());
                        trgCmd.increaseLumiBlock(rr.getRunParams().getRunNumber());

                        trgCmd.resume();
                    }
                    catch(ers::Issue& ex) {
                        ers::error(ex);
                    }
                } else {
                    ers::warning(daq::rc::MasterTriggerNotDefined(ERS_HERE));
                }

                try {
                    const auto& now = std::chrono::system_clock::now();
                    ISInfoUnsignedLong i(std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()).count());
                    m_is_dict.checkin(RootController::START_TIME_INFO_IS_SERVER, i);
                }
                catch(daq::is::Exception& ex) {
                    ers::warning(ex);
                }
            }

            break;
        }
        default:
            break;
    }

    if(trCmd.isRestart() == false) {
        try {
            infoPublisher().transitionDone(trCmd.fsmCommand(), trCmd.fsmDestinationState());
        }
        catch(daq::es::ESInterface::CannotNotify& ex) {
            ers::error(ex);
        }
    }

    RunController::fsmTransitionDone(trCmd);

    ers::info(daq::rc::CompletedTransition(ERS_HERE, std::string("\"") + trCmd.fsmCommand() + "\""));
}

void RootController::fsmTransitionException(const TransitionCmd& trCmd, std::exception& ex) {
    try {
        // There is no need to send a FATAL if the user routines are failing
        // They are executed early enough in the transition process, that the
        // transition command is not propagate at all (basically doing in this
        // way, it will be possible to just retry to send again the transition
        // command)
        const daq::rc::UserRoutineFailed& urEx = dynamic_cast<const daq::rc::UserRoutineFailed&>(ex);
        ers::error(urEx);

        RunController::fsmTransitionDoneAfterException(trCmd);
    }
    catch(std::bad_cast&) {
        // It is not a failed user routine, let the RunControl take the decision
        try {
            // If the failure was due to a database reload, then let the ES know
            // that the reload is over (so that it can eventually set the error state)
            dynamic_cast<const daq::rc::DBReloadFailure&>(ex);
            infoPublisher().dbReloaded();
        }
        catch(std::bad_cast&) {
        }
        catch(daq::es::ESInterface::CannotNotify& ex) {
            ers::error(ex);
        }

        RunController::fsmTransitionException(trCmd, ex);
    }
}

void RootController::configCallback(const std::vector< ::ConfigurationChange*>&, void* rootController) {
    ERS_DEBUG(0, "Received call-back from RDB");

    try {
        RootController* rc = static_cast<RootController*>(rootController);
        rc->makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::RELOAD_DB)));
    }
    catch(ers::Issue& ex) {
        ers::fatal(daq::rc::DBReloadFailure(ERS_HERE, ex.message(), ex));
    }
}

void RootController::notifyStatusChange(const ApplicationSelfInfo& myInfo, int actionTimeout, int exitTimeout) {
    infoPublisher().publish(id(), actionTimeout, exitTimeout, myInfo);
}

void RootController::receive(const ers::Issue& issue) {
    // Received the MAX_EVT_DONE issue from the ers stream
    ERS_LOG(issue);

    const auto& fsmStatus = fsmController().status();
    const FSM_STATE currState = std::get < 0 > (fsmStatus);
    const FSM_STATE destState = std::get < 1 > (fsmStatus);
    if((currState == FSM_STATE::RUNNING) && (destState == FSM_STATE::RUNNING)) {
        // Send the stop only if we are in RUNNING and not moving from that state
        try {
            makeTransition(std::shared_ptr<TransitionCmd>(new TransitionCmd(FSM_COMMAND::STOP)));
        }
        catch(ers::Issue& ex) {
            ers::error(ex);
        }
    }
}

void RootController::onExit() {
    ers::StreamManager::instance().remove_receiver(this);

    cleanUp(std::get<0>(fsmController().status()));

    try {
        OnlineServices::instance().getConfiguration().unsubscribe(m_config_cbk);
        ERS_DEBUG(0, "Subscription to database changes has been removed");
    }
    catch(daq::config::Generic& ex) {
        ers::warning(ex);
    }

    // Remove any published information about the current config version
    try {
        daq::core::set_config_version(partition(), "", false);
    }
    catch(daq::config::Exception& ex) {
        ers::warning(ex);
    }

    RunController::onExit();

    infoPublisher().removeProcessInfo(id());
}

std::unique_ptr<const ApplicationStatusCmd> RootController::getParentStatus() const {
    return std::unique_ptr<const ApplicationStatusCmd>();
}

void RootController::cleanUp(FSM_STATE currState) {
    const unsigned int currState_int = static_cast<unsigned int>(currState);

    if((currState_int >= static_cast<unsigned int>(FSM_STATE::GTHSTOPPED)) &&
       (currState_int <= static_cast<unsigned int>(FSM_STATE::RUNNING)))
    {
        RootRoutines& rr = static_cast<RootRoutines&>(userRoutines());
        rr.stop(false);
    }
}

std::string RootController::getMasterTrigger() const {
    std::shared_lock<std::shared_mutex> lk(m_mutex);
    return m_mt_name;
}

void RootController::setMasterTrigger(const std::string& name) {
    std::lock_guard<std::shared_mutex> lk(m_mutex);
    m_mt_name = name;
}

std::string RootController::findMasterTrigger() const {
    OnlineServices& os = OnlineServices::instance();

    std::string masterTriggerName;

    const daq::core::MasterTrigger* masterTrigger = os.getPartition().get_MasterTrigger();
    if(masterTrigger == nullptr) {
        TriggerStateNamed ts(os.getIPCPartition(), RootController::TRG_IS_SERVER);
        ts.triggerStatus = TriggerStateNamed::NoTrigger;
        try {
            ts.checkin();
        }
        catch(ers::Issue& ex) {
            ers::warning(ex);
        }

        ers::warning(daq::rc::MasterTriggerNotDefined(ERS_HERE));
    } else {
        try {
            masterTriggerName = masterTrigger->get_Controller()->UID();
            ERS_DEBUG(1, "Master Trigger: " << masterTriggerName);
        }
        catch(ers::Issue& ex) {
            TriggerStateNamed ts(os.getIPCPartition(), RootController::TRG_IS_SERVER);
            ts.triggerStatus = TriggerStateNamed::NoTrigger;
            try {
                ts.checkin();
            }
            catch(ers::Issue& ex) {
                ers::warning(ex);
            }

            ers::warning(daq::rc::MasterTriggerNotDefined(ERS_HERE, ex));
        }
    }

    return masterTriggerName;
}

}}
