/*
 * RootRoutines.cxx
 *
 *  Created on: Jun 20, 2013
 *      Author: avolio
 */

#include "RunControl/Controller/Root/RootRoutines.h"
#include "RunControl/Controller/Root/ActiveTime.h"
#include "RunControl/Controller/Root/RunParameters.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/Exceptions.h"

#include <ers/ers.h>
#include <ers/LocalContext.h>

#include <string>


namespace daq { namespace rc {

RootRoutines::RootRoutines(const std::shared_ptr<RunParameters>& runParams,
                           const std::shared_ptr<ActiveTime>& activeTime)
    : m_run_params(runParams), m_active_time(activeTime)
{

}

RootRoutines::~RootRoutines() noexcept {
}

void RootRoutines::prepareAction(const TransitionCmd& cmd) {
    const bool afterCrashing = cmd.isRestart();

    // Note that the ActiveTime is initialized by the RootController in the onInit() method
    m_active_time->start(afterCrashing);

    if(afterCrashing == false) {
        // Note that the RunParameters is initializes by the RootController in the fsmTransitionDone()
        // method just after the INIT_FSM transition has been completed
        m_run_params->start();

        const auto& rn = m_run_params->getRunNumber();
        ers::info(daq::rc::StartOfRun(ERS_HERE, std::to_string(rn)));
    }
}

void RootRoutines::stopArchivingAction(const TransitionCmd& trCmd) {
    stop(trCmd.isRestart());
}

bool RootRoutines::clearAction() noexcept {
    // We can fail only for not being able to retrieve the run number
    // returning true will give us the possibility to try again
    return true;
}

void RootRoutines::stop(bool isRestart) {
    if(isRestart == false) {
        ers::info(daq::rc::EndOfRun(ERS_HERE, "End of Run"));
    }

    ERS_DEBUG(3, "Stopping Active Time");
    m_active_time->stop();

    if(isRestart == false) {
        ERS_DEBUG(3, "Stopping Run Params");
        m_run_params->stop();
    }
}

RunParameters& RootRoutines::getRunParams() const {
    return *m_run_params;
}

ActiveTime& RootRoutines::getActiveTime() const {
    return *m_active_time;
}

}}

