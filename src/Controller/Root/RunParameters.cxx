#include "RunControl/Controller/Root/RunParameters.h"
#include "RunControl/Controller/Root/ActiveTime.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/Constants.h"

#include <rc/RCDataQualityNamed.h>
#include <rc/RunParamsNamed.h>
#include <TTCInfo/LumiBlockNamed.h>
#include <config/Configuration.h>
#include <dal/Detector.h>
#include <dal/Partition.h>
#include <dal/ResourceSet.h>
#include <dal/BaseApplication.h>
#include <dal/ResourceBase.h>
#include <DFdal/ROS.h>
#include <DFdal/InputChannel.h>
#include <eformat/DetectorMask.h>
#include <eformat/SourceIdentifier.h>
#include <ipc/partition.h>
#include <owl/time.h>
#include <config/DalObject.h>
#include <config/map.h>
#include <ers/Issue.h>
#include <ers/IssueFactory.h>
#include <ers/LocalContext.h>
#include <ers/ers.h>
#include <is/exceptions.h>
#include <rn/rn.h>

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/lexical_cast.hpp>

#include <chrono>
#include <fstream>
#include <cstdlib>
#include <set>
#include <vector>

#include <stdint.h>
#include <time.h>


namespace daq { namespace rc {

const std::string RunParameters::RN_ENV_VAR = "TDAQ_RUN_NUMBER_CONNECT";
const std::string RunParameters::FIXED_RN = "FIXED_NUMBER";
const std::string RunParameters::RUN_PARAMS = Constants::RUN_PARAMS_IS_SERVER + ".RunParams";
const std::string RunParameters::RUN_PARAMS_SOR = Constants::RUN_PARAMS_IS_SERVER + ".SOR_RunParams";
const std::string RunParameters::RUN_PARAMS_EOR = Constants::RUN_PARAMS_IS_SERVER + ".EOR_RunParams";
const std::string RunParameters::RUN_PARAMS_DQ = Constants::RUN_PARAMS_IS_SERVER + ".TDAQ_DQ_Flags";
const std::string RunParameters::LB_INFO_NAME = Constants::RUN_PARAMS_IS_SERVER + ".LumiBlock";

RunParameters::RunParameters(const std::shared_ptr<ActiveTime>& at)
    : m_error_state(false), m_active_time(at), m_rn_srvc(nullptr, RnDeleter()), m_rn(0)
{
}

RunParameters::~RunParameters() {
}

void RunParameters::init() {
    try {
        RunParamsNamed runParams(OnlineServices::instance().getIPCPartition(), RunParameters::RUN_PARAMS);
        if(runParams.isExist() == false) {
            runParams.run_number = 0;
            runParams.max_events = 0;
            runParams.recording_enabled = 0;
            runParams.trigger_type = 0;
            runParams.run_type = getDefaultRunType();
            runParams.det_mask = getDetectorMask();
            runParams.beam_type = 0;
            runParams.beam_energy = 0;
            runParams.filename_tag = "";
            runParams.T0_project_tag = "";
            runParams.timeSOR = OWLTime(static_cast<time_t>(0));
            runParams.timeEOR = OWLTime(static_cast<time_t>(0));
            runParams.totalTime = 0;

            runParams.checkin();
        } else {
            runParams.checkout();
            m_rn = runParams.run_number;
        }
    }
    catch(daq::is::Exception& ex) {
        ers::error(daq::rc::RunParams(ERS_HERE, "The run parameters cannot be initialized", ex));
    }
}

void RunParameters::start() {
    const IPCPartition& ipcPartition = OnlineServices::instance().getIPCPartition();

    RunParamsNamed runParamsSoR(ipcPartition, RunParameters::RUN_PARAMS_SOR);
    RunParamsNamed runParams(ipcPartition, RunParameters::RUN_PARAMS);
    try {
        runParams.checkout();

        // First close the old run (if needed)
        RunParamsNamed runParamsOldEoR(ipcPartition, RunParameters::RUN_PARAMS_EOR);

        bool closeRun = false;
        if(runParamsSoR.isExist() == true) {
            if(runParamsOldEoR.isExist() == false) {
                closeRun = true;
            } else {
                runParamsOldEoR.checkout();
                if(runParams.run_number > runParamsOldEoR.run_number) {
                    closeRun = true;
                }
            }
        }

        if(closeRun == true) {
            try {
                RunParamsNamed runParamsEoR(ipcPartition, RunParameters::RUN_PARAMS_EOR);
                runParamsEoR.run_number = runParams.run_number;
                runParamsEoR.max_events = runParams.max_events;
                runParamsEoR.recording_enabled = runParams.recording_enabled;
                runParamsEoR.trigger_type = runParams.trigger_type;
                runParamsEoR.run_type = runParams.run_type;
                runParamsEoR.det_mask = runParams.det_mask;
                runParamsEoR.beam_type = runParams.beam_type;
                runParamsEoR.beam_energy = runParams.beam_energy;
                runParamsEoR.filename_tag = runParams.filename_tag;
                runParamsEoR.T0_project_tag = runParams.T0_project_tag;
                runParamsEoR.timeSOR = runParams.timeSOR;
                runParamsEoR.timeEOR = OWLTime(time(0));
                runParamsEoR.totalTime = runParamsEoR.timeEOR.c_time() - runParams.timeSOR.c_time();

                runParamsEoR.checkin();
            }
            catch(daq::is::Exception& ex) {
                const std::string& msg = std::string("Failed closing the previous run number ") + std::to_string(runParams.run_number);
                ers::error(daq::rc::RunNumber(ERS_HERE, msg, ex));
            }

            try {
                RCDataQualityNamed dqFlag(ipcPartition, RunParameters::RUN_PARAMS_DQ);
                dqFlag.RunNumber = runParams.run_number;
                dqFlag.Owner = RCDataQualityNamed::runctrl;
                if((runParams.T0_project_tag != "data_test") &&
                   (boost::algorithm::to_upper_copy(runParams.run_type) == "PHYSICS") &&
                   (runParams.recording_enabled != 0))
                {
                    dqFlag.Flag = 2; // criteria for good run are met but there was a problem closing it
                } else {
                    dqFlag.Flag = 3; // criteria for good run not met
                }

                dqFlag.checkin();
            }
            catch(daq::is::Exception& ex) {
                ers::error(ex);
            }

            if(m_rn_srvc.get() != nullptr) {
                m_rn_srvc->set_comments("Run was NOT stopped cleanly");
            }
        }
    }
    catch(daq::is::Exception& ex) {
        ers::warning(daq::rc::RunNumber(ERS_HERE, "Cannot determine if the previous run number has been properly closed", ex));
    }

    // Connect to the Run Number Server. If not specified, provide a dummy run number.
    const std::string runNumberSrvName = (std::getenv(RunParameters::RN_ENV_VAR.c_str()) == nullptr) ? "" : std::getenv(RunParameters::RN_ENV_VAR.c_str());
    if(runNumberSrvName.empty()) {
        const std::string& msg = std::string("No Run Number Server provided: check the \"") + RunParameters::RN_ENV_VAR +
                                "\" variable in the DB. A dummy run number will be provided.";
        ers::warning(daq::rc::RunNumber(ERS_HERE, msg));

        m_rn = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    } else if(runNumberSrvName.find(RunParameters::FIXED_RN) != std::string::npos) {
        try {
            const std::string::size_type s = runNumberSrvName.find(":");
            const std::string& filename = runNumberSrvName.substr(s + 1);

            std::ifstream ifs;
            ifs.exceptions(std::ifstream::eofbit | std::ifstream::failbit | std::ifstream::badbit);
            ifs.open(filename.c_str());

            std::string runNumberValue;
            std::getline(ifs, runNumberValue);
            ifs.close();

            m_rn = boost::lexical_cast<unsigned long>(runNumberValue);

            const std::string& msg = std::string("A fixed run number is taken from \"") + filename + "\" with value " + runNumberValue;
            ers::warning(daq::rc::RunNumber(ERS_HERE, msg));
        }
        catch(std::exception& ex) {
            const std::string& msg = std::string("Failed to get run number from \"") + runNumberSrvName + "\". Reason: " + ex.what();
            throw daq::rc::RunNumber(ERS_HERE, msg, ex);
        }
    } else {
        try {
            m_rn_srvc.reset(new ::tdaq::RunNumber(runNumberSrvName, ipcPartition.name()));
            m_rn = m_rn_srvc->get_number();
            if(m_rn == 0) {
                m_rn_srvc.reset(nullptr);
                throw daq::rc::RunNumber(ERS_HERE, "Got a null (zero) run number from the Run Number service");
            }
        }
        catch(tdaq::rn::Exception& ex) {
            const std::string& msg = std::string("Impossible to allocate a new run number: ") + ex.message();
            throw daq::rc::RunNumber(ERS_HERE, msg, ex);
        }
    }

    // Time to be user for SOR and LB publication
    const auto& now = std::chrono::system_clock::now();

    // Update the fields in the Run Parameters structure.
    runParams.run_number = m_rn;
    runParams.det_mask = getDetectorMask();
    runParams.timeSOR = OWLTime(std::chrono::system_clock::to_time_t(now));
    runParams.timeEOR = OWLTime(static_cast<time_t>(0));
    runParams.totalTime = 0;

    try {
        runParams.checkin();
    }
    catch(daq::is::Exception& ex) {
        ers::error(daq::rc::RunParams(ERS_HERE, "The run parameters cannot be updated", ex));
    }

    // Create and populate Start of Run Parameters structure
    runParamsSoR.run_number = runParams.run_number;
    runParamsSoR.max_events = runParams.max_events;
    runParamsSoR.recording_enabled = runParams.recording_enabled;
    runParamsSoR.trigger_type = runParams.trigger_type;
    runParamsSoR.run_type = runParams.run_type;
    runParamsSoR.det_mask = runParams.det_mask;
    runParamsSoR.beam_type = runParams.beam_type;
    runParamsSoR.beam_energy = runParams.beam_energy;
    runParamsSoR.filename_tag = runParams.filename_tag;
    runParamsSoR.T0_project_tag = runParams.T0_project_tag;
    runParamsSoR.timeSOR = runParams.timeSOR;
    runParamsSoR.timeEOR = runParams.timeEOR;
    runParamsSoR.totalTime = runParams.totalTime;

    try {
        runParamsSoR.checkin();
    }
    catch(daq::is::Exception& ex) {
        ers::error(daq::rc::RunParams(ERS_HERE, "The Start Of Run (SOR) parameters cannot be set", ex));
    }

    // Initialize LB with value equal to zero
    LumiBlockNamed lumiInfo(ipcPartition, RunParameters::LB_INFO_NAME);
    lumiInfo.LumiBlockNumber = 0;
    lumiInfo.RunNumber = m_rn;
    lumiInfo.Time = std::chrono::duration_cast<std::chrono::nanoseconds>(now.time_since_epoch()).count();

    try {
        lumiInfo.checkin();
    }
    catch(daq::is::Exception& ex) {
        ers::error(daq::rc::RunParams(ERS_HERE, "The Luminosity Block could not be initialized to zero", ex));
    }
}

void RunParameters::stop() {
    const IPCPartition& ipcPartition = OnlineServices::instance().getIPCPartition();

    RunParamsNamed runParams(ipcPartition, RunParameters::RUN_PARAMS);
    try {
        runParams.checkout();
    }
    catch(daq::is::Exception& ex) {
        ers::error(daq::rc::RunParams(ERS_HERE,
                                      "Cannot retrieve the run parameters at the end of run: the run will not be properly closed",
                                      ex));
        return;
    }

    try {
        // Update the fields in the Run Parameters structure.
        runParams.timeEOR = OWLTime(time(0));
        runParams.totalTime = m_active_time->get_activeTime();

        runParams.checkin();
    }
    catch(daq::is::Exception& ex) {
        ers::error(daq::rc::RunParams(ERS_HERE, "Cannot update the run parameters at the end of the run", ex));
    }

    try {
        RunParamsNamed runParamsEoR(ipcPartition, RunParameters::RUN_PARAMS_EOR);
        runParamsEoR.run_number = runParams.run_number;
        runParamsEoR.max_events = runParams.max_events;
        runParamsEoR.recording_enabled = runParams.recording_enabled;
        runParamsEoR.trigger_type = runParams.trigger_type;
        runParamsEoR.run_type = runParams.run_type;
        runParamsEoR.det_mask = runParams.det_mask;
        runParamsEoR.beam_type = runParams.beam_type;
        runParamsEoR.beam_energy = runParams.beam_energy;
        runParamsEoR.filename_tag = runParams.filename_tag;
        runParamsEoR.T0_project_tag = runParams.T0_project_tag;
        runParamsEoR.timeSOR = runParams.timeSOR;
        runParamsEoR.timeEOR = runParams.timeEOR;
        runParamsEoR.totalTime = runParams.totalTime;

        runParamsEoR.checkin();
    }
    catch(daq::is::Exception& ex) {
        ers::error(daq::rc::RunNumber(ERS_HERE, "Failed to update the IS information in order to properly close this run number", ex));
    }

    try {
        RCDataQualityNamed dqFlag(ipcPartition, RunParameters::RUN_PARAMS_DQ);
        dqFlag.RunNumber = runParams.run_number;
        dqFlag.Owner = RCDataQualityNamed::runctrl;

        if((runParams.T0_project_tag != "data_test") &&
           (boost::algorithm::to_upper_copy(runParams.run_type) == "PHYSICS") &&
           (runParams.recording_enabled != 0))
        {
            dqFlag.Flag = 3; // criteria for good run are met
        } else {
            dqFlag.Flag = 1; // criteria for good run not met
        }

        dqFlag.checkin();
    }
    catch(daq::is::Exception& ex) {
        ers::error(daq::rc::RunParams(ERS_HERE, "Failed to update the run control Data Quality information", ex));
    }

    // Add comment to the Run Number Server
    const std::string comment = (m_error_state == false) ? "Clean stop of run" : "No clean stop of run occurred";
    if(m_rn_srvc.get() != nullptr) {
        m_rn_srvc->set_comments(comment);
    }

    // Try to close a previously opened run
    // (if ROOT was restarted during run, or a previous run number service lost connection to DB)
    if((m_rn_srvc.get() == nullptr) && (m_rn != 0)) {
        const std::string runNumberSrvName = (std::getenv(RunParameters::RN_ENV_VAR.c_str()) == nullptr) ? "" : std::getenv(RunParameters::RN_ENV_VAR.c_str());
        if((runNumberSrvName.empty() == false) && (runNumberSrvName.find(RunParameters::FIXED_RN) == std::string::npos)) {
            try {
                m_rn_srvc.reset(new ::tdaq::RunNumber(runNumberSrvName, ipcPartition.name(), m_rn));
                m_rn_srvc->set_comments(comment);
            }
            catch(tdaq::rn::Exception& ex) {
                ers::warning(ex);
            }
        }
    }

    m_rn_srvc.reset(nullptr);
}

unsigned long RunParameters::getRunNumber() const {
    return m_rn;
}

std::string RunParameters::getDefaultRunType() const {
    std::string rt = "Physics";

    try {
        const daq::core::Partition& partition = OnlineServices::instance().getPartition();
        const std::vector<std::string>& runTypes = partition.get_RunTypes();
        if(runTypes.empty() == false) {
            rt = runTypes[0];
        }
    }
    catch(daq::rc::ConfigurationIssue& ex) {
        const std::string& msg = std::string("Cannot get the list of possible run types, defaulting to \"") + rt + "\"";
        ers::warning(daq::rc::RunParams(ERS_HERE, msg, ex));
    }

    return rt;
}

void RunParameters::setErrorState(bool errorState) {
    m_error_state = errorState;
}

std::string RunParameters::getDetectorMask() const {
    // use eformat to encode detector mask
    eformat::helper::DetectorMask detMask;

    try {
        const daq::core::Partition& partition = OnlineServices::instance().getPartition();
        Configuration& config = OnlineServices::instance().getConfiguration();

        std::set<std::string> app_types = { "SwRodApplication", "ROS", "HLTSVApplication", "SFOApplication", "SFOngApplication" };
        const auto& appConfigs = partition.get_all_applications(&app_types);

        // Loop over all ROS to extract Detector IDs; if we have emulated ROSs the detector ID is kept in the ROBs
        for(const auto app_ : appConfigs) {
            const std::string& baseAppClassName = app_->class_name();
            if((baseAppClassName == "ROS") || (baseAppClassName == "SwRodApplication")) {
                const daq::df::ROS* ros = config.cast<daq::df::ROS>(app_);
                const auto logicalID = ros->get_Detector()->get_LogicalId();
                if(logicalID != 0) {
                    try {
                        const uint32_t detectorId = (logicalID << 16);
                        const eformat::helper::SourceIdentifier sid(detectorId);
                        detMask.set(sid.subdetector_id());
                    }
                    catch(ers::Issue& ex) {
                        ers::warning(ex);
                    }
                } else {
                    const auto& robins = ros->get_Contains(); // For SWROD, this is a SwRodModule
                    for(const auto* robin_ : robins) {
                        if(robin_->disabled(partition) == false) {
                            const daq::core::ResourceSet* robin = config.cast<daq::core::ResourceSet>(robin_);
                            if(robin != nullptr) {
                                const auto& robs = robin->get_Contains(); // For SWROD, this is a SwRodRob
                                for(const auto* rob_ : robs) {
                                    if(rob_->disabled(partition) == false) {
                                        const daq::df::InputChannel* rob = config.cast<daq::df::InputChannel>(rob_);
                                        if(rob != nullptr) {
                                            try {
                                                const eformat::helper::SourceIdentifier sid(rob->get_Id());
                                                detMask.set(sid.subdetector_id());
                                            }
                                            catch(ers::Issue& ex) {
                                                ers::warning(ex);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else if(baseAppClassName == "HLTSVApplication") {
                detMask.set(eformat::TDAQ_HLT);
            } else if(baseAppClassName == "SFOApplication" || baseAppClassName == "SFOngApplication") {
                detMask.set(eformat::TDAQ_SFO);
            }
        }
    }
    catch(ers::Issue& ex) {
        ers::error(daq::rc::RunParams(ERS_HERE, "The detector mask could not be properly determined", ex));
    }

    return detMask.string();
}

void RunParameters::reloadDetectorMask() {
    try {
        RunParamsNamed runParams(OnlineServices::instance().getIPCPartition(), RunParameters::RUN_PARAMS);
        if(runParams.isExist() == true) {
            runParams.checkout();
            runParams.det_mask = getDetectorMask();
            runParams.checkin();
        }
    }
    catch(daq::is::Exception& ex) {
        ers::error(daq::rc::RunParams(ERS_HERE, "Cannot update the detector mask after a database reload", ex));
    }
}

}}
