/*
 * ControllerApplication.cxx
 *
 *  Created on: Nov 2, 2012
 *      Author: avolio
 */

#include "RunControl/Controller/ControllerApplication.h"
#include "RunControl/Common/Exceptions.h"

#include <config/Errors.h>
#include <dal/Segment.h>
#include <dal/BaseApplication.h>
#include <dal/util.h>
#include <ers/LocalContext.h>

namespace daq { namespace rc {

ControllerApplication::ControllerApplication(Configuration& db,
                                             const daq::core::Partition& p,
                                             const daq::core::BaseApplication& appConfig,
                                             bool isParentRoot)
    :   RunControlApplication(db, p, appConfig, isParentRoot)
{
	int actionTimeout;
	int shortTimeout;

    const daq::core::Segment* seg = appConfig.get_segment();
	try {
		seg->get_timeouts(actionTimeout, shortTimeout);
	}
	catch(daq::config::Exception& ex) {
	    const std::string& msg = std::string("Cannot retrieve the information about application \"") +  getName() + "\" from the database";
	    const std::string& reason = ex.message();
	    throw daq::rc::ConfigurationIssue(ERS_HERE, msg + ": " + reason, reason, ex);
	}
	catch(daq::core::AlgorithmError& ex) {
	    const std::string& msg = std::string("Cannot retrieve the information about application \"") +  getName() + "\" from the database";
	    const std::string& reason = ex.message();
	    throw daq::rc::ConfigurationIssue(ERS_HERE, msg + ": " + reason, reason, ex);
	}

	// This throws daq::rc::ConfigurationIssue
	Algorithms::subTransitions(*seg, m_subtransitions);

	RunControlApplication::setActionTimeout(static_cast<unsigned int>(actionTimeout));
	RunControlApplication::setExitTimeout(static_cast<unsigned int>(shortTimeout));
}

ControllerApplication::~ControllerApplication() noexcept {
}

ApplicationType ControllerApplication::type() const noexcept {
	return ApplicationType::CHILD_CONTROLLER;
}

const Algorithms::SubtransitionMap& ControllerApplication::subTransitions() const {
    return m_subtransitions;
}

}}
