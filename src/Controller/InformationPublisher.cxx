/*
 * InformationPublisher.cxx
 *
 *  Created on: Jan 31, 2013
 *      Author: avolio
 */

#include "RunControl/Controller/InformationPublisher.h"
#include "RunControl/Controller/ApplicationSelfInfo.h"
#include "RunControl/Controller/ApplicationSupervisorInfo.h"
#include "RunControl/Common/RunControlBasicCommand.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/ThreadPool.h"
#include "RunControl/Common/Constants.h"
#include "RunControl/FSM/FSMStates.h"

#include <ExpertSystemInterface/ExpertSystemInterface.h>
#include <ExpertSystemInterface/RemoteES.h>
#include <ExpertSystemInterface/Exceptions.h>
#include <rc/DAQApplicationInfoNamed.h>
#include <rc/RCStateInfoNamed.h>
#include <rc/ApplicationTestInfoNamed.h>
#include <rc/TestInfo.h>
#include <ers/ers.h>
#include <ers/Issue.h>
#include <ipc/partition.h>

#include <type_traits>


namespace daq { namespace rc {

InformationPublisher::InformationPublisher(ControllerInfoFn fn)
    : m_self_info_time(0), m_info_fn(std::move(fn)),
      m_es_interface(new daq::es::rc::RemoteES(OnlineServices::instance().getIPCPartition().name(), daq::es::centralESName())),
      m_esnotifier_tp(new ThreadPool(5)), m_rc_isupdater_tp(new ThreadPool(1)), m_setup_isupdater_tp(new ThreadPool(1))
{
    // Be sure that information is properly updated/removed at exit
    m_rc_isupdater_tp->waitOnExit(true);
    m_esnotifier_tp->waitOnExit(true);
    m_setup_isupdater_tp->waitOnExit(true);
}

InformationPublisher::~InformationPublisher() {
}

void InformationPublisher::publish(const std::map<std::string, ApplicationSelfInfo>& infoList) {
    const auto& thisCtrlInfo = m_info_fn();

    std::map<std::string, daq::es::rc::StateInfo> allInfo;
    for(const auto& entry : infoList) {
        // Copying the string is strange but would allow to
        // execute the "std::make_pair" move version
        std::string appName = entry.first;

        daq::es::rc::StateInfo stateInfo{};
        InformationPublisher::fillInfo(entry.second, stateInfo, thisCtrlInfo);

        allInfo.insert(std::make_pair(std::move(appName), std::move(stateInfo)));
    }

    m_esnotifier_tp->submit<void>([this, allInfo]()
                                  {
                                      try {
                                          m_es_interface->applicationUpdate(allInfo);
                                      }
                                      catch(daq::es::ESInterface::Exception& ex) {
                                          ers::error(ex);
                                      }
                                  }
    );
}

void InformationPublisher::publish(const std::map<std::string, ApplicationSVInfo>& infoList) {
    const auto& thisCtrlInfo = m_info_fn();

    std::map<std::string, daq::es::rc::ProcessInfo> allInfo;
    for(const auto& entry : infoList) {
        // Copying the string is strange but would allow to
        // execute the "std::make_pair" move version
        std::string appName = entry.first;

        daq::es::rc::ProcessInfo stateInfo{};
        InformationPublisher::fillInfo(entry.second, stateInfo, thisCtrlInfo);

        allInfo.insert(std::make_pair(std::move(appName), std::move(stateInfo)));
    }

    m_esnotifier_tp->submit<void>([this, allInfo]()
                                  {
                                      try {
                                          m_es_interface->applicationUpdate(allInfo);
                                      }
                                      catch(daq::es::ESInterface::Exception& ex) {
                                          ers::error(ex);
                                      }
                                  }
    );
}

void InformationPublisher::publish(const std::string& appName,
                                   const ApplicationSVInfo& appInfo,
                                   InformationPublisher::Destinations_t dest)
{
    if((dest & InformationPublisher::EXPERT_SYSTEM) == InformationPublisher::EXPERT_SYSTEM) {
        const auto& thisCtrlInfo = m_info_fn();

        daq::es::rc::ProcessInfo procInfo{};
        InformationPublisher::fillInfo(appInfo, procInfo, thisCtrlInfo);

        m_esnotifier_tp->submit<void>([this, procInfo, appName]()
                                      {
                                          try {
                                              m_es_interface->applicationUpdate(appName, procInfo);
                                          }
                                          catch(daq::es::ESInterface::Exception& ex) {
                                              ers::error(ex);
                                          }
                                      }
        );
    }

    if((dest & InformationPublisher::IS_SERVER) == InformationPublisher::IS_SERVER) {
        m_rc_isupdater_tp->submit<void>([appName, appInfo]()
                                        {
                                            DAQApplicationInfoNamed info(OnlineServices::instance().getIPCPartition(),
                                                                         Constants::RC_IS_SERVER_NAME + "." + Constants::RC_SV_INFO + "." + appName);
                                            info.applicationName = appName;
                                            info.host = appInfo.getRunningHost();
                                            info.status = ApplicationStates::statusToString(appInfo.getStatus());
                                            info.membership = appInfo.isMembership();
                                            info.restarting = appInfo.isRestarting();
                                            info.notResponding = appInfo.isNotResponding();
                                            info.exitCode = appInfo.getExitCode();
                                            info.exitSignal = appInfo.getExitSignal();
                                            info.parentName = OnlineServices::instance().applicationName();

                                            try {
                                                info.checkin();
                                            }
                                            catch(ers::Issue& ex) {
                                                ers::warning(ex);
                                            }
                                        }
        );
    }
}

void InformationPublisher::publish(const std::string& appName,
                                   int actionTimeout,
                                   int exitTimeout,
                                   const ApplicationSelfInfo& appInfo,
                                   InformationPublisher::Destinations_t dest)
{
    if((dest & InformationPublisher::EXPERT_SYSTEM) == InformationPublisher::EXPERT_SYSTEM) {
        const auto& thisCtrlInfo = m_info_fn();

        daq::es::rc::StateInfo stateInfo{};
        InformationPublisher::fillInfo(appInfo, stateInfo, thisCtrlInfo);

        m_esnotifier_tp->submit<void>([this, stateInfo, appName]()
                                      {
                                          try {
                                              m_es_interface->applicationUpdate(appName, stateInfo);
                                          }
                                          catch(daq::es::ESInterface::Exception& ex) {
                                              ers::error(ex);
                                          }
                                      }
        );
    }

    if((dest & InformationPublisher::IS_SERVER) == InformationPublisher::IS_SERVER) {
        m_rc_isupdater_tp->submit<void>([appName, actionTimeout, exitTimeout, appInfo, this]()
                                        {
                                            const auto this_time = appInfo.getInfoTimeTag();
                                            if(this_time >= m_self_info_time) {
                                                try {
                                                    RCStateInfoNamed rcInfo(OnlineServices::instance().getIPCPartition(),
                                                                            Constants::RC_IS_SERVER_NAME + "." + appName);
                                                    rcInfo.state = FSMStates::stateToString(appInfo.getFSMState());
                                                    rcInfo.busy = appInfo.isBusy();
                                                    rcInfo.transitionTime = appInfo.getLastTransitionTime();
                                                    rcInfo.fault = appInfo.isInternalError();
                                                    rcInfo.action_timeout = actionTimeout;
                                                    rcInfo.short_timeout = exitTimeout;
                                                    rcInfo.transitioning = appInfo.isTransitioning();
                                                    rcInfo.errorReasons = appInfo.getErrorReasons();
                                                    rcInfo.badChildren = appInfo.getBadChildren();

                                                    const auto& currTr = appInfo.getCurrentTransitionCommand();
                                                    if(currTr.get() != nullptr) {
                                                        rcInfo.currentTransitionName = currTr->fsmCommand();

                                                        // The transition's arguments may be the same of the FSM command
                                                        const auto& args = currTr->arguments();
                                                        if((args.size() != 1) || (args[0] != rcInfo.currentTransitionName)) {
                                                            rcInfo.currentTransitionArgs = args;
                                                        }

                                                        rcInfo.currentTransitionFull = currTr->serialize();
                                                    }

                                                    const auto& lastTr = appInfo.getLastTransitionCommand();
                                                    if(lastTr.get() != nullptr) {
                                                        rcInfo.lastTransitionName = lastTr->fsmCommand();

                                                        // The transition's arguments may be the same of the FSM command
                                                        const auto& args = lastTr->arguments();
                                                        if(((args.size() != 1) || (args[0] != rcInfo.lastTransitionName))) {
                                                            rcInfo.lastTransitionArgs = args;
                                                        }

                                                        rcInfo.lastTransitionFull = lastTr->serialize();
                                                    }

                                                    const auto& lastCmd = appInfo.getLastCommand();
                                                    if(lastCmd.get() != nullptr) {
                                                        rcInfo.lastCmdName = lastCmd->name();
                                                        rcInfo.lastCmdArgs = lastCmd->arguments();
                                                        rcInfo.lastCmdFull = lastCmd->serialize();
                                                    }

                                                    rcInfo.fullyInitialized = appInfo.isFullyInitialized();

                                                    rcInfo.checkin();

                                                    // REMARK: this works only because we use a single thread to update the IS information
                                                    // If multiple threads will even be used, then here we need some synchronization
                                                    m_self_info_time = this_time;
                                                }
                                                catch(ers::Issue& ex) {
                                                    ers::warning(ex);
                                                }
                                            } else {
                                                ERS_LOG("Out of order IS update detected!");
                                            }
                                        }
        );
    }
}

void InformationPublisher::publish(const std::string& appName,
                                   const daq::es::rc::TestInfo& testInfo,
                                   const ::TestInfo& globalResult,
                                   const std::vector< ::TestInfo>& components,
                                   InformationPublisher::Destinations_t dest)
{
    if((dest & InformationPublisher::EXPERT_SYSTEM) == InformationPublisher::EXPERT_SYSTEM) {
        m_esnotifier_tp->submit<void>([this, appName, testInfo]()
                                      {
                                        try {
                                            m_es_interface->applicationUpdate(appName, testInfo);
                                        }
                                        catch(daq::es::ESInterface::Exception& ex) {
                                            ers::error(ex);
                                        }
                                      }
        );
    }

    if((dest & InformationPublisher::IS_SERVER) == InformationPublisher::IS_SERVER) {
        m_setup_isupdater_tp->submit<void>([appName, globalResult, components]()
                                           {
                                                try {
                                                    ApplicationTestInfoNamed info(OnlineServices::instance().getIPCPartition(),
                                                                                  Constants::SETUP_IS_SERVER_NAME + "." +
                                                                                      OnlineServices::instance().segmentName() + "." +
                                                                                      appName);

                                                    info.globalResult = std::move(globalResult);
                                                    info.componentsResult = std::move(components);

                                                    info.checkin();
                                                }
                                                catch(ers::Issue& ex) {
                                                    ers::warning(ex);
                                                }
                                           }
        );
    }
}

void InformationPublisher::dbReloading() {
    m_es_interface->dbReloadInit();
}

void InformationPublisher::dbReloaded() {
    m_es_interface->dbReloadDone();
}

void InformationPublisher::shutdownExecuted() {
    m_es_interface->reset();
}

void InformationPublisher::transitionDone(const std::string& trCommand, const std::string& fsmState) {
    m_es_interface->transitionDone(trCommand, fsmState);
}

void InformationPublisher::removeProcessInfo(const std::string& appName) {
    m_rc_isupdater_tp->submit<void>([appName]()
                                    {
                                        try {
                                            DAQApplicationInfoNamed daqInfo(OnlineServices::instance().getIPCPartition(),
                                                                            Constants::RC_IS_SERVER_NAME + "." +
                                                                                Constants::RC_SV_INFO + "." +
                                                                                appName);

                                            daqInfo.remove();
                                        }
                                        catch(ers::Issue& ex) {
                                            ers::warning(ex);
                                        }
                                    }
    );
}

void InformationPublisher::removeTestInfo(const std::string& appName) {
    m_setup_isupdater_tp->submit<void>([appName] ()
                                       {
                                            try {
                                                ApplicationTestInfoNamed info(OnlineServices::instance().getIPCPartition(),
                                                                              Constants::SETUP_IS_SERVER_NAME + "." +
                                                                                  OnlineServices::instance().segmentName() + "." +
                                                                                  appName);

                                                info.remove();
                                            }
                                            catch(ers::Issue& ex) {
                                                ers::warning(ex);
                                            }
                                       }
    );
}

void InformationPublisher::removeRCInfo(const std::string& appName) {
    m_rc_isupdater_tp->submit<void>([appName]()
                                    {
                                        try {
                                            RCStateInfoNamed rcInfo(OnlineServices::instance().getIPCPartition(),
                                                                    Constants::RC_IS_SERVER_NAME + "." + appName);
                                            rcInfo.remove();
                                        }
                                        catch(ers::Issue& ex) {
                                            ers::warning(ex);
                                        }
                                    }
    );
}
void InformationPublisher::fillInfo(const ApplicationSVInfo& from, daq::es::rc::ProcessInfo& to, const ControllerInfo& cInfo) {
    to.m_status = ApplicationStates::statusToString(from.getStatus());
    to.m_running_host = from.getRunningHost();
    to.m_membership = from.isMembership();
    to.m_restarting = from.isRestarting();
    to.m_not_responding = from.isNotResponding();
    to.m_exit_code = from.getExitCode();
    to.m_exit_signal = from.getExitSignal();
    to.m_info_time = from.getInfoWallTime();
    to.m_parent_info.m_exiting = cInfo.first;
    to.m_parent_info.m_transitioning = cInfo.second.isTransitioning();
    to.m_parent_info.m_initializing = !(cInfo.second.isFullyInitialized());
    to.m_parent_info.m_fsm_state = FSMStates::stateToString(cInfo.second.getFSMState());

    const auto& parentTr = cInfo.second.getCurrentTransitionCommand();
    if(parentTr.get() != nullptr) {
        to.m_parent_info.m_current_transition.m_uid = parentTr->uid();
        to.m_parent_info.m_current_transition.m_command = parentTr->fsmCommand();
        to.m_parent_info.m_current_transition.m_source_state = parentTr->fsmSourceState();
        to.m_parent_info.m_current_transition.m_destination_state = parentTr->fsmDestinationState();
    }

    const auto& lastParentTr = cInfo.second.getLastTransitionCommand();
    if(lastParentTr.get() != nullptr) {
        to.m_parent_info.m_last_transition.m_uid = lastParentTr->uid();
        to.m_parent_info.m_last_transition.m_command = lastParentTr->fsmCommand();
        to.m_parent_info.m_last_transition.m_source_state = lastParentTr->fsmSourceState();
        to.m_parent_info.m_last_transition.m_destination_state = lastParentTr->fsmDestinationState();
    }
}

void InformationPublisher::fillInfo(const ApplicationSelfInfo& from, daq::es::rc::StateInfo& to, const ControllerInfo& cInfo) {
    to.m_fsm_state = FSMStates::stateToString(from.getFSMState());
    to.m_last_transition_time = from.getLastTransitionTime();
    to.m_internal_error = from.isInternalError();
    to.m_busy = from.isBusy();
    to.m_info_time = from.getInfoWallTime();

    const auto& lastCmd = from.getLastCommand();
    if(lastCmd.get() != nullptr) {
        to.m_last_command.m_uid = lastCmd->uid();
        to.m_last_command.m_name = lastCmd->name();
        to.m_last_command.m_arguments = lastCmd->arguments();
    }

    const auto& currTr = from.getCurrentTransitionCommand();
    if(currTr.get() != nullptr) {
        to.m_current_transition.m_uid = currTr->uid();
        to.m_current_transition.m_command = currTr->fsmCommand();
        to.m_current_transition.m_source_state = currTr->fsmSourceState();
        to.m_current_transition.m_destination_state = currTr->fsmDestinationState();
    }

    const auto& lastTr = from.getLastTransitionCommand();
    if(lastTr.get() != nullptr) {
        to.m_last_transition.m_uid = lastTr->uid();
        to.m_last_transition.m_command = lastTr->fsmCommand();
        to.m_last_transition.m_source_state = lastTr->fsmSourceState();
        to.m_last_transition.m_destination_state = lastTr->fsmDestinationState();
    }

    to.m_parent_info.m_exiting = cInfo.first;
    to.m_parent_info.m_transitioning = cInfo.second.isTransitioning();
    to.m_parent_info.m_initializing = !(cInfo.second.isFullyInitialized());
    to.m_parent_info.m_fsm_state = FSMStates::stateToString(cInfo.second.getFSMState());

    const auto& parentTr = cInfo.second.getCurrentTransitionCommand();
    if(parentTr.get() != nullptr) {
        to.m_parent_info.m_current_transition.m_uid = parentTr->uid();
        to.m_parent_info.m_current_transition.m_command = parentTr->fsmCommand();
        to.m_parent_info.m_current_transition.m_source_state = parentTr->fsmSourceState();
        to.m_parent_info.m_current_transition.m_destination_state = parentTr->fsmDestinationState();
    }

    const auto& lastParentTr = cInfo.second.getLastTransitionCommand();
    if(lastParentTr.get() != nullptr) {
        to.m_parent_info.m_last_transition.m_uid = lastParentTr->uid();
        to.m_parent_info.m_last_transition.m_command = lastParentTr->fsmCommand();
        to.m_parent_info.m_last_transition.m_source_state = lastParentTr->fsmSourceState();
        to.m_parent_info.m_last_transition.m_destination_state = lastParentTr->fsmDestinationState();
    }
}

}}
