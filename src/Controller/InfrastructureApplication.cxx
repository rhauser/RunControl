/*
 * InfrastructureApplication.cxx
 *
 *  Created on: Nov 1, 2012
 *      Author: avolio
 */

#include "RunControl/Controller/InfrastructureApplication.h"

namespace daq { namespace rc {

InfrastructureApplication::InfrastructureApplication(Configuration& db,
                                                     const daq::core::Partition& p,
                                                     const daq::core::BaseApplication& appConfig,
                                                     bool isParentRoot) :
		Application(db, p, appConfig, isParentRoot)
{
}

InfrastructureApplication::~InfrastructureApplication() noexcept {
}

ApplicationType InfrastructureApplication::type() const noexcept {
	return ApplicationType::INFRASTRUCTURE;
}

}}
