/*
 * ApplicationSupervisorInfo.cxx
 *
 *  Created on: Nov 13, 2012
 *      Author: avolio
 */

#include "RunControl/Controller/ApplicationSupervisorInfo.h"
#include "RunControl/Common/Exceptions.h"

#include <ers/IssueFactory.h>
#include <ers/LocalContext.h>

#include <boost/assign/list_of.hpp>
#include <boost/algorithm/string.hpp>

#include <chrono>


namespace daq { namespace rc {

const std::string ApplicationStates::NOTAV_STATE = "NOTAV";
const std::string ApplicationStates::ABSENT_STATE = "ABSENT";
const std::string ApplicationStates::REQUESTED_STATE = "REQUESTED";
const std::string ApplicationStates::UP_STATE = "UP";
const std::string ApplicationStates::TERMINATING_STATE = "TERMINATING";
const std::string ApplicationStates::EXITED_STATE = "EXITED";
const std::string ApplicationStates::FAILED_STATE = "FAILED";

ApplicationStates::bm_type ApplicationStates::STATUS_MAP = boost::assign::list_of<ApplicationStates::bm_type::relation>(APPLICATION_STATUS::NOTAV, ApplicationStates::NOTAV_STATE)
                                                                                                                       (APPLICATION_STATUS::ABSENT, ApplicationStates::ABSENT_STATE)
                                                                                                                       (APPLICATION_STATUS::REQUESTED, ApplicationStates::REQUESTED_STATE)
                                                                                                                       (APPLICATION_STATUS::UP, ApplicationStates::UP_STATE)
                                                                                                                       (APPLICATION_STATUS::TERMINATING, ApplicationStates::TERMINATING_STATE)
                                                                                                                       (APPLICATION_STATUS::EXITED, ApplicationStates::EXITED_STATE)
                                                                                                                       (APPLICATION_STATUS::FAILED, ApplicationStates::FAILED_STATE);

std::string ApplicationStates::statusToString(APPLICATION_STATUS state) {
    const ApplicationStates::bm_type::left_map::const_iterator& it = ApplicationStates::STATUS_MAP.left.find(state);
    return it->second;
}

APPLICATION_STATUS ApplicationStates::stringToStatus(const std::string& state) {
    const ApplicationStates::bm_type::right_map::const_iterator& it = ApplicationStates::STATUS_MAP.right.find(boost::algorithm::to_upper_copy(state));
    if(it != ApplicationStates::STATUS_MAP.right.end()) {
        return it->second;
    } else {
        throw daq::rc::InvalidStatus(ERS_HERE, state);
    }
}

ApplicationSVInfo::ApplicationSVInfo(APPLICATION_STATUS appStatus,
                                     bool membership,
                                     const std::string& runningHost,
                                     bool isRestarting,
                                     bool notResponding,
                                     const std::string& pmgHandle,
                                     unsigned int lastId,
                                     int exitCode,
                                     int signal,
                                     bool forceLookup,
                                     daq::tmgr::TestResult testResult,
                                     infotime_t infoTimeTag,
                                     long long infoWallTime)
        : m_status(appStatus), m_membership(membership), m_running_host(runningHost), m_restarting(isRestarting),
          m_not_responding(notResponding), m_pmg_handle(pmgHandle), m_last_id(lastId), m_exit_code(exitCode),
          m_exit_signal(signal), m_force_lookup(forceLookup), m_test_status(testResult), m_time_tag(infoTimeTag), m_wall_time(infoWallTime)
{
}

ApplicationSVInfo::ApplicationSVInfo() :
		m_status(APPLICATION_STATUS::NOTAV), m_membership(true), m_restarting(false), m_not_responding(false), m_last_id(0),
		m_exit_code(0), m_exit_signal(0), m_force_lookup(false), m_test_status(daq::tmgr::TestResult::TmUndef), m_time_tag(0),
		m_wall_time(std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count())
{
}

ApplicationSVInfo::~ApplicationSVInfo() {
}

bool ApplicationSVInfo::isMembership() const {
	return m_membership;
}

void ApplicationSVInfo::setMembership(bool membership) {
	m_membership = membership;
	updateTimeTags();
}

bool ApplicationSVInfo::isRestarting() const {
	return m_restarting;
}

void ApplicationSVInfo::setRestarting(bool restarting) {
	m_restarting = restarting;
	updateTimeTags();
}

std::string ApplicationSVInfo::getRunningHost() const {
	return m_running_host;
}

void ApplicationSVInfo::setRunningHost(const std::string& runningHost) {
	m_running_host = runningHost;
	updateTimeTags();
}

APPLICATION_STATUS ApplicationSVInfo::getStatus() const {
	return m_status;
}

void ApplicationSVInfo::setStatus(APPLICATION_STATUS status) {
	m_status = status;
	updateTimeTags();
}

bool ApplicationSVInfo::isNotResponding() const {
    return m_not_responding;
}

void ApplicationSVInfo::setNotResponding(bool notResponding) {
    m_not_responding = notResponding;
    updateTimeTags();
}

std::string ApplicationSVInfo::getHandle() const {
    return m_pmg_handle;
}

void ApplicationSVInfo::setHandle(const std::string& pmgHandle) {
    m_pmg_handle = pmgHandle;
    updateTimeTags();
}

unsigned int ApplicationSVInfo::getLastID() const {
    return m_last_id;
}

void ApplicationSVInfo::setLastID(unsigned int id) {
    m_last_id = id;
    updateTimeTags();
}

int ApplicationSVInfo::getExitCode() const {
    return m_exit_code;
}

void ApplicationSVInfo::setExitCode(int exCode) {
    m_exit_code = exCode;
    updateTimeTags();
}

int ApplicationSVInfo::getExitSignal() const {
    return m_exit_signal;
}

void ApplicationSVInfo::setExitSignal(int signal) {
    m_exit_signal = signal;
    updateTimeTags();
}

bool ApplicationSVInfo::isForceLookup() const {
    return m_force_lookup;
}

void ApplicationSVInfo::setForceLookup(bool force) {
    m_force_lookup = force;
    updateTimeTags();
}

void ApplicationSVInfo::setTestStatus(daq::tmgr::TestResult status) {
    m_test_status = status;
    updateTimeTags();
}

daq::tmgr::TestResult ApplicationSVInfo::getTestStatus() const {
    return m_test_status;
}

infotime_t ApplicationSVInfo::getInfoTimeTag() const {
    return m_time_tag;
}

long long ApplicationSVInfo::getInfoWallTime() const {
    return m_wall_time;
}

void ApplicationSVInfo::updateTimeTags() {
    m_time_tag = clock_t::now().time_since_epoch().count();
    m_wall_time = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

}}
