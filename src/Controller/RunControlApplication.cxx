/*
 * RunControlApplication.cxx
 *
 *  Created on: Nov 2, 2012
 *      Author: avolio
 */

#include "RunControl/Controller/RunControlApplication.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/Exceptions.h"

#include <config/Configuration.h>
#include <config/DalObject.h>
#include <config/Errors.h>
#include <config/map.h>
#include <dal/BaseApplication.h>
#include <dal/RunControlApplicationBase.h>
#include <dal/util.h>
#include <ers/LocalContext.h>

namespace daq { namespace rc {

const std::string RunControlApplication::PROBE_ENV = "TDAQ_PROBE_INTERVAL";
const std::string RunControlApplication::FULLSTAT_ENV = "TDAQ_PUBLISHSTATISTICS_INTERVAL";

RunControlApplication::RunControlApplication(Configuration& db,
                                             const daq::core::Partition& p,
                                             const daq::core::BaseApplication& appConfig,
                                             bool isParentRoot)
    : Application(db, p, appConfig, isParentRoot)
{
	try {
		const daq::core::RunControlApplicationBase* rc = db.cast<daq::core::RunControlApplicationBase>(&appConfig);
		if(rc != nullptr) {
			m_action_timeout = rc->get_ActionTimeout();

			const int probInt = rc->get_ProbeInterval();
			m_probe_interval = (probInt > 0) ? probInt : 0;

			const int fullStatInt = rc->get_FullStatisticsInterval();
			m_fullstat_interval = (fullStatInt > 0) ? fullStatInt : 0;
		} else {
		    const std::string& msg = std::string("Cannot retrieve the information about application \"") +  getName() + "\" from the database";
		    const std::string reason = "the application cannot be casted to a run-control application, but it should";
		    throw daq::rc::ConfigurationIssue(ERS_HERE, msg + ": " + reason, reason);
		}
	}
	catch(daq::config::Exception& ex) {
        const std::string& msg = std::string("Cannot retrieve the information about application \"") +  getName() + "\" from the database";
        const std::string& reason = ex.message();
        throw daq::rc::ConfigurationIssue(ERS_HERE, msg + ": " + reason, reason, ex);
	}
	catch(daq::core::AlgorithmError& ex) {
        const std::string& msg = std::string("Cannot retrieve the information about application \"") +  getName() + "\" from the database";
        const std::string& reason = ex.message();
        throw daq::rc::ConfigurationIssue(ERS_HERE, msg + ": " + reason, reason, ex);
	}

	const std::string& ap = additionalParameters();
	Application::addStartParameters(ap);
	Application::addRestartParameters(ap);

	Application::addEnvironment(RunControlApplication::PROBE_ENV, std::to_string(m_probe_interval));
	Application::addEnvironment(RunControlApplication::FULLSTAT_ENV, std::to_string(m_fullstat_interval));
}

RunControlApplication::~RunControlApplication() noexcept {
}

ApplicationType RunControlApplication::type() const noexcept {
	return ApplicationType::RUN_CONTROL;
}

unsigned int RunControlApplication::getActionTimeout() const {
	return m_action_timeout;
}

unsigned int RunControlApplication::getProbeInterval() const {
	return m_probe_interval;
}

unsigned int RunControlApplication::getFullStatInterval() const {
	return m_fullstat_interval;
}

void RunControlApplication::setActionTimeout(unsigned int timeout) {
	m_action_timeout = timeout;
}

std::string RunControlApplication::additionalParameters() const {
    const std::string& p = std::string("-n ") + getName() + " -P " + OnlineServices::instance().applicationName() + " -s " + getSegment();
	return p;
}

}}
