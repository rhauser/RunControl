/*
 * Controller.cxx
 *
 *  Created on: Dec 5, 2012
 *      Author: avolio
 */

#include "RunControl/Controller/Controller.h"
#include "RunControl/Controller/RunController.h"
#include "RunControl/Common/UserRoutines.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/OnlineServices.h"
#include "RunControl/Common/CmdLineParser.h"

#include <ers/LocalContext.h>

#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

namespace daq { namespace rc {

namespace {
    // Pointer types are atomic and can be accessed in signal handlers:
    // http://www.gnu.org/software/libc/manual/html_node/Atomic-Types.html
    Controller* CTRL = 0;
}

struct SignaHandlerInstaller {
    SignaHandlerInstaller() {
        struct sigaction sig_act;
        ::sigemptyset(&sig_act.sa_mask);
        ::sigaddset(&sig_act.sa_mask, SIGINT);
        ::sigaddset(&sig_act.sa_mask, SIGTERM);
        sig_act.sa_flags = 0;
        sig_act.sa_handler = &SignaHandlerInstaller::signal_handler;

        ::sigaction(SIGTERM, &sig_act, 0);
        ::sigaction(SIGINT, &sig_act, 0);
    }

    static void signal_handler(int) {
        if(CTRL != 0) {
            CTRL->shutdown();
        } else {
            // Note: documentation says that calling 'exit' in a signal handler is not safe
            ::_exit(EXIT_SUCCESS);
        }
    }
};

namespace {
    SignaHandlerInstaller sigInstaller;
}

// throws daq::rc::ControllerInitializationFailed
Controller::Controller(const CmdLineParser& cmdLine)
    : Controller(cmdLine.applicationName(), cmdLine.parentName(), cmdLine.partitionName(), cmdLine.segmentName())
{
}

// throws daq::rc::ControllerInitializationFailed
Controller::Controller(const std::string& name,
                       const std::string& parent,
                       const std::string& partition,
                       const std::string& segment)
       : Controller(name, parent, partition, segment, std::shared_ptr<UserRoutines>(new UserRoutines()))
{
}

// throws daq::rc::ControllerInitializationFailed
Controller::Controller(const CmdLineParser& cmdLine, const std::shared_ptr<UserRoutines>& userActions)
    : Controller(cmdLine.applicationName(), cmdLine.parentName(), cmdLine.partitionName(),
                 cmdLine.segmentName(), userActions)
{
}

// throws daq::rc::ControllerInitializationFailed
Controller::Controller(const std::string& name,
                       const std::string& parent,
                       const std::string& partition,
                       const std::string& segment,
                       const std::shared_ptr<UserRoutines>& userActions)
{
    if(name.empty() || parent.empty() || partition.empty() || segment.empty()) {
        throw daq::rc::ControllerInitializationFailed(ERS_HERE, "application name, partition, segment and parent names cannot be empty");
    }

    if(userActions.get() == nullptr) {
        throw daq::rc::ControllerInitializationFailed(ERS_HERE, "user routines cannot be null");
    }

    try {
        OnlineServices::initialize(partition, segment);
    }
    catch(daq::rc::OnlineServicesFailure& ex) {
        const std::string& errMsg = std::string("failed accessing some online services (") + ex.message() + ")";
        throw daq::rc::ControllerInitializationFailed(ERS_HERE, errMsg, ex);
    }

    m_impl.reset(new RunController(name, parent, partition, segment, userActions, false));
}

Controller::~Controller() {
}

// throws daq::rc::ControllerInitializationFailed
void Controller::init() {
    m_impl->init();

    // Do this after the init has been done!
    CTRL = this;
}

void Controller::run() {
    m_impl->run();
}

void Controller::shutdown() {
    // Be careful to what you call here! This may be called in a signal handler and not everything is safe!!!
    // https://www.securecoding.cert.org/confluence/pages/viewpage.action?pageId=1420
    m_impl->shutdown();
}

}}
